//
//  TestPDFReader.h
//  Vikatan
//
//  Created by mobileveda on 07/06/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestPDFReader : UIViewController {

    NSString * wrapperUrl;

}

@property (nonatomic, retain) NSString * wrapperUrl;

@end
