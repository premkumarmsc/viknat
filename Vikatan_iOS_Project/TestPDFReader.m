//
//  TestPDFReader.m
//  Vikatan
//
//  Created by mobileveda on 07/06/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import "TestPDFReader.h"
#import "Constant.h"

@interface TestPDFReader ()

@end

@implementation TestPDFReader

@synthesize wrapperUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *titleView = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.frame];
    self.navigationItem.titleView = titleView;
    
    //Add Header Image Control
    UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(293, 5, header.size.width, header.size.height)];
    headerImageView.image = header;
    [titleView addSubview:headerImageView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(290+header.size.width, 10, 75, 20)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @" இதழ்கள்";
    lblTitle.textAlignment= UITextAlignmentLeft;
    [titleView addSubview:lblTitle];
    
    NSLog(@"wrapperUrl is %@", wrapperUrl);
    UIImage *image = [UIImage imageWithContentsOfFile:wrapperUrl];
    
    UIImageView * wrapperImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height-75)];//150+75+37, 212+106+53
    wrapperImage.image = image;
    [self.view addSubview:wrapperImage];
    
    if(!IsIpad()) {
       
        wrapperImage.frame = CGRectMake(0,0, 320, 470);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
