//
//  News.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "asyncimageview.h"
#import "JSON.h"
#import "CustomButton.h"


@interface News : UIViewController {
    
    NSMutableData *responseData;
	UIActivityIndicatorView *spinner; 
	
	UIImage *image;
	NSString *imageURL;
	NSString *articleTitle;
	NSString *articleID;
	NSString *categoryID;
	NSString *categoryTitle;
	UIScrollView *scrollArticleView1;
    
    
    //View declaration for news view
    
    UIImageView *headerImageView;
    UIView *detailNewsView;
    UIWebView *webView;
    UIScrollView *scrollCategoryView;
    UIScrollView *scrollArticleView;
    UIView *tempScrollView;
    UIImageView *headerView;
    UIButton *refreshImageView , 
    *commentsImageView, 
    *newsDetailTitle,
    *back;
    bool detailView;
    
}
@property (nonatomic, retain) UIActivityIndicatorView *spinner;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIScrollView *scrollArticleView1;


@property (nonatomic, retain) UIImageView *headerImageView;
@property (nonatomic, retain) UIView *detailNewsView;
@property (nonatomic, retain) UIButton *back;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIScrollView *scrollCategoryView;
@property (nonatomic, retain) UIScrollView *scrollArticleView;
@property (nonatomic, retain) UIView *tempScrollView;
@property (nonatomic, retain) UIButton *newsDetailTitle ;
@property (nonatomic, retain) UIImageView *headerView;
@property (nonatomic, retain) UIButton *refreshImageView;
@property (nonatomic, retain) UIButton *commentsImageView;
- (void)loadImage;
-(void) myExceptionHandler :(NSException *)exception;

@end