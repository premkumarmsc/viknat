



#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "AQGridViewCell.h"
#import "HJManagedImageV.h"
#import "Book_ipad.h"
#import "CustomUIButton.h"
#import "sqlite3.h"
#import "Constant.h"
#import "EpubViewControlleriPhone.h"
#import <QuartzCore/QuartzCore.h>
#import "Login.h"
#import "LoginView.h"
#import "MagazineLib.h"

#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"


@class ASIHTTPRequest;

#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"
#define kInAppPurchaseProUpgradeProductId @"com.runmonster.runmonsterfree.upgradetopro"
//#define KDEVICEPAYMENTFAILED @"Device payment is failed"

//#define KMAGPAYMENTSUCESS  @"http://api.viduthalai.in/api/device_payment.php"
#define KMAGPAYMENTDONE    @"success" 
//#define KMAGPAYMENTSUCESS  @"http://mcps.fublish.com/api/device_payment.php"


@interface BookCell : AQGridViewCell <UINavigationControllerDelegate, UIAlertViewDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver, NSURLConnectionDelegate, NSURLConnectionDownloadDelegate, NSURLConnectionDataDelegate>
{
    sqlite3 * database;
    ASIHTTPRequest *asihttprequest;
    UILabel * _title;
    UILabel * _author;
    UILabel * _price;
    
    UIButton * action_button;
    NSString * imageUrl;
    NSNumber * filesize;
    HJManagedImageV* imgV;
    UIProgressView * downloadprogress;
    NSFileManager *fileManager;
    NSError * error;
    NSString * downloadpath;
    
    NSString * zipcontentpath;
    NSString * cachePath;

    UIViewController *parent;
    
    NSString * catid;
    NSString * catname;
    NSString * contentid;
    NSString * contentname;
    NSString * authorname;
    NSString * contentdate;
    NSString * contentformat;
    NSString * content_price;
    NSString * selectedPrdIndt;

    NSString *epubDocPath;
    int btnStatus;
           
    SKProduct *proUpgradeProduct;
    SKProductsRequest *productsRequest;
    
    AFHTTPRequestOperation *downloadOperation;
    AFHTTPClient * apiClient;
}

@property (retain, nonatomic) ASIHTTPRequest *asihttprequest;
@property (retain, nonatomic) AFHTTPRequestOperation *downloadOperation;
@property (retain, nonatomic) AFHTTPClient *httpClient;

@property (nonatomic, assign) id<Book_ipad_protocol> delegate; 
@property (nonatomic, assign) UIViewController *parent;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * author;
@property (nonatomic, copy) NSString * price;
@property (nonatomic, copy) NSString * contdesc;
@property (nonatomic, copy) NSString * downloadpath;
@property (nonatomic, copy) NSString * zipcontentpath;
@property (nonatomic, copy) NSString * cachePath, *  downloadManagerVersion;
@property (nonatomic, copy) UILabel * contentDesc;

@property (nonatomic, retain) UIButton * action_button;
@property (nonatomic, retain) HJManagedImageV* imgV;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) CustomUIButton * custombutton, * resumeBtn, * cancelBtn, * pauseBtn, *downloadLblBtn;
@property (nonatomic, retain) UIProgressView * downloadprogress;
@property (nonatomic, retain) NSNumber * filesize;

@property (nonatomic, retain) NSString * epubDocPath;
@property (nonatomic, retain) NSString * catid;
@property (nonatomic, retain) NSString * catname;
@property (nonatomic, retain) NSString * contentid;
@property (nonatomic, retain) NSString * contentname;
@property (nonatomic, retain) NSString * authorname;
@property (nonatomic, retain) NSString * contentdate;
@property (nonatomic, retain) NSString * contentformat;
@property (nonatomic, retain) NSString * downloadurl;
@property (nonatomic, retain) NSString * content_price;
@property (nonatomic, retain) NSString * selectedPrdIndt;
@property (nonatomic) int btnStatus;
@property (nonatomic) BOOL transactionStatus, cancelDownload, allowVersion;
@property (nonatomic,retain) UILabel * lblAuthor;
@property (nonatomic,retain) UILabel * lblPrice;
@property (nonatomic,retain) UILabel * lblCurrency;
@property(nonatomic, retain)UIPopoverController *popoverController;

@property(nonatomic) int totalBytesExpectedToReadFromUrl, totalBytesReadFromUrl;

-(void) performButtonAction:(int) id;
- (void)startdownloadrequest: (NSString *) downloadurlstring;
- (void) saveData;

-(void) openbook:(NSString *) epubDocPathval;
-(BOOL) checkInternet;
- (BOOL) connectedToNetwork;
-(void) _showAlert:(NSString *) msg;
- (void) productRequesting:(NSArray*)ar;
- (void)requestProductData:(NSArray *) productidentifier;
- (UIColor *) colorWithHexString: (NSString *) hex;
-(void)showInAppList;
- (void) closePops;
- (void)closeModalView;
-(void) updateCheckDownloadProgress: (NSString *) setValue;
-(void) unzipDownloadedFile;
-(void) downloadFailedMethod;
-(void) setPauseMode;

-(void) cancelProgressingDownload;
@end
