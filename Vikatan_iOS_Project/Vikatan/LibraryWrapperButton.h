//
//  LibraryWrapperButton.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 19/08/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LibraryWrapperButton : UIButton  {
    
    NSInteger IssueID, MagID,gridInd;
    NSString *issueDate, *issueFolderPath;
    
}

@property (nonatomic, retain) NSString *issueDate,*issueFolderPath;


@property (nonatomic) NSInteger IssueID, MagID, gridInd;

@end
