//
//  MagPostCmnt.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/09/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MagPostCmnt : UIViewController {
    
    NSString *articleID,*magID;
}

@property (nonatomic,retain) NSString *articleID,*magID;

@end
