//
//  Calendar_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 14/10/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Calendar_iPad.h"


@implementation Calendar_iPad


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [magStore release],magStore = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    int yWidth = 165;
    UIView *serachView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,300, 300)];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    //[serachView setBackgroundColor:[UIColor blackColor]];
    CGRect pickerFrame = CGRectMake(0, 5, 300, 150);
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    [datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; 
    [dateFormatter setDateFormat:@"MM.yy"]; 
    NSDate *date = [dateFormatter dateFromString:@"08.2011"];
    //NSDate *date = [dateFormatter dateFromString:minAvalDate];
    
    datePicker.minimumDate = date;
    datePicker.datePickerMode = UIDatePickerModeDate;
    //  [datePicker setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    
    [datePicker setBackgroundColor:[UIColor clearColor]];
    [datePicker setCalendar: [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar]];
    [datePicker setDate:date];
    [datePicker setSelected:TRUE];
    [serachView addSubview:datePicker];
    
    [datePicker release];
    if(!PSIsIpad()) yWidth = 250;
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(95, yWidth, 100, 30)];
    [searchBtn setImage:[UIImage imageNamed:@"get_issues.png"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchClicked:) forControlEvents:UIControlEventTouchUpInside];
    [serachView addSubview:searchBtn];
    
    [self.view addSubview:serachView];
    
    [super viewDidLoad];
}

- (id)initWithMag:(MagazineStore_iPad *) magController:(NSString*)validFrom  {
    
    NSLog(@"Valid from date is to be : %@", issValidFrm);
    
    issValidFrm = validFrom;
    if ((self = [super init])) {
        magStore = magController; // weak
        
    }
    
    return self;
}
- (void)searchClicked:(id)sender {
  //  if(curMonth !=0 || curYear !=0)
   // {
        magStore.selMonth = curMonth;
        magStore.selYear = curYear;
        magStore.magArchieve = TRUE;
        [magStore callBackArchive];
        [self dismissModalViewControllerAnimated:TRUE];
    /*
    } else {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Invalid Date"  message:@"Please choose a valid date"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
    */
    
}
- (void)pickerChanged:(id)sender
{
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[sender date]];
    // NSInteger day = [components day];    
    curMonth = [components month];
    curYear = [components year];
    
}

- (void)viewDidUnload
{
    magStore = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end
