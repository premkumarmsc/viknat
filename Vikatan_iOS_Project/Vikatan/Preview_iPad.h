//
//  Preview_iPad.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 28/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "ZipArchive.h"
#import "PDFScrollView.h"


@interface Preview_iPad : UIViewController {
    
    NSString *prevwDnldURl, *magName, *issueDate, * wrapperUrl;
    ASINetworkQueue *networkQueue;
    UIProgressView *progressIndicator;
    BOOL failed;
    NSNumber  *magID, *issueID;
    
    UIScrollView* pagingView;
    
    NSMutableString *tempImg;
    BOOL pdfOpen;
   
}
@property (nonatomic, retain) NSString *prevwDnldURl, *magName, *issueDate,* wrapperUrl;
@property (nonatomic, retain)  NSMutableString *tempImg;
@property (nonatomic, retain) NSNumber *magID,*issueID;
- (void)imageDownloadSart:(ASIHTTPRequest *)request;
- (void)imageFetchComplete:(ASIHTTPRequest *)request;
- (void)imageFetchFailed:(ASIHTTPRequest *)request;
- (void) createMagRootFolder;
- (void) loadPDF;
- (BOOL)isLandscape;
@end
