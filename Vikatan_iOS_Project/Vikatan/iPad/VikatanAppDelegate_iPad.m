//
//  VikatanAppDelegate_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "VikatanAppDelegate_iPad.h"

@implementation VikatanAppDelegate_iPad

@synthesize globalCount, 
            pdfDocPath,
            issValidFrom;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    DefaultSHKConfigurator *configurator = [[[DefaultSHKConfigurator alloc] init] autorelease];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];
        
    [self copyDatabaseIfNeeded];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([standardUserDefaults objectForKey:CHECK_DOWNLOAD_PROGRESS] == nil )
    {
        [standardUserDefaults setValue:@"DAY" forKey:CHECK_DOWNLOAD_PROGRESS];
    }
    
    if([standardUserDefaults objectForKey:CURRENT_TEXT_SIZE] == nil )
    {
        [standardUserDefaults setValue:@"150" forKey:CURRENT_TEXT_SIZE];
    }
    
    UINavigationController *localNavigationController;
	tabBarController = [[UITabBarController alloc] init];
    tabBarController.delegate = self;
    
	NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:5];
	
    
    News_iPad *myViewController;
	myViewController = [[News_iPad alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController];
	myViewController.title = @"செய்திகள்";
    myViewController.tabBarItem.image = [UIImage imageNamed:@"news_norm.png"];
   // myViewController.navigationController.toolbar.barStyle = UIBarStyleBlackOpaque;
    myViewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    myViewController.navigationController.tabBarItem.enabled = TRUE;
   // myViewController.modalPresentationStyle = UIModalPresentationFormSheet;
   // myViewController.navigationController.navigationBarHidden = TRUE;
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];
	
    Library_iPad *myViewController1;
	myViewController1 = [[Library_iPad alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController1];
	myViewController1.title = @"நூலகம்";
    myViewController1.tabBarItem.image = [UIImage imageNamed:@"my_library_norm.png"];
    myViewController1.navigationController.toolbar.barStyle = UIBarStyleBlackOpaque;
    myViewController1.navigationController.navigationBar.tintColor = [UIColor blackColor];
    myViewController1.navigationController.tabBarItem.enabled = TRUE;
    
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];
    
    MagazineStore_iPad *myViewController2;
	myViewController2 = [[MagazineStore_iPad alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController2];
	myViewController2.title = @"இதழ்கள்";
    myViewController2.tabBarItem.image = [UIImage imageNamed:@"magazines_norm.png"];
    myViewController2.navigationController.tabBarItem.enabled = TRUE;
    myViewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];
    
    Book_ipad *myViewController3;
	myViewController3 = [[Book_ipad alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController3];
	myViewController3.title = @"புத்தகங்கள்";
    myViewController3.tabBarItem.image = [UIImage imageNamed:@"books_norm.png"];
    myViewController3.navigationController.tabBarItem.enabled = TRUE;
    myViewController3.navigationController.navigationBar.tintColor = [UIColor blackColor];
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];
    
    
    Information_iPad *myViewController4;
	myViewController4 = [[Information_iPad alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController4];
	myViewController4.title = @"Info";
    myViewController4.navigationController.toolbar.barStyle = UIBarStyleBlack;
    myViewController4.tabBarItem.image = [UIImage imageNamed:@"tabAbout.png"];
    myViewController4.navigationController.tabBarItem.enabled = TRUE;
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];
    
	// release since we are done with this for now
	[localNavigationController release];
	[myViewController release];

    FeedBackForm *myViewController5;
	myViewController5 = [[FeedBackForm alloc] init];
	localNavigationController = [[UINavigationController alloc] initWithRootViewController:myViewController5];
	myViewController5.title = @"Feedback";
    myViewController5.navigationController.toolbar.barStyle = UIBarStyleBlack;
    myViewController5.tabBarItem.image = [UIImage imageNamed:@"feedback.png"];
    myViewController5.navigationController.tabBarItem.enabled = TRUE;
    myViewController5.navigationController.navigationBar.tintColor = [UIColor blackColor];
	// add the new nav controller (with the root view controller inside it)
	// to the array of controllers
	[localControllersArray addObject:localNavigationController];

	// release since we are done with this for now
	[localNavigationController release];
	[myViewController5 release];

	tabBarController.viewControllers = localControllersArray;

	// release the array because the tab bar controller now has it
	[localControllersArray release];

	// add the tabBarController as a subview in the window
	[self.window addSubview:tabBarController.view];

    [self.window setRootViewController:tabBarController]; //Added Newly MV modifications

    [self.window makeKeyAndVisible];
// Override point for customization after application launch.
    
   [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];

     //[self.navigationController setNavigationBarHidden:NO animated:NO];
      return YES;
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    //Upload of the device token to server for Push Notofication IMplemenetation.
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceTokenVal = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTokenVal = [deviceTokenVal stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Device Token from userdefault is :%@",[standardUserDefaults objectForKey:@"devicetokenkey"]);
    if([standardUserDefaults valueForKey:@"devicetokenkey"] == NULL||[standardUserDefaults valueForKey:@"devicetokenkey"] == nil||[standardUserDefaults valueForKey:@"devicetokenkey"] == @"") //this means you don't have that key
    {
        
        NSString *reqUrl = [NSString stringWithFormat:@"%@%@",@"http://api.vikatan.com/add_device_token.php?type=addtoken&platform=ios&request_from=reader&v=1.4&devicetoken=",deviceTokenVal];

        NSLog(@"Url :%@",reqUrl);
        NSError * err;NSStringEncoding encoding;
        NSString *deviceTokenRes = [[NSString alloc]initWithContentsOfURL:[NSURL URLWithString:reqUrl] 
                                                                 encoding:NSUTF8StringEncoding  error:&err];
        // NSLog(@"Error is fons to be :%@", err);
        
        NSLog(@"Device token is writing:%@",deviceTokenRes);
        if([deviceTokenRes isEqualToString:@"\"Success\""]){ 
           // NSLog(@"Writing device token");
            {[standardUserDefaults setValue:deviceTokenVal forKey:@"devicetokenkey"];}
            [standardUserDefaults synchronize];
        }
    } else {
        NSLog(@"Device token is present");
    }
    
	NSLog(@"My token is: %@", deviceToken);
}

- (BOOL) copyDatabaseIfNeeded {
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	//NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath]; 
	
    NSLog(@"Database path is to be : %@",dbPath);
    
	if(!success) { 
        
        NSFileManager *fmngr = [[NSFileManager alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"vikatan.sqlite" ofType:nil];
        NSError *error;
        if(![fmngr copyItemAtPath:filePath toPath:[NSString stringWithFormat:KAPPDBPATH, NSHomeDirectory()] error:&error]) {
            // handle the error
           // NSLog(@"Error creating the database: %@", [error description]);
            
        }
        [fmngr release];
                
}	
    return FALSE;
}

- (NSString *) getDBPath {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory , NSUserDomainMask, YES);
	NSString *documentsDir = [paths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:DATABASE_NAME];
    
}
- (void)dealloc
{
	[super dealloc];
}



@end
