//
//  VikatanAppDelegate_iPad.h
//  Vikatan
//
//  Created by MobileVeda on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VikatanAppDelegate.h"
#import "News_iPad.h"
#import "Library_iPad.h"
#import "Information_iPad.h"
#import "MagazineStore_iPad.h"
#import "Book_ipad.h"
#import "FeedBackForm.h"
#import "SHKConfiguration.h"

#define DATABASE_NAME @"vikatan.sqlite"

@interface VikatanAppDelegate_iPad : VikatanAppDelegate <UITabBarControllerDelegate,UITabBarDelegate> {
    
    UITabBarController *tabBarController;
    
    UIPopoverController *popover;
    
    //PDF Current Page Count Holder
    NSNumber *globalCount;
    
    //Current PDF Document Path
    NSString* pdfDocPath;
    
     NSString* issValidFrom;
    
}
- (BOOL) copyDatabaseIfNeeded;

- (NSString *) getDBPath;

@property (nonatomic,copy)  NSNumber *globalCount;
@property (nonatomic,copy)  NSString* pdfDocPath;
@property (nonatomic,copy)  NSString *issValidFrom;

@end
