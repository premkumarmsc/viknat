//
//  MagazineStore_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "MagazineStore_iPhone.h"
#import "Utilities.h"
#import "MagazineLib.h"
#import <StoreKit/StoreKit.h>

@implementation MagazineStore_iPhone
@synthesize scrollArticleView, 
scrollCategoryView, loginView,
scrollArchCategoryView, 
scrollArchArticleView,
productIdentView,
popoverController,
disableView;
@synthesize coverflow,covers;
@synthesize selYear,selMonth, 
magArchieve,selectedPrdIndt,dwnRequest;
@synthesize singleIssDic,subIssDic;
@synthesize gridView=_gridView;
@synthesize thumbnailSize = thumbnailSize_;
@synthesize thumbnailMargin = thumbnailMargin_;
@synthesize back, btnRefer, archBtn, allowDownloadVersion;

@synthesize apiClient, downloadOperation, totalBytesReadFromUrl, totalBytesExpectedToReadFromUrl,
imageProgressIndicator, resumeBtn, cancelBtn, pauseBtn, downloadManagerVersion, DownloadTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    ///if([self.view.subviews containsObject:progressIndicator])
    //   [progressIndicator release], progressIndicator = nil;
    // if([self.view.subviews containsObject:networkQueue])
    //   [networkQueue reset],[networkQueue release], networkQueue = nil;
    if(loginMod !=NULL)
        [loginMod release], loginMod = nil;
    if(magzineList !=NULL)
        [magzineList release], magzineList = nil;
    if(magazineIDS !=NULL)
        [magazineIDS release], magazineIDS = nil;
    
    if([self.view.subviews containsObject:scrollCategoryView])
        [scrollCategoryView release], scrollCategoryView = nil;
    if([self.view.subviews containsObject:scrollArticleView])
        [scrollArticleView release], scrollArticleView  = nil;
    if([self.view.subviews containsObject:scrollArchCategoryView])
        [scrollArchCategoryView release], scrollArchCategoryView = nil;
    if([self.view.subviews containsObject:scrollArchArticleView])
        [scrollArchArticleView release], scrollArchArticleView = nil;
    
    articleImgUrl = nil;
    [articleImgUrl release];
    [belView removeFromSuperview];
    [belView release];
    //[coverflow release];
	//[covers release];
    
    [downloadView removeFromSuperview];[downloadView release];
    [dwnldListView removeFromSuperview];[dwnldListView release];
    [titleView removeFromSuperview];[titleView release];
    //[dwnldMgrBtn removeFromSuperview];[dwnldMgrBtn release];
    maxDowncount = 0;
    [currentDwnldIDS release],currentDwnldIDS = nil;
    [serachView removeFromSuperview],[serachView release],serachView = nil;
    [popController release], popController = nil;
    [productIdentView removeFromSuperview],[productIdentView release], productIdentView = nil;
    [singleIssueState release], singleIssueState = nil;
    [singleIssuePrice release], singleIssuePrice = nil;
    [sub_promoList release], sub_promoList = nil;
    [selectedPrdIndt release],selectedPrdIndt = nil;
    [dwnRequest release],dwnRequest = nil;
    [singleIssDic release]; [subIssDic release];
    singleIssDic = nil, subIssDic = nil;
    [disableView release], disableView = nil;
    [_imageNames release], _imageNames = nil;
    [showSub release], showSub = nil;
    
    gridView.delegate = nil;
    [gridView release];
    
    [responseString release], responseString = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview. [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad
{
    singleSelected = TRUE;
    allowDownloadVersion = TRUE;

    currentDwnldIDS = [[NSMutableArray alloc] init];
    [self buildDwnlMgr];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	CGRect r = self.view.bounds;
	r.size.height = 150;
    back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame = CGRectMake(10,5,30 ,30);
    [back addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"arrow-back_32.png"] forState:UIControlStateNormal];
    [back setHidden:TRUE];
    [self.navigationController.view addSubview:back];
    
    
    btnRefer = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRefer.frame = CGRectMake(280,5,30 ,30);
    [btnRefer setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
    [btnRefer addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:btnRefer];
    
    
    // self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
    archBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    archBtn.frame = CGRectMake(230,5,38 ,38);
    [archBtn setImage:[UIImage imageNamed:@"Archives.png"] forState:UIControlStateNormal];
    [archBtn addTarget:self action:@selector(showArchievesPage:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:archBtn];
    
    dwnldSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	dwnldSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    dwnldSpinner.hidden = TRUE;
    dwnldSpinner.frame = CGRectMake(50,5,30 ,30);
    [self.navigationController.view addSubview:dwnldSpinner];
    
    //self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:leftToolBar] autorelease];
    
    
    titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
    // self.navigationItem.titleView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
   	[self LoadingIcon];
       //Add Header Image Control
	UIImage *header = [UIImage imageNamed:@"iphone_vikatan_logo_ver_1.png"];
	UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
	[titleView addSubview:headerImageView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-14, 5, 100, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"இதழ்கள்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];
    
     self.navigationItem.titleView = titleView;    
    [self LoadingIcon];
    
    
    hidView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 380, self.view.frame.size.width, self.view.frame.size.height)];
    hidView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    hidView.userInteractionEnabled = FALSE;
    hidView.backgroundColor = [UIColor blackColor];
    hidView.alpha = 0.4;
    hidView.hidden = TRUE;
    hidView.exclusiveTouch = FALSE;
    [self.view addSubview:hidView];
    
    logMod = [[Login alloc] init];restoreallpurc = FALSE;
    [self magazineOnLoad];
    [super viewDidLoad];
    
    // thumbnailSize_   = CGSizeMake(162, 264);//CGSizeMake(162.f, 200.f);
    // thumbnailMargin_ = CGSizeMake(0.f, 0.f);
    //150/1.2, yWidth = 275 - 65
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        thumbnailSize_   = CGSizeMake(162.f, 224.f);
        thumbnailMargin_ = CGSizeMake(10.f, 40.f);
    } else {
        thumbnailSize_   = CGSizeMake(150/1.2,  168/*275 - 65*/);//CGSizeMake(162.f/1.75, 224.f/1.75);
        thumbnailMargin_ = CGSizeMake(10.f/1.75, 40.f/1.75);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(callBackArchive) name:@"magazinerefereshiphone" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(callInapp) name:@"purccontinueiphone" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(restorealltran) name:@"purcrestoreiphone" object: nil];
   
    self.navigationController.navigationBar.layer.shadowColor = [ [UIColor whiteColor ] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0,1.0);//, <#CGFloat height#>)
    self.navigationController.navigationBar.layer.shadowOpacity = 0.40;
}
- (void) buildDwnlMgr {
    //Download view initialize
    maxDowncount = 1;
    downloadView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 120)];
    [[downloadView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[downloadView layer] setBorderWidth:5];
    [[downloadView layer] setCornerRadius:15];
    
    dwnldListView = [[UIScrollView alloc] initWithFrame:CGRectMake(10,30, 320, 300)];
    [downloadView addSubview:dwnldListView];
    [downloadView setHidden:TRUE];
    [downloadView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:downloadView];

    DownloadTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 20)];
    DownloadTitle.text=@"";
    [DownloadTitle setTextColor:[UIColor whiteColor]];
    [DownloadTitle setBackgroundColor:[UIColor clearColor]];
    [dwnldListView addSubview:DownloadTitle];
    
    pauseBtn = [[CustomUIButton alloc] init];
    pauseBtn.frame = CGRectMake(235,40, 28, 28);
    [pauseBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [pauseBtn addTarget:self action:@selector(pausedownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:pauseBtn];
    
    resumeBtn = [[CustomUIButton alloc] init];
    resumeBtn.frame = CGRectMake(235, 40, 28, 28);
    [resumeBtn setBackgroundImage:[UIImage imageNamed:@"resume.png"] forState:UIControlStateNormal];
    [resumeBtn addTarget:self action:@selector(resumedownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:resumeBtn];
    
    cancelBtn = [[CustomUIButton alloc] init];
    cancelBtn.frame = CGRectMake(270,40, 28, 28);
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(canceldownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:cancelBtn];
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{   
    //[UIView setAnimationDuration:0.f];
    //[self.gridView reloadData];
    //[self.gridView reloadInputViews];
    //[UIView setAnimationDuration:0.3f];
}

- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return NO;
}
- (void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	//[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque]; 
}
- (void) viewDidAppear:(BOOL)animated{
    if(issueView)
        back.hidden = FALSE;
    //archBtn.hidden = FALSE;
    //btnRefer.hidden = FALSE;
	[super viewDidAppear:animated];
	//[coverflow bringCoverAtIndexToFront:[covers count] animated:NO];
}
- (void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	//[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

/*- (void) changeNumberOfCovers{
 
 NSInteger index = coverflow.currentIndex;
 NSInteger no = arc4random() % 200;
 NSInteger newIndex = MAX(0,MIN(index,no-1));
 
 NSLog(@"Covers Count: %d index: %d",no,newIndex);
 
 [coverflow setNumberOfCovers:no];
 coverflow.currentIndex = newIndex;
 
 }*/

- (void ) back_clicked:(id)sender 
{
    if(!downLoadInProgress) {
    CGRect frame = CGRectMake(0, 10, self.view.bounds.size.width, self.view.bounds.size.height - 10 );   
        
        //CGRect frame = self.view.bounds;
    self.gridView.frame = frame;
    [subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
    btnRefer.hidden = FALSE;
    archBtn.hidden  = FALSE;
    [downloadView setHidden:TRUE];
    [productIdentView removeFromSuperview];
    issueView = FALSE;
    [self showTabBar];
    [back setHidden:TRUE];
    [self buildStorePage:responseString];
    [self.gridView reloadData];
    }
    else {
        
        [self _showAlert:@"Download inprogress, please wait."];
    }
}

- (void) magClicked:(id)sender {
    
    
    [self hideTabBarView];
    [back setHidden:FALSE];
    issueView = TRUE;
    
    DownloadControl *magCoverBtn = (DownloadControl*)sender;
    _imageNames = [[NSMutableArray alloc] init];
    tempCurrentCoverIndex = magCoverBtn.tag;
    NSArray *ar = [articleImgUrl objectForKey:[NSString stringWithFormat:@"%d",magCoverBtn.tag]];
  
    for(int i = 0 ;i< [ar count];i++) {
        NSDictionary *article = (NSDictionary *)[ar objectAtIndex:i];
        NSMutableString *urlString = [NSMutableString stringWithString:KVIKSERVERURL];
        [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
        [_imageNames addObject:urlString];
         // NSLog(@"checking imageurl %@",urlString);
    }
    if( [magzineList count] > 0 ) 
    {
        [subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
        
        NSLog(@"showstrip: %@", [showSub objectAtIndex:magCoverBtn.tag]);
        
        if ([[showSub objectAtIndex:magCoverBtn.tag] isEqualToString:@"Y"] ) {
        //[subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
        subScriptionbtnView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,5,self.view.frame.size.width ,90)];
        subScriptionbtnView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        subScriptionbtnView.contentMode = UIViewContentModeCenter;
        CustomButton *subScription = [CustomButton buttonWithType:UIButtonTypeCustom];
        [subScription addTarget:self action:@selector(subscriptionClicked:) forControlEvents:UIControlEventTouchUpInside];
        subScription.frame = CGRectMake(0,0,self.view.frame.size.width ,90);
        [subScription setImage:[UIImage imageNamed:@"substrip.png"] forState:UIControlStateNormal];
        [subScription setCateGoryID:[[magazineIDS objectAtIndex:magCoverBtn.tag] intValue]];
        
        [self.view addSubview:subScription];
        UILabel *lblSub = [[UILabel alloc]initWithFrame:CGRectMake(0, 12.5, self.view.frame.size.width ,45)];
        lblSub.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        lblSub.text = [NSString stringWithFormat:@"%@",
                       [sub_promoList objectAtIndex:magCoverBtn.tag]] ;
        
        lblSub.backgroundColor = [UIColor clearColor];
        lblSub.textColor = [UIColor darkGrayColor];
        lblSub.textAlignment = UITextAlignmentCenter;
        [subScription addSubview:lblSub];
        [subScriptionbtnView addSubview:subScription];
        }
        //[self.view addSubview:subScriptionbtnView];
    }
    
    [self.gridView reloadData];
    
}
-(void) close_dwnlmng_Clicked:(id)sender {
    
    //UIButton *btn = (UIButton*)btn;
    //[btn.superview setHidden:TRUE];
    
    //[downloadView setHidden:TRUE];
    //downloadView.frame = CGRectMake(0,0, 0, 0);
    //[downloadView setBackgroundColor:[UIColor clearColor]];
}

- (void) hideInappView {
    
    [self disableAction];
    [productIdentView setHidden:TRUE];
    [productIdentView removeFromSuperview];
}
//Referesh Clicked
- (void)refresh_clicked:(id)sender {
    
    if(![self checkInternet]) 
        return;
    issueView = false;
    magArchieve = FALSE;
    [self hideInappView];
    [self disableAction];
    hidView.hidden = TRUE;
    if(!downLoadInProgress) {
        [self LoadingIcon];
        //covers = [NSMutableArray new];
        [self magazineOnLoad];
    } else {
       //[self _showAlert:@"Download inprogress, please wait."];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                        message:@"Do you want to cancel?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
        
        
        [alert show];
        [alert release];
    }
}

- (void)showDwnldPage:(id)sender {
    
    /*
     // [downloadView setHidden:FALSE];
     // [self.view bringSubviewToFront:downloadView];
     UIViewController *ct = [[UIViewController alloc] init];
     [ct.view setBackgroundColor:[UIColor clearColor]];
     [ct.view addSubview:downloadView];
     
     UIPopoverController *popoverControl = [[UIPopoverController alloc] initWithContentViewController:ct] ;
     CGRect popoverRect = [self.view convertRect:[sender frame] 
     fromView:[sender superview]];    
     popoverRect.size.width = MIN(popoverRect.size.width, 100);
     CGSize size = CGSizeMake(440, 120); // size of view in popover
     popoverControl.popoverContentSize = size;
     
     [popoverControl 
     presentPopoverFromRect:popoverRect 
     inView:self.view 
     permittedArrowDirections:UIPopoverArrowDirectionAny 
     animated:YES];
     // remember controller
     self.popoverController = popoverControl;
     */
}

- (void) subscriptionClicked:(id)sender {
    
    if(![self checkInternet]) 
        return;
    if(![self.view.subviews containsObject:spinner]) 
    {
        if(!downLoadInProgress)
        {
            //[networkQueue cancelAllOperations];
            //[downloadView setHidden:TRUE];
            loginMod = nil;
            [loginView removeFromSuperview];
            ///Login Authentication
            loginMod = [[Login alloc]init];
            [loginMod setDelegate:self];
            loginMod.downLaoding = TRUE;
            CustomButton *btn = (CustomButton*)sender;
            int magID = btn.CateGoryID;
            singleSelected = FALSE;
            magazineID = magID;
            issueID = 0;
            downloadBtntmp = sender;            
            if (![loginMod isUserDetailsExists]) {
                loginMod.fromInfo = FALSE;
                LoginView *logView = [[LoginView alloc] init];
                UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:logView] autorelease];
                logView.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closeModalView)] autorelease];
                [self presentModalViewController:navController animated:NO ];
                
            } else {
                CustomButton *btn = (CustomButton*)sender;
                int magID = btn.CateGoryID;
                singleSelected = FALSE;
                magazineID = magID;
                issueID = 0;
                [self showInAppList:sender];
            }
        } else 
        {
            [self _showAlert:@"Download inprogress, please wait."];
        }
    }
}
-(void)closeClicked:(id)sender
{
    [productIdentView removeFromSuperview];
    selectedPrdIndt = @"";
    [self disableAction];
    UIButton *btn = (UIButton*)sender;
    [btn.superview setHidden:TRUE];
    [self removeObjectsOnTranFail];
    downLoadInProgress = FALSE;
    [self StopLoading];
    
    //[inappView removeFromSuperview];
}

- (void) submitClicked:(id)sender
{
    
    NSLog(@"HELLO SUBMIT PAYMENT");
    
    transactionStatus = FALSE;
    
    if(![self checkInternet]) 
        return;
    
    CustomButton *btn = (CustomButton*)sender;
   // if( selectedPrdIndt !=NULL) { 
        // selectedPrdIndt = btn.productIdentifier;
        if([selectedPrdIndt isEqualToString:@""] ||selectedPrdIndt == NULL||selectedPrdIndt == Nil)
        {
            [self  _showAlert:KINVALIDPRDTIDNT];
        } 
        else
        {
            magazineID = btn.CateGoryID;
            
            NSLog(@"MAG ID:%d",magazineID);
            NSLog(@"SELECTED ID:%@",selectedPrdIndt);
            
            NSMutableArray *ar = [[NSMutableArray alloc] init];
            //[ar addObject:selectedPrdIndt];
            
            [ar addObject:@"com.vikatan.reader.av19062013"];
            
            [self productRequesting:ar];
            //[self getProductList:magazineID];
            [self LoadingIcon];
            downLoadInProgress = TRUE;
        }
  //  }
    //[networkQueue go];
    //selectedPrdIndt = @"";
}

-(void)readNowClicked:(id)sender
{
    if(hidView.hidden) {
        
        DownloadControl *magBtn = (DownloadControl*)sender;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        /*NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                documentsDirectory,
                                @"/",KMAGROOTPATH,@"/",magBtn.MagID,@"_",magBtn.IssueID,
                                @"/magazine_",magBtn.MagID,@"_",magBtn.IssueID,@".pdf"];*/
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
         NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",
         documentsDirectory,
         @"/",KMAGROOTPATH,@"/",magBtn.MagID,@"_",magBtn.IssueID,
         @"/wrapper.jpg"];
        
        if([fileManager fileExistsAtPath:pdfDocPath])
        {
            TestPDFReader * prevMag = [[TestPDFReader alloc] init];
            [prevMag setWrapperUrl:pdfDocPath];
            [self.navigationController pushViewController:prevMag animated:TRUE];
        }
    }
    
}
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    if([response.products count] > 0 )
    {
        for (NSString *invalidProductId in response.invalidProductIdentifiers)
        {
            NSLog(@"Invalid product id: %@" , invalidProductId);
        }
        
        [request autorelease];
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        SKPayment *payment = [SKPayment paymentWithProductIdentifier:selectedPrdIndt ];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        selectedPrdIndt = @"";
        [self disableAction];
    }
    downLoadInProgress = FALSE;
    [self StopLoading];
}
-(void)showInAppList:(id)sender {
    transactionStatus = FALSE;
    DownloadControl *downloadBtn = (DownloadControl*)sender;
    int xWidth = 320;
    [productIdentView removeFromSuperview];
    productIdentView = [[InappView alloc] initWithFrame:CGRectMake(0, 0, xWidth, 884/2)];
    [productIdentView setBackgroundColor:[UIColor whiteColor]];
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,45)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    //[commentTitle setTitle:[sub_promoList objectAtIndex:magazineID-1] forState:UIControlStateNormal];
    [commentTitle setTitle:[sub_promoList objectAtIndex:tempCurrentCoverIndex] forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(0, 12, 300, 25);
    commentTitle.enabled = FALSE;
    [commentTitle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cmntHeader addSubview:commentTitle];
    [productIdentView addSubview:cmntHeader];
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(290, 10 , 32, 32)];
    closeBtn.hidden = FALSE;
    [closeBtn setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [productIdentView addSubview:closeBtn];
    
    NSMutableArray *subs = [[NSMutableArray alloc] init];
    CustomButton *subMit = [CustomButton buttonWithType:UIButtonTypeCustom]; 
    subMit.CateGoryID = magazineID;
    int ypos = 0;    
    UIScrollView *inappContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, xWidth, 300/2)];
    
    NSLog(@"looping sir %@",[NSNumber numberWithInt:magazineID]);
    
    if(singleSelected)
    {
        /*
        subs = (NSMutableArray*)[subIssDic objectForKey:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:magazineID]]];
        for (int y = 0 ; y < subs.count; y++)
        {			
            
            CustomButton *but = (CustomButton*)[subs objectAtIndex:y];NSLog(@"Subscriptin anme sir %@",but.subsname);
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setSelected:FALSE];
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            but.tag = 0;
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, xWidth- 70, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];//UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            //lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [inappContainer addSubview:lbl];
            [singleIssuePrice addObject:but.subprice];
            ypos += 50;
        }
        */
     
             CustomButton *but = [CustomButton buttonWithType:UIButtonTypeCustom]; NSLog(@"Subscriptin anme sir %@",downloadBtn.issuName);
            //CustomButton *but = (CustomButton*)[subs objectAtIndex:y];NSLog(@"Subscriptin anme sir %@",but.subsname);
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setProductIdentifier:downloadBtn.productIdentifier];
            [but setSelected:FALSE];
            [but setSubprice:downloadBtn.issuePrice];but.tag = 1;
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, xWidth-70, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", downloadBtn.issuName,@" $",downloadBtn.issuePrice]];
           // [lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];//UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            //lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [inappContainer addSubview:lbl];
            //[singleIssuePrice addObject:but.subprice];
           [singleIssuePrice addObject:downloadBtn.issuePrice];
            ypos += 50;
     } else {
        subs = (NSMutableArray*)[subIssDic objectForKey:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:magazineID]]];
        for (int y = 0 ; y < subs.count; y++)
        {			
            
            CustomButton *but = (CustomButton*)[subs objectAtIndex:y];
            
            NSLog(@"Subscriptin anme sir Hello %@",but.subsname);
            
            NSLog(@"PREM SUBSCRIPTION PAGE:%@",but.subsname);
            
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setSelected:FALSE];
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, xWidth-70, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];// UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            //lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [inappContainer addSubview:lbl];
            [singleIssuePrice addObject:but.subprice];
            ypos += 50;
        }
    }
    
    [subMit setImage:[UIImage imageNamed:@"buynow.png"] forState:UIControlStateNormal];
    subMit.frame = CGRectMake(10, ypos + 20, 100, 30) ;
    [subMit addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(30,0,100,27)];
    [lblTitle setText:@"Submit"];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    [subMit addSubview:lblTitle];
    [inappContainer addSubview:subMit];
    inappContainer.contentSize = CGSizeMake(xWidth,ypos+ 50);
    
    UIWebView *termWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, ypos + 140,xWidth, 250)] ;
    termWebView.scalesPageToFit = YES;
    termWebView.delegate = self;
    NSMutableString *postUrl = [NSMutableString stringWithString:KPAYMENTTERMSIPHN];//[urlMore appendString:categoryID];
    NSMutableString *uriString = [[NSMutableString alloc] initWithString:postUrl];
    NSURL* url = [[NSURL alloc] initWithString:uriString];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    [termWebView loadRequest:urlRequest];
    [termWebView setNeedsLayout];
    [productIdentView addSubview:termWebView];
    
    webSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [webSpinner setCenter:productIdentView.center];
    //spinner.backgroundColor = [UIColor lightGrayColor];
    webSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    //Add the loading spinner to view
    [productIdentView addSubview:webSpinner];
    //Start animating
    [webSpinner startAnimating];
    [productIdentView bringSubviewToFront:webSpinner];
    
    //  [txtView scrollRangeToVisible:range];
    [productIdentView addSubview:inappContainer];
    [self.view addSubview:productIdentView];
    [self.view bringSubviewToFront:productIdentView];
    
}
- (void)disableAction {
    
    /*
     if(hidView.hidden)
     {
     hidView.hidden = FALSE;
     [self.view bringSubviewToFront:hidView];
     }else {
     hidView.hidden = TRUE;
     }
     */
    /*
     if(self.view.userInteractionEnabled)
     {
     [self.view setUserInteractionEnabled:FALSE];
     }
     else
     {
     [self.view setUserInteractionEnabled:TRUE];
     }
     */ 
}
- (void) requestProductData:(NSArray*)ar {
    
    NSLog(@"PREM requestProductData %@", ar);
    
    NSArray *arr=[NSArray arrayWithObjects:@"com.vikatan.reader.av19062013", nil];
    
    NSLog(@"ARR:%@",arr);
    
    [self LoadingIcon];
    NSSet *productIdentifiers = [NSSet setWithArray:arr];    
    SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: productIdentifiers];
    request.delegate = self;
    [request start];
    [ar release];
    
}
- (void)checkboxButton:(UIButton *)button{
    
    CustomButton *custBtn = (CustomButton*)button;
    
    for (CustomButton *but in [custBtn.superview subviews]) {
        if ([but isKindOfClass:[CustomButton class]] && ![but isEqual:custBtn]) {
            [but setSelected:NO];
        }
    }
    
    if (custBtn.selected) {
        custBtn.selected = !custBtn.selected;
        selectedPrdIndt = @"";
       // NSLog(@"Checked");
    } else {
        custBtn.selected = TRUE;
        selectedPrdIndt = custBtn.productIdentifier;
        NSLog(@"Not Checked");
        //downLoadInProgress = TRUE;
    }
    subprice = custBtn.subprice ;
    subid = custBtn.subid;    
    
    if(custBtn.tag == 0)
        singleSelected = FALSE;
    else
        singleSelected = TRUE;
    // NSLog(@"Selected tag for the buy is : %d", custBtn.tag);
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error  
{  
    NSLog(@"Failed to connect with error: %@", [error localizedDescription]);
}  

- (void) productRequesting:(NSArray*)ar {
    
    NSLog(@"iphone magazine productRequesting %@", ar);
    
    downLoadInProgress = TRUE;
    
    if ([SKPaymentQueue canMakePayments]) {
        [self  requestProductData:ar];
    } else {
        NSLog(@"Cannot proceed");
        [self _showAlert:@"Check your setting, in app purchase is restricted."];
    }
}

//Payment module
//
// kick off the upgrade transaction
//
- (void)purchaseProUpgrade
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchaseProUpgradeProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
    
}

#pragma -
#pragma Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"record transaction");
    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"proUpgradeTransactionReceipt" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
    if ([productId isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isProUpgradePurchased" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"record muru");
}
//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    
    [self StopLoading];NSLog(@"Completed transaction Two");
    [self hideInappView]; NSLog(@"Completed transaction finishTransaction");
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful)
    {
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];  
        
        NSString *receiptData = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
        Login *logModule = [[Login alloc]init];
        
        [logModule setDelegate:self];
        
        NSMutableDictionary * getUrlAndParams = [logModule getDeviceDetails];
        
        NSLog(@"finishTransaction emailID, userID, sessionID, password %@ ==>%@==>%@==>%@", [getUrlAndParams objectForKey:@"email_id"], [getUrlAndParams objectForKey:@"userID"], [getUrlAndParams objectForKey:@"session_id"], [getUrlAndParams objectForKey:@"password"]);
        
        
        BOOL st = logModule.isUserDetailsExists;
        NSURL *requestURL = [NSURL URLWithString:KMAGPAYMENTSUCESS];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
        
        [request setPostValue:[getUrlAndParams objectForKey:@"password"] forKey:REQUEST_PASSWORD];
        [request setPostValue:[getUrlAndParams objectForKey:@"userID"] forKey:RESPONSE_USER_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"session_id"] forKey:RESPONSE_SESSION_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"email_id"] forKey:RESPONSE_EMAIL_ID];

        NSLog(@"iphone finish transaction email id ==> %@, userId ==> %@, password ==> %@, session ID==> %@", logModule.emailID, logModule.userID, logModule.password, logModule.sessionID);

        [request setPostValue:[NSString stringWithFormat:@"%d",magazineID] forKey:@"mid"];
        [request setPostValue:[NSString stringWithFormat:@"%d",issueID] forKey:@"issue_id"];
        [request setPostValue:[NSString stringWithFormat:@"%d",subid] forKey:@"subid"];
        [request setPostValue:[NSString stringWithFormat:@"%@",subprice] forKey:@"amount"];
        [request setPostValue:[NSString stringWithFormat:@"%d",0] forKey:@"trans_status"];
        [request setPostValue:[NSString stringWithFormat:@"%@",receiptData] forKey:@"receipt-data"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"Apple"] forKey:@"bank"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"ios"] forKey:@"platform"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"iphone"] forKey:@"device"];
        [request setPostValue:[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueIdentifier]] forKey:@"udid"];
       
        //[request setPostValue:@"mobileveda" forKey:@"debug"];
        
        if(transactionStatus)
            [request setPostValue:[NSString stringWithFormat:@"%@",@"restore"] forKey:@"request_type"];
        else
            [request setPostValue:[NSString stringWithFormat:@"%@",@""] forKey:@"request_type"];
        
        [request startSynchronous];
        NSError *error = [request error];NSLog(@"error log token %@", [error localizedDescription]);
        if (!error) {
            
            NSString *res = [request responseString];
            
           // NSLog(@"after token %@", res);
            
            if(![res isEqualToString:KMAGPAYMENTDONE])
            {
               // [self _showAlert:KSERVERPAYMENTFAILED];
                
            } else {
                if (!transactionStatus) 
                {
                issueDwnlTime = res;
                if(singleSelected )
                {
                   // NSLog(@"Completed transaction Four");
                    [self stopDownlLoading];
                    [self downldLoading];NSLog(@"Completed transaction Five");
                    //Recent
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
                    NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                                              @"/",magazineID,
                                              @"_",issueID,
                                              @".zip"];
                    
                    imageProgressIndicator = [[[UIProgressView alloc] initWithFrame:CGRectMake(10, 50, 220, 20)] autorelease];
                    imageProgressIndicator.backgroundColor = [UIColor clearColor];
                    imageProgressIndicator.tag = issueID;
                    [imageProgressIndicator setHidden:TRUE];
                    [dwnldListView addSubview:imageProgressIndicator];
                    
                    DownloadTitle.text = [NSString stringWithFormat:@"%@%@%@",magazineName,@" - ",issueDate] ;
                    DownloadTitle.tag = issueID;
                    
                    
                    NSString * downloadUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                                              downldUrlString,@"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                              @"&email_id=",loginMod.emailID,
                                              @"&password=",loginMod.password, @"&platform=ios&v=1.4&device=iphone&dim=150X150"];
                    
                    NSString * afDownloadUrl = [NSString stringWithFormat:@"%@%@%@%@",
                                              downldUrlString,@"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                              @"&platform=ios&v=1.4&device=iphone&dim=150X150"];

                    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                    downloadManagerVersion = [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY];
                    
                    NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];
                    
                    if (order == NSOrderedSame || order == NSOrderedDescending) { // OS version Greater than or equal 5
                        allowDownloadVersion = TRUE;
                        
                    }  else { // OS version less than 5
                        allowDownloadVersion = FALSE;
                        downloadManagerVersion = SEL_DOWNLOAD_MANAGER; // Overriding Download Manager Version for progress view support
                    }

                    if(![downloadManagerVersion isEqualToString:@"v2"]) {
                        
                        NSLog(@"iphone payment success ASIHTTPRequest download url is calling %@", downloadUrl);
                        
                        resumeBtn.hidden = TRUE;
                        pauseBtn.hidden = TRUE;
                        cancelBtn.hidden = FALSE;
                        
                        ASIHTTPRequest *request;
                        request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:downloadUrl]];
                        [request setDownloadDestinationPath:destinyPath];
                        [request setUserInfo:[NSDictionary dictionaryWithObject:@"request1" forKey:@"name"]];
                        [request setIssueID:[NSNumber numberWithInteger:issueID]];
                        [request setMagazineID:[NSNumber numberWithInteger:magazineID]];
                        [request setIssueDate:issueDate];
                        [request setSupplementary:FALSE];
                        [request setMagName:magazineName];
                        [request setDownloadProgressDelegate:imageProgressIndicator];
                        
                        networkQueue = [[ASINetworkQueue alloc] init];	
                        [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
                        [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
                        [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
                        [networkQueue setShowAccurateProgress:TRUE];
                        [networkQueue setDelegate:self];
                        
                        //Recent

                        [networkQueue addOperation:request];
                        [networkQueue go];
                    } else {
                            [self initiateAFDownloadRequest:afDownloadUrl destinyPath:destinyPath];
                    }
 
                    UILabel *lblSUC = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, 20)];
                    lblSUC.text = KPURCHASESCUCCS;
                    [lblSUC setTextColor:UIColorFromRGB(0x3399FF)];
                    [lblSUC setBackgroundColor:[UIColor clearColor]];
                    
                    [dwnldListView addSubview:lblSUC];
                    [downloadView addSubview:dwnldListView];
                    [downloadView setHidden:FALSE];
                    [self.view bringSubviewToFront:downloadView];
                    maxDowncount++;
                    downLoadInProgress = TRUE;
                } else {
                    [self _showAlert:@"Magazine Subscription success.."];
                    [self callBackArchive];
                }
                }
                //[networkQueue cancelAllOperations];
            }
            
        } {
            
        }
        
        [loginMod release], loginMod = nil;
    }
    else
    {
        NSLog(@"Completed transaction Erroe");
        downLoadInProgress = FALSE;
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
        
        [self removeObjectsOnTranFail];
        
    }
    //Release the allocated Async request
    //  [networkQueue release];
    [self disableAction];
    
}

- (void) sendTransactionReceipt:(NSString*)data {
    
   // Login *logMod = [[Login alloc]init];
    //[logMod setDelegate:self];
    BOOL st = logMod.isUserDetailsExists;
    //Login *logMod = [[Login alloc]init];
    responseData = [[NSMutableData data] retain];
    NSURLRequest *request;
	//Send http request
    if(magArchieve !=TRUE) {
        NSString *magONLoadReq = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@",KMAGPAYMENTSUCESS,
                                  @"&user_id=",logMod.userID,@"&password=",logMod.password,
                                  @"&session_id=",logMod.sessionID,
                                  @"&uid=",logMod.UID];
       // NSLog(@"uRL for onload %@", magONLoadReq);
        request  = [NSURLRequest requestWithURL:[NSURL URLWithString:[logMod getDeviceDetails:magONLoadReq]]];
    }
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
	[logMod release], logMod = nil;
}
-(void) removeObjectsOnTranFail {
    [downloadView setHidden:TRUE];
    for (UIView *but in [dwnldListView subviews]) {
        if ([but isKindOfClass:[UILabel class]]) {
            //[but removeFromSuperview];
        }
    }
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    downLoadInProgress = FALSE;
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    transactionStatus = TRUE;
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    downLoadInProgress = FALSE;
    [self removeObjectsOnTranFail];
    
    NSLog(@"Transaction failed error :%@",[transaction.error description]);
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        
        [self _showAlert:KDEVICEPAYMENTFAILED]; 
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
    [self StopLoading];
    //[networkQueue cancelAllOperations];
    //Release the allocated Async request
    //[networkQueue release];
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
       // NSLog(@"trn station %@", transaction.transactionState);
      //  NSLog(@"trn station2 %@", transaction.payment);
        switch (transaction.transactionState)
        {
                transactionStatus = FALSE;
            case SKPaymentTransactionStatePurchased:
               // NSLog(@"one station %@", @"pur");
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"one station %@", @"failed");
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
               // NSLog(@"one station %@", @"resgore");
                transactionStatus = TRUE;
                [self restoreTransaction:transaction];
                break;
            default:
                break;
                
                //  [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        }
    }
    if(restoreallpurc)
        [self _showAlert:@"Restore Completed"];
    restoreallpurc = FALSE;
}


- (void) resetNetwork {
    
    // if (!networkQueue) {
    networkQueue = [[ASINetworkQueue alloc] init];	
    //	}
    
	failed = NO;
	[networkQueue reset];
    [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
	[networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
	[networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
	[networkQueue setShowAccurateProgress:TRUE];
	[networkQueue setDelegate:self];
    
}
- (void) magazineOnLoad {
     if(!onLoad){
    [self initializeGridView];
    [self resetNetwork];
    [self getBooksMagaList];
         onLoad = TRUE;
     }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    if(loginMod !=NULL)
        loginMod = nil;
    if(magzineList !=NULL)
        magzineList = nil;
    if(magazineIDS !=NULL)
        magazineIDS = nil;
    
    sub_promoList = nil;
    
    singleIssDic = nil, subIssDic = nil;
    disableView = nil;
    _imageNames = nil;
    
    self.gridView.delegate = nil;
    self.gridView = nil;
    
    btnRefer=nil, [btnRefer release];
    responseString = nil;
    
    logMod = nil;
    downloadManagerVersion=nil;
    DownloadTitle=nil;
}


-(void) authenticationCompleted:(BOOL) success {
    
    if (success) {
        NSLog(@"SERVER AUTH Success");
        if ([loginMod isPaidUser]) {
            NSLog(@"SERVER AUTH paid user");
        } else {
            NSLog(@"SERVER AUTH not paid user");
        }
    } else {
        NSLog(@"SERVER AUTH Failed");
    }
    
}

- (void) downloadStart {
    
    if ([loginMod isPaidUser]) {
    } else {
        
    }
    
}

- (void) LoadingIcon {
    
    if(![self.view.subviews containsObject:spinner]) 
    {
        if([self.popoverController isPopoverVisible])
            [self.popoverController dismissPopoverAnimated:TRUE];
        //[self.view setBackgroundColor:[UIColor blackColor]];
        //[self.view setOpaque:0.8];
        [self.view setUserInteractionEnabled:FALSE];
        //  self.view.opaque = 0.4;
        //Initialize activity window for loading process
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //Set position
        [spinner setCenter:self.view.center];
        //spinner.backgroundColor = [UIColor lightGrayColor];
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge ;//UIActivityIndicatorViewStyleGray;
        //Add the loading spinner to view
        [self.view addSubview:spinner];
        //Start animating
        [spinner startAnimating];
        [self.view bringSubviewToFront:spinner];
        disableView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height + 300)];
        [disableView setBackgroundColor:[UIColor blackColor]];
        disableView.opaque = 0.4;
        disableView.alpha = 0.4;
        [self.view addSubview:disableView];
    }
}

- (void) StopLoading {
    
    if([self.view.subviews containsObject:spinner]) 
    {
        //[self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
        [self.view setOpaque:0.0];
        [self.view setUserInteractionEnabled:TRUE];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        [disableView removeFromSuperview];
    }
    [spinner release], spinner = nil;
}

- (void) downldLoading {
    [dwnldSpinner startAnimating];
}

- (void) stopDownlLoading {
    
    if(!dwnldSpinner.hidden) 
    {
        //[dwnldMgrBtn setHidden:TRUE];
        [dwnldSpinner stopAnimating];
        [dwnldSpinner setHidden:TRUE];
        
    }
    //[dwnldSpinner release], dwnldSpinner = nil;
    // downloadView.hidden = TRUE;
}
- (BOOL) findCurrentIssueDwnld:(NSInteger)issID {
    
    BOOL statu = FALSE;
    for( int i =0; i<[currentDwnldIDS count];i++)
    {
        if(issID == [[currentDwnldIDS objectAtIndex:i] intValue])
            statu = TRUE;
    }
    
    return statu;
}

-(void)downloadClicked:(id)sender
{
    if(![self checkInternet]) 
        return;
    currentCoverIndex = tempCurrentCoverIndex;
    if(hidView.hidden) {

        singleSelected = TRUE;

        DownloadControl* btn = (DownloadControl*)sender;
        tmpDwnlBtn = btn;
        issueID = btn.IssueID;
        magazineName = btn.magazineName;
        issueDate    = btn.issueDate; 

        if( !downLoadInProgress)
        {

            loginMod = nil;
            [loginView removeFromSuperview];
            loginMod = [[Login alloc]init];
            loginMod.downLaoding = TRUE;
            MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
            [loginMod isUserDetailsExists];
            
            NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];

            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                                      @"/",btn.MagID,
                                      @"_",btn.IssueID,
                                      @".zip"];
    
            
            imageProgressIndicator = [[[UIProgressView alloc] initWithFrame:CGRectMake(10, 50, 220, 20)] autorelease];
            imageProgressIndicator.backgroundColor = [UIColor clearColor];
            imageProgressIndicator.tag = btn.IssueID;
            [imageProgressIndicator setHidden:TRUE];
            [dwnldListView addSubview:imageProgressIndicator];
            [self.view bringSubviewToFront:downloadView];

            downLoadInProgress = TRUE;
            DownloadTitle.text = [NSString stringWithFormat:@"%@%@%@",btn.magazineName,@" - ",btn.issueDate] ;
            downloadManagerVersion = [[NSUserDefaults standardUserDefaults] objectForKey:DOWNLOAD_MANAGER_KEY];

           /* NSString * downloadUrl = [NSString stringWithFormat:@"%@%@%@%@",
                                      btn.downldUrlString,@"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                      @"&platform=ios&v=1.4&device=iphone&dim=150X150"];*/
            
            NSString * downloadUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                                      btn.downldUrlString,@"&udid=",[[UIDevice currentDevice] uniqueIdentifier],                                                           @"&email_id=",loginMod.emailID,
                                      @"&password=",loginMod.password, @"&platform=ios&v=1.4&device=ipad&dim=150X150"];
            
            NSString * afDownloadUrl = [NSString stringWithFormat:@"%@%@%@%@",
                                        btn.downldUrlString,
                                        @"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                        @"&platform=ios&v=1.4&device=ipad&dim=150X150"];


            NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];
            
            if (order == NSOrderedSame || order == NSOrderedDescending) { // OS version Greater than or equal 5
                allowDownloadVersion = TRUE;
                
            }  else {
                allowDownloadVersion = FALSE;
                downloadManagerVersion = SEL_DOWNLOAD_MANAGER; // Overriding Download Manager Version for progress view support
            }

            if(![downloadManagerVersion isEqualToString:@"v2"]) { //ASIHTTPRequest download manager
                ASIHTTPRequest *request;
                
               // NSLog(@"ASIHTTPRequest download manager downloadUrl is %@", downloadUrl);

                resumeBtn.hidden = TRUE;
                pauseBtn.hidden = TRUE;
                cancelBtn.hidden = FALSE;
            
                request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:downloadUrl]];

                [self removeObjectsOnTranFail];

                [request setDownloadDestinationPath:destinyPath];
                [request setUserInfo:[NSDictionary dictionaryWithObject:@"request1" forKey:@"name"]];
                [request setIssueID:[NSNumber numberWithInteger:btn.IssueID]];
                [request setMagazineID:[NSNumber numberWithInteger:btn.MagID]];
                [request setIssueDate:btn.issueDate];
                [request setSupplementary:FALSE];
                [request setMagName:btn.magazineName];
                [request setDownloadProgressDelegate:imageProgressIndicator];
                [request setAllowCompressedResponse:YES];
                
                networkQueue = [[ASINetworkQueue alloc] init];	
                [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
                [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
                [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
                [networkQueue setShowAccurateProgress:TRUE];
                [networkQueue setDelegate:self];

                ///Recent
                [currentDwnldIDS addObject:[NSNumber numberWithInteger:btn.IssueID]];
                [networkQueue addOperation:request];
                [networkQueue go];
                
                downLoadInProgress = TRUE;
                [self stopDownlLoading];
                [self downldLoading];
                [downloadView setHidden:FALSE];
                
            } else {
               // NSLog(@"initiateAFDownloadRequest download manager");
                
                [currentDwnldIDS addObject:[NSNumber numberWithInteger:btn.IssueID]];
                [self stopDownlLoading];
                [self downldLoading];
                [downloadView setHidden:FALSE];
                //[self.view bringSubviewToFront:downloadView];
                               
                [self initiateAFDownloadRequest:afDownloadUrl destinyPath:destinyPath];
            }
            
            [magazineLibObj release];
        } else {
            
            [self _showAlert:@"Download inprogress, please wait."];
        }
        
        magazineID = btn.MagID;
        issueID    = btn.IssueID;    
        // [loginMod release],loginMod = nil;
    }
}



//Buy Now Button Clicked
-(void)byeNowClicked:(id)sender
{
    if(![self checkInternet]) 
        return;
    currentCoverIndex = tempCurrentCoverIndex;
    if(hidView.hidden ) {
        //if(networkQueue !=nil)
        
        if(![self.view.subviews containsObject:spinner]) 
        {
            singleSelected = TRUE;
            //[downloadView setHidden:TRUE];
            DownloadControl* btn = (DownloadControl*)sender;
            tmpDwnlBtn = btn;
            issueID = btn.IssueID;
            magazineName = btn.magazineName;
            issueDate    = btn.issueDate; 
            
            
            ///recent
            [self removeObjectsOnTranFail];
            [currentDwnldIDS addObject:[NSNumber numberWithInteger:btn.IssueID]];
            issueDate = btn.issueDate;
            magazineName = btn.magazineName;
            issueID = btn.IssueID;
            downldUrlString = btn.downldUrlString;
            magazineID = btn.MagID;
            downloadBtntmp = sender ;
            //[self showInAppList:sender]; 
            if(!downLoadInProgress)
            {
                //if(networkQueue !=nil)
                //    [networkQueue cancelAllOperations];
                loginMod = nil;
                [loginView removeFromSuperview];
                ///Login Authentication
                loginMod = [[Login alloc]init];
                loginMod.downLaoding = TRUE;
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                
                if (![loginMod isUserDetailsExists]) {
                    
                  //  [loginMod showLoginViewAndAuthenticateUser:self.view];
                    loginMod.fromInfo = FALSE;
                    
                    // [loginMod showLoginViewAndAuthenticateUser:self.view];
                    LoginView *logView = [[LoginView alloc] init];
                    
                    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:logView] autorelease];
                    logView.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closeModalView)] autorelease];
                    //controller.navigationItem.title = self.document.title;
                    [self presentModalViewController:navController animated:NO];
                    
                } else {
                    
                    //Recently Added
                    [self removeObjectsOnTranFail];
                    [currentDwnldIDS addObject:[NSNumber numberWithInteger:btn.IssueID]];
                    issueDate = btn.issueDate;
                    magazineName = btn.magazineName;
                    issueID = btn.IssueID;
                    downldUrlString = btn.downldUrlString;
                    magazineID = btn.MagID;
                    [self showInAppList:sender];                    
                }
                [magazineLibObj release];
            } else {
                
               [self _showAlert:@"Download inprogress, please wait."];
            }
        }
    }
}


- (void) getProductList:(int)magID {
    
    NSMutableArray *ar = [[NSMutableArray alloc] init];
    for (UIView *view in [self.view subviews]) {
        NSLog(@"product recieved:" );
        if ([view isKindOfClass:[InappView class]] ) {
            InappView *inapp = (InappView*)view;
            
            if(magID == inapp.CateGoryID && inapp.subid == 0)
            {
                NSLog(@"Matching Category ID: %d" , inapp.CateGoryID);
                
                for (UIView *subview in [inapp subviews]) {
                    if ([subview isKindOfClass:[UIScrollView class]] ) {
                        
                        for (UIView *subview2 in [subview subviews]) {
                            if ([subview2 isKindOfClass:[CustomButton class]] ) {
                                
                                CustomButton *cust = (CustomButton*)subview2;
                                [ar addObject:cust.productIdentifier]; 
                                //[ar setValue:cust.productIdentifier forKey:cust.productIdentifier];
                               // NSLog(@"Matching product Identifier: %@" , cust.productIdentifier);
                            }
                        }
                    }
                }
            }
        }
    }
    
    [self productRequesting:ar];
    
    magazineID = magID;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    [theTextField resignFirstResponder];
    return YES;
}
-(void) serverAuthenticationCompleted:(BOOL) success {
    
    if (success) {
        NSLog(@"SERVER AUTH Success");
        [loginView removeFromSuperview];
        if ([loginMod isPaidUser]) {
           // NSLog(@"SERVER AUTH paid user");
        } else {
           // NSLog(@"SERVER AUTH not paid user");
        }
    } else {
       // NSLog(@"SERVER AUTH Failed");
        
        [self _showAlert:@"Dear user you have to subscribe for downloading the Magazine..."];
    }
}

-(void)showpreview:(id)sender
{
    if(![self checkInternet]) 
        return;
    if(!downLoadInProgress)
        
    {
    back.hidden = TRUE;
    loginMod = [[Login alloc]init];
    BOOL st = loginMod.isUserDetailsExists;
    
    DownloadControl *btn = (DownloadControl*) sender;
    [loginView removeFromSuperview];
    Preview_iPad* prevMag = [[Preview_iPad alloc] init];
    
    [prevMag setPrevwDnldURl: [NSString stringWithFormat:@"%@%@%@%@%@%@", 
                               btn.downldUrlString,
                               @"&email_id=",loginMod.emailID,
                               @"&password=",loginMod.password, @"&platform=ios&device=iphone&dim=150X150"]];
    
    [prevMag setMagID:[NSNumber numberWithInteger:btn.MagID]];
    [prevMag setIssueID:[NSNumber numberWithInteger:btn.IssueID]]; 
    [prevMag setMagName:btn.magazineName];
    [prevMag setIssueDate:btn.issueDate];
    [prevMag setWrapperUrl:btn.wrapperUrl];
    [self.navigationController pushViewController:prevMag animated:TRUE];
    } else {
        
        [self _showAlert:@"Download inprogress, please wait."];
    
    }
    // [prevMag release];
}

- (void)imageDownloadSart:(ASIHTTPRequest *)request
{
    [request.downloadProgressDelegate setHidden:FALSE];
}

- (void)unzipContent:(ASIHTTPRequest *)request {
    
    NSString* destiny = [request downloadDestinationPath] ; //[ NSHomeDirectory() stringByAppendingPathComponent:@"Documents/1_246.zip"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    
    NSString *destinyPath = [NSString stringWithFormat:@"%@%@%d%@%d",[paths objectAtIndex:0],
                             @"/",[request.magazineID intValue],
                             @"_",[request.issueID intValue]];
    
    NSString* unZipFolder = destinyPath;// [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/1_246"];
    
    ZipArchive *za = [[ZipArchive alloc] init];
    
    if ([za UnzipOpenFile: destiny]) {
       // NSLog(@"inside ziping");
        BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
        if (NO == ret){
           // NSLog(@"Failed");
            [za UnzipCloseFile];
        } else {
            [za UnzipCloseFile];
            [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
        }
    }
    [za release];
    // NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //  NSString *magPath = [NSString stringWithFormat:@"%@%@",destinyPath,@"/all.pdf"];
    
    // if([fileManager  fileExistsAtPath:magPath])
    // {
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@','%@'",[request.magazineID integerValue],[request.issueID integerValue],request.issueDate,request.supplementary];
    if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ]) {
        NSLog(@"Magazine Inserted Successfully %@",queryValues);
    }
    //  }
    
    [request.downloadProgressDelegate setHidden:TRUE];
    [request updateDownloadProgress];
}
- (void)imageFetchComplete:(ASIHTTPRequest *)request
{
    
    [currentDwnldIDS removeObject:request.issueID];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self createMagRootFolder];
        NSString* destiny = [request downloadDestinationPath] ; 
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
        
        NSString *destinyPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],
                                 @"/",KMAGROOTPATH,@"/",[request.magazineID intValue],
                                 @"_",[request.issueID intValue]];
        
        NSString* unZipFolder = destinyPath;
        
        ZipArchive *za = [[ZipArchive alloc] init];
        
        if ([za UnzipOpenFile: destiny]) {
            NSLog(@"inside ziping");
            BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
            if (NO == ret){
               // NSLog(@"Failed");
                [za UnzipCloseFile];
            } else {
                [za UnzipCloseFile];
                [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
            }
        }
        [za release];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [request.downloadProgressDelegate setHidden:TRUE];
            [request updateDownloadProgress];
            [self stopDownlLoading];
            [self removeObjectsOnTranFail];
            // [downloadView setHidden:TRUE];
            downLoadInProgress = FALSE;
            [networkQueue release];
            
            [self.gridView reloadData];
            Login *logMod = [[Login alloc] init];
            BOOL st = logMod.isUserDetailsExists;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if([fileManager fileExistsAtPath:pdfDocPath]) 
            {
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@'",[request.magazineID integerValue],[request.issueID integerValue],request.issueDate,request.supplementary, request.magName];
                if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ]) 
                {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%d%@%d%@%d%@%d%@",documentsDirectory,@"/",magazineID,@"_",issueID,
                                            @"/magazine_",magazineID,@"_",issueID,@".pdf"];
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:pdfDocPath]) 
                    {
                        
                    }
                }
                if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
                    logMod.username = @"Guest";
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: [NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]
                                      message:[NSString stringWithFormat:@"%@%@%@%@",magazineName,@" - ",issueDate, @" added to Library successfully, do you want to read now?"]
                                      delegate: self
                                      cancelButtonTitle:@"Not Now"
                                      otherButtonTitles:@"Read Now",nil];
                [alert show];
                [alert release];
            } else 
            {
                [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@%d%@%d",documentsDirectory,@"/",magazineID,@"_",issueID] ] error:NULL];
            }
            [logMod release], logMod = nil;
            //[self _showAlert:@"Magazine added to Libary successfully.."];
        });
    });
    // [issueDwnlTime release], issueDwnlTime = nil;
}
- (void) createMagRootFolder {
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *newDir;
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, 
                                                   NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    newDir = [docsDir stringByAppendingPathComponent:KMAGROOTPATH];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:newDir])
    {
        
        if ([filemgr createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error: NULL] == NO)
        {
           // NSLog(@"Failed to create directoy");
            // Failed to create directory
        } else {
           // NSLog(@"Success");
        }
    }
    [filemgr release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    if (buttonIndex != 0)
    {
        if( downLoadInProgress) {
            [self cancelProgressingDownload];
        } else {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
           /* NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];*/
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",
                                    documentsDirectory,
                                    @"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/wrapper.jpg"];
            
            if([fileManager fileExistsAtPath:pdfDocPath]) 
            {
                TestPDFReader * prevMag = [[TestPDFReader alloc] init];
                [prevMag setWrapperUrl:pdfDocPath];
                [self.navigationController pushViewController:prevMag animated:TRUE];
            }
        }
    }
}
- (void)imageFetchFailed:(ASIHTTPRequest *)request
{
    [currentDwnldIDS removeObject:request.issueID];   
    [self removeObjectsOnTranFail];
    [self stopDownlLoading];
	//if (!failed) {
    if ([[request error] domain] != NetworkRequestErrorDomain || [[request error] code] != ASIRequestCancelledErrorType) {
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Download failed" message:KMAGDWNLDFAILED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [alertView show];
    }
    failed = YES;
    //[request release];
    [networkQueue release];
    maxDowncount -=1;
    downLoadInProgress = FALSE;
    networkQueue = nil;
    [self resetNetwork];
    
}

- (void)getBooksMagaList {
    
   logMod = [[Login alloc]init];
    
    responseData = [[NSMutableData data] retain];
    
    NSMutableURLRequest *request;
    NSString *oNLoadReqUrl;
    NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];

    //Send http request
    if(magArchieve !=TRUE) {
        oNLoadReqUrl = [NSString stringWithFormat:@"%@%@",KMAGSTORE, [getUrlAndParams objectForKey:@"devDet"]];
    }
    else {
        oNLoadReqUrl = [NSString stringWithFormat:@"%@%@%d%@%@%d%@%@%d%@",kMAGARCH,@"mid=",0,@"&",@"year=",selYear,@"&",@"month=",selMonth,
                            [getUrlAndParams objectForKey:@"devDet"]];
    }

    NSLog(@"PREM:oNLoadReqUrl is %@", oNLoadReqUrl);
    
    NSURL *aUrl = [NSURL URLWithString:oNLoadReqUrl];
    request = [NSMutableURLRequest requestWithURL:aUrl
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[getUrlAndParams objectForKey:@"userDet"] dataUsingEncoding:NSUTF8StringEncoding]];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];

    btnRefer.enabled=FALSE;
	[logMod release], logMod = nil;
}

//HTTP Request Handler to get Book/Mag list from Store

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];  NSLog(@"rec:");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data]; NSLog(@"append:");
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self _showAlert:KINTERNETNOTAVBL];
    [self StopLoading];
    btnRefer.enabled=TRUE;
    onLoad = FALSE;
}

- (void) _showAlert:(NSString*)title
{
    Login *logMod = [[Login alloc]init];
    BOOL st = logMod.isUserDetailsExists;
    if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
        logMod.username = @"Guest";
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]  message:title  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
    [logMod release],logMod = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    
    responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    // NSLog(@"response is %@",responseString);
    [self StopLoading];
    
    if(responseString !=@"" || responseString !=NULL){
        
        [self buildStorePage:( NSString *) responseString]; 
    } else {
        
         [self _showAlert:KFIREWALLBLOCK];
    }
    btnRefer.enabled=TRUE;
    magArchieve = FALSE;
    onLoad = FALSE;
}

- (void)buildStorePage:(NSString *)responseString {
    NSLog(@"Loggin in");
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
    [subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
    CGRect frame = CGRectMake(0, 10, self.view.bounds.size.width, self.view.bounds.size.height - 10 );   
    self.gridView.frame = frame;
    // NSLog(@"zero testin %@",responseString);
    int singleIssue = 0, highLightIss;
    [magzineList release];[magazineIDS release];[magProdIdnt release];
    [singleIssueState release];[singleIssuePrice release];[sub_promoList release];
    magzineList = [[NSMutableArray alloc] init];
    magazineIDS = [[NSMutableArray alloc] init];
    magProdIdnt = [[NSMutableArray alloc] init];
    singleIssueState = [[NSMutableArray alloc] init];
    singleIssuePrice = [[NSMutableArray alloc] init];
    sub_promoList    = [[NSMutableArray alloc] init];
    showSub          = [[NSMutableArray alloc] init];
    singleIssDic     = [[NSMutableDictionary alloc]init];
    subIssDic        = [[NSMutableDictionary alloc]init];
    ///DownloadControl *downloadBtn;
    int ndx, imageWidth;
	NSString *titleString;
	NSInteger categoryid;
    
    // UIScrollView *inappContainer;
    SBJSON *jsonParser = [[SBJSON new] autorelease];
   	id response = [jsonParser objectWithString:responseString];
    NSMutableDictionary *newsfeed = (NSMutableDictionary *)response;
    NSArray *categories = (NSArray *)[newsfeed valueForKey:KSTOREMAGS];
    
    if(categories == NULL ) {
        [self _showAlert:KFIREWALLBLOCK];
        return;
    }
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:[newsfeed valueForKey:DOWNLOAD_VERSION_KEY] forKey:DOWNLOAD_MANAGER_KEY];
    
    if([standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY] == NULL )
    {
        [standardUserDefaults setValue:SEL_DOWNLOAD_MANAGER forKey:DOWNLOAD_MANAGER_KEY];
    }

    NSLog(@"SEL_DOWNLOAD_MANAGER in magazine store json is %@", [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY]);
    
    issValidFrm  = [newsfeed valueForKey:KMAGISSUEVALID];
    highLightIss = [[newsfeed valueForKey:KMAGISSUEHGHLGHT] intValue];
    //  NSString *subString1 = [NSString stringWithFormat:@"%@",[newsfeed valueForKey:KSTOREMAGS]];
	NSDictionary *category;
    
    [coverImgList release], coverImgList = nil;
    //coverImgList = [[NSMutableArray alloc]initWithCapacity:categories.count];
    coverImgList = [[NSMutableArray alloc] init];
    NSLog(@"before testin");
    
    [articleImgUrl release], articleImgUrl = nil;
    //articleImgUrl = [[NSMutableDictionary alloc] initWithCapacity:categories.count];
    articleImgUrl = [[NSMutableDictionary alloc] init];
   // _imageNames = [[NSMutableArray alloc] initWithCapacity:[categories count]];
    _imageNames = [[NSMutableArray alloc] init];
	// loop over all the stream objects and print their titles
	for (ndx = 0; ndx < categories.count ; ndx++) {
		NSLog(@"one testin");
        category    = (NSDictionary *)[categories objectAtIndex:ndx];
		
        NSArray *articles = (NSArray *)[category valueForKey:KMAGISSUES];
        imageWidth = 20 ;
        if(articles !=NULL || articles !=Nil )
        {
            titleString = urlStringDecode([category valueForKey:KMAGINENAME]);
            categoryid  = (NSInteger)[category valueForKey:KMAGINEID];
            [magazineIDS addObject:[category valueForKey:KMAGINEID]];
            [magzineList addObject:titleString];
            [magProdIdnt addObject:@"product identifier"];
            [sub_promoList addObject:[category valueForKey:KMAGMAGSUBPROMO]];
            [articleImgUrl setObject:articles forKey:[NSString stringWithFormat:@"%d",ndx]];
            //Loop through individual articles in each category
            for (int y = 0 ; y < articles.count; y++)
            {			
                // NSLog(@"two testing");
                NSDictionary *article = (NSDictionary *)[articles objectAtIndex:y];
                NSMutableString *urlString = [NSMutableString stringWithString:KVIKSERVERURL];
                [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
                if(y == 0) {
                    //[coverImgList addObject:(NSString*)urlString];
                    NSMutableString *urlString = [NSMutableString stringWithString:KVIKSERVERURL];
                    [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
                    [_imageNames addObject:urlString];
                   // NSLog(@"Image path setting here sir %@", urlString);
                }
            }
            singleIssue = 1;
            [singleIssueState addObject:[NSNumber numberWithInteger:singleIssue]];
            ///END of single issues
            NSMutableArray *subs = (NSMutableArray *)[category valueForKey:KMAGSUBSISS];
            
            NSString *subString = [NSString stringWithFormat:@"%@",[category valueForKey:KMAGSUBSISS]];
            
            if( [subString length] > 0)
            {
                NSMutableArray *arList = [[NSMutableArray alloc]init];
                for (int y = 0 ; y < subs.count; y++)
                {			
                    
                    NSDictionary *sub = (NSDictionary *)[subs objectAtIndex:y];
                    CustomButton *but = [CustomButton buttonWithType:UIButtonTypeCustom];
                    [but setProductIdentifier:[sub valueForKey:KMAGISSPRODINDENTI]];
                    [but setSubid:[[sub valueForKey:@"subid"] intValue]];
                    [but setSubprice:[sub valueForKey:@"subprice"]];
                    [but setSubsname:[sub valueForKey:@"subsname"]];
                    [but setCateGoryID:[[category valueForKey:KMAGINEID] intValue]];
                    [but setBackgroundColor:[UIColor clearColor]];
                    [showSub addObject:[sub valueForKey:KSHOWSUBSTRIP]];
                    [arList addObject:but];
                }
                [subIssDic setObject:arList forKey:[category valueForKey:KMAGINEID] ];
            } 
            //END of scubscription
        } 
        
    }

    if(_imageNames.count > 0 )
        // [self setImageToCovers:highLightIss]; 
        [self.gridView reloadData];
    else
        [self _showAlert:KMAGNOTAVLB];
    
    
    jsonParser = nil;
    [jsonParser release];
    newsfeed = nil;
    categories = nil;
    category = nil;
    responseString = nil;
    jsonParser = nil;
    [pool release];
    
}
- (void)closeModalView {
    [self dismissModalViewControllerAnimated:YES];
}
- (void)presentModalViewControllerWithCloseButton:(UIViewController *)controller {
    
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
    
    // UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    navController.navigationController.toolbar.barStyle = UIBarStyleBlackOpaque;
    navController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closeModalView)] ;
    //controller.navigationItem.title = self.document.title;
    [self presentModalViewController:navController animated:NO];
    
}
- (void) showArchievesPage:(id)sender 
{
    if(!downLoadInProgress)
    {
       // archBtn.hidden = TRUE;
       // btnRefer.hidden = TRUE;
        [self hideInappView];
        [self disableAction];
        hidView.hidden = TRUE;
        Calendar_iPad *calView = [[Calendar_iPad alloc]initWithMag:self:issValidFrm];
        calView.view.frame = CGRectMake(0, 50, 320, 320);
        //[self presentModalViewController:calView animated:TRUE];
        [self presentModalViewControllerWithCloseButton:calView];
       // [self.navigationController pushViewController:calView animated:TRUE];
        
    } else {
        [self _showAlert:@"Download inprogress, please wait."];
    }
}

- (void) callBackArchive {
    
    if(![self checkInternet]) 
        return;
    [self LoadingIcon];
    //covers = [NSMutableArray new];
    issueView = FALSE;
    [self magazineOnLoad];
}
- (void) callInapp {
    
    if(![self checkInternet]) 
        return;
    [self showInAppList:downloadBtntmp];
    //[self _showAlert:@"Download inprogress, please wait."];
}

- (void) restorealltran {
    if(![self checkInternet]) 
        return;
    restoreallpurc = TRUE;
   // NSLog(@"restoring sir");
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue]  restoreCompletedTransactions];
}
//Gridview implementation

#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return [_imageNames count] ;
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aGridView {
    int xWidth =  150/1.2, yWidth = 225.0;
    if(!issueView){
        yWidth = 188;
    }
    return ( CGSizeMake(xWidth, yWidth) );
}
/*
 - (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
 {
 //  if([self isLandscape])
 //     return ( CGSizeMake(213/1.75, 275.0/1.75) );
 //  else
 return ( CGSizeMake(150/1.75, 275.0/1.75) );
 
 
 }
 */
- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    static NSString *ThumbCellIdentifier = @"PSPDFThumbnailGridViewCell";
    ImageDemoGridViewCell * cell = (ImageDemoGridViewCell *)[self.gridView dequeueReusableCellWithIdentifier:ThumbCellIdentifier];
    
    int xWidth =  150/1.2, yWidth = 275 - 65, dwnldyPos = 222/1.2;
    if(!issueView){
        tempCurrentCoverIndex = index;
        yWidth = 212/1.2;
        cell = [[[ImageDemoGridViewCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, roundf(self.thumbnailSize.width*1.f), roundf(self.thumbnailSize.height*1.f)) reuseIdentifier:ThumbCellIdentifier] autorelease];
    } else {
        
        cell = [[[ImageDemoGridViewCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, roundf(self.thumbnailSize.width*1.f), roundf(self.thumbnailSize.height*1.f + 40)) reuseIdentifier:ThumbCellIdentifier] autorelease];
        btnRefer.hidden = TRUE;
        archBtn.hidden  = TRUE;
    }
    
    NSMutableString *urlString = [_imageNames objectAtIndex:index];
    AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                  initWithFrame:CGRectMake(0,0,self.thumbnailSize.width,self.thumbnailSize.height)] ;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:asyncImage.bounds];
    [asyncImage.layer setMasksToBounds:YES];
    [asyncImage.layer setCornerRadius:5.0f];
    [asyncImage.layer setBorderWidth:1.0f];
    asyncImage.layer.shadowPath = path.CGPath;
    asyncImage.moduleIndi = 7;
    
    NSLog(@"PREM: URL String:%@",urlString);
    
    
    asyncImage.imageURL = urlString;
    [asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
    [cell addSubview:asyncImage];
    
    if(issueView){
        
        cell.layer.borderWidth = 0;
        NSArray *ar = [articleImgUrl objectForKey:[NSString stringWithFormat:@"%d",tempCurrentCoverIndex]];
        MagazineLib *magazineLibObj = [[MagazineLib alloc] init] ;
        NSDictionary *article = (NSDictionary *)[ar objectAtIndex:index];
        
        UILabel *siteLabel_ = [[UILabel alloc] initWithFrame:CGRectZero];         
        siteLabel_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7f];
        siteLabel_.textColor = [UIColor colorWithWhite:0.8f alpha:1.f];
        siteLabel_.shadowColor = [UIColor blackColor];
        siteLabel_.shadowOffset = CGSizeMake(1.f, 1.f);
        siteLabel_.textAlignment = UITextAlignmentCenter;
        siteLabel_.font = [UIFont boldSystemFontOfSize:14];
        siteLabel_.text = [article valueForKey:KMAGISSDATE];
        [cell addSubview:siteLabel_];
        siteLabel_.frame = CGRectMake(0, self.thumbnailSize.height-ksiteLabelHeight, self.thumbnailSize.width, ksiteLabelHeight);
        
        DownloadControl *downloadBtn;
        imageProgressIndicator1 = [[[UIProgressView alloc] initWithFrame:CGRectMake(0 ,155 ,100,20)] autorelease];
        //imageProgressIndicator1 = [[[UIProgressView alloc] initWithFrame:CGRectMake(10 ,h ,300,20)] autorelease];
        imageProgressIndicator1.backgroundColor = [UIColor clearColor];
        imageProgressIndicator1.tag =[ [article valueForKey:KMAGISSID] intValue];
        [imageProgressIndicator1 setHidden:TRUE];
        [cell addSubview:imageProgressIndicator1];
        //[dwnldListView addSubview:imageProgressIndicator1];
        
        loginMod = [[Login alloc]init];
        BOOL st = loginMod.isUserDetailsExists;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
        NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                                  @"/",[[articleImgUrl valueForKey:KMAGINEID] intValue],
                                  @"_",[[article valueForKey:KMAGISSID] intValue],
                                  @".zip"];
        NSMutableString* issuID = [NSMutableString stringWithFormat:@"%@", [article valueForKey:KMAGISSID]];
        
        downloadBtn = [DownloadControl buttonWithType:UIButtonTypeCustom];
        [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
        [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
        [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
        [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
        [downloadBtn setDownldUrlString:[article valueForKey:KMAGDWNLDURL]];
        //[downloadBtn setDwnRequest:request];
        downloadBtn.backgroundColor = [UIColor clearColor];
        downloadBtn.frame = CGRectMake(12.5  ,self.thumbnailSize.height+10 , 100, 30) ;
        downloadBtn.tag = [issuID intValue];
        
        NSString *dwnlURl = [article valueForKey:KMAGDWNLDURL];
        NSString *dwndable = [article valueForKey:KMAGDOWNDABLE];
        
        NSArray *fields = [[NSArray alloc] initWithObjects:@"issue_id", nil];
        NSString *cond = [NSString stringWithFormat:@"issue_id=%d",[issuID intValue]];
        NSMutableArray *mutableArr = [[NSMutableArray alloc] initWithArray:[magazineLibObj
                                                                            selectMagazineField:fields when:cond]];
        if([mutableArr count] > 0)
        {
            [downloadBtn setImage:[UIImage imageNamed: @"read_now.png"] forState:UIControlStateNormal];
            [downloadBtn addTarget:self action:@selector(readNowClicked:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20,0,80,27)];
            [lblTitle setText:@"Read Now"];
            [lblTitle setBackgroundColor:[UIColor clearColor]];
            [lblTitle setTextColor:[UIColor blackColor]];
            lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [downloadBtn addSubview:lblTitle];
            [cell addSubview:downloadBtn];
            //[container addSubview:downloadBtn];
        } 
        else 
        {
            // if([singleIssueState count] < index)
            //{
          
                if([dwndable isEqualToString:@"N"])
                {
                    if([dwnlURl length] >0 ){
                        [downloadBtn setImage:[UIImage imageNamed: @"buynow.png"] forState:UIControlStateNormal];
                        [downloadBtn addTarget:self action:@selector(byeNowClicked:) forControlEvents:UIControlEventTouchUpInside];
                        [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
                        [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
                        [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
                        [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
                        [downloadBtn setDownldUrlString:[article valueForKey:KMAGDWNLDURL]];
                        [downloadBtn setIssuName:[article valueForKey:KSINGLEISSNAM]];
                        [downloadBtn setIssuePrice:[article valueForKey:KSINGLEISSPRICE]];
                        [downloadBtn setProductIdentifier:[article valueForKey:KSINGLEISSPRDID]];
                        downloadBtn.frame = CGRectMake(12.5  ,self.thumbnailSize.height+10 , 100, 30);
                        
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(25,0,100,27)];
                        [lblTitle setText:@"Buy Now"];
                         
                        [lblTitle setBackgroundColor:[UIColor clearColor]];
                        [lblTitle setTextColor:[UIColor whiteColor]];
                        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
                        [downloadBtn addSubview:lblTitle];
                    }
                } 
                else 
                {
                    if([dwnlURl length] >0 ){
                        [downloadBtn setImage:[UIImage imageNamed: @"add_2lib.png"] forState:UIControlStateNormal];
                        [downloadBtn addTarget:self action:@selector(downloadClicked:) forControlEvents:UIControlEventTouchUpInside];
                        downloadBtn.frame = CGRectMake(12.5  ,self.thumbnailSize.height+10 , 100, 30) ;
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(7.5,0,100,27)];
                        [lblTitle setText:@"Add to Library"];
                        [lblTitle setBackgroundColor:[UIColor clearColor]];
                        [lblTitle setTextColor:[UIColor whiteColor]];
                        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
                        [downloadBtn addSubview:lblTitle];
                    }
                }
            
            [cell addSubview:downloadBtn];
            downloadBtn = [DownloadControl buttonWithType:UIButtonTypeCustom];
            [downloadBtn setDownldUrlString:[article valueForKey:KMAGPREVDWNLDURL]];
            downloadBtn.wrapperUrl = [NSString stringWithFormat:@"http://www.vikatan.com/%@", [article valueForKey:KMAGISSWRAPURL]];
            downloadBtn.backgroundColor = [UIColor clearColor];
            downloadBtn.frame = CGRectMake( 66,self.thumbnailSize.height-ksiteLabelHeight - 48 , 67, 39) ;
            downloadBtn.tag = index;//[issuID intValue];
            [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
            [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
            [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
            [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
            [downloadBtn setImage:[UIImage imageNamed: @"preview.png"] forState:UIControlStateNormal];
            [downloadBtn addTarget:self action:@selector(showpreview:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:downloadBtn];
        }
        if([dwnlURl isEqualToString:@""]) {
            downloadBtn.enabled = FALSE;
        }
        [magazineLibObj release], magazineLibObj = nil;
    }
    
    return cell;
}

- (void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)gridIndex {
    
   
    if(!issueView) {
        
        CGRect frame = CGRectMake(0, 10, self.view.bounds.size.width, self.view.bounds.size.height - 10 );   
        
        self.gridView.frame = frame;
         
        [back setHidden:FALSE];
        issueView = TRUE;
        _imageNames = [[NSMutableArray alloc] init];
        tempCurrentCoverIndex = gridIndex;
        NSArray *ar = [articleImgUrl objectForKey:[NSString stringWithFormat:@"%d",gridIndex]];
        for(int i = 0 ;i< [ar count];i++) {
            NSDictionary *article = (NSDictionary *)[ar objectAtIndex:i];
            NSMutableString *urlString = [NSMutableString stringWithString:KVIKSERVERURL];
            [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
            [_imageNames addObject:urlString];
             NSLog(@"checking Imageurl %@",urlString);
        }
        
        ////Subscription Strip sir
        
        if( [magzineList count] > 0 ) 
        {
            [subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
           // NSLog(@"showstrip: %@", [showSub objectAtIndex:gridIndex]);
           
            if ([[showSub objectAtIndex:gridIndex] isEqualToString:@"Y"] ) {

                CGRect frame = CGRectMake(0, 70, self.view.bounds.size.width, self.view.bounds.size.height - 70 );   
                self.gridView.frame = frame;
                //[subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;
                subScriptionbtnView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,15,self.view.frame.size.width ,50)];
                subScriptionbtnView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
                subScriptionbtnView.contentMode = UIViewContentModeCenter;
                subScriptionbtnView.backgroundColor = [UIColor lightGrayColor];
                subScriptionbtnView.opaque = 0.9;
                subScriptionbtnView.alpha = 0.9;

                CustomButton *subScription = [CustomButton buttonWithType:UIButtonTypeCustom];
                [subScription addTarget:self action:@selector(subscriptionClicked:) forControlEvents:UIControlEventTouchUpInside];
                subScription.frame = CGRectMake(100,10,115 ,30);
                [subScription setImage:[UIImage imageNamed:@"sub.png"] forState:UIControlStateNormal];
                [subScription setCateGoryID:[[magazineIDS objectAtIndex:gridIndex] intValue]];
                
                [self.view addSubview:subScription];
                [subScriptionbtnView addSubview:subScription];
                [self.view addSubview:subScriptionbtnView];
            
            }
        }

        [self.gridView reloadData];
        [self showTabBar];
    }
    [self.gridView deselectItemAtIndex:gridIndex animated:NO];
   
}
//Customize Grid View
- (void)initializeGridView {
    if (!self.gridView) {
        // init thumbs view
        self.gridView = [[[AQGridView alloc] initWithFrame:CGRectZero] autorelease];
        self.gridView.backgroundColor = [UIColor clearColor];
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;  
        self.gridView.scrollEnabled = TRUE;
        self.gridView.layoutDirection = AQGridViewLayoutDirectionVertical;

        self.gridView.hidden = NO;
        self.gridView.scrollsToTop = YES;
        [self.view addSubview:self.gridView];
        CGRect frame = CGRectMake(0, 10, self.view.bounds.size.width, self.view.bounds.size.height - 10 );   
        
        self.gridView.frame = frame;
        // add a footer view for tabbar
        if (!PSIsIpad()) {
            self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 0)] autorelease];
        }
    }
}

- (BOOL) connectedToNetwork
{
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:KVIKSERVERURL]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
} 

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
		[self _showAlert:KINTERNETNOTAVBL];
		return NO;
	}
	else {
		return YES;
	}
}

//Hide the tab bar view
-(void) hideTabBarView
{
    
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 1064, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 1064)];
        }
    }
    [UIView commitAnimations];	
}
- (void) showTabBar
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
	
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        // NSLog(@"%@", view);
		
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, screenHeight-48, view.frame.size.width, view.frame.size.height)]; //432
			
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, screenHeight-48)]; //432
        }
    }
    [UIView commitAnimations]; 
}

-(void)webViewDidStartLoad:(UIWebView *) portal {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {    
	[webSpinner stopAnimating];
    [webSpinner removeFromSuperview];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [webSpinner stopAnimating];
    [webSpinner removeFromSuperview];
}


-(void) initiateAFDownloadRequest:(NSString *) downloadUrl destinyPath:(NSString *) destinyPath {
    

    if (allowDownloadVersion) { // OS version Greater than or equal 5
        allowDownloadVersion = TRUE;
        resumeBtn.hidden = TRUE;
        pauseBtn.hidden = FALSE;
        cancelBtn.hidden = FALSE;
        
    }  else { // OS version less than 5
        allowDownloadVersion = FALSE;
        pauseBtn.hidden = TRUE; //Resume not supported for below ios 5.0 due to non-avail of progress bar.
        resumeBtn.hidden = TRUE;
        cancelBtn.hidden = FALSE;
    }
    
    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:downloadUrl]];
    
    Login *logModule = [[Login alloc]init];
    NSMutableDictionary * getUrlAndParams = [logModule getDeviceDetails];
    logModule=nil, [logModule release];

    NSLog(@"initiateAFDownloadRequest download url in iphone is %@", downloadUrl);
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:downloadUrl]];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [getUrlAndParams objectForKey:REQUEST_EMAIL_ID], REQUEST_EMAIL_ID,
                            [getUrlAndParams objectForKey:REQUEST_PASSWORD], REQUEST_PASSWORD,
                            [getUrlAndParams objectForKey:RESPONSE_USER_NAME], RESPONSE_USER_NAME,
                            [getUrlAndParams objectForKey:RESPONSE_SOCIAL_FLAG], RESPONSE_SOCIAL_FLAG,
                            nil];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:downloadUrl
                                                      parameters:params];
    
    // AFHTTPRequestOperation download functions

    downloadOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    downloadOperation.outputStream = [NSOutputStream outputStreamToFileAtPath:destinyPath append:NO];
    
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [downloadOperation start];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setPauseMode) name:@"setPauseMode" object: nil];
    
    maxDowncount++;
    downLoadInProgress = TRUE;
    [self stopDownlLoading];
    [self downldLoading];
    imageProgressIndicator.hidden=FALSE;
    
    [downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(totalBytesExpectedToReadFromUrl == -1 ) { //Download failed
           downloadOperation=nil, [downloadOperation release];
        } else {
            
            [self AFNetworkUnZipFile:destinyPath];
            
            downloadOperation=nil, [downloadOperation release];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *errors) {
               
        
        if(totalBytesReadFromUrl >= totalBytesExpectedToReadFromUrl) {
            NSLog(@"downloaded success in failure block totalBytesReadFromUrl > totalBytesExpectedToReadFromUrl");

            [self AFNetworkUnZipFile:destinyPath];
            downloadOperation=nil, [downloadOperation release];
        } else {
            [currentDwnldIDS removeObject:[NSString stringWithFormat:@"%d", issueID]];
            [self removeObjectsOnTranFail];
            [self stopDownlLoading];
            
            UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Download failed" message:KMAGDWNLDFAILED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            [alertView show];
            [alertView release];
            
            failed = YES;
            
            maxDowncount -=1;
            downLoadInProgress = FALSE;
            networkQueue = nil;
            [self resetNetwork];
            
            if([self.popoverController isPopoverVisible])
                [self.popoverController dismissPopoverAnimated:TRUE];
            
            
            downloadOperation=nil, [downloadOperation release];
        }
     
    }];
    
    if (allowDownloadVersion) {
        [downloadOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            
            float percentDone = totalBytesRead/(float)totalBytesExpectedToRead;
           // NSLog(@"percentDone ------%f",percentDone);
            [imageProgressIndicator setProgress:percentDone animated:YES];
            
            totalBytesExpectedToReadFromUrl = totalBytesExpectedToRead;
            totalBytesReadFromUrl = totalBytesRead;
        }];
    } else {
        
        UILabel *DownloadText = [[UILabel alloc] initWithFrame:CGRectMake(40, 40, 200, 40)];
        DownloadText.text=@"Downloading.... ";
        [DownloadText setTextColor:[UIColor whiteColor]];
        [DownloadText setBackgroundColor:[UIColor clearColor]];
        [dwnldListView addSubview:DownloadText];
        
        [downloadOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {

            float percentDone = totalBytesRead/(float)totalBytesExpectedToRead;
            NSLog(@"percentDone ------%f",percentDone);

            totalBytesExpectedToReadFromUrl = totalBytesExpectedToRead;
            totalBytesReadFromUrl = totalBytesRead;
        }];
        
        imageProgressIndicator.hidden = TRUE;
    }
}

//AFNetworking functions

-(IBAction) pausedownload: (id) sender {
    
    [self setPauseMode];
}

-(void) setPauseMode  {

    [downloadOperation pause];
    
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden=FALSE;
}

-(IBAction) resumedownload: (id) sender {
    
    if([self checkInternet] == YES) {
        
        pauseBtn.hidden = FALSE;
        resumeBtn.hidden=TRUE;
        [downloadOperation resume];
    }
}

-(IBAction) canceldownload: (id) sender {
    
   // [self cancelProgressingDownload];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                    message:@"Do you want to cancel Download?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok",nil];
    [alert show];
    [alert release];
}

-(void) cancelProgressingDownload {
    
    NSLog(@"cancelProgressingDownload is calling");
    
    if(![downloadManagerVersion isEqualToString:@"v2"]) {
        
        [networkQueue cancelAllOperations];
    } else {
        
        [downloadOperation cancel];
        downloadOperation=nil, [downloadOperation release];
    }
    
    downLoadInProgress = FALSE;
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    imageProgressIndicator.hidden = TRUE;
    
    [self removeObjectsOnTranFail];
    [self stopDownlLoading];
}

-(void) AFNetworkUnZipFile:(NSString *) destinationPath {
    
    [currentDwnldIDS removeObject:[NSString stringWithFormat:@"%d", issueID]];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self createMagRootFolder];
        NSString* destiny = destinationPath ;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
        NSString *destinyPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],
                                 @"/",KMAGROOTPATH,@"/",magazineID,
                                 @"_",issueID];
        
        NSString* unZipFolder = destinyPath;
        
        ZipArchive *za = [[ZipArchive alloc] init];
        
        if ([za UnzipOpenFile: destiny]) {
            BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
            if (NO == ret){
                [za UnzipCloseFile];
            } else {
                [za UnzipCloseFile];
                [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
            }
        }
        [za release];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            [self stopDownlLoading];
            [self removeObjectsOnTranFail];
            downLoadInProgress = FALSE;
            
            if(![downloadManagerVersion isEqualToString:@"v2"]) {
                [networkQueue cancelAllOperations];
                [networkQueue release];
            }

            if(tempCurrentCoverIndex != currentCoverIndex)
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];
            else {
                if(currentCoverIndex == [coverflow numberOfCovers]-1)
                {
                    if(currentCoverIndex != 0)
                        [coverflow bringCoverAtIndexToFront:currentCoverIndex-1  animated:TRUE];
                    
                } else
                {
                    [coverflow bringCoverAtIndexToFront:currentCoverIndex+1  animated:TRUE];
                }
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];
            }
            if([self.popoverController isPopoverVisible])
                [self.popoverController dismissPopoverAnimated:TRUE];
            Login *logMod = [[Login alloc] init];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if([fileManager fileExistsAtPath:pdfDocPath])
            {
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@'",magazineID,issueID,issueDate,0, magazineName]; //request.supplementary
                if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ])
                {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%d%@%d%@%d%@%d%@",documentsDirectory,@"/",magazineID,@"_",issueID,
                                            @"/magazine_",magazineID,@"_",issueID,@".pdf"];
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:pdfDocPath])
                    {
                    }
                }
                if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
                    logMod.username = @"Guest";
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: [NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]
                                      message:[NSString stringWithFormat:@"%@%@%@%@",magazineName,@" - ",issueDate, @" Added to Library successfully, do you want to read now?"]
                                      delegate: self
                                      cancelButtonTitle:@"Not Now"
                                      otherButtonTitles:@"Read Now",nil];
                [alert show];
                [alert release];
            } else
            {
                [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@%d%@%d",documentsDirectory,@"/",magazineID,@"_",issueID] ] error:NULL];
            }
            [logMod release], logMod = nil;
        });
    });
}
@end
