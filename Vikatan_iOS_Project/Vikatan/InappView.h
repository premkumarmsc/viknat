//
//  CustomButton.h
//  Vikatan
//
//  Created by MobileVeda on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InappView : UIView {
    
    NSString *UrlString,
            *CateGoryTitle,
            *commentsCount,
            *articleCount,
            *CateGoryIDStr,
            *artWebURL,
            *articleTitle,
            *productIdentifier;
    int CateGoryID;
    int ArticleID;
    
    int subid;
    NSString *subsname, 
             *subprice,
             *ios_uid;
    BOOL singleIssue;
    
}
@property (nonatomic, retain) NSString *UrlString, *CateGoryTitle,
                                        *commentsCount, *articleCount,
                                        *artWebURL,*CateGoryIDStr, *articleTitle,
                                        *productIdentifier,
                                        *subsname, 
                                        *subprice,
                                        *ios_uid;
@property (nonatomic) int CateGoryID;
@property (nonatomic) int ArticleID;
@property (nonatomic) int subid;
@property (nonatomic) BOOL singleIssue;
@end
