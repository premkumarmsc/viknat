//
//  MagazineStore_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "MagazineStore_iPad.h"
#import "Utilities.h"
#import "MagazineLib.h"
#import <StoreKit/StoreKit.h>

@implementation MagazineStore_iPad
@synthesize scrollArticleView, 
scrollCategoryView, loginView,
scrollArchCategoryView, 
scrollArchArticleView,
productIdentView,
popoverController,
disableView;
@synthesize coverflow,covers;
@synthesize selYear,selMonth, 
magArchieve,selectedPrdIndt,dwnRequest;
@synthesize singleIssDic,subIssDic;
@synthesize gridView=_gridView;
@synthesize thumbnailSize = thumbnailSize_;
@synthesize thumbnailMargin = thumbnailMargin_;

@synthesize apiClient, downloadOperation, totalBytesReadFromUrl, totalBytesExpectedToReadFromUrl,
imageProgressIndicator, resumeBtn, cancelBtn, pauseBtn, downloadManagerVersion, DownloadTitle,
allowDownloadVersion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
   
    if(loginMod !=NULL)
        [loginMod release], loginMod = nil;
    if(magzineList !=NULL)
        [magzineList release], magzineList = nil;
    if(magazineIDS !=NULL)
        [magazineIDS release], magazineIDS = nil;
    
    if([self.view.subviews containsObject:scrollCategoryView])
        [scrollCategoryView release], scrollCategoryView = nil;
    if([self.view.subviews containsObject:scrollArticleView])
        [scrollArticleView release], scrollArticleView  = nil;
    if([self.view.subviews containsObject:scrollArchCategoryView])
        [scrollArchCategoryView release], scrollArchCategoryView = nil;
    if([self.view.subviews containsObject:scrollArchArticleView])
        [scrollArchArticleView release], scrollArchArticleView = nil;
    
    articleImgUrl = nil;
    [articleImgUrl release];
    [belView removeFromSuperview];
    [belView release];
    [coverflow release];
	[covers release];
    
    [downloadView removeFromSuperview];[downloadView release];
    [dwnldListView removeFromSuperview];[dwnldListView release];
    [titleView removeFromSuperview];[titleView release];
    [dwnldMgrBtn removeFromSuperview];[dwnldMgrBtn release];
    maxDowncount = 0;
    [currentDwnldIDS release],currentDwnldIDS = nil;
    [serachView removeFromSuperview],[serachView release],serachView = nil;
    [popController release], popController = nil;
    [productIdentView removeFromSuperview],[productIdentView release], productIdentView = nil;
    [singleIssueState release], singleIssueState = nil;
    [singleIssuePrice release], singleIssuePrice = nil;
    [sub_promoList release], sub_promoList = nil;
    [selectedPrdIndt release],selectedPrdIndt = nil;
    [dwnRequest release],dwnRequest = nil;
    [singleIssDic release]; [subIssDic release];
    singleIssDic = nil, subIssDic = nil;
    [disableView release], disableView = nil;
    [_imageNames release], _imageNames = nil;
    [showSub release], showSub = nil;
    
    gridView.delegate = nil;
    [gridView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview. [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad
{
    
    singleSelected = TRUE;
    allowDownloadVersion = TRUE;
    
    currentDwnldIDS = [[NSMutableArray alloc] init];
    [self buildDwnlMgr];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	CGRect r = self.view.bounds;
    r.origin.x = 20;
	r.size.height = 465;
    
	coverflow = [[TKCoverflowView alloc] initWithFrame:r];
	coverflow.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	coverflow.coverflowDelegate = self;
	coverflow.dataSource = self;
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
		coverflow.coverSpacing = 80;
        coverflow.coverSize = CGSizeMake(150+75+37, 212+106+53);
	}
	coverflow.backgroundColor = [UIColor clearColor];
	[self.view addSubview:coverflow];
	
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,5,153 ,30);
    [btn setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *archBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    archBtn.frame = CGRectMake(0,0,48 ,48);
    [archBtn setImage:[UIImage imageNamed:@"Archives.png"] forState:UIControlStateNormal];
    [archBtn addTarget:self action:@selector(showArchievesPage:) forControlEvents:UIControlEventTouchUpInside];
    
    UIToolbar *compoundView = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 180.f, self.navigationController.navigationBar.frame.size.height)] autorelease];
    compoundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    compoundView.barStyle = UIBarStyleBlackOpaque;
    compoundView.tintColor = [UIColor blackColor];
    
    //Mobileveda customization
    
    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil] autorelease];
    space.width = 3.f;
    
    NSMutableArray *compountItems = [NSMutableArray array];
    
    UIBarButtonItem *archButton = [[UIBarButtonItem alloc] initWithCustomView:archBtn];
    archButton.style = UIBarButtonSystemItemFixedSpace;
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:archButton, nil]];
    UIBarButtonItem *refButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    refButton.style = UIBarButtonSystemItemFixedSpace;
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:refButton, space, nil]];
    compoundView.items = compountItems;
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:compoundView] autorelease];
    
    
    NSMutableArray *leftItems = [NSMutableArray array];
   
    dwnldMgrBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dwnldMgrBtn.frame = CGRectMake(0,0,130 ,43);
    dwnldMgrBtn.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    [dwnldMgrBtn setTitle:@"Downloading ..." forState:UIControlStateNormal];
    [dwnldMgrBtn setBackgroundColor:[UIColor clearColor]];
    [dwnldMgrBtn setHidden:TRUE];

    [dwnldMgrBtn addTarget:self action:@selector(showDwnldPage:) forControlEvents:UIControlEventTouchUpInside];
    
    UIToolbar *leftToolBar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 150.f, self.navigationController.navigationBar.frame.size.height)] autorelease];
    leftToolBar.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    leftToolBar.barStyle = UIBarStyleBlackOpaque;
    leftToolBar.tintColor = [UIColor blackColor];
    dwnldSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	
    //Set position

	dwnldSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    dwnldSpinner.hidden = TRUE;
    UIBarButtonItem *spnButton = [[UIBarButtonItem alloc] initWithCustomView:dwnldSpinner];
    [leftItems addObjectsFromArray:[NSArray arrayWithObjects:spnButton, nil]];
    
    UIBarButtonItem *dwnldButton = [[UIBarButtonItem alloc] initWithCustomView:dwnldMgrBtn];
    [leftItems addObjectsFromArray:[NSArray arrayWithObjects:dwnldButton, nil]];
    
    leftToolBar.items = leftItems;
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:leftToolBar] autorelease];
    
    titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
   	[self LoadingIcon];
    
    //Add Header Image Control
	UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
	UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:
                                    CGRectMake(0, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-14, 5, 100, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"இதழ்கள்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];
    
    self.navigationItem.titleView = titleView;    
    [self LoadingIcon];
    covers = [NSMutableArray new];
    
    hidView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 380, self.view.frame.size.width, self.view.frame.size.height)];
    hidView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    hidView.userInteractionEnabled = FALSE;
    hidView.backgroundColor = [UIColor blackColor];
    hidView.alpha = 0.4;
    hidView.hidden = TRUE;
    hidView.exclusiveTouch = FALSE;
    [self.view addSubview:hidView];
    
    loginMod = [[Login alloc]init];
    [self magazineOnLoad];
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(callBackArchive) name:@"magazinereferesh" object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(closePops) name:@"popviewclose" object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(callInapp) name:@"purccontinue" object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(restorealltran) name:@"purcrestoreipad" object: nil];
    thumbnailSize_   = CGSizeMake(162, 264);
    thumbnailMargin_ = CGSizeMake(0.f, 0.f);
    
    self.navigationController.navigationBar.layer.shadowColor = [ [UIColor whiteColor ] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0,1.0);//, <#CGFloat height#>)
    self.navigationController.navigationBar.layer.shadowOpacity = 0.40;
    restoreallpurc = FALSE;
}
- (void) buildDwnlMgr {
    maxDowncount = 1;
    downloadView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 440, 120)];
    [loginView setClipsToBounds: YES];
    dwnldListView = [[UIScrollView alloc] initWithFrame:CGRectMake(10,30, 440, 300)];
    [downloadView addSubview:dwnldListView];
    [downloadView setHidden:FALSE];
    [downloadView setBackgroundColor:[UIColor clearColor]];
    
    DownloadTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 400, 20)];
    DownloadTitle.text=@"";
    [DownloadTitle setTextColor:[UIColor whiteColor]];
    [DownloadTitle setBackgroundColor:[UIColor clearColor]];
    [dwnldListView addSubview:DownloadTitle];

    pauseBtn = [[CustomUIButton alloc] init];
    pauseBtn.frame = CGRectMake(340,40, 28, 28);
    [pauseBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [pauseBtn addTarget:self action:@selector(pausedownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:pauseBtn];
    
    resumeBtn = [[CustomUIButton alloc] init];
    resumeBtn.frame = CGRectMake(340, 40, 28, 28);
    [resumeBtn setBackgroundImage:[UIImage imageNamed:@"resume.png"] forState:UIControlStateNormal];
    [resumeBtn addTarget:self action:@selector(resumedownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:resumeBtn];
    
    cancelBtn = [[CustomUIButton alloc] init];
    cancelBtn.frame = CGRectMake(380,40, 28, 28);
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(canceldownload:) forControlEvents:UIControlEventTouchDown];
    [dwnldListView addSubview:cancelBtn];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.view sendSubviewToBack:subScriptionbtnView];

}

- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	//if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
	//	return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
	return YES;
}
- (void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}
- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
   
}
- (void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void) changeNumberOfCovers{
	
	NSInteger index = coverflow.currentIndex;
	NSInteger no = arc4random() % 200;
	NSInteger newIndex = MAX(0,MIN(index,no-1));
	
    
	[coverflow setNumberOfCovers:no];
	coverflow.currentIndex = newIndex;
	
}

-(void) close_dwnlmng_Clicked:(id)sender {
    
    //UIButton *btn = (UIButton*)btn;
    //[btn.superview setHidden:TRUE];
    
    //[downloadView setHidden:TRUE];
    //downloadView.frame = CGRectMake(0,0, 0, 0);
    //[downloadView setBackgroundColor:[UIColor clearColor]];
}

- (void) hideInappView {
    
    [self disableAction];
    for (UIView *view in [self.view subviews]) {
        
        if ([view isKindOfClass:[InappView class]] ) 
        {
            InappView *inapp = (InappView*)view;
            
            if(magazineID == inapp.CateGoryID)
            { 
                [inapp setHidden:TRUE];
                
            }
        }
    }
}
//Referesh Clicked
- (void)refresh_clicked:(id)sender {
    
    
    
    if(![self checkInternet]) 
        return;
    magArchieve = FALSE;
    [self hideInappView];
    [self disableAction];
    hidView.hidden = TRUE;

    if(!downLoadInProgress) {
        [self LoadingIcon];
        covers = [NSMutableArray new];
        [self magazineOnLoad];
    } else {
       //[self _showAlert:@"Download inprogress, please wait."];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                        message:@"Do you want to cancel Download?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
        [alert release];
    }
}

- (void)showDwnldPage:(id)sender {
    // [downloadView setHidden:FALSE];
    // [self.view bringSubviewToFront:downloadView];
    
    NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];
    if (order == NSOrderedSame || order == NSOrderedDescending) {

        UIViewController *ct = [[UIViewController alloc] init];
    [ct.view setBackgroundColor:[UIColor clearColor]];
    [ct.view addSubview:downloadView];
    
    UIPopoverController *popoverControl = [[UIPopoverController alloc] initWithContentViewController:ct] ;
    CGRect popoverRect = [self.view convertRect:[sender frame] 
                                       fromView:[sender superview]];    
    popoverRect.size.width = MIN(popoverRect.size.width, 100);
    CGSize size = CGSizeMake(440, 120); // size of view in popover
    popoverControl.popoverContentSize = size;
    
    [popoverControl 
     presentPopoverFromRect:popoverRect 
     inView:self.view 
     permittedArrowDirections:UIPopoverArrowDirectionAny 
     animated:YES];
    // remember controller
    self.popoverController = popoverControl;
    } 
}
- (void) showDwnldView {
   
    UIViewController *ct = [[UIViewController alloc] init];
    [ct.view setBackgroundColor:[UIColor clearColor]];
    [ct.view addSubview:downloadView];
    
    UIPopoverController *popoverControl = [[UIPopoverController alloc] initWithContentViewController:ct] ;
    CGRect popoverRect = [self.view convertRect:[dwnldMgrBtn frame] 
                                       fromView:[dwnldMgrBtn superview]];    
    popoverRect.size.width = MIN(popoverRect.size.width, 100);
    CGSize size = CGSizeMake(440, 120); // size of view in popover
    popoverControl.popoverContentSize = size;
    
    [popoverControl 
     presentPopoverFromRect:popoverRect 
     inView:self.view 
     permittedArrowDirections:UIPopoverArrowDirectionAny 
     animated:YES];
    // remember controller
    self.popoverController = popoverControl;
}

- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasBroughtToFront:(int)index{
    
    _imageNames = [[NSMutableArray alloc] init];
    tempCurrentCoverIndex = index;
    NSArray *ar = [articleImgUrl objectForKey:[NSString stringWithFormat:@"%d",index]];
    
    for(int i = 0 ;i< [ar count];i++) {
        NSDictionary *article = (NSDictionary *)[ar objectAtIndex:i];
        NSMutableString *urlString = [NSMutableString stringWithString:@"http://www.vikatan.com/"];
        [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
        [_imageNames addObject:urlString];
    }
    if( [magzineList count] > 0 ) 
    {
        [subScriptionbtnView removeFromSuperview], [subScriptionbtnView release],subScriptionbtnView = nil;

        if ([[showSub objectAtIndex:index] isEqualToString:@"Y"] ) {
        
        
            subScriptionbtnView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,295,self.view.frame.size.width ,90)];
            subScriptionbtnView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            subScriptionbtnView.contentMode = UIViewContentModeCenter;
            CustomButton *subScription = [CustomButton buttonWithType:UIButtonTypeCustom];
            subScription.index = index;
            [subScription addTarget:self action:@selector(subscriptionClicked:) forControlEvents:UIControlEventTouchUpInside];
            subScription.frame = CGRectMake(0,0,self.view.frame.size.width ,90);
            [subScription setImage:[UIImage imageNamed:@"substrip.png"] forState:UIControlStateNormal];
            [subScription setCateGoryID:[[magazineIDS objectAtIndex:index] intValue]];
            
            [self.view addSubview:subScription];
            UILabel *lblSub = [[UILabel alloc]initWithFrame:CGRectMake(0, 12.5, self.view.frame.size.width ,45)];
            lblSub.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            lblSub.text = [NSString stringWithFormat:@"%@",
                           [sub_promoList objectAtIndex:index]] ;
            
            lblSub.backgroundColor = [UIColor clearColor];
            lblSub.textColor = [UIColor darkGrayColor];
            lblSub.textAlignment = UITextAlignmentCenter;
            [subScription addSubview:lblSub];
            [subScriptionbtnView addSubview:subScription];
            [self.view addSubview:subScriptionbtnView];
        }
    }
    
    [self.gridView reloadData];
    //[self.gridView reloadInputViews];
}

- (void) subscriptionClicked:(id)sender {
    
    if(![self checkInternet])
        return;
    if(![self.view.subviews containsObject:spinner]) 
    {
        if(!downLoadInProgress)
        {
            //[networkQueue cancelAllOperations];
            //[downloadView setHidden:TRUE];
            loginMod = nil;
            [loginView removeFromSuperview];
            ///Login Authentication
            loginMod = [[Login alloc]init];
            [loginMod setDelegate:self];
            loginMod.downLaoding = TRUE;
            
            
            ////
            CustomButton *btn = (CustomButton*)sender;
            
            int magID = btn.CateGoryID;
            singleSelected = FALSE;
            magazineID = magID;
             issueID = 0;
            downloadBtntmp = sender;
           // [self showInAppList:sender];
            if (![loginMod isUserDetailsExists]) {
                
                loginMod.fromInfo = FALSE;
                //[loginMod showLoginViewAndAuthenticateUser:self.view];
                
                LoginView *ct = [[LoginView alloc] init];
                [ct.view setBackgroundColor:[UIColor clearColor]];
                
                UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:ct] ;
                popoverController.delegate = self;
                ///[popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                
                CGRect popoverRect = [self.view convertRect:[sender frame] 
                                                   fromView:[sender superview]];    
                popoverRect.size.width = MIN(popoverRect.size.width, 100);
                CGSize size = CGSizeMake(768, 450); // size of view in popover
                popoverController.popoverContentSize = size;
                [popoverController 
                 presentPopoverFromRect:popoverRect 
                 inView:self.view 
                 permittedArrowDirections:UIPopoverArrowDirectionAny 
                 animated:YES];
                self.popoverController = popoverController;
                //[loginMod showLoginViewAndAuthenticateUser:self.view];
                
            } else {
                CustomButton *btn = (CustomButton*)sender;
                
                int magID = btn.CateGoryID;
                singleSelected = FALSE;
                magazineID = magID;
                 issueID = 0;
                [self showInAppList:sender];
            }
        } else 
        {
            [self _showAlert:@"Download inprogress, please wait."];
        }
    }
}
-(void)closeClicked:(id)sender
{
    [productIdentView removeFromSuperview];
    selectedPrdIndt = @"";
    [self disableAction];
    UIButton *btn = (UIButton*)sender;
    [btn.superview setHidden:TRUE];
    [self removeObjectsOnTranFail];
    downLoadInProgress = FALSE;
    [self StopLoading];
    if([networkQueue requestsCount] > 0 )
        [networkQueue cancelAllOperations];
    //[inappView removeFromSuperview];
}

- (void) submitClicked:(id)sender
{
    transactionStatus = FALSE;
    if(![self checkInternet]) 
        return;
    
    CustomButton *btn = (CustomButton*)sender;
    //if( selectedPrdIndt !=NULL) { 
        // selectedPrdIndt = btn.productIdentifier;
    
        if([selectedPrdIndt isEqualToString:@""]||selectedPrdIndt == NULL||selectedPrdIndt == Nil)
        {
            [self  _showAlert:KINVALIDPRDTIDNT];
            
        } else {
            magazineID = btn.CateGoryID;
            NSMutableArray *ar = [[NSMutableArray alloc] init];
    
            [ar addObject:selectedPrdIndt];
            [self productRequesting:ar];
            [self LoadingIcon];
            downLoadInProgress = TRUE;
        }
   // }
    //[networkQueue go];
    //selectedPrdIndt = @"";
}

-(void)readNowClicked:(id)sender
{
    if(hidView.hidden) {

        DownloadControl *magBtn = (DownloadControl*)sender;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        /*NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                documentsDirectory,
                                @"/",KMAGROOTPATH,@"/",magBtn.MagID,@"_",magBtn.IssueID,
                                @"/magazine_",magBtn.MagID,@"_",magBtn.IssueID,@".pdf"];*/
        
        NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",
                                documentsDirectory,
                                @"/",KMAGROOTPATH,@"/",magBtn.MagID,@"_",magBtn.IssueID,
                                @"/wrapper.jpg"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSLog(@"pdfDocPath is %@", pdfDocPath);
        if([fileManager fileExistsAtPath:pdfDocPath]) 
        {
            
            TestPDFReader * prevMag = [[TestPDFReader alloc] init];
            [prevMag setWrapperUrl:pdfDocPath];
            [self.navigationController pushViewController:prevMag animated:TRUE];
            
        } else {
          //  [self callBackArchive];
        }
    }
}
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    if([response.products count] > 0 )
    {
        for (NSString *invalidProductId in response.invalidProductIdentifiers)
        {
            NSLog(@"Invalid product id: %@" , invalidProductId);
        }
        
        [request autorelease];
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        SKPayment *payment = [SKPayment paymentWithProductIdentifier:selectedPrdIndt ];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        selectedPrdIndt = @"";
        [self disableAction];
    }
    downLoadInProgress = FALSE;
   // [self StopLoading];
}
-(void)showInAppList:(id)sender {
    transactionStatus = FALSE;
    DownloadControl *downloadBtn = (DownloadControl*)sender;
    [productIdentView removeFromSuperview];
    productIdentView = [[InappView alloc] initWithFrame:CGRectMake(0, 0, 768, 884/2)];
    [productIdentView setBackgroundColor:[UIColor clearColor]];
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,768,45)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
   // [commentTitle setTitle:[sub_promoList objectAtIndex:magazineID-1] forState:UIControlStateNormal];
    [commentTitle setTitle:[sub_promoList objectAtIndex:tempCurrentCoverIndex] forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(200, 12, 300, 25);
    commentTitle.enabled = FALSE;
    [commentTitle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cmntHeader addSubview:commentTitle];
    [productIdentView addSubview:cmntHeader];
    NSMutableArray *subs = [[NSMutableArray alloc] init];
    CustomButton *subMit = [CustomButton buttonWithType:UIButtonTypeCustom]; 
    subMit.CateGoryID = magazineID;
    int ypos = 0;    
    UIScrollView *inappContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 768, 300)];

    if(singleSelected)
    {
        /*
        subs = (NSMutableArray*)[subIssDic objectForKey:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:magazineID]]];
        for (int y = 0 ; y < subs.count; y++)
        {			
            
            CustomButton *but = (CustomButton*)[subs objectAtIndex:y];NSLog(@"Subscriptin anme sir %@",but.subsname);
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setSelected:FALSE];
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, 768, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];//UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            //lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [inappContainer addSubview:lbl];
            [singleIssuePrice addObject:but.subprice];
            
            
            ypos += 50;
        }
        */
     //   subs = (NSMutableArray*)[singleIssDic objectForKey:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:magazineID]]];
   //     for (int y = 0 ; y < subs.count; y++)
     //   {			
             CustomButton *but = [CustomButton buttonWithType:UIButtonTypeCustom];

            //CustomButton *but = (CustomButton*)[subs objectAtIndex:y];NSLog(@"Subscriptin anme sir %@",downloadBtn.issuName);
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setSelected:FALSE];
            [but setProductIdentifier:downloadBtn.productIdentifier];
            [but setSubprice:downloadBtn.issuePrice];
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, 768, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            //[lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", downloadBtn.issuName,@" $",downloadBtn.issuePrice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];//UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            //lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
            [inappContainer addSubview:lbl];
            //[singleIssuePrice addObject:but.subprice];
            [singleIssuePrice addObject:downloadBtn.issuePrice];
            ypos += 50;
    //    }
        
        
    } else {
        subs = (NSMutableArray*)[subIssDic objectForKey:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:magazineID]]];
        for (int y = 0 ; y < subs.count; y++)
        {			
            
            CustomButton *but = (CustomButton*)[subs objectAtIndex:y];
            [but setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [but setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
            [but setFrame:CGRectMake(10, ypos+10, 32, 32)];
            [but setSelected:FALSE];
            [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
            [inappContainer addSubview:but];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, ypos, 768, 45)];
            lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
            [lbl setText:[NSString stringWithFormat:@"%@%@%@", but.subsname,@" $",but.subprice]];
            [lbl setBackgroundColor:[UIColor clearColor]];
            lbl.textColor = [UIColor blackColor];// UIColorFromRGB(0x3399FF);
            lbl.lineBreakMode = UILineBreakModeWordWrap;
            lbl.numberOfLines = 4;
            [inappContainer addSubview:lbl];
            [singleIssuePrice addObject:but.subprice];
            ypos += 50;
        }
    }
    
    [subMit setImage:[UIImage imageNamed:@"buynow.png"] forState:UIControlStateNormal];
    subMit.frame = CGRectMake(10, ypos + 20, 100, 30) ;
    [subMit addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchUpInside];
    [inappContainer addSubview:subMit];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(30,0,100,27)];
    [lblTitle setText:@"Submit"];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setTextColor:[UIColor whiteColor]];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    [subMit addSubview:lblTitle];
    inappContainer.contentSize = CGSizeMake(350,ypos+ 50);
    
    
    UILabel *lblTrmTitle = [[UILabel alloc] initWithFrame:CGRectMake(250, ypos + 110, 768 , 40)];
    lblTrmTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
    [lblTrmTitle setText:@"Terms & Condition"];
    [lblTrmTitle setBackgroundColor:[UIColor clearColor]];
    lblTrmTitle.textColor =[UIColor darkGrayColor];// UIColorFromRGB(0x3399FF);
    //[productIdentView addSubview:lblTrmTitle];
    //txtView.frame = CGRectMake(5, ypos + 150, 700 , 350) ;
    
    UIWebView *termWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, ypos + 140,768, 300)] ;
    termWebView.scalesPageToFit = NO;
    termWebView.delegate = self;
    NSMutableString *postUrl = [NSMutableString stringWithString:KPAYMENTTERMSIPAD];//[urlMore appendString:categoryID];KPURTERMS
    NSMutableString *uriString = [[NSMutableString alloc] initWithString:[loginMod getDeviceDetails:postUrl]];
    NSURL* url = [[NSURL alloc] initWithString:uriString];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    [termWebView loadRequest:urlRequest];
    [termWebView setNeedsLayout];
    [productIdentView addSubview:termWebView];
    
    webSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [webSpinner setCenter:productIdentView.center];
    //spinner.backgroundColor = [UIColor lightGrayColor];
    webSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    //Add the loading spinner to view
    [productIdentView addSubview:webSpinner];
    //Start animating
    [webSpinner startAnimating];
    [productIdentView bringSubviewToFront:webSpinner];
    
    //  [txtView scrollRangeToVisible:range];
    [productIdentView addSubview:inappContainer];
    
    UIViewController *ct = [[UIViewController alloc] init];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = ct.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor, (id)[UIColor lightGrayColor].CGColor, nil];
    [ct.view.layer addSublayer:gradient];
    //[ct.view setBackgroundColor:[UIColor whiteColor]];
    [ct.view addSubview:productIdentView];
    
    UIPopoverController *popoverControl = [[UIPopoverController alloc] initWithContentViewController:ct] ;
    CGRect popoverRect = [self.view convertRect:[sender frame] 
                                       fromView:[sender superview]];    
    popoverRect.size.width =MIN(popoverRect.size.width, 100);
    CGSize size = CGSizeMake(768, 480); // size of view in popover
    popoverControl.popoverContentSize = size;
    
    [popoverControl 
     presentPopoverFromRect:popoverRect 
     inView:self.view 
     permittedArrowDirections:UIPopoverArrowDirectionAny 
     animated:YES];
    // remember controller
    self.popoverController = popoverControl;
    
    
}
- (void)disableAction {
    
    /*
     if(hidView.hidden)
     {
     hidView.hidden = FALSE;
     [self.view bringSubviewToFront:hidView];
     }else {
     hidView.hidden = TRUE;
     }
     */
    /*
     if(self.view.userInteractionEnabled)
     {
     [self.view setUserInteractionEnabled:FALSE];
     }
     else
     {
     [self.view setUserInteractionEnabled:TRUE];
     }
     */ 
}
- (void) requestProductData:(NSArray*)ar {

    [self LoadingIcon];
    NSSet *productIdentifiers = [NSSet setWithArray:ar];    
    SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers: productIdentifiers];
    request.delegate = self;
    [request start];
    //[ar release];
    
}
- (void)checkboxButton:(UIButton *)button{
    
    CustomButton *custBtn = (CustomButton*)button;
    
    for (CustomButton *but in [custBtn.superview subviews]) {
        if ([but isKindOfClass:[CustomButton class]] && ![but isEqual:custBtn]) {
            [but setSelected:NO];
        }
    }
    
    if (custBtn.selected) {
        custBtn.selected = !custBtn.selected;
        selectedPrdIndt = @"";
       // NSLog(@"Checked");
    } else {
        custBtn.selected = TRUE;
        selectedPrdIndt = custBtn.productIdentifier;
        NSLog(@"Not Checked");
        //downLoadInProgress = TRUE;
    }
    subprice = custBtn.subprice ;
    subid = custBtn.subid;    
    
    NSLog(@"Subscription price is subjected to be: %@ %d %@", subprice, subid, selectedPrdIndt);
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error  
{  
    NSLog(@"Failed to connect with error: %@", [error localizedDescription]);
}  

- (void) productRequesting:(NSArray*)ar {
    
    NSLog(@"magazine store ipad productRequesting ar is %@",ar);
    
    downLoadInProgress = TRUE;
    
    if ([SKPaymentQueue canMakePayments]) {
        [self  requestProductData:ar];
    } else {
       // NSLog(@"Cannot proceed");
        [self _showAlert:@"Check your setting, in app purchase is restricted."];
    }
}

//Payment module
//
// kick off the upgrade transaction
//
- (void)purchaseProUpgrade
{
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchaseProUpgradeProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

#pragma -
#pragma Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk2
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Inside magazine store ipad recordTransaction");

    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"proUpgradeTransactionReceipt" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
    NSLog(@"magazine store ipad productId is %@", productId);
    
    if ([productId isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isProUpgradePurchased" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"record paymentQueueRestoreCompletedTransactionsFinished");
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    [self StopLoading];
    [self hideInappView];
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful)
    {
       // NSLog(@"magazine store ipad payemnt success");
        
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];  
        
        NSString *receiptData = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];

        NSURL *requestURL = [NSURL URLWithString:KMAGPAYMENTSUCESS];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
        
        
        Login *logMod = [[Login alloc]init];
        [logMod setDelegate:self];

        NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];
        
        NSLog(@"finishTransaction emailID, userID, sessionID, password %@ ==>%@==>%@==>%@", [getUrlAndParams objectForKey:@"email_id"], [getUrlAndParams objectForKey:@"userID"], [getUrlAndParams objectForKey:@"session_id"], [getUrlAndParams objectForKey:@"password"]);

        [request setPostValue:[getUrlAndParams objectForKey:@"password"] forKey:REQUEST_PASSWORD];
        [request setPostValue:[getUrlAndParams objectForKey:@"userID"] forKey:RESPONSE_USER_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"session_id"] forKey:RESPONSE_SESSION_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"email_id"] forKey:RESPONSE_EMAIL_ID];

        [request setPostValue:[NSString stringWithFormat:@"%d",magazineID] forKey:@"mid"];
        [request setPostValue:[NSString stringWithFormat:@"%d",issueID] forKey:@"issue_id"];
        [request setPostValue:[NSString stringWithFormat:@"%d",subid] forKey:@"subid"];
        [request setPostValue:[NSString stringWithFormat:@"%@",subprice] forKey:@"amount"];
        [request setPostValue:[NSString stringWithFormat:@"%d",0] forKey:@"trans_status"];
        [request setPostValue:[NSString stringWithFormat:@"%@",receiptData] forKey:@"receipt-data"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"Apple"] forKey:@"bank"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"ios"] forKey:@"platform"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"ipad"] forKey:@"device"];
        [request setPostValue:[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueIdentifier]] forKey:@"udid"];
       
        //Remove before going live
        //[request setPostValue:@"mobileveda" forKey:@"debug"];
        
        if(transactionStatus)
        
            [request setPostValue:[NSString stringWithFormat:@"%@",@"restore"] forKey:@"request_type"];
        
        else
            [request setPostValue:[NSString stringWithFormat:@"%@",@""] forKey:@"request_type"];


        [request startSynchronous];
        
        
        NSError *error = [request error];
        if (!error) {
            
            NSString *res = [request responseString];
            if(![res isEqualToString:KMAGPAYMENTDONE])
            {
               // [self _showAlert:KSERVERPAYMENTFAILED];
                
            } else {
                if(!transactionStatus) {
                issueDwnlTime = res;
                if(singleSelected )
                {
                    [self stopDownlLoading];
                    [self downldLoading];
                    //Recent
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
                    NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                                              @"/",magazineID,
                                              @"_",issueID,
                                              @".zip"];
                    
                    imageProgressIndicator = [[[UIProgressView alloc] initWithFrame:CGRectMake(10, 50, 320, 20)] autorelease];
                    imageProgressIndicator.backgroundColor = [UIColor clearColor];
                    imageProgressIndicator.tag = issueID;
                    [imageProgressIndicator setHidden:TRUE];
                    [dwnldListView addSubview:imageProgressIndicator];

                    DownloadTitle.text = [NSString stringWithFormat:@"%@%@%@",magazineName,@" - ",issueDate] ;
                    DownloadTitle.tag = issueID;
                    
                    Login *logMod = [[Login alloc]init];
                    NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];
                    logMod=nil, [logMod release];

                    NSString * downloadUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                                              downldUrlString,@"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                              @"&email_id=",loginMod.emailID,
                                              @"&password=",loginMod.password, @"&platform=ios&v=1.4&device=ipad&dim=150X150"];
                    
                    NSString * afDownloadUrl = [NSString stringWithFormat:@"%@%@%@%@",
                                              downldUrlString,
                                              @"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                              @"&platform=ios&v=1.4&device=ipad&dim=150X150"];
                    
                    NSLog(@"magazine downloadUrl is %@", downloadUrl);
                    
                    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                    downloadManagerVersion = [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY];
                    
                    NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];
                    
                    if (order == NSOrderedSame || order == NSOrderedDescending) { // OS version Greater than or equal 5
                        allowDownloadVersion = TRUE;
                        
                    }  else { // OS version less than 5
                        allowDownloadVersion = FALSE;
                        downloadManagerVersion = SEL_DOWNLOAD_MANAGER; // Overriding Download Manager Version for progress view support
                    };

                    if(![downloadManagerVersion isEqualToString:@"v2"]) {
                        resumeBtn.hidden = TRUE;
                        cancelBtn.hidden = FALSE;
                        ASIHTTPRequest *request;
                        request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:downloadUrl]];
                       
                        [request setDownloadDestinationPath:destinyPath];
                        [request setUserInfo:[NSDictionary dictionaryWithObject:@"request1" forKey:@"name"]];
                        [request setIssueID:[NSNumber numberWithInteger:issueID]];
                        [request setMagazineID:[NSNumber numberWithInteger:magazineID]];
                        [request setIssueDate:issueDate];
                        [request setSupplementary:FALSE];
                        [request setMagName:magazineName];
                        [request setDownloadProgressDelegate:imageProgressIndicator];
                        
                        networkQueue = [[ASINetworkQueue alloc] init];	
                        [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
                        [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
                        [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
                        [networkQueue setShowAccurateProgress:TRUE];
                        [networkQueue setDelegate:self];
                        
                        //Recent

                        [networkQueue addOperation:request];
                        [networkQueue go];
                    } else {
                        [self initiateAFDownloadRequest:afDownloadUrl destinyPath:destinyPath];
                    }
                    
                    UILabel *lblSUC = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 20)];
                    lblSUC.text = KPURCHASESCUCCS;
                    [lblSUC setTextColor:UIColorFromRGB(0x3399FF)];
                    [lblSUC setBackgroundColor:[UIColor clearColor]];
                    [dwnldListView addSubview:lblSUC];
                    [self showDwnldView];
                    maxDowncount++;
                    downLoadInProgress = TRUE;
                    
                    getUrlAndParams=nil;
                    
                } else {
                    [self _showAlert:@"Magazine Subscription success.."];
                    [self callBackArchive];
                }
                }
                //[networkQueue cancelAllOperations];
            }
            
        } {
            
        }
        
        [loginMod release], loginMod = nil;
    }
    else
    {
        NSLog(@"Inside magazine store ipad finishTransaction was not Successful");
        
        downLoadInProgress = FALSE;
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
        
        [self removeObjectsOnTranFail];
        
    }
    //Release the allocated Async request
    //  [networkQueue release];
    [self disableAction];
    
}

-(void) removeObjectsOnTranFail {
    
    for (UIView *but in [dwnldListView subviews]) {
        if ([but isKindOfClass:[UILabel class]]) {
            //[but removeFromSuperview]; // MV modification commented to display issue title
        }
    }
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    downLoadInProgress = FALSE;
    [self recordTransaction:transaction];
    //[self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    
    transactionStatus = TRUE;
    [self recordTransaction:transaction.originalTransaction];
   // [self recordTransaction: transaction content:transaction.originalTransaction.payment.productIdentifier];
    //[self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    downLoadInProgress = FALSE;
    [self removeObjectsOnTranFail];
    
   // NSLog(@"Transaction failed error :%@",[transaction.error description]);
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        [self _showAlert:KDEVICEPAYMENTFAILED]; 
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
    [self StopLoading];
    //[networkQueue cancelAllOperations];
    //Release the allocated Async request
    //[networkQueue release];
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    

    for (SKPaymentTransaction *transaction in transactions)
    {
        transactionStatus = FALSE;
       // NSLog(@"updating guru  %@",transaction.transactionIdentifier);
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
               // NSLog(@"updating purc  %@",transaction.description);
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
               // NSLog(@"updating restored  %@",transaction.transactionIdentifier);
               // NSLog(@"restoring guru ");
                transactionStatus = TRUE;
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
        
      //  [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
    
    if(restoreallpurc)
        [self _showAlert:@"Restore Completed"];
    restoreallpurc = FALSE;
}
//end of payment module
- (TKCoverflowCoverView*) coverflowView:(TKCoverflowView*)coverflowView coverAtIndex:(int)index{
	
	TKCoverflowCoverView *cover = [coverflowView dequeueReusableCoverView];
	
	if(cover == nil){
		BOOL phone = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone;
		//CGRect rect = phone ? CGRectMake(0, 0, 224, 300) : CGRectMake(0, 0, 300, 300);
        //CGRect rect = phone ? CGRectMake(0, 0, 224, 300) : CGRectMake(0, 0, 175, 500);
		//CGRect rect = phone ? CGRectMake(0, 0, 224, 300) : CGRectMake(0, 0, KCOVERFLOWWIDTH, KCOVERFLOWHEIGHT);
        CGRect rect = phone ? CGRectMake(0, 0, 224, 300) : CGRectMake(0, 0, 150+75+37, 424);
		cover = [[[TKCoverflowCoverView alloc] initWithFrame:rect] autorelease]; // 224
		//cover.baseline = 212;
        //cover.baseline =100;// KCOVERFLOWHEIGHT;
        cover.baseline =224;
	}    
    
    AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                  initWithFrame:CGRectMake(0,0,150+75+37, 424)] ;
    asyncImage.moduleIndi = 5;
    
    asyncImage.imageURL = [coverImgList objectAtIndex:index];//@"http://www.vikatan.com/news//2011//11//images//150X150//5013_thumb.jpg";
    [asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
    cover.sync = asyncImage;
    // NSLog(@"Covers count %d",index%[covers count]);
	cover.image = [covers objectAtIndex:index%[covers count]];
    
	return cover;
	
}
- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasDoubleTapped:(int)index{
	
	
	TKCoverflowCoverView *cover = [coverflowView coverAtIndex:index];
	if(cover == nil) return;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:cover cache:YES];
	[UIView commitAnimations];
		
}

- (void) resetNetwork {
    
   // if (!networkQueue) {
		networkQueue = [[ASINetworkQueue alloc] init];	
//	}
    
	failed = NO;
	[networkQueue reset];
    [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
	[networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
	[networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
	[networkQueue setShowAccurateProgress:TRUE];
	[networkQueue setDelegate:self];
    
}
- (void) magazineOnLoad {
    if(!onLoad){
        [self initializeGridView];
        [self resetNetwork];
        [self getBooksMagaList];
        onLoad = TRUE;
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    if(loginMod !=NULL)
        loginMod = nil;
    if(magzineList !=NULL)
        magzineList = nil;
    if(magazineIDS !=NULL)
        magazineIDS = nil;

    sub_promoList = nil;

    singleIssDic = nil, subIssDic = nil;
    disableView = nil;
    _imageNames = nil;

    btn=nil, [btn release];
    self.gridView.delegate = nil;
    self.gridView = nil;
    loginMod = nil;
    downloadManagerVersion=nil;
    DownloadTitle=nil;
}


-(void) authenticationCompleted:(BOOL) success {
    
    if (success) {
       // NSLog(@"SERVER AUTH Success");
        if ([loginMod isPaidUser]) {
           // NSLog(@"SERVER AUTH paid user");
        } else {
           // NSLog(@"SERVER AUTH not paid user");
        }
    } else {
        NSLog(@"SERVER AUTH Failed");
    }
    
}

- (void) downloadStart {
    
    if ([loginMod isPaidUser]) {
    } else {
        
    }
    
}

- (void) LoadingIcon {
    
    if(![self.view.subviews containsObject:spinner]) 
    {
        if([self.popoverController isPopoverVisible])
            [self.popoverController dismissPopoverAnimated:TRUE];
        //[self.view setBackgroundColor:[UIColor blackColor]];
        //[self.view setOpaque:0.8];
        [self.view setUserInteractionEnabled:FALSE];

        //Initialize activity window for loading process
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //Set position

        [spinner setCenter:self.view.center];
        //spinner.backgroundColor = [UIColor lightGrayColor];
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        //Add the loading spinner to view
        [self.view addSubview:spinner];
        //Start animating
        [spinner startAnimating];
        [self.view bringSubviewToFront:spinner];
        disableView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height + 300)];
        [disableView setBackgroundColor:[UIColor blackColor]];
        disableView.opaque = 0.4;
        disableView.alpha = 0.4;
        [self.view addSubview:disableView];
    }
}

- (void) StopLoading {
    
    if([self.view.subviews containsObject:spinner]) 
    {
        [self.view setOpaque:0.0];
        [self.view setUserInteractionEnabled:TRUE];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        [disableView removeFromSuperview];
    }
    [spinner release], spinner = nil;
}

- (void) StopLoadingWebView 
{
    
}
- (void) downldLoading {
    [dwnldMgrBtn setHidden:FALSE];
    /*
     //Initialize activity window for loading process
     dwnldSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     //Set position
     [dwnldSpinner setCenter:CGPointMake(43,25)];
     //spinner.backgroundColor = [UIColor lightGrayColor];
     dwnldSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
     //Add the loading spinner to view
     [titleView addSubview:dwnldSpinner];
     */
	//Start animating
	[dwnldSpinner startAnimating];
    
}

- (void) stopDownlLoading {
    
    if(!dwnldSpinner.hidden) 
    {
        [dwnldMgrBtn setHidden:TRUE];
        [dwnldSpinner stopAnimating];
        [dwnldSpinner setHidden:TRUE];
        [self.popoverController dismissPopoverAnimated:FALSE];
    }
    //[dwnldSpinner release], dwnldSpinner = nil;
    // downloadView.hidden = TRUE;
}
- (BOOL) findCurrentIssueDwnld:(NSInteger)issID {
    
    BOOL statu = FALSE;
    for( int i =0; i<[currentDwnldIDS count];i++)
    {
        if(issID == [[currentDwnldIDS objectAtIndex:i] intValue])
            statu = TRUE;
    }
    
    return statu;
}

-(void)downloadClicked:(id)sender
{
    if(![self checkInternet]) 
        return;
    currentCoverIndex = tempCurrentCoverIndex;
    if(hidView.hidden) {
        //if(networkQueue !=nil)
        singleSelected = TRUE;
        // [downloadView setHidden:TRUE];
        DownloadControl* dwnldbtn = (DownloadControl*)sender;
        tmpDwnlBtn = dwnldbtn;
        issueID = dwnldbtn.IssueID;
        magazineName = dwnldbtn.magazineName;
        issueDate    = dwnldbtn.issueDate; 
        //[self productRequesting];
        if( !downLoadInProgress)
        {
           // [networkQueue cancelAllOperations];
            loginMod = nil;
            [loginView removeFromSuperview];
            loginMod = [[Login alloc]init];
            loginMod.downLaoding = TRUE;
            MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
            [loginMod isUserDetailsExists];

            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                                      @"/",dwnldbtn.MagID,
                                      @"_",dwnldbtn.IssueID,
                                      @".zip"];

            imageProgressIndicator = [[[UIProgressView alloc] initWithFrame:CGRectMake(10, 50, 320, 20)] autorelease];
            imageProgressIndicator.backgroundColor = [UIColor clearColor];
            imageProgressIndicator.tag = dwnldbtn.IssueID;
            [imageProgressIndicator setHidden:TRUE];
            [dwnldListView addSubview:imageProgressIndicator];

            
            DownloadTitle.text = [NSString stringWithFormat:@"%@%@%@",dwnldbtn.magazineName,@" - ",dwnldbtn.issueDate] ;
            DownloadTitle.tag = dwnldbtn.IssueID;

            //Recent
            
            NSString * downloadUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                                      dwnldbtn.downldUrlString,
                                      @"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                      @"&email_id=",loginMod.emailID,
                                      @"&password=",loginMod.password,
                                      @"&platform=ios&v=1.4&device=ipad&dim=150X150"];
           
            
            NSString * afDownloadUrl = [NSString stringWithFormat:@"%@%@%@%@",
                                      dwnldbtn.downldUrlString,
                                      @"&udid=",[[UIDevice currentDevice] uniqueIdentifier],
                                      @"&platform=ios&v=1.4&device=ipad&dim=150X150"];
            
            
            NSLog(@"magazine downloadUrl is %@", downloadUrl);

            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            downloadManagerVersion = [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY];
            
            NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];
            
            if (order == NSOrderedSame || order == NSOrderedDescending) { // OS version Greater than or equal 5
                allowDownloadVersion = TRUE;
                
            }  else { // OS version less than 5
                allowDownloadVersion = FALSE;
                downloadManagerVersion = SEL_DOWNLOAD_MANAGER; // Overriding Download Manager Version for progress view support
            }

            if(![downloadManagerVersion isEqualToString:@"v2"]) { //ASIHTTPRequest download manager

                cancelBtn.hidden = FALSE;
                resumeBtn.hidden = TRUE;
                pauseBtn.hidden = TRUE;
                
                ASIHTTPRequest *request;

                request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:downloadUrl]];
                
                [request setDownloadDestinationPath:destinyPath];
                [request setUserInfo:[NSDictionary dictionaryWithObject:@"request1" forKey:@"name"]];
                [request setIssueID:[NSNumber numberWithInteger:dwnldbtn.IssueID]];
                [request setMagazineID:[NSNumber numberWithInteger:dwnldbtn.MagID]];
                [request setIssueDate:dwnldbtn.issueDate];
                [request setSupplementary:FALSE];
                [request setMagName:dwnldbtn.magazineName];
                [request setDownloadProgressDelegate:imageProgressIndicator];
                
                
                networkQueue = [[ASINetworkQueue alloc] init];
                [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
                [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
                [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
                [networkQueue setShowAccurateProgress:TRUE];
                [networkQueue setDelegate:self];
                
                ///Recent
                [currentDwnldIDS addObject:[NSNumber numberWithInteger:dwnldbtn.IssueID]];
                [networkQueue addOperation:request/*btn.dwnRequest*/];
                [networkQueue go];
                maxDowncount++;
                [self removeObjectsOnTranFail];
                downLoadInProgress = TRUE;
                [self stopDownlLoading];
                [self downldLoading];
            } else {
                [self initiateAFDownloadRequest:afDownloadUrl destinyPath:destinyPath];
            }
                [magazineLibObj release];
        } else {
            
            [self _showAlert:@"Download inprogress, please wait."];
        }
        magazineID = dwnldbtn.MagID;
        issueID    = dwnldbtn.IssueID;    
        // [loginMod release],loginMod = nil;
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self _showAlert:@"Download completed"];
}

- (void) showLogin {
    loginMod = nil;
    ///Login Authentication
    loginMod = [[Login alloc]init];
    if (![loginMod isUserDetailsExists]) {
        loginMod.fromInfo = FALSE;
       
        //[loginMod showLoginViewAndAuthenticateUser:self.view];
        
        LoginView *ct = [[LoginView alloc] init];
        //[self.view addSubview:ct];
        
        [ct.view setBackgroundColor:[UIColor clearColor]];
        
        UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:ct] ;
        ///[popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        CGRect popoverRect = [self.view convertRect:[self.tabBarController frame] 
                                           fromView:[self.tabBarController superview ]];    
        popoverRect.size.width = MIN(popoverRect.size.width, 100);
        CGSize size = CGSizeMake(768, 450); // size of view in popover
        popoverController.popoverContentSize = size;
        [popoverController 
         presentPopoverFromRect:popoverRect 
         inView:self.view 
         permittedArrowDirections:UIPopoverArrowDirectionAny 
         animated:YES];
         
        self.popoverController = popoverController;
    }

    
}
//Buy Now Button Clicked
-(void)byeNowClicked:(id)sender
{
    if(![self checkInternet]) 
        return;
    currentCoverIndex = tempCurrentCoverIndex;
    if(hidView.hidden ) {
        //if(networkQueue !=nil)
        
        if(![self.view.subviews containsObject:spinner]) 
        {
            singleSelected = TRUE;
            //[downloadView setHidden:TRUE];
            DownloadControl* dwnldbtn = (DownloadControl*)sender;
            tmpDwnlBtn = dwnldbtn;
            issueID = dwnldbtn.IssueID;
            magazineName = dwnldbtn.magazineName;
            issueDate    = dwnldbtn.issueDate;    
            if(!downLoadInProgress)
            {
                //if(networkQueue !=nil)
                //    [networkQueue cancelAllOperations];
                loginMod = nil;
                [loginView removeFromSuperview];
                ///Login Authentication
                loginMod = [[Login alloc]init];
                loginMod.downLaoding = TRUE;
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                
                
                ///temp
                [self removeObjectsOnTranFail];
                [currentDwnldIDS addObject:[NSNumber numberWithInteger:dwnldbtn.IssueID]];
                issueDate = dwnldbtn.issueDate;
                magazineName = dwnldbtn.magazineName;
                issueID = dwnldbtn.IssueID;
                downldUrlString = dwnldbtn.downldUrlString;
                magazineID = dwnldbtn.MagID;
                downloadBtntmp = sender;
                      
                
                if (![loginMod isUserDetailsExists]) {
                    loginMod.fromInfo = FALSE;
                    //[loginMod showLoginViewAndAuthenticateUser:self.view];
                    
                    LoginView *ct = [[LoginView alloc] init];
                    [ct.view setBackgroundColor:[UIColor clearColor]];
                    
                    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:ct] ;
                    ///[popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                    popoverController.delegate = self;
                    CGRect popoverRect = [self.view convertRect:[sender frame] 
                                                       fromView:[sender superview]];    
                    popoverRect.size.width = MIN(popoverRect.size.width, 100);
                    CGSize size = CGSizeMake(768, 450); // size of view in popover
                    popoverController.popoverContentSize = size;
                    [popoverController 
                     presentPopoverFromRect:popoverRect 
                     inView:self.view 
                     permittedArrowDirections:UIPopoverArrowDirectionAny 
                     animated:YES];
                    self.popoverController = popoverController;
                 //   [loginMod showLoginViewAndAuthenticateUser:self.view];
                    
                } else {
                    
                    //Recently Added
                    [self removeObjectsOnTranFail];
                     [currentDwnldIDS addObject:[NSNumber numberWithInteger:dwnldbtn.IssueID]];
                    issueDate = dwnldbtn.issueDate;
                    magazineName = dwnldbtn.magazineName;
                    issueID = dwnldbtn.IssueID;
                    downldUrlString = dwnldbtn.downldUrlString;
                                        magazineID = dwnldbtn.MagID;
                    
                    [self showInAppList:sender];                    
                }
                [magazineLibObj release];
            } else {
                
                [self _showAlert:@"Download inprogress, please wait."];
            }
        }
    }
}


- (void) getProductList:(int)magID {
    
    NSMutableArray *ar = [[NSMutableArray alloc] init];
    for (UIView *view in [self.view subviews]) {
        if ([view isKindOfClass:[InappView class]] ) {
            InappView *inapp = (InappView*)view;
            
            if(magID == inapp.CateGoryID && inapp.subid == 0)
            {
                NSLog(@"Matching Category ID: %d" , inapp.CateGoryID);
                for (UIView *subview in [inapp subviews]) {
                    if ([subview isKindOfClass:[UIScrollView class]] ) {
                        
                        for (UIView *subview2 in [subview subviews]) {
                            if ([subview2 isKindOfClass:[CustomButton class]] ) {
                                
                                CustomButton *cust = (CustomButton*)subview2;
                                [ar addObject:cust.productIdentifier]; 
                                //[ar setValue:cust.productIdentifier forKey:cust.productIdentifier];
                               // NSLog(@"Matching product Identifier: %@" , cust.productIdentifier);
                            }
                        }
                    }
                }
            }
        }
    }
    
    [self productRequesting:ar];
    
    magazineID = magID;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    
    [theTextField resignFirstResponder];
    return YES;
}
-(void) serverAuthenticationCompleted:(BOOL) success {
    
    if (success) {
       // NSLog(@"SERVER AUTH Success");
        [loginView removeFromSuperview];
        if ([loginMod isPaidUser]) {
           // NSLog(@"SERVER AUTH paid user");
        } else {
           // NSLog(@"SERVER AUTH not paid user");
            
        }
    } else {
       // NSLog(@"SERVER AUTH Failed");
        
        [self _showAlert:@"Dear user you have to subscribe for downloading the Magazine..."];
    }
}

-(void)showpreview:(id)sender
{
    if(![self checkInternet]) 
        return;
    
    if(!downLoadInProgress)
        
    {
    loginMod = [[Login alloc]init];
    BOOL st = loginMod.isUserDetailsExists;
    
    DownloadControl *dwldbtn = (DownloadControl*) sender;
    [loginView removeFromSuperview];
    Preview_iPad* prevMag = [[Preview_iPad alloc] init];
    
    [prevMag setPrevwDnldURl: [NSString stringWithFormat:@"%@%@%@%@%@%@", 
                               dwldbtn.downldUrlString,
                               @"&email_id=",loginMod.emailID,
                               @"&password=",loginMod.password, @"&platform=ios&device=ipad&dim=150X150"]];
    
    [prevMag setMagID:[NSNumber numberWithInteger:dwldbtn.MagID]];
    [prevMag setIssueID:[NSNumber numberWithInteger:dwldbtn.IssueID]]; 
    [prevMag setMagName:dwldbtn.magazineName];
    [prevMag setIssueDate:dwldbtn.issueDate];
    [prevMag setWrapperUrl:dwldbtn.wrapperUrl];
   // [prevMag setTempImg:[_imageNames objectAtIndex:btn.tag]];
    [self.navigationController pushViewController:prevMag animated:TRUE];
    } else {
        [self _showAlert:@"Download inprogress, please wait."];
    }
    // [prevMag release];
}

- (void)imageDownloadSart:(ASIHTTPRequest *)request
{
    [request.downloadProgressDelegate setHidden:FALSE];
}

/*- (void)unzipContent:(ASIHTTPRequest *)request {
    
    NSString* destiny = [request downloadDestinationPath] ; //[ NSHomeDirectory() stringByAppendingPathComponent:@"Documents/1_246.zip"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    
    NSString *destinyPath = [NSString stringWithFormat:@"%@%@%d%@%d",[paths objectAtIndex:0],
                             @"/",[request.magazineID intValue],
                             @"_",[request.issueID intValue]];
    
    NSString* unZipFolder = destinyPath;// [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/1_246"];
    
    ZipArchive *za = [[ZipArchive alloc] init];
    
    if ([za UnzipOpenFile: destiny]) {
        BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
        if (NO == ret){
            [za UnzipCloseFile];
        } else {
            [za UnzipCloseFile];
            [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
        }
    }
    [za release];
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@','%@'",[request.magazineID integerValue],[request.issueID integerValue],request.issueDate,request.supplementary];
    if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ]) {
        NSLog(@"Magazine Inserted Successfully %@",queryValues);
    }
    [request.downloadProgressDelegate setHidden:TRUE];
    [request updateDownloadProgress];
}*/

- (void) createMagRootFolder {
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *newDir;
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, 
                                                   NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    newDir = [docsDir stringByAppendingPathComponent:KMAGROOTPATH];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:newDir])
    {
       
        if ([filemgr createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error: NULL] == NO)
        {
            NSLog(@"Failed to create directoy");
            // Failed to create directory
        } else {
            NSLog(@"Sucess");
        }
    }
    [filemgr release];
}


-(void) AFNetworkUnZipFile:(NSString *) destinationPath {

    [currentDwnldIDS removeObject:[NSString stringWithFormat:@"%d", issueID]];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self createMagRootFolder];
        NSString* destiny = destinationPath ;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
        NSString *destinyPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],
                                 @"/",KMAGROOTPATH,@"/",magazineID,
                                 @"_",issueID];
        
        NSString* unZipFolder = destinyPath;
        
        ZipArchive *za = [[ZipArchive alloc] init];
        
        if ([za UnzipOpenFile: destiny]) {
            BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
            if (NO == ret){
                [za UnzipCloseFile];
            } else {
                [za UnzipCloseFile];
                [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
            }
        }
        [za release];
        
        dispatch_async( dispatch_get_main_queue(), ^{
    
            [self stopDownlLoading];
            [self removeObjectsOnTranFail];
            downLoadInProgress = FALSE;
            
            if(![downloadManagerVersion isEqualToString:@"v2"]) {
                
                [networkQueue cancelAllOperations];
                [networkQueue release];
            }

            if(tempCurrentCoverIndex != currentCoverIndex)
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];
            else {
                if(currentCoverIndex == [coverflow numberOfCovers]-1)
                {
                    if(currentCoverIndex != 0)
                        [coverflow bringCoverAtIndexToFront:currentCoverIndex-1  animated:TRUE];
                    
                } else
                {
                    [coverflow bringCoverAtIndexToFront:currentCoverIndex+1  animated:TRUE];
                }
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];
            }
            if([self.popoverController isPopoverVisible])
                [self.popoverController dismissPopoverAnimated:TRUE];
            Login *logMod = [[Login alloc] init];
            BOOL st = logMod.isUserDetailsExists;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if([fileManager fileExistsAtPath:pdfDocPath])
            {
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@'",magazineID,issueID,issueDate,0, magazineName]; //request.supplementary
                if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ])
                {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%d%@%d%@%d%@%d%@",documentsDirectory,@"/",magazineID,@"_",issueID,
                                            @"/magazine_",magazineID,@"_",issueID,@".pdf"];
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:pdfDocPath])
                    {
                       
                    }
                }
                if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
                    logMod.username = @"Guest";
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: [NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]
                                      message:[NSString stringWithFormat:@"%@%@%@%@",magazineName,@" - ",issueDate, @" Added to Library successfully, do you want to read now?"]
                                      delegate: self
                                      cancelButtonTitle:@"Not Now"
                                      otherButtonTitles:@"Read Now",nil];
                [alert show];
                [alert release];
            } else
            {
                [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@%d%@%d",documentsDirectory,@"/",magazineID,@"_",issueID] ] error:NULL];
            }
            [logMod release], logMod = nil;
            //[self _showAlert:@"Magazine added to Libary successfully.."];
        });
    });
}

- (void)imageFetchComplete:(ASIHTTPRequest *)request
{
    [currentDwnldIDS removeObject:request.issueID];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self createMagRootFolder];
        NSString* destiny = [request downloadDestinationPath] ; 
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
        
        NSString *destinyPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],
                                 @"/",KMAGROOTPATH,@"/",[request.magazineID intValue],
                                 @"_",[request.issueID intValue]];
        
        NSString* unZipFolder = destinyPath;
        
        ZipArchive *za = [[ZipArchive alloc] init];
        
        if ([za UnzipOpenFile: destiny]) {
            BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
            if (NO == ret){
                [za UnzipCloseFile];
            } else {
                [za UnzipCloseFile];
                [[NSFileManager defaultManager] removeItemAtPath:destiny error:NULL];
            }
        }
        [za release];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [request.downloadProgressDelegate setHidden:TRUE];
            [request updateDownloadProgress];
            [self stopDownlLoading];
            [self removeObjectsOnTranFail];
            // [downloadView setHidden:TRUE];
            downLoadInProgress = FALSE;
            [networkQueue release];
            if(tempCurrentCoverIndex != currentCoverIndex)
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];
            else {
                if(currentCoverIndex == [coverflow numberOfCovers]-1)
                {
                    if(currentCoverIndex != 0)
                        [coverflow bringCoverAtIndexToFront:currentCoverIndex-1  animated:TRUE];
                    
                } else
                {
                    [coverflow bringCoverAtIndexToFront:currentCoverIndex+1  animated:TRUE];
                }
                [coverflow bringCoverAtIndexToFront:currentCoverIndex  animated:TRUE];  
            }
            if([self.popoverController isPopoverVisible])
                [self.popoverController dismissPopoverAnimated:TRUE];
            Login *logMod = [[Login alloc] init];
            BOOL st = logMod.isUserDetailsExists;
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if([fileManager fileExistsAtPath:pdfDocPath]) 
            {
                MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
                NSString *queryValues = [NSString stringWithFormat:@"%d,%d,'%@',%d,'%@'",[request.magazineID integerValue],[request.issueID integerValue],request.issueDate,request.supplementary, request.magName];
                if ([magazineLibObj insertMagazineFields:@"magazine_id, issue_id, issue_date, supplementary,lib_magazine_name" values:queryValues ]) 
                {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%d%@%d%@%d%@%d%@",documentsDirectory,@"/",magazineID,@"_",issueID,
                                            @"/magazine_",magazineID,@"_",issueID,@".pdf"];
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:pdfDocPath]) 
                    {
                       
                    }
                }
                if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
                    logMod.username = @"Guest";
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: [NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]
                                      message:[NSString stringWithFormat:@"%@%@%@%@",magazineName,@" - ",issueDate, @" Added to Library successfully, do you want to read now?"]
                                      delegate: self
                                      cancelButtonTitle:@"Not Now"
                                      otherButtonTitles:@"Read Now",nil];
                [alert show];
                [alert release];
            } else 
            {
                [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@%d%@%d",documentsDirectory,@"/",magazineID,@"_",issueID] ] error:NULL];
            }
            [logMod release], logMod = nil;
            //[self _showAlert:@"Magazine added to Libary successfully.."];
        });
    });
    // [issueDwnlTime release], issueDwnlTime = nil;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    if (buttonIndex != 0)
    {
        if( downLoadInProgress) {
            [self cancelProgressingDownload];
        } else {
        
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            /*NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                                    documentsDirectory,@"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/magazine_",magazineID,@"_",issueID,@".pdf"];*/
            
            NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",
                                    documentsDirectory,
                                    @"/",KMAGROOTPATH,@"/",magazineID,@"_",issueID,
                                    @"/wrapper.jpg"];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if([fileManager fileExistsAtPath:pdfDocPath]) 
            {

                TestPDFReader * prevMag = [[TestPDFReader alloc] init];
                [prevMag setWrapperUrl:pdfDocPath];
                [self.navigationController pushViewController:prevMag animated:TRUE];
            }
        }
    }
}
- (void)imageFetchFailed:(ASIHTTPRequest *)request
{
    [currentDwnldIDS removeObject:request.issueID];   
    [self removeObjectsOnTranFail];
    [self stopDownlLoading];
	//if (!failed) {
    if ([[request error] domain] != NetworkRequestErrorDomain || [[request error] code] != ASIRequestCancelledErrorType) {
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Download failed" message:KMAGDWNLDFAILED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [alertView show];
    }
    failed = YES;
    //[request release];
    [networkQueue release];
    maxDowncount -=1;
    downLoadInProgress = FALSE;
    networkQueue = nil;
    [self resetNetwork];
    //[networkQueue cancelAllOperations];
    if([self.popoverController isPopoverVisible])
        [self.popoverController dismissPopoverAnimated:TRUE];
}

- (void)getBooksMagaList {
    
    Login *logMod = [[Login alloc]init];
    [logMod setDelegate:self];

    responseData = [[NSMutableData data] retain];
    
    NSMutableURLRequest *request;
    NSString *oNLoadReqUrl;
    NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];

	//Send http request
    if(magArchieve !=TRUE) {
       oNLoadReqUrl = [NSString stringWithFormat:@"%@%@",KMAGSTORE, [getUrlAndParams objectForKey:@"devDet"]];
        
    }
    else {
        oNLoadReqUrl = [NSString stringWithFormat:@"%@%@%d%@%@%d%@%@%d%@",kMAGARCH,@"mid=",0,@"&",@"year=",selYear,@"&",@"month=",selMonth,
                        [getUrlAndParams objectForKey:@"devDet"]];
        
    }

   // NSLog(@"oNLoadReqUrl is %@", oNLoadReqUrl);
    
    NSURL *aUrl = [NSURL URLWithString:oNLoadReqUrl];
    request = [NSMutableURLRequest requestWithURL:aUrl
                                      cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[getUrlAndParams objectForKey:@"userDet"] dataUsingEncoding:NSUTF8StringEncoding]];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
   
    
    btn.enabled=FALSE;
	[logMod release], logMod = nil;
}

//HTTP Request Handler to get Book/Mag list from Store

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    btn.enabled=TRUE;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //[self _showAlert:KINETUNAVAL];
    [self StopLoading];
    onLoad = FALSE;
}

- (void) _showAlert:(NSString*)title
{
    Login *logMod = [[Login alloc]init];
    BOOL st = logMod.isUserDetailsExists;
    
    if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
        logMod.username = @"Guest";
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]  message:title  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
    [logMod release],logMod = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];

    [self StopLoading];
    
    if(responseString !=@"" || responseString !=NULL){
        
        [self buildStorePage:( NSString *) responseString]; 
    } else {
        
        [self _showAlert:KFIREWALLBLOCK];
    }
    btn.enabled=TRUE;
    magArchieve = FALSE;
    onLoad = FALSE;
}

- (void)buildStorePage:(NSString *)responseString {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	

    int singleIssue = 0, highLightIss;
    [magzineList release];[magazineIDS release];[magProdIdnt release];
    [singleIssueState release];[singleIssuePrice release];[sub_promoList release];
    magzineList = [[NSMutableArray alloc] init];
    magazineIDS = [[NSMutableArray alloc] init];
    magProdIdnt = [[NSMutableArray alloc] init];
    singleIssueState = [[NSMutableArray alloc] init];
    singleIssuePrice = [[NSMutableArray alloc] init];
    sub_promoList    = [[NSMutableArray alloc] init];
    showSub          = [[NSMutableArray alloc] init];
    singleIssDic     = [[NSMutableDictionary alloc]init];
    subIssDic        = [[NSMutableDictionary alloc]init];
    ///DownloadControl *downloadBtn;
    int ndx, imageWidth;
	NSString *titleString;
	NSInteger categoryid;
    
    // UIScrollView *inappContainer;
    SBJSON *jsonParser = [[SBJSON new] autorelease];
   	id response = [jsonParser objectWithString:responseString];
    NSMutableDictionary *newsfeed = (NSMutableDictionary *)response;
    NSArray *categories = (NSArray *)[newsfeed valueForKey:KSTOREMAGS];
    if(categories == NULL ) {
        [self _showAlert:KFIREWALLBLOCK];
        return;
    }

    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:[newsfeed valueForKey:DOWNLOAD_VERSION_KEY] forKey:DOWNLOAD_MANAGER_KEY];
    
    if([standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY] == NULL )
    {
        [standardUserDefaults setValue:SEL_DOWNLOAD_MANAGER forKey:DOWNLOAD_MANAGER_KEY];
    }
    
    NSLog(@"SEL_DOWNLOAD_MANAGER in magazine store json is %@", [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY]);
    
    issValidFrm  = [newsfeed valueForKey:KMAGISSUEVALID];
    highLightIss = [[newsfeed valueForKey:KMAGISSUEHGHLGHT] intValue];
    //  NSString *subString1 = [NSString stringWithFormat:@"%@",[newsfeed valueForKey:KSTOREMAGS]];
	NSDictionary *category;
    
    [coverImgList release], coverImgList = nil;
    coverImgList = [[NSMutableArray alloc] init];
    
    [articleImgUrl release], articleImgUrl = nil;
    articleImgUrl = [[NSMutableDictionary alloc] init];
    
	// loop over all the stream objects and print their titles
	for (ndx = 0; ndx < categories.count ; ndx++) {
        category    = (NSDictionary *)[categories objectAtIndex:ndx];

        NSArray *articles = (NSArray *)[category valueForKey:KMAGISSUES];
        imageWidth = 20 ;
        if(articles !=NULL || articles !=Nil )
        {
            titleString = urlStringDecode([category valueForKey:KMAGINENAME]);
            categoryid  = (NSInteger)[category valueForKey:KMAGINEID];
            [magazineIDS addObject:[category valueForKey:KMAGINEID]];
            [magzineList addObject:titleString];
            [magProdIdnt addObject:@"product identifier"];
            [sub_promoList addObject:[category valueForKey:KMAGMAGSUBPROMO]];
            [articleImgUrl setObject:articles forKey:[NSString stringWithFormat:@"%d",ndx]];
            //Loop through individual articles in each category
            for (int y = 0 ; y < articles.count; y++)
            {			
                NSDictionary *article = (NSDictionary *)[articles objectAtIndex:y];
                NSMutableString *urlString = [NSMutableString stringWithString:@"http://www.vikatan.com/"];
                [urlString appendString: [article valueForKey:KMAGISSWRAPURL]];
                if(y == 0)
                    [coverImgList addObject:(NSString*)urlString];
            }
            
           
            singleIssue = 1;
            [singleIssueState addObject:[NSNumber numberWithInteger:singleIssue]];
            ///END of single issues
            NSMutableArray *subs;
            subs = (NSMutableArray *)[category valueForKey:KMAGSUBSISS];
            NSString *subString = [NSString stringWithFormat:@"%@",[category valueForKey:KMAGSUBSISS]];
            if( [subString length] > 0)
            {
                NSMutableArray *arList = [[NSMutableArray alloc]init];
                for (int y = 0 ; y < subs.count; y++)
                {			
                    NSDictionary *sub = (NSDictionary *)[subs objectAtIndex:y];
                    CustomButton *but = [CustomButton buttonWithType:UIButtonTypeCustom];
                    [but setProductIdentifier:[sub valueForKey:KMAGISSPRODINDENTI]];
                    [but setSubid:[[sub valueForKey:@"subid"] intValue]];
                    [but setSubprice:[sub valueForKey:@"subprice"]];
                    [but setSubsname:[sub valueForKey:@"subsname"]];
                    [but setCateGoryID:[[category valueForKey:KMAGINEID] intValue]];
                    [but setBackgroundColor:[UIColor clearColor]];
                    [arList addObject:but];
                    [showSub addObject:[sub valueForKey:KSHOWSUBSTRIP]];
                }
                [subIssDic setObject:arList forKey:[category valueForKey:KMAGINEID]];
            } 
            //END of scubscription
        } 
    }
    if([coverImgList count] > 0 )
        [self setImageToCovers:highLightIss]; 
    else
        [self _showAlert:KMAGNOTAVLB];
    jsonParser = nil;
    [jsonParser release];
    newsfeed = nil;
    categories = nil;
    category = nil;
    responseString = nil;
    jsonParser = nil;
    [pool release];
    
}

-(void) setImageToCovers:(int)highLightIss {
    
    
    for(int i =0 ;i<coverImgList.count;i++) {
        if ([coverImgList objectAtIndex:i]) {
            NSURL * imageURL = [NSURL URLWithString:[coverImgList objectAtIndex:i]];
            // NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage * image = [[UIImage alloc]init];
            [covers addObject:image];
        }
    }
    
    [self StopLoading];
    [coverflow setNumberOfCovers:coverImgList.count ];
    
    //int frontCover = (coverImgList.count == 1) ? 0:coverImgList.count/2 ;
    if(highLightIss< [coverImgList count])
        [coverflow bringCoverAtIndexToFront:highLightIss  animated:TRUE]; 
    
}

- (void) LoadImageData:(NSString *)url {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    // NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    // UIImage * image = [UIImage imageWithData:imageData];
    //AsyncImageView *image = 
    AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                  initWithFrame:CGRectMake(0,0,140,185)] ;
    asyncImage.moduleIndi = 2;
    asyncImage.imageURL = url;
    [asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
    
    [covers addObject:asyncImage];
    [pool release];
}



- (void) showArchievesPage:(id)sender 
{
    if(!downLoadInProgress)
    {
        [self hideInappView];
        [self disableAction];
        hidView.hidden = TRUE;
        //[downloadView setHidden:TRUE];
        // VikatanAppDelegate_iPad *appDelegate  = [[UIApplication sharedApplication] delegate];
        //  appDelegate.issValidFrom = issValidFrm;
        // magArchieve = TRUE;
        Calendar_iPad *calView = [[Calendar_iPad alloc]initWithMag:self:issValidFrm];
        UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:calView] ;
        CGRect popoverRect = [self.view convertRect:[sender frame] 
                                           fromView:[sender superview]];    
        popoverRect.size.width = MIN(popoverRect.size.width, 100);
        popoverController.popoverContentSize = CGSizeMake(300, 210);
        [popoverController 
         presentPopoverFromRect:popoverRect
         inView:self.view 
         permittedArrowDirections:UIPopoverArrowDirectionAny 
         animated:YES];
        popController = popoverController;
        
    } else {
        [self _showAlert:@"Download inprogress, please wait."];
    }
}

- (void) callBackArchive {
    
    if(![self checkInternet]) 
        return;
    [self LoadingIcon];
    covers = [NSMutableArray new];
    [self magazineOnLoad];
}
- (void) callInapp {
    
    if(![self checkInternet]) 
        return;
    [self showInAppList:downloadBtntmp];
   //[self _showAlert:@"Download inprogress, please wait."];
}

- (void) restorealltran {
    if(![self checkInternet]) 
        return;
    restoreallpurc = TRUE;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
   [[SKPaymentQueue defaultQueue]  restoreCompletedTransactions];
}
- (void) closePops {
    [self.popoverController dismissPopoverAnimated:FALSE];
}
//Gridview implementation

#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    // return 5;
    return  [_imageNames count] ;
}
- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    if([self isLandscape])
        return ( CGSizeMake(213, 275.0) );
    else
        return ( CGSizeMake(164, 275.0) );
    
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    
    NSArray *ar = [articleImgUrl objectForKey:[NSString stringWithFormat:@"%d",tempCurrentCoverIndex]];
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init] ;
    NSDictionary *article = (NSDictionary *)[ar objectAtIndex:index];
    
    static NSString * PlainCellIdentifier = @"PlainCellIdentifier";
    CGRect fram1 = CGRectMake(0 , 0, 150, 212);
    AQGridViewCell * cell = nil;
    ImageDemoGridViewCell * plainCell = (ImageDemoGridViewCell *)[aGridView dequeueReusableCellWithIdentifier: PlainCellIdentifier];
    plainCell = [[[ImageDemoGridViewCell alloc] initWithFrame: CGRectMake(0 , 0, 150, 260)
                                              reuseIdentifier: PlainCellIdentifier] autorelease];
    [plainCell setBackgroundColor:[UIColor clearColor]];
    cell = plainCell;
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(125, -15 , 32, 32)];
    closeBtn.hidden = TRUE;
    [closeBtn setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [cell addSubview:closeBtn];
    
    
    NSMutableString *urlString = [_imageNames objectAtIndex:index];
    AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                  initWithFrame:fram1] ;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:asyncImage.bounds];
    [asyncImage.layer setMasksToBounds:YES];
    [asyncImage.layer setCornerRadius:1.0f];
    [asyncImage.layer setBorderWidth:1.0f];
    asyncImage.layer.shadowPath = path.CGPath;
    asyncImage.moduleIndi = 4;
    asyncImage.imageURL = urlString;
    [asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
    
    DownloadControl *magCoverBtn = [DownloadControl buttonWithType:UIButtonTypeCustom];

    [magCoverBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]];
    
    [magCoverBtn setMagID:[[magazineIDS objectAtIndex:index] intValue]];
    
    [magCoverBtn setMagazineName:[magzineList objectAtIndex:index]];
    
    [magCoverBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
    
    [magCoverBtn setFrame:fram1];
    
    [magCoverBtn setBackgroundColor:[UIColor darkGrayColor]];
    [magCoverBtn.layer setMasksToBounds:YES];
    [magCoverBtn.layer setCornerRadius:5.0f];
    [magCoverBtn.layer setBorderWidth:1.0f];
    magCoverBtn.backgroundColor = [UIColor lightGrayColor];
    magCoverBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    magCoverBtn.layer.shadowOpacity = 0.6f;
    magCoverBtn.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    magCoverBtn.layer.shadowRadius = 4.0f;
    magCoverBtn.layer.masksToBounds = NO;
    [magCoverBtn addSubview:asyncImage];
    [plainCell addSubview:magCoverBtn];
    
    UILabel *siteLabel_ = [[UILabel alloc] initWithFrame:CGRectZero];         
    siteLabel_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7f];
    siteLabel_.textColor = [UIColor colorWithWhite:0.8f alpha:1.f];
    siteLabel_.shadowColor = [UIColor blackColor];
    siteLabel_.shadowOffset = CGSizeMake(1.f, 1.f);
    siteLabel_.textAlignment = UITextAlignmentCenter;
    siteLabel_.font = [UIFont boldSystemFontOfSize:14];
    siteLabel_.text = [article valueForKey:KMAGISSDATE];
    [plainCell addSubview:siteLabel_];
    siteLabel_.frame = CGRectMake(magCoverBtn.frame.origin.x, magCoverBtn.frame.origin.y+magCoverBtn.frame.size.height-ksiteLabelHeight, magCoverBtn.frame.size.width, ksiteLabelHeight);
    
    DownloadControl *downloadBtn;
    imageProgressIndicator1 = [[[UIProgressView alloc] initWithFrame:CGRectMake(0 ,155 ,100,20)] autorelease];
    //imageProgressIndicator1 = [[[UIProgressView alloc] initWithFrame:CGRectMake(10 ,h ,300,20)] autorelease];
    imageProgressIndicator1.backgroundColor = [UIColor clearColor];
    imageProgressIndicator1.tag =[ [article valueForKey:KMAGISSID] intValue];
    [imageProgressIndicator1 setHidden:TRUE];
    [cell addSubview:imageProgressIndicator1];
    //[dwnldListView addSubview:imageProgressIndicator1];
    
    loginMod = [[Login alloc]init];
    BOOL st = loginMod.isUserDetailsExists;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    NSString *destinyPath =  [NSString stringWithFormat:@"%@%@%d%@%d%@",[paths objectAtIndex:0],
                              @"/",[[articleImgUrl valueForKey:KMAGINEID] intValue],
                              @"_",[[article valueForKey:KMAGISSID] intValue],
                              @".zip"];
    NSMutableString* issuID = [NSMutableString stringWithFormat:@"%@", [article valueForKey:KMAGISSID]];
 
    downloadBtn = [DownloadControl buttonWithType:UIButtonTypeCustom];
    [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
    [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
    [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
    [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
    [downloadBtn setDownldUrlString:[article valueForKey:KMAGDWNLDURL]];
    [downloadBtn setIndex:index];
    //[downloadBtn setDwnRequest:request];
    downloadBtn.backgroundColor = [UIColor clearColor];
    downloadBtn.frame = CGRectMake(25  ,222 , 100, 30) ;
    downloadBtn.tag = [issuID intValue];
    
    NSString *dwnlURl = [article valueForKey:KMAGDWNLDURL];
    NSString *dwndable = [article valueForKey:KMAGDOWNDABLE];
    
    NSArray *fields = [[NSArray alloc] initWithObjects:@"issue_id", nil];
    NSString *cond = [NSString stringWithFormat:@"issue_id=%d",[issuID intValue]];
    NSMutableArray *mutableArr = [[NSMutableArray alloc] initWithArray:[magazineLibObj
                                                                        selectMagazineField:fields when:cond]];
    if([mutableArr count] > 0)
    {
        [downloadBtn setImage:[UIImage imageNamed: @"read_now.png"] forState:UIControlStateNormal];
        [downloadBtn addTarget:self action:@selector(readNowClicked:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(22.5,0,80,27)];
        [lblTitle setText:@"Read Now"];
        [lblTitle setBackgroundColor:[UIColor clearColor]];
        [lblTitle setTextColor:[UIColor blackColor]];
        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
        [downloadBtn addSubview:lblTitle];
        [plainCell addSubview:downloadBtn];
    } 
    else 
    {

        // if([singleIssueState count] < index)
        //{
      //  if([[singleIssueState objectAtIndex:index] intValue] == 1) 
    //    {
            if([dwndable isEqualToString:@"N"])
            {
                if([dwnlURl length] >0 ){
                    [downloadBtn setImage:[UIImage imageNamed: @"buynow.png"] forState:UIControlStateNormal];
                    [downloadBtn addTarget:self action:@selector(byeNowClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
                    [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
                    [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
                    [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
                    [downloadBtn setDownldUrlString:[article valueForKey:KMAGDWNLDURL]];
                    [downloadBtn setIssuName:[article valueForKey:KSINGLEISSNAM]];
                    [downloadBtn setIssuePrice:[article valueForKey:KSINGLEISSPRICE]];
                    [downloadBtn setProductIdentifier:[article valueForKey:KSINGLEISSPRDID]];
                    
                    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(30,0,100,27)];
                    [lblTitle setText:@"Buy Now"];
                    /*
                    
                    if(st){
                         lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(30,0,100,27)];
                        [lblTitle setText:@"Buy Now"];
                
                    }
                    else{
                        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(25,0,100,27)];
                        [lblTitle setText:@"Login Now"];
                        
                    }
                     */
                    
                    [lblTitle setBackgroundColor:[UIColor clearColor]];
                    [lblTitle setTextColor:[UIColor whiteColor]];
                    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
                    [downloadBtn addSubview:lblTitle];
                    downloadBtn.frame = CGRectMake(20  ,222 , 110, 30) ;
                }
            } 
            else 
            {
                if([dwnlURl length] >0 ){
                    [downloadBtn setImage:[UIImage imageNamed: @"add_2lib.png"] forState:UIControlStateNormal];
                    [downloadBtn addTarget:self action:@selector(downloadClicked:) forControlEvents:UIControlEventTouchUpInside];
                    downloadBtn.frame = CGRectMake( 20 ,220 , 115, 35) ;
                    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15,2.5,100,27)];
                    [lblTitle setText:@"Add to Library"];
                    [lblTitle setBackgroundColor:[UIColor clearColor]];
                    [lblTitle setTextColor:[UIColor whiteColor]];
                    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
                    [downloadBtn addSubview:lblTitle];
                }
            }
      //  }
        
        [plainCell addSubview:downloadBtn];

        downloadBtn = [DownloadControl buttonWithType:UIButtonTypeCustom];
        [downloadBtn setDownldUrlString:[article valueForKey:KMAGPREVDWNLDURL]];
        downloadBtn.wrapperUrl = [NSString stringWithFormat:@"http://www.vikatan.com/%@", [article valueForKey:KMAGISSWRAPURL]];
        downloadBtn.backgroundColor = [UIColor clearColor];
        downloadBtn.frame = CGRectMake( 94,130 , 67, 39) ;
        downloadBtn.tag = index;//[issuID intValue];
        [downloadBtn setIssueID:[[article valueForKey:KMAGISSID] intValue]]; 
        [downloadBtn setMagID:[[magazineIDS objectAtIndex:tempCurrentCoverIndex] intValue]];
        [downloadBtn setMagazineName:[magzineList objectAtIndex:tempCurrentCoverIndex]];
        [downloadBtn setIssueDate:[article valueForKey:KMAGISSDATE]];
        [downloadBtn setImage:[UIImage imageNamed: @"preview.png"] forState:UIControlStateNormal];
        [downloadBtn addTarget:self action:@selector(showpreview:) forControlEvents:UIControlEventTouchUpInside];
        [plainCell addSubview:downloadBtn];
    }
    if([dwnlURl isEqualToString:@""]) {
        downloadBtn.enabled = FALSE;
    }
    [magazineLibObj release], magazineLibObj = nil;
    cell.backgroundColor = [UIColor clearColor];
    return ( cell );
}
- (void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)gridIndex {
    
}
//Customize Grid View
- (void)initializeGridView {
    if (!self.gridView) {
        // init thumbs view
        self.gridView = [[[AQGridView alloc] initWithFrame:CGRectZero] autorelease];
        self.gridView.backgroundColor = [UIColor clearColor];
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;  
        self.gridView.scrollEnabled = TRUE;
        
        self.gridView.layoutDirection = AQGridViewLayoutDirectionVertical;
        
        // if we have a translucent bar, make some room
        if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent) {
            self.gridView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
        }else {
            self.gridView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }

        self.gridView.hidden = NO;
        self.gridView.scrollsToTop = YES;
        [self.view addSubview:self.gridView];
        CGRect frame = CGRectMake(0, 370, self.view.bounds.size.width, self.view.bounds.size.height - 390);   
        self.gridView.frame = frame;
        
        // add a footer view for tabbar
        if (!PSIsIpad()) {
            self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 50)] autorelease];
        }
    }
    
}
- (BOOL) connectedToNetwork
{
    
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.vikatan.com/"]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
} 

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
		[self _showAlert:@"Check your connection, network unavailable. Error Code - 100"];
		return NO;
	}
	else {
		return YES;
	}
}
-(void)webViewDidStartLoad:(UIWebView *) portal {
	
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {    
	[webSpinner stopAnimating];
    [webSpinner removeFromSuperview];
  
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [webSpinner stopAnimating];
    [webSpinner removeFromSuperview];
}

-(void) initiateAFDownloadRequest:(NSString *) downloadUrl destinyPath:(NSString *) destinyPath {

    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:downloadUrl]];
    
    Login *logMod = [[Login alloc]init];
    NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];
    logMod=nil, [logMod release];
    
   // NSLog(@"initiateAFDownloadRequest download url in ipad is %@", downloadUrl);
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:downloadUrl]];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [getUrlAndParams objectForKey:REQUEST_EMAIL_ID], REQUEST_EMAIL_ID,
                            [getUrlAndParams objectForKey:REQUEST_PASSWORD], REQUEST_PASSWORD,
                            [getUrlAndParams objectForKey:RESPONSE_USER_NAME], RESPONSE_USER_NAME,
                            [getUrlAndParams objectForKey:RESPONSE_SOCIAL_FLAG], RESPONSE_SOCIAL_FLAG,
                            nil];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:downloadUrl
                                                      parameters:params];
    
    // AFHTTPRequestOperation download functions

    if (allowDownloadVersion) { // OS version Greater than or equal 5
        allowDownloadVersion = TRUE;
        resumeBtn.hidden = TRUE;
        pauseBtn.hidden = FALSE;
        cancelBtn.hidden = FALSE;
        
    }  else { // OS version less than 5
        allowDownloadVersion = FALSE;
        pauseBtn.hidden = TRUE; //Resume not supported for below ios 5.0 due to non-avail of progress bar.
        resumeBtn.hidden = TRUE;
        cancelBtn.hidden = FALSE;
    }
    
    downloadOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    downloadOperation.outputStream = [NSOutputStream outputStreamToFileAtPath:destinyPath append:NO];
    
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [downloadOperation start];
   
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setPauseMode) name:@"setPauseMode" object: nil];
    
    maxDowncount++;
    [self removeObjectsOnTranFail];
    downLoadInProgress = TRUE;
    [self stopDownlLoading];
    [self downldLoading];
    imageProgressIndicator.hidden=FALSE;
    
    [downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(totalBytesExpectedToReadFromUrl == -1 ) {
            NSLog(@"downloaded failure success block");
            downloadOperation=nil, [downloadOperation release];
        } else {
            NSLog(@"downloaded success");
            
            [self AFNetworkUnZipFile:destinyPath];
            
            // [self unzipDownloadedFile];
            downloadOperation=nil, [downloadOperation release];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *errors) {

        if(totalBytesReadFromUrl > totalBytesExpectedToReadFromUrl) {
             NSLog(@"downloaded success in failure block totalBytesReadFromUrl > totalBytesExpectedToReadFromUrl");
            
            
            [self AFNetworkUnZipFile:destinyPath];
            downloadOperation=nil, [downloadOperation release];
        } else {
        
            [currentDwnldIDS removeObject:[NSString stringWithFormat:@"%d", issueID]];
            [self removeObjectsOnTranFail];
            [self stopDownlLoading];
            
            UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Download failed" message:KMAGDWNLDFAILED delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            [alertView show];

            failed = YES;
           
            maxDowncount -=1;
            downLoadInProgress = FALSE;
            networkQueue = nil;
            [self resetNetwork];
            
            if([self.popoverController isPopoverVisible])
                [self.popoverController dismissPopoverAnimated:TRUE];
            
            
            downloadOperation=nil, [downloadOperation release]; [alertView release];
        }
     
    }];
    
    if (allowDownloadVersion) {
        
        
        
        
        [downloadOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            
            float percentDone = totalBytesRead/(float)totalBytesExpectedToRead;
           
            NSDate *today = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            NSString *currentTime = [dateFormatter stringFromDate:today];
            [dateFormatter release];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            
            NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
            [timeFormat setDateFormat:@"HH:mm:ss"];
            
            NSDate *now = [[NSDate alloc] init];
            
            NSString *theDate = [dateFormat stringFromDate:now];
            NSString *theTime = [timeFormat stringFromDate:now];

           /* NSLog(@"======================================== ");
            NSLog(@"\n""theDate: |%@| \n" "theTime: |%@| \n", theDate, theTime);
            NSLog(@"download percentDone %f",percentDone);
            NSLog(@"totalBytesRead is %lld",totalBytesRead);
            NSLog(@"======================================== ");*/
            
            [imageProgressIndicator setProgress:percentDone animated:YES];
                  
            totalBytesExpectedToReadFromUrl = totalBytesExpectedToRead;
            totalBytesReadFromUrl = totalBytesRead;
            
            [dateFormat release];
            [timeFormat release];
            [now release];
        }];
    } else {
        imageProgressIndicator.hidden = TRUE;
       
    }
}

//AFNetworking functions

-(IBAction) pausedownload: (id) sender {
    
    [self setPauseMode];
}

-(void) setPauseMode  {
    
    [downloadOperation pause];
    
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden=FALSE;
}

-(IBAction) resumedownload: (id) sender {
    
    if([self checkInternet] == YES) {
        pauseBtn.hidden = FALSE;
        resumeBtn.hidden=TRUE;
        [downloadOperation resume];
    }
}

-(IBAction) canceldownload: (id) sender {
      
    //[self cancelProgressingDownload];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                    message:@"Do you want to cancel Download?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok",nil];
    [alert show];
    [alert release];
}

-(void) cancelProgressingDownload {

    NSLog(@"cancelProgressingDownload is calling");
    
    if(![downloadManagerVersion isEqualToString:@"v2"]) {
        
        [networkQueue cancelAllOperations];
    } else {
        
        [downloadOperation cancel];
        downloadOperation=nil, [downloadOperation release];
    }
    
    downLoadInProgress = FALSE;
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    imageProgressIndicator.hidden = TRUE;
    
    [self removeObjectsOnTranFail];
    [self stopDownlLoading];
}


@end
