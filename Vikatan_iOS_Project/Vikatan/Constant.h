//
//  Constant.h
//  Vikatan
//
//  Created by MobileVeda on 18/05/11.
//  Copyright 2011 MobileVeda. All rights reserved. 
//
//Old One http://www.mobileveda.com/r_d/mcps/news.json

//Platform Extra Parameters
#define KIOSPLATFORM            @"&platform=ios"
#define KAPPVERSIONNO           @"1.4"

#define KIPADNEWSURL            @"http://api.vikatan.com/?module=news&v=1.4&dim=150X150&url_encode=off"//@"http://admin.vikatan.com/mobile/news_test.php" //http://admin.vikatan.com/mobile/?module=news&dim=125X125
#define KCMNTURL                @"http://api.vikatan.com/?module=news&v=1.4&view=comment&url_encode=on&aid="
#define KNEWSMOREURL            @"http://api.vikatan.com/?module=news&v=1.4&view=artthumb&cid="
#define KCATEGORY               @"clist"
#define KCATEGORYTITLE          @"ctitle"
#define KCATEGORYID             @"cid"
#define KARTICLE                @"alist"
#define KARTICLEID              @"aid"
#define KARTICLETITLE           @"atitle"
#define KARTICLEDATE            @"adate"
#define KARTICLEURL             @"aurl"
#define KARTICLEWEBURL          @"weburl"
#define KARTICLEIMG             @"aimg"
#define KARTICLECOUNT           @"acount"
#define KIPADARTICLEDESC        @"adesc"
#define KARTCMNTCOUNT           @"cmtcount"
#define KTHUMBROWCOUNT          6
#define KIPADBPATH              @"http://api.vikatan.com/" //@"http://new.vikatan.com/"
#define KIPADBIMGPATH           @"http://www.vikatan.com/"
#define KIPADPOSTCMNTSURL       @"http://api.vikatan.com/?module=news&v=1.4&type=cmt&resolution=768X1024&device=ipad&nid=" 
#define KIPADTHUMBIMAGESIZE     200
#define KIPADWIDTH              768
#define KIPADHEIGHT             1024
#define KIPADTHUMBIMGWIDTH      150
#define KIPADTHUMBIMGHEIGHT     150
#define KIPADCATEBROWSELOC      940
#define KIPADCATBRWBTNHGHT      40
#define KIPADROWHEIGHT          200

#define KIPADROWWIDTH           732
#define KIPADDIM                @"&dim=768X1024&device=ipad&resolution=768X1024"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//IPHONE CONSTANTS
#define KIPHNNEWSURL            @"http://api.vikatan.com/?module=news&v=1.4&dim=125X125&url_encode=off"
#define KIPHNTHUMBIMAGESIZE     100
#define KIPHNWIDTH              320
#define KIPHNHEIGHT             480
#define KIPHNTHUMBIMGWIDTH      100
#define KIPHNTHUMBIMGHEIGHT     100
#define KIPHNCATEBROWSELOC      400
#define KIPHNCATBRWBTNHGHT      40
#define KIPHNROWHEIGHT          150
#define KIPHNROWWIDTH           320
#define KIPHNDIM                @"&dim=320X480&device=iphone"
#define KIPHNPOSTCMNTSURL       @"http://api.vikatan.com/?module=news&v=1.4&type=cmt&resolution=320X480&device=iphone&nid="
//Messages 

#define KINETUNAVAL             @"Internet Not available. Please check your network."
#define KSERVERDOWN             @"Sorry for the Inconvenience, our servers are unavailable right now"
#define KCMNTSOFFSET            10

//MAGAZINE CONSTANTS Remove before going live
#define KMAGSTORE               @"http://api.vikatan.com/?module=mag&dim=150X150&platform=ios&v=1.4"

//#define KMAGSTORE               @"http://api.vikatan.com/?module=mag&dim=150X150&platform=ios"
#define kMAGARCH                @"http://api.vikatan.com/?module=mag&v=1.4&view=archive&resolution=768X1024&device=ipad&"
#define KNEWUSERURL             @"http://www.vikatan.com/online/?platform=ios"
#define KSTOREMAGS              @"magazines"
#define KMAGISSUES              @"issues"
#define KMAGINEID               @"magazine_id" 
#define KMAGINENAME             @"mag"
#define KMAGISSID               @"iss_id"
#define KMAGISSDATE             @"iss_date"
#define KMAGISSWRAPURL          @"wrapper_url"
#define KMAGISSDESCR            @"description"
#define KMAGDWNLDURL            @"download_url"
#define KMAGPREVDWNLDURL        @"preview_url"
#define KMAGDOWNDABLE           @"downloadable"
#define KMAGSUBSISS             @"subs"
#define KMAGSINGLEISSUE         @"singleissue"
#define KMAGMAGSUBPROMO         @"sub_promo"
#define KMAGSTOREISSUEPERROW    5

#define KCLNEWSMODL             1
#define KCLMAGMODL              2
#define KCLMAGMODLIPAD          4
#define KCLBOOKMODL             3

#define KMAGCMNTURL             @"http://api.vikatan.com/?module=mag&v=1.4&view=comment&url_encode=on&aid="
#define KMAGIPADPOSTCMNTSURL    @"http://api.vikatan.com/?module=mag&v=1.4&type=cmt&resolution=768X1024&device=ipad&aid="
//#define KMAGIPADPOSTCMNTSURL    @"http://api.vikatan.com/?module=mag&type=cmt&resolution=768X1024&device=ipad&nid="

//SAMPLE DOWNLOAD LINK FOR MAGAZINE
#define KSAMPLEDOWNLOAD         @"http://api.vikatan.com/magazine/1_261.zip"

#define KLIBMAXMAGCOUNT         150

#define KMAGTOCJSONNAME         @"/menu.json"
#define KMAGSUBID               @"subid" 
#define KMAGSUBNAME             @"subsname"
#define KMAGSUBPRICE            @"subprice"
#define KMAGISSPRODINDENTI      @"ios_uid"

//Login Module Constants
#define SERVER_AUTH_URL         @"http://api.vikatan.com/?module=login"

#define SOCIAL_LOGIN_FORM       @"http://api.vikatan.com/app_slogin.php"
#define SOCIAL_LOGIN_URL        @"http://api.vikatan.com/app_slogin.php?login_type=social"

#define FEEDBACK_URL            @"http://api.vikatan.com/?module=info&view=feedback&platform=ios&v=1.4"

#define REQUEST_USER_ID         @"uid"
#define REQUEST_PASSWORD        @"password"
#define REQUEST_EMAIL_ID        @"email_id"
#define REQUEST_FIRST_NAME      @"first_name"
#define REQUEST_SOCIAL_FLAG     @"social"
#define REQUEST_LOGIN_TYPE      @"login_type"

#define RESPONSE_USER_TYPE      @"user_type"
#define RESPONSE_SUB_END        @"sub_end"
#define RESPONSE_SESSION_ID     @"session_id"

#define RESPONSE_EMAIL_ID       @"email_id"
#define RESPONSE_UID            @"uid"
#define RESPONSE_USER_ID        @"user_id"
#define RESPONSE_USER_NAME      @"user_name"
#define RESPONSE_USER_PASSWORD  @"password" 
#define RESPONSE_SOCIAL_FLAG    @"social"

#define REQUEST_FEEDBACK                @"feedback"
#define REQUEST_FEEDBACK_FIRST_NAME     @"first_name"
#define REQUEST_FEEDBACK_EMAIL_ID       @"email_id"

#define FREE_USER               @"F"
#define PAID_USER               @"P"

//Coverdflow Size
#define KCOVERFLOWWIDTH         150//175
#define KCOVERFLOWHEIGHT        212//247

//Payment
#define KMAGPAYMENTSUCESS       @"http://api.vikatan.com/device_payment.php?&v=1.4"
#define KMAGPAYMENTDONE         @"success"
#define KMAGPAYMENTFAIL         @"failed"

//Information IPAD
#define KINFOFAQLBL             @"FAQ", 
#define KINFOTERMLBL            @"Terms & Conditions"
#define KINFOCONTLBL            @"Contact Us"
#define KINFODEVBYLBL           @"Developed by: MobileVeda"

#define KINFOFAQURL             @"FAQ", 
#define KINFOTERMURL            @"Terms & Conditions"
#define KINFOCONTURL            @"Contact Us"
#define KINFODEVBYURL           @"Developed by: MobileVeda"
#define KMAGISSUEVALID          @"issues_vaild_from"
#define KMAGISSUEHGHLGHT        @"highlight"

//Common
#define KVIKSERVERURL           @"http://www.vikatan.com/"
#define KREQEXTRASIPAD          @"&dim=768X1024&device=ipad&resolution=768X1024&version=1.1"
#define KREQEXTRASIPHONE        @"&dim=320X480&device=iphone&resolution=320X480v&version=1.1"
#define KINTERNETNOTAVBL        @"Internet Not available. Please check your network."
#define ksiteLabelHeight 24

#define KPURCHASESCUCCS        @" Purchase successful, downloading the Magazine..";
#define KPURTERMS              @"http://www.vikatan.com/info.php?type=terms"
#define KMAGROOTPATH           @"vikatanmagazine"
#define KMAGPREVROOTPATH       @"preview"
#define KMAGCACHPAGE           @"PSPDFKit"
#define KFIREWALLBLOCK         @"Server unreachable. Please check your network settings."
#define KMAGNOTAVLB            @"No issues available for the selected date. Please select another date."
#define KMAGSUBDETAILSURL      @"http://api.vikatan.com/subscription.php"
#define KSERVERPAYMENTFAILED   @"Transaction failed, please try again - Error Code 104"
#define KDEVICEPAYMENTFAILED   @"Transaction failed , please try again - Error Code 105"
#define KMAGDWNLDFAILED        @"Failed to download Magazine"
#define KMAGDELETEFAILED       @"Please try again - Error Code 107"

#define KVIKTERMSCONDTIONIPHN      @"http://www.vikatan.com/info.php?type=terms&platform=ios&device=iphone"
#define KVIKTFAQIPHN               @"http://www.vikatan.com/info.php?type=faq&platform=ios&device=iphone"
#define KVIKTCONTACTUSIPHN         @"http://www.vikatan.com/info.php?type=contact&platform=ios&device=iphone"
#define KVIKTSUBSCRIPHN            @"http://www.vikatan.com/subscription.php&platform=ios&device=iphone"
#define KPAYMENTTERMSIPHN          @"http://www.vikatan.com/info.php?type=payment_terms&platform=ios&device=iphone"

#define KVIKTERMSCONDTIONIPAD      @"http://www.vikatan.com/info.php?type=terms&platform=ios&device=ipad"
#define KVIKTFAQIPAD               @"http://www.vikatan.com/info.php?type=faq&platform=ios&device=ipad"
#define KVIKTCONTACTUSIPAD         @"http://www.vikatan.com/info.php?type=contact&platform=ios&device=ipad"
#define KVIKTSUBSCRIPAD            @"http://www.vikatan.com/subscription.php&platform=ios&device=ipad"
#define KPAYMENTTERMSIPAD          @"http://www.vikatan.com/info.php?type=payment_terms&platform=ios&device=ipad"

#define KSINGLEISSNAM          @"subsname"
#define KSINGLEISSPRICE        @"subprice"
#define KSINGLEISSPRDID        @"ios_uid"
#define KINVALIDPRDTIDNT       @"Please select the purchase option"//@"Invalid Purchase option, please try again later."

#define KAPPDBPATH             @"%@/Library/Caches/vikatan.sqlite"
#define KAPPBASECONTPATH       @"/Library/Caches"
#define KSHOWSUBSTRIP          @"show_sub_strip"
//#define KLOGMSG                @"To Subscribe or Purchase you need to have a Vikatan account."
#define KLOGMSG                @"To synchronize all your magazine purchases across different devices, you need to have a Vikatan account."
#define KRESTOREMSG            @"To restore all your issue's and subscription purchases, click OK and enter your iTune's account        information. Restore process may take a while and will be notified on completion."
#define KINOISSUES             @"No issues available currently in library. Please Go to இதழ்கள் and download/Buy the books."
#define KLOGINHD               @"Vikatan.com - Login"
#define KSUCCESS               @"Success"
#define KINOBOOKS             @"No books available currently in library. Please Go to புத்தகங்கள் and download/Buy the books."

//Books Constants

#define BOOK_JSON_URL               @"http://api.vikatan.com/?module=book"

#define APP_ITUNES_URL              @"https://itunes.apple.com/in/app/vikatan/id452698974?mt=8"
#define APP_NAME_DESC               @"Vikatan IOS Application"
#define SHARE_TITLE                 @"Share this article"
#define SHARE_FACEBOOK              @"Share on Facebook"
#define POST_TWITTER                @"Post on Twitter"
#define SEND_EMAIL                  @"Send Email"
#define CANCEL_BUTTON_TITLE         @"Maybe Later"

#define CATEGORY                    @"category"
#define CAT_ID                      @"catid"
#define CAT_NAME                    @"catname"

#define BOOKS                       @"books"
#define BOOK_ID                     @"book_id"
#define BOOK_NAME                   @"book_name"
#define BOOK_AUTHOR                 @"author"
#define BOOK_WRAPPER_URL            @"wrapper_url"
#define BOOK_DOWNLOADABLE           @"downloadable"
#define BOOK_PRICE                  @"price"
#define BOOK_DOWNLOAD_URL           @"download_url"
#define BOOK_DOWNLOAD_PREVIEW_URL   @"preview_url"
#define BOOK_PUB_DATE               @"pub_date"
#define CONTENT_PROD_IDENTIFIER     @"ios_uid"
#define BOOK_META_DATA              @"metadata"


#define LIB_READ_NOW                @"Read Now"
#define LIB_ADD_TO_LIB              @"Add to Library"
#define LIB_BUY_NOW                 @"Buy Now"

#define DBNAME                      @"vikatan"
#define BOOK_LIB_TABLE              @"lib_book"
#define BOOK_LIB_CATEGORY           @"lib_category"

#define CONTENT_ID                  @"book_id"
#define CATEGORY_ID                 @"catid"
#define CATEGORY_NAME               @"catname"
#define CONTENT_NAME                @"book_name"
#define AUTHOR_NAME                 @"author_name"
#define CONTENT_FORMAT              @"content_format"
#define CONTENT_DATE                @"pub_date"
#define STATUS                      @"status"

#define CONTENT_EPUB_FORMAT        @"epub"
#define CONTENT_PDF_FORMAT         @"pdf"

#define STORE_BTN_BUYNOW           @"buynow.png"
#define STORE_BTN_ADDTOLIB         @"add_2lib.png"
#define STORE_BTN_READNOW          @"read_now.png"


#define KLIBMAXBOOKCOUNT            @"25"
#define CURRENT_TEXT_SIZE           @"150"


#define IMAGES_CACHE_PATH           @"Library/Caches/Images/"
#define STANDARD_USER_DEFAULT       @"magdata"
#define STANDARD_USER_NEWS_DEFAULT  @"newsdata"
#define BOOK_NIGHT_MODE             @"DAY"
#define IsIpad() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

//BOOK Store Constants
#define CONTENT_PREFIX                          @"content_"

#define CATEGORY                                @"category"       //cat_name //catname

#define BOOKS                                   @"books"     // book_name content_name
#define CONTENT_FORMAT                          @"content_format"
#define CONTENT_PROD_IDENTIFIER                 @"ios_uid"
#define CONTENT_AUTHOR                          @"author"
#define CONTENT_PRICE                           @"price"
#define CONTENT_WRAPPER_URL                     @"wrapper_url"
#define CONTENT_DOWNLOADABLE                    @"downloadable"
#define CONTENT_DOWNLOAD_URL                    @"download_url"
#define CONTENT_DOWNLOAD_PREVIEW_URL            @"preview_url"
#define CONTENT_PUB_DATE                        @"pub_date"
#define CONTENT_METADATA                        @"metadata"
#define CONTENT_DRM                             @"drm"
#define CONTENT_DRM_ENABLED                     @"enabled"
#define CONTENT_DRM_FONT                        @"font"
#define STORE_CATEGORY_TABLE                    @"store_category"
#define STORE_BOOK_TABLE                        @"store_book"

#define CHECK_DOWNLOAD_PROGRESS                 @"FALSE"


#define DOWNLOAD_VERSION_KEY                    @"download"
#define SEL_DOWNLOAD_MANAGER                    @"v1"
#define DOWNLOAD_MANAGER_KEY                    @"download_manager"

//Check os version
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//BOOk Store Constans
