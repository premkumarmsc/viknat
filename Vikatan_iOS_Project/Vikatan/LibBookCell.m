
#import "LibBookCell.h"
#import "LibCustomButton.h"
#import "Library_ipad.h"
#import "EpubViewControlleriPAD.h"
#import "EpubViewController.h"


@implementation LibBookCell

@synthesize libcustombutton, parent;

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );

    self.backgroundColor = [UIColor colorWithWhite: 0.95 alpha: 1.0];
    self.contentView.backgroundColor = self.backgroundColor;

    libcustombutton = [[LibCustomButton alloc] initWithFrame: CGRectMake(0,0,150,212)];
    [libcustombutton addTarget:self action:@selector(open_book:) forControlEvents:UIControlEventTouchDown];

    [self.contentView addSubview: libcustombutton];
    
    return ( self );
}


-(IBAction) open_book:(id)sender {
    
    LibCustomButton *content_choosen_btn = (LibCustomButton *)sender; 

    for(UIView *subview in [self.contentView subviews])  {
        if([subview isKindOfClass:[UIButton class]]) {
            NSString * content_path = content_choosen_btn.libcontentpath;
            UIViewController* epubView;

            if(IsIpad()) {
                epubView = [[EpubViewControlleriPAD alloc] init];
            } else {                
                epubView = [[EpubViewController alloc] init];
            }
            
            [epubView loadEpub:[NSURL fileURLWithPath:content_path]];
            epubView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            
            UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:epubView] autorelease];
            pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            pdfNavController.navigationController.toolbar.hidden=FALSE;
            [parent presentModalViewController:pdfNavController animated:YES];
        }
    }  
}

@end