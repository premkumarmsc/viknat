//
//  Magazine_iPad.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 13/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "VikatanAppDelegate_iPad.h"
#import "PDFScrollView.h"
#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5
#import "ReaderViewController.h"
@class PDFScrollView;
@class VikatanAppDelegate_iPad;

@interface Magazine_iPad : UIViewController<UIScrollViewDelegate,UIWebViewDelegate,ReaderViewControllerDelegate> {
  
    UISlider *slider;
    NSMutableArray   *_contentArray, *_pdfImageArray,*_pdfMainViewArray;     ///< Array of items from which show the preview.
    UIImageView      *_imageViewPreview; ///< UIImageView that will show the preview image.
    int              _lastIndexPreviewed;///< Last index updated for the current array.
    CGPDFDocumentRef _pdf;               ///< PDF Document from which extract pages to perform the preview.
    CGFloat          _yCoordPreview;     ///< y coordinate of the UIImageView that show the preview image.
    BOOL             _generatingPreview,_htmlView; ///< Flag that indicate that the preview image is going to be generated.
    UIScrollView    *_thumbNailView;
    int             pageNum, lastPage, currentPageCount, pageCount,incr,pdfViewCount;
    
    
    //For Zoom on Tap
    UIScrollView *imageScrollView;
    UIImageView *imageView;
    NSString        *currentImageName;
    BOOL           _generatingPDFThumbs;
    UIView          *_masterView, *_thumbPlaceHolderView, *_textView,*_pageNOView, *_loadingView;
    UIButton *textViewButton;
    UILabel *pageNO;
    /////////
    CFURLRef pdfURL;
    float oldX, newX;
    UIImage *pdfImage;
    UIActivityIndicatorView *spinner;
    ///JSON KEY VALUE FOR PDF PAGE AND HTML PAGE
    NSDictionary *magDIC;
    ///For PDF View Library
    //PDFScrollView *sv;
    PDFScrollView *pdfViewController;
    //UIScrollView    *_thumbNailView;
    ///For Paging textign buddy
    UIScrollView *pagingView;
    NSMutableSet *visiblePages;
    NSMutableSet *recycledPages;
    BOOL isAnimating;
    int pageOnScrollView; 
    int totalPages;
    NSString* pdfDocPath;
    
    //
    BOOL isPreview;
    VikatanAppDelegate_iPad *appDelegate;

}

@property (nonatomic, retain)  UIScrollView *imageScrollView;
@property (nonatomic, retain)  UIImageView *imageView;
@property (nonatomic, retain)  UIView *_masterView, *_thumbPlaceHolderView, *_textView,*_pageNOView, *_loadingView;
@property (nonatomic, readonly) UIButton *textViewButton;
@property (nonatomic, readonly) NSInteger pageNum;
@property (nonatomic, readonly) UILabel *pageNO;

@property (nonatomic, readonly) NSInteger lastIndexPreviewed;

@property (nonatomic, readonly) UIImage *pdfImage;
@property (nonatomic, readonly) UIActivityIndicatorView *spinner;
@property (nonatomic, retain) NSString* pdfDocPath;

/// Open the given PDF document with optional password.
/**
 @param path    String representing the file path of the PDF document.
 @param pwd     String representing the password with which open and encript the document. Give nil if no password is required.
 @param error   Error containing the problem occourred during the PDF opening.
 @return Return YES on success, NO if some problem occourred.
 */
- (BOOL)pdfPath:(NSString *)path pdfPassword:(NSString *)pwd error:(NSError **)error;

/// Assign the array of images to be previewed.
/**
 @param path    Array of images filenames.
 @param error   Error containing the problem found in image's paths given as input.
 @return Return YES on success, NO if some problem occourred.
 */
- (BOOL)imagesPathArray:(NSArray *)path error:(NSError **)error;

/// Initialize internal variables.
- (void)_initializeProperties;

/// Allows to shoe/hide the UIImageView representing the preview.
/**
 @param show        Boolean value used to hise/show the UIImageView
 @param animated    Boolean value used to perform the hide/show animated or no.
 */
- (void)_showPreviewThumb:(BOOL)show animated:(BOOL)animated;

/// Check from which element the preview has to be done (PDF, array of images) and create the preview image at the given index.
/**
 @param index Index of the element in array from which generate the preview.
 */
- (void)_updatePreviewAtIndex:(NSInteger)index;

/// Create the PDF page preview.
/**
 @param page Page number to extract and draw from PDF Document.
 */
- (void)_createPreviewForPDFPage:(NSNumber *)page;

/// Create the image preview.
/**
 @param page Page number to extract and draw from contentArray.
 */
- (void)_createPreviewForImage:(NSNumber *)page;


- (UIImage*) _getPDFView:(NSInteger)pageIndex;

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;

- (void) showPDF:(NSInteger)pageIndex;

-(void)rebuildImageArray:(NSInteger)cnt;

-(void) _generatePDFImages:(NSInteger)cnt;

-(void)bindImage:(UIImage*)img;

- (void) LoadingIcon;

- (void) StopLoading;

-(void) readMagazinIndex;

//tile View Method
- (void)tilePages;

-(void)unloadPreviousPage:(int)index;

-(void)loadNextPage:(int)index;

- (CGRect)frameForPageAtIndex:(NSInteger)index;

-(void) getPDF:(int)index;

-(void) showExtras:(NSInteger)pageIndex;

-(void)addPDFInArray:(int)index;

@end
