//
//  News_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 13/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "News_iPad.h"

@implementation News_iPad



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [webView release];
    [detailNewsView release];
    
    [super dealloc];
}



#pragma mark - View lifecycle

/*
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 // Do any additional setup after loading the view from its nib.
 }
 
 - (void)viewDidUnload
 {
 [super viewDidUnload];
 // Release any retained subviews of the main view.
 // e.g. self.myOutlet = nil;
 }
 */


@synthesize headerImageView, detailNewsView, back, webView,scrollCategoryView,
scrollArticleView, tempScrollView, newsDetailTitle ,
headerView, refreshImageView, commentsImageView, spinner, close,
closeView, thumbTitle, categoryBrowseView, fontView, cmntCntTxt,
categoryBrowsView, moreScrollView, pageControl, moreViewPlaceHolder,
urlMore, offset, tempBtn, postMsgView, postView, shareView, 
commentsListView, categListView,titleView,
titleViewDetPage,socBtn,post,increase,decrease;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
	return YES;
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
     [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 145, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
   	[self LoadingIcon];
    
    //Add Header Image Control
	UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
	headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
	
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width+15, 5, 100, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @" செய்திகள்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];
    
    self.navigationItem.titleView = titleView;
    
    refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
	[refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
    refreshImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
	commentsImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[commentsImageView setImage:[UIImage imageNamed:@"iphone_comment_box_ver_2.png"] forState:UIControlStateNormal];
	commentsImageView.frame =  CGRectMake(0, 10, 48, 48);
	[commentsImageView addTarget:self action:@selector(show_comments_clicked:) forControlEvents:UIControlEventTouchUpInside];

	//Add Back Button but hidden
	back = [UIButton buttonWithType:UIButtonTypeCustom];
	back.frame =  CGRectMake(10, 5, 0, 0);
    [back setImage:[UIImage imageNamed:@"arrow-back_32"] forState:UIControlStateNormal];
	[back addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self showShare];
    //[self showPostComment];
    closeView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,580,0,0)];
    closeView.backgroundColor = [UIColor darkGrayColor];

    thumbTitle = [[[UIButton alloc] initWithFrame:CGRectMake(0,2 , 150, 30)] autorelease];
	[thumbTitle setTitle:@"" forState:UIControlStateNormal];
	thumbTitle.backgroundColor = [UIColor clearColor];
    [thumbTitle addTarget:self action:@selector(close_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [closeView addSubview:thumbTitle];

    close = [UIButton buttonWithType:UIButtonTypeCustom];
    close.backgroundColor = [UIColor clearColor];
	[close setImage:[UIImage imageNamed:@"close-icon.png"] forState:UIControlStateNormal];
	close.frame =  CGRectMake(735, 0,32,32);
    close.tag = 5;
	[close addTarget:self action:@selector(close_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeView];

    //thumbscrollview
    categoryBrowsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,200 /*KIPADCATEBROWSELOC*/, self.view.bounds.size.width, KIPADCATBRWBTNHGHT)];
    [categoryBrowsView setBackgroundColor:[UIColor clearColor]];
    [categoryBrowseView setHidden:TRUE];
    
    //font size view
    fontView = [[UIView alloc] initWithFrame:CGRectMake(570,0,100,48)];
    increase = [UIButton buttonWithType:UIButtonTypeCustom];
	[increase setImage:[UIImage imageNamed:@"A+_32.png"] forState:UIControlStateNormal];
	increase.frame =  CGRectMake(0, 0, 48, 48);
    increase.tag = 2;
	[increase addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
    
    decrease = [UIButton buttonWithType:UIButtonTypeCustom];
	[decrease setImage:[UIImage imageNamed:@"A-_32.png"] forState:UIControlStateNormal];
	decrease.frame =  CGRectMake(0, 0, 48, 48);
    decrease.tag = 1;
	[decrease addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
    
    scrollCategoryView.userInteractionEnabled = TRUE;
    
    logMod = [[Login alloc] init];
    //Send request to get news
	[self NewsGetRequest];
    moreCLicked = FALSE;
   
	//[pool drain];
    self.navigationController.navigationBar.layer.shadowColor = [ [UIColor whiteColor ] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0,1.0);//, <#CGFloat height#>)
    self.navigationController.navigationBar.layer.shadowOpacity = 0.40;
    
    //Custom Toolbar for news
    UIToolbar *leftToolBar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 100.f, self.navigationController.navigationBar.frame.size.height)] autorelease];        
    leftToolBar.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    // leftToolBar.barStyle = !self.isDarkHUD ? UIBarStyleDefault : UIBarStyleBlack;
    leftToolBar.barStyle = UIBarStyleBlackOpaque;
    leftToolBar.tintColor = [UIColor blackColor];
    back.frame = CGRectMake(0.0, 0.0, 48, 48);
    back.hidden = TRUE;
    NSMutableArray *leftItems = [NSMutableArray array];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:back];
    // [buttonImage release];[aButton release];
    [leftItems addObjectsFromArray:[NSArray arrayWithObjects:backButton, nil]];
    leftToolBar.items = leftItems;
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:leftToolBar] autorelease];
    // check if modal
    if (self == [[[self navigationController] viewControllers] objectAtIndex:0]) {
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:leftToolBar] autorelease];
        
    }
    
    
    UIToolbar *compoundView = [[[UIToolbar alloc] initWithFrame:CGRectMake(30.f, 0.f,180.f * 2- 30, self.navigationController.navigationBar.frame.size.height)] autorelease];
    compoundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    compoundView.barStyle = UIBarStyleBlackOpaque;
    compoundView.tintColor = [UIColor blackColor];
    
    //Mobileveda customization
    
    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil] autorelease];
    space.width = 0.f;
    
    NSMutableArray *compountItems = [NSMutableArray array];
    UIImage *img = [UIImage imageNamed:@"socialnetwork_32_ver_1.png"];
    shareView =  [[UIView alloc] initWithFrame:CGRectMake(520,0,48,48)];
    shareView.backgroundColor = [UIColor clearColor];
    socBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    socBtn.frame = CGRectMake(0,0,48,48);// <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    [socBtn setImage:img forState:UIControlStateNormal];
    [socBtn addTarget:self action:@selector(showShare_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    [socBtn setHidden:TRUE];
    UIBarButtonItem *socButton = [[UIBarButtonItem alloc] initWithCustomView:socBtn];
    
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:socButton, space, nil]];
    
    [increase setHidden:TRUE];
    UIBarButtonItem *incButton = [[UIBarButtonItem alloc] initWithCustomView:increase];
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:incButton, space, nil]];
    
    [decrease setHidden:TRUE];
    UIBarButtonItem *decButton = [[UIBarButtonItem alloc] initWithCustomView:decrease];
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:decButton, space, nil]];
    
    [commentsImageView setHidden:TRUE];   
    UIBarButtonItem *cmntButton = [[UIBarButtonItem alloc] initWithCustomView:commentsImageView];
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:cmntButton, space, nil]];
    
    
    img = [UIImage imageNamed:@"comments_post_32_ver_1.png"];
    postView = [[UIView alloc] initWithFrame:CGRectMake(715,0,50,30)];
    post = [UIButton buttonWithType:UIButtonTypeCustom];
	[post setTitle:@"Post" forState:UIControlStateNormal];//<#(UIControlState)#>
	post.frame =  CGRectMake(0, 0, 48, 48);
    post.backgroundColor = [UIColor clearColor];
    [post setImage:img forState:UIControlStateNormal];
	[post addTarget:self action:@selector(PostMessage:) forControlEvents:UIControlEventTouchUpInside];
    [post setHidden:TRUE];
    UIBarButtonItem *postButton = [[UIBarButtonItem alloc] initWithCustomView:post];
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:postButton, space, nil]];
    
    
    refreshImageView.frame = CGRectMake( 0, 0.0, 30, 32);
    
    UIBarButtonItem *refButton = [[UIBarButtonItem alloc] initWithCustomView:refreshImageView];
    // [buttonImage release];[aButton release];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:refButton, space, nil]];
    
    
    compoundView.items = compountItems;
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:compoundView] autorelease];
    
    
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}


// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self StopLoading];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self _showAlert:KINTERNETNOTAVBL];
}

- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:title  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    [self StopLoading];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //responseString = [responseString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self Show_CategoryView:( NSString *) responseString];
    
}


-(void) NewsGetRequest {
	
	responseData = [[NSMutableData data] retain];
    NSURLRequest *request;
	//Send http request
    request  = [NSURLRequest requestWithURL:[NSURL URLWithString:[logMod getDeviceDetails:KIPADNEWSURL]]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self] ;
	
}

//On News Detail Page

-(void)webViewDidStartLoad:(UIWebView *) portal {
	[self StopLoading];
	[self LoadingIcon]; 
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {    
	[self StopLoading];	
    back.hidden = FALSE;
    isLoading = FALSE;
    [UIView beginAnimations:@"webView" context:nil];
    webView.alpha = 1.0;
    [UIView commitAnimations];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    isLoading = FALSE;
    back.hidden = FALSE;
}
- (void) Show_CategoryView:(NSString*)responseString{
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
    int ndx, imageWidth, titlesY = 30, categorySectionY = 0, xCoord = 0 ;
	NSString *titleString;
	NSInteger categoryid;
    
    SBJSON *jsonParser = [[SBJSON new] autorelease];
   	id response = [jsonParser objectWithString:responseString];
    NSDictionary *newsfeed = (NSDictionary *)response;
    NSArray *categories = (NSArray *)[newsfeed valueForKey:KCATEGORY];
	NSDictionary *category;
    [scrollCategoryView removeFromSuperview];
    
	CGRect mainContainer = CGRectMake(-5, 10, self.view.bounds.size.width, self.view.bounds.size.height);
	scrollCategoryView = [[UIScrollView alloc] initWithFrame:mainContainer];
	scrollCategoryView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
    scrollArticleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
	scrollCategoryView.showsVerticalScrollIndicator = NO;
	scrollCategoryView.bounces = NO;
	//scrollCategoryView.delaysContentTouches = NO;
	//For news scroll view
	CGRect subContainer = CGRectMake(0, 0, KIPADWIDTH, KIPADTHUMBIMGHEIGHT);
	scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
	scrollArticleView.contentSize = CGSizeMake(KIPADWIDTH, KIPADTHUMBIMGHEIGHT);
	scrollArticleView.showsHorizontalScrollIndicator = NO;
	scrollArticleView.bounces = YES;
	
    if(categories.count == 0 )
        [self _showAlert:KSERVERDOWN];
	UILabel *cateGoryTitle;
	// loop over all the stream objects and print their titles
	for (ndx = 0; ndx < categories.count ; ndx++) {
		
		category = (NSDictionary *)[categories objectAtIndex:ndx];
		titleString = urlStringDecode([category valueForKey:KCATEGORYTITLE]);
		categoryid  = (NSInteger)[category valueForKey:KCATEGORYID];
		UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0,titlesY - 5 , 100, 25)];
		titleView.backgroundColor = [UIColor darkGrayColor];
        
		titleView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"news_title_norm.png"]];
		cateGoryTitle = [[[UILabel alloc] initWithFrame:CGRectMake(-38,70 , KIPADTHUMBIMGHEIGHT - 10, 30)] autorelease];
		subContainer = CGRectMake(50, categorySectionY + 30, self.view.bounds.size.width - 58, KIPADTHUMBIMGHEIGHT+ 40);
		
		titlesY = titlesY + 225;
		categorySectionY = categorySectionY + 225 ;
		cateGoryTitle.text = titleString;cateGoryTitle.textAlignment = UITextAlignmentCenter;
        cateGoryTitle.numberOfLines = 1;cateGoryTitle.transform =CGAffineTransformMakeRotation( M_PI/-2 );
		cateGoryTitle.backgroundColor = [UIColor clearColor];
		cateGoryTitle.textColor = [UIColor whiteColor];
		cateGoryTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
		cateGoryTitle.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
		cateGoryTitle.shadowOffset = CGSizeMake(0, -1.0);
		cateGoryTitle.tag = categoryid;
		[scrollCategoryView addSubview:titleView];
		
        [self addCategoryBrowse:cateGoryTitle:xCoord];
        xCoord = xCoord + 185;
		scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
		scrollArticleView.tag = categoryid;
		
        CustomButton *titleImageView = [CustomButton buttonWithType:UIButtonTypeCustom];
        [titleImageView setImage:[UIImage imageNamed:@"titel_header.png"] forState:UIControlStateNormal];
        titleImageView.frame =  CGRectMake(0, categorySectionY - 170, 58, 176);
        [titleImageView addSubview:cateGoryTitle];
        [titleImageView setCateGoryID:categoryid];
        [titleImageView setArticleCount:[category valueForKey:KARTICLECOUNT]];
        [titleImageView setCateGoryTitle:titleString];
        [titleImageView setCateGoryIDStr:[category valueForKey:KCATEGORYID]];
        [titleImageView setEnabled:FALSE];
		NSArray *articles = (NSArray *)[category valueForKey:KARTICLE];
		imageWidth = 10 ;
		//Loop through individual articles in each category
		for (int y = 0 ; y < articles.count; y++){
            
			NSDictionary *article = (NSDictionary *)[articles objectAtIndex:y];
            NSMutableString *urlString = [NSMutableString stringWithString:KIPADBPATH];
			CGRect frame;
			frame.size.width = 150; frame.size.height = 150 + 30;// KIPADTHUMBIMGHEIGHT;
			frame.origin.x = imageWidth; frame.origin.y = 0;
			
			AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                          initWithFrame:frame] ;
            asyncImage.moduleIndi = KCLNEWSMODL;
			asyncImage.tag = y;
            urlString = [NSMutableString stringWithString:KIPADBIMGPATH];
			asyncImage.categoryTitle = urlStringDecode([article valueForKey:KARTICLETITLE]);
            asyncImage.dateTime      = [article valueForKey:KARTICLEDATE];
            asyncImage.commentCount  = [article valueForKey:KARTCMNTCOUNT];
            asyncImage.articleDesc   = urlStringDecode([article valueForKey:KIPADARTICLEDESC]);
			if ([@"" isEqualToString:[article valueForKey:KARTICLEIMG]]) {
				asyncImage.imageURL = @"";
				[asyncImage showHalfArticle];
                
			}else {
                [urlString appendString: [article valueForKey:KARTICLEIMG]];
				asyncImage.imageURL = urlString;
                
			}	
            // NSLog(asyncImage.imageURL);
			[asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
			
			CustomButton *thumbNailView = [CustomButton buttonWithType:UIButtonTypeCustom];
			thumbNailView.backgroundColor = [UIColor clearColor];
			thumbNailView.frame = CGRectMake(frame.origin.x+1, frame.origin.y + 16, frame.size.width , frame.size.height) ;
			thumbNailView.tag = [[article valueForKey:KARTICLEID] intValue];
            [thumbNailView setImage:[UIImage imageNamed: @"ipad_imgoutline_white.png"] forState:UIControlStateHighlighted];
            urlString = [NSMutableString stringWithString:KIPADBPATH];
            [urlString appendString: [article valueForKey:KARTICLEURL]];
            [thumbNailView setCateGoryTitle:titleString];
			[thumbNailView setUrlString:urlString];
			[thumbNailView setCateGoryID:categoryid];
            [thumbNailView setCommentsCount:[article valueForKey:KARTCMNTCOUNT]];
            [thumbNailView setArticleID: [[article valueForKey:KARTICLEID] intValue]];
            [thumbNailView setArticleTitle:[[article valueForKey:KARTICLETITLE] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [thumbNailView setArtWebURL:[article valueForKey:KARTICLEWEBURL]];
            [thumbNailView addTarget:self action:@selector(article_clicked:) forControlEvents:UIControlEventTouchUpInside];
            [scrollArticleView addSubview:asyncImage];
			[scrollArticleView addSubview:thumbNailView];
			imageWidth = imageWidth + 180;
        }
		scrollArticleView.showsHorizontalScrollIndicator = NO;
		scrollArticleView.backgroundColor = [UIColor clearColor];
		scrollArticleView.contentSize = CGSizeMake(articles.count * 180, KIPADTHUMBIMGHEIGHT + 10);
		scrollArticleView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        subContainer = CGRectMake(36, categorySectionY - 210, self.view.bounds.size.width - 36, KIPADROWHEIGHT + 20);
        UIImage *rowImage = [UIImage imageNamed:@"form_bg_option_2.png"];
        UIImageView *rowImageView = [[UIImageView alloc] initWithFrame:subContainer];
        rowImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        rowImageView.image = rowImage;
        [scrollCategoryView addSubview:rowImageView];
        [scrollCategoryView addSubview:scrollArticleView];
        [scrollCategoryView addSubview:titleImageView];
        articles = nil;
	}
	scrollCategoryView.contentSize = CGSizeMake(self.view.bounds.size.width, (235*categories.count) - 30);
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
    categoryBrowsView.contentSize = CGSizeMake(185 * categories.count, 40);
    scrollCategoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:scrollCategoryView];
	jsonParser = nil;
	[jsonParser release];
	newsfeed = nil;
	categories = nil;
	category = nil;
	responseString = nil;
    jsonParser = nil;
    [pool release];
}
- (void) show_comments_clicked:(id)sender {
    
    if(![self checkInternet]) 
        return;
   // [categoryBrowsView setHidden:TRUE];
    [postMsgView removeFromSuperview];
    [self hideThumbScrollView];
    if(![self.view.subviews containsObject:commentsListView])
    {
        commentsListView = [[UIView alloc] initWithFrame:CGRectMake(34,100,700,650)];
        [[commentsListView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[commentsListView layer] setBorderWidth:5];
        //[[commentsListView layer] setCornerRadius:15];
        [commentsListView setClipsToBounds: YES];
        UIButton *cmnt = (UIButton*)sender;
        CommentView_iPad *viewController = [[CommentView_iPad alloc] init] ;
        // NSLog([NSString stringWithFormat:@"%d", cmnt.tag]);
        viewController.articleID = [NSString stringWithFormat:@"%d", cmnt.tag];
        viewController.cmntCount = cmntCntTxt.text;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        UIView   *cmntBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,634)];
        [cmntBox addSubview:viewController.view];
        [commentsListView addSubview:cmntBox];
        [self.view addSubview:commentsListView];
    }
    // back.frame =  CGRectMake(10, 5, 0, 0);
}
- (void) close_clicked:(id)sender {
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
	tempScrollView.frame = CGRectMake(-1300, 825,prevArticleViewFrame.size.width,prevArticleViewFrame.size.height);
    newsDetailTitle.frame = CGRectMake(0, 940, 175, 40);
    closeView.frame = CGRectMake(0,1000,175,50);
    [UIView commitAnimations];	
}

- (void) moretitle_clicked:(id)sender {
    
    [moreViewPlaceHolder removeFromSuperview];
}
- (void ) more_clicked:(id)sender {
    
    CustomButton *btn = (CustomButton*)sender;
    offset = 0;
    [moreViewPlaceHolder removeFromSuperview];
    moreViewPlaceHolder = [[UIView alloc] initWithFrame:CGRectMake(0,0,KIPADWIDTH,KIPADHEIGHT)];
    [moreViewPlaceHolder setBackgroundColor:[UIColor darkGrayColor]];
    [self.view addSubview:moreViewPlaceHolder];
    
    CustomButton *title = [CustomButton buttonWithType:UIButtonTypeCustom];
    title.backgroundColor = [UIColor lightGrayColor];
    title.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    [title setTitle:btn.CateGoryTitle forState:UIControlStateNormal];
    title.frame = CGRectMake(10,10,200,30);// <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    [title addTarget:self action:@selector(moretitle_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [moreViewPlaceHolder addSubview:title];
    
    [pageControl removeFromSuperview];
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake(300, 5, 200, 20);
    pageControl.numberOfPages = [btn.articleCount intValue] / 50;
    pageControl.currentPage = 0;
    pageControl.userInteractionEnabled = TRUE;
    pageControl.enabled = TRUE;
    pageControl.tag = btn.tag;
    categoryID = btn.CateGoryIDStr;
    [pageControl setHighlighted:YES];
    [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    moreCLicked = TRUE;
    [self nextPageURL];
    
    [btn release];
}

- (void) nextPageURL {
    
    [moreScrollView removeFromSuperview];
    moreScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,40,KIPADWIDTH,KIPADHEIGHT)];
    [moreScrollView setBackgroundColor:[UIColor clearColor]];
    urlMore = [NSMutableString stringWithString:KNEWSMOREURL];//[urlMore appendString:categoryID];
    [urlMore appendString:categoryID];
    [urlMore appendString:@"&"];
    [urlMore appendString:@"offset="];[urlMore appendString:[NSString stringWithFormat:@"%d", offset]];
    [urlMore appendString:@"&alimit="];[urlMore appendString:@"50"];
    [self NewsGetRequest];
}
- (void) viewDidAppear:(BOOL)animated {
    
    commentsImageView.enabled = TRUE;
}

- (void) viewDidDisappear:(BOOL)animated {
    commentsImageView.enabled = FALSE;
}

//Event on Article Clicked
- (void)article_clicked:(id)sender {
    
    if(![self checkInternet]) 
        return;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    isLoading = TRUE;
    //webView = nil;
    //detailNewsView = nil;
    CustomButton *urlButton = (CustomButton *)sender;
    [cmntCntTxt removeFromSuperview];
    [closeView removeFromSuperview];
    int framHght = 40;
    if([self isLandscape])
        framHght = 290;
    if([urlButton.commentsCount intValue]>0) {
        
        cmntCntTxt = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, 20, 20)]; //0, -1, 35, 27
        cmntCntTxt.backgroundColor = [UIColor clearColor];
        cmntCntTxt.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:10];
        cmntCntTxt.textColor = [UIColor whiteColor];
        cmntCntTxt.text = urlButton.commentsCount;
        cmntCntTxt.textAlignment= UITextAlignmentCenter;
        [commentsImageView addSubview:cmntCntTxt];
        commentsImageView.tag = urlButton.ArticleID;
        commentsImageView.hidden = FALSE;
    } else {
        
        commentsImageView.hidden = TRUE;
    }
    
	NSMutableString *urlString = (NSMutableString *)urlButton.UrlString;
    [urlString appendString:KIPADDIM];
	NSMutableString *uriString = [[NSMutableString alloc] initWithString:[logMod getDeviceDetails:urlString]];
	NSURL* url = [[NSURL alloc] initWithString:uriString];
	NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url]; NSLog(@"detail news url %@", urlString);	
	CGRect frame;
	frame.size.width = 50; frame.size.height = 25;
	frame.origin.x = 5; frame.origin.y = 2;
	
	//if(detailView == FALSE){	
    if([self.view.subviews containsObject:detailNewsView]) {
        [detailNewsView removeFromSuperview];
        detailNewsView = nil;
        [webView release];
        webView = nil;
        webView.delegate = nil;
    }
    
    detailNewsView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-framHght)] autorelease];
    detailNewsView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    detailNewsView.backgroundColor = [UIColor blackColor];
    detailNewsView.userInteractionEnabled = TRUE;detailNewsView.delegate = self;
    webView = [[CustomWebView alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width, self.view.bounds.size.height-framHght)] ;
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;;
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setOpaque:NO];
    webView.scalesPageToFit = NO;
    webView.delegate = self;
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]; 
    tgr.delegate = self;
    [webView addGestureRecognizer:tgr];
    [tgr autorelease];
    [self.view  addSubview:detailNewsView];
    detailNewsView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height );
    [detailNewsView addSubview:webView];
    [self hideTabBarView];
    detailNewsView.hidden = FALSE;
    webView.hidden = FALSE;
    detailView = TRUE;
	//}
	//else {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.7];
    tempScrollView.frame = CGRectMake(-1300, 825,prevArticleViewFrame.size.width,prevArticleViewFrame.size.height);
    closeView.frame = CGRectMake(0,1000,175,50);
    [UIView commitAnimations];		
	//}
    webView.alpha = 0.4;
    [webView loadRequest:urlRequest];
	[webView setNeedsLayout];
    [pool release];
	textFontSize = 120;
    [self.view addSubview:closeView];
    // [self.view addSubview:categoryBrowsView];
    articleID = [[NSString alloc] initWithFormat:@"%d", 
                 urlButton.tag]; 
    //Select the particular btn
    int position = 0;
    for(UIView *subview in [categoryBrowsView subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
            if(subview.tag == urlButton.CateGoryID ) {
                [subview setBackgroundColor:[UIColor blackColor]];
                [categoryBrowsView setContentOffset:CGPointMake(position * 175,0) animated:YES];
                [self highlightCategory:subview];
            } else {
                [subview setBackgroundColor:[UIColor clearColor]];
            }
            position++;
        }
    }
    artWebURL = urlButton.artWebURL;
    articleTitle = urlButton.articleTitle;
    //[categoryBrowsView setHidden:TRUE];
    [self HideCategoryBrowse];
    //[categoryBrowsView setHidden:FALSE];
    back.hidden = FALSE;
    post.hidden = FALSE;
    increase.hidden = FALSE;
    decrease.hidden = FALSE;
    socBtn.hidden = FALSE;
    refreshImageView.hidden = TRUE;
}

- (void) showShare_Clicked:(id)sender {
    
    if(![self checkInternet]) 
        return;
    //[categoryBrowsView setHidden:TRUE];
    
    [self hideThumbScrollView];
    
    /*
    NSMutableString *webMainURL = [[NSMutableString alloc] initWithString:KIPADBIMGPATH];
    [webMainURL appendString:artWebURL];

    SHKItem *item = [SHKItem URL:[ NSURL URLWithString:webMainURL]  title:articleTitle];
    [item setShareType:SHKShareTypeURL];
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    
    [[SHK currentHelper] setRootViewController:self];
    [actionSheet showInView:self.view];*/
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:SHARE_TITLE delegate:self cancelButtonTitle:CANCEL_BUTTON_TITLE destructiveButtonTitle:nil otherButtonTitles:SHARE_FACEBOOK, POST_TWITTER, SEND_EMAIL, nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    NSMutableString *webMainURL = [[NSMutableString alloc] initWithString:KIPADBIMGPATH];
    [webMainURL appendString:artWebURL];
    
    NSURL *url = [NSURL URLWithString:webMainURL];
    SHKItem *item = [SHKItem URL:url title:articleTitle
                     contentType:(SHKURLContentTypeUndefined)];
    [SHK setRootViewController:self];
    
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        if (buttonIndex == 0) { //[actionSheet destructiveButtonIndex]
            // Add Facebook Connect code here.
            [SHKFacebook shareItem:item];
        }
        if (buttonIndex == 1) {
            // Add Twitter code here.
            [SHKTwitter shareItem:item];
        }
        if (buttonIndex == 2) {
            // Add Email code here.
            
            [SHKMail shareItem:item];
        }
    }
    
    url=nil, item=nil, webMainURL=nil;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (void) tap:(id)sender {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    tempScrollView.frame = CGRectMake(0, 1500,tempScrollView.frame.size.width,tempScrollView.frame.size.height);
    //[self HideCategoryBrowse];
    [UIView commitAnimations];
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    
}
- (void) highlightCategory:(UIButton*)sender {
    
    [self disableSelection:categoryBrowsView];
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed: @"ipad_category_horizontal.png"] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }else {
        [sender setImage:[UIImage imageNamed: @"ipad_category_horizontal_roll.png"] forState:UIControlStateSelected];
        [sender setSelected:YES];
    }
    
}

- (void) HideCategoryBrowse {
    
    //if(categoryBrowsView.hidden)
   // {
        
        if([self isLandscape]) {
            categoryBrowsView.frame = CGRectMake(0,690,self.view.bounds.size.width,40);  
            NSLog(@"Logging 1");
        }
        else {
            categoryBrowsView.frame = CGRectMake(0,940,self.view.bounds.size.width,40);
           // NSLog(@"Logging 2");
        }
        
        [categoryBrowsView setBackgroundColor:[UIColor blackColor]];
        [self.view bringSubviewToFront:categoryBrowseView];
      //  [categoryBrowsView setHidden:FALSE];
        //if(![self.view.subviews containsObject:categoryBrowseView])
            if(!categoryBrowseView)
                [self.view addSubview:categoryBrowsView];
        
//    }
/*
else {
        NSLog(@"else");
        [categoryBrowsView setBackgroundColor:[UIColor clearColor]];
        
    }
 */
    [categoryBrowsView setHidden:FALSE];
}
- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}
- (void) hightlightArticle:(CustomButton*)artBtn {
    
    /*
     for(UIView *subview in [artBtn.superview subviews]) 
     {
     if([subview isKindOfClass:[UIButton class]]) 
     {
     NSLog(@"asdasd");
     //[artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
     [artBtn setSelected:NO];
     }
     }
     */
    if(articleBtnTagID>0) {
        UIButton* btnTemp = (UIButton*)[artBtn.superview viewWithTag:articleBtnTagID];
        [btnTemp setSelected:NO];
        [btnTemp setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
    }
    if ([artBtn isSelected]) {
        //[artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
        [artBtn setSelected:NO];
    }else {
        [artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_white.png"] forState:UIControlStateSelected];
        [artBtn setSelected:YES];
        
    }
    articleBtnTagID = artBtn.ArticleID;
}

- (void) hideThumbScrollView {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    tempScrollView.frame = CGRectMake(0, 1500,tempScrollView.frame.size.width,tempScrollView.frame.size.height);
    
    [UIView commitAnimations];
}
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord{
    
    newsDetailTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    // [newsDetailTitle setImage:[UIImage imageNamed:@"titel_header.png"] forState:UIControlStateNormal];
    [newsDetailTitle setImage:[UIImage imageNamed:@"ipad_category_horizontal.png"] forState:UIControlStateNormal];
    [newsDetailTitle setImage:[UIImage imageNamed:@"ipad_category_horizontal_roll.png"] forState:UIControlStateHighlighted];
    //[newsDetailTitle setTitle:urlButton.text forState:UIControlStateNormal];
    newsDetailTitle.backgroundColor = [UIColor clearColor];
    newsDetailTitle.alpha  = 0.5;
    newsDetailTitle.frame =  CGRectMake(xCoord, 0, 175, 40);
    newsDetailTitle.tag = urlButton.tag;
    UILabel *cateGoryTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0,0 , 175, 30)] autorelease];
    cateGoryTitle.text = urlButton.text;cateGoryTitle.textAlignment = UITextAlignmentCenter;
    cateGoryTitle.numberOfLines = 1;
    [cateGoryTitle setTextColor:[UIColor whiteColor]];
    [cateGoryTitle setBackgroundColor:[UIColor clearColor]];
    cateGoryTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
    [newsDetailTitle addSubview:cateGoryTitle];
    [newsDetailTitle addTarget:self action:@selector(expand_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [categoryBrowsView  addSubview:newsDetailTitle];
    NSLog(urlButton.text);
}
//Methos to Increase font size in webview component
- (IBAction)changeTextFontSize:(id)sender
{
    [self hideThumbScrollView];
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    UIButton *btn = (UIButton*)sender; 
    btn.enabled = FALSE;
    switch ([sender tag]) {
        case 1: // A-
            textFontSize = (textFontSize > 80) ? textFontSize -20 : textFontSize;
            break;
        case 2: // A+
            textFontSize = (textFontSize < 160) ? textFontSize + 20 : textFontSize;
            //textFontSize = textFontSize + 5;
            break;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", 
                          textFontSize];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    btn.enabled = TRUE;
}

- (void)expand_clicked:(id)sender {
	
    tempScrollView.frame = prevArticleViewFrame;
    [tempScrollView setBackgroundColor:[UIColor clearColor]];
	[scrollCategoryView addSubview:tempScrollView];
    UIButton *btnCatg = (UIButton*)sender;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
	//newsDetailTitle.frame =  CGRectMake(-350, 955, 0, 0);
    //to test the category switch
    if(scrollCategoryView!= nil)
    {
        for(UIView *subview in [scrollCategoryView subviews]) 
        {
            if([subview isKindOfClass:[UIScrollView class]]) 
            {
                
                if(subview.tag == btnCatg.tag)
                {
                    [subview removeFromSuperview];
                    prevArticleViewFrame = subview.frame;
                    CGRect size;
                    if([self isLandscape]) {
                        //categoryBrowsView.frame = CGRectMake(0,690,self.view.bounds.size.width,40);  
                        size = CGRectMake(0, 490 + 10,self.view.bounds.size.width,subview.bounds.size.height);
                    }
                    else {
                        //categoryBrowsView.frame = CGRectMake(0,940,self.view.bounds.size.width,40);
                        size = CGRectMake(0, 740 +10,self.view.bounds.size.width,subview.bounds.size.height);
                    }
                    tempScrollView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
                    tempScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
                    tempScrollView = subview;
                    tempScrollView.frame = size;
                    //[self.view  addSubview:tempScrollView];	
                    [self.view  addSubview:tempScrollView];
                    //[thumbTitle setTitle:btnCatg.titleLabel.text forState:UIControlStateNormal] ;
                    
                    [tempScrollView setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"bg_nw_vikatan_ipad.png"]]];
                    //tempScrollView.alpha = 0.8;
                    tempScrollView.layer.cornerRadius = 8;
                    tempScrollView.layer.masksToBounds = YES;
                }  
            }
        }
    }
    [self highlightCategory:btnCatg];
    [UIView commitAnimations];
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
}


- (void) disableSelection:(UIScrollView*)mainView {
    
    for(UIView *subview in [mainView subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
            [subview setImage:[UIImage imageNamed: @"ipad_category_horizontal.png"] forState:UIControlStateNormal];
            [subview setSelected:NO];
        }
    }
    
}
- (void) disableSelection2:(UIView*)mainView {
    
    for(UIView *subview in [mainView subviews]) 
    {
        if([subview isKindOfClass:[CustomButton class]]) 
        {
            
            CustomButton* btn = (CustomButton*)subview;
            [btn setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
            [btn setSelected:NO];
        }
    }
    
}
- (void)back_clicked:(id)sender {
    
    if(isLoading)
        return;
    
    [categoryBrowseView setHidden:TRUE];
    back.hidden = TRUE;
    post.hidden = TRUE;
    increase.hidden = TRUE;
    decrease.hidden = TRUE;
    socBtn.hidden = TRUE;
    commentsImageView.hidden = TRUE;
    refreshImageView.hidden = FALSE;
    
    [self StopLoading];	
    [cmntCntTxt removeFromSuperview];
    closeView.frame = CGRectMake(0,770,0,0);
    //commentsImageView.frame = CGRectMake(-500, 10, 0, 0);
    [closeView removeFromSuperview];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
    
    [detailNewsView removeFromSuperview];
    
    detailNewsView = nil;
    [webView release];
    webView = nil;
    webView.delegate = nil;
    //[webView removeFromSuperview];
	//[detailNewsView removeFromSuperview];
    detailNewsView.hidden = TRUE;
    webView.hidden = TRUE;
    [self showTabBar];
	tempScrollView.frame = prevArticleViewFrame;
    [tempScrollView setBackgroundColor: [UIColor clearColor]];
	[scrollCategoryView addSubview:tempScrollView];
    //[self.navigationController.navigationBar addSubview:headerImageView];
	//headerView.frame = CGRectMake(0.0, 0, self.view.bounds.size.width, 50);
	detailView = FALSE; 
	//	self.ShowArticleBrowse;
    [tempScrollView setBackgroundColor:[UIColor clearColor]];
    //[postView removeFromSuperview];
    //[shareView removeFromSuperview];
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    
    [UIView commitAnimations];
    
}


-(void) close:(id)sender {
    
    
}
- (void)refresh_clicked:(id)sender {
    
    // Reorder_Category *viewController = [[Reorder_Category alloc] initWithStyle:UITableViewStylePlain] ;
    //[self.view addSubview:viewController.view];
	
    for(UIView *subview in [self.view subviews]) 
    {
        [subview removeFromSuperview];
    }
    //[spinner stopAnimating];
    //[spinner removeFromSuperview];
    
	[self LoadingIcon];
    tempScrollView = nil;
	[self NewsGetRequest];
	
}
-(void)makeTabBarHidden:(BOOL)hide
{
    // Custom code to hide TabBar
    if ( [self.tabBarController.view.subviews count] < 2 ) {
        return;
    }
    
    UIView *contentView;
    
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    } else {
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    }
    
    if (hide) {
        contentView.frame = self.tabBarController.view.bounds;       
    }
    else {
        contentView.frame = CGRectMake(self.tabBarController.view.bounds.origin.x,
                                       self.tabBarController.view.bounds.origin.y,
                                       self.tabBarController.view.bounds.size.width,
                                       self.tabBarController.view.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    }
    
    self.tabBarController.tabBar.hidden = hide;
}

//Hide the tab bar view
-(void) hideTabBarView{
    
    //[self makeTabBarHidden:TRUE];
    
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 1064, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 1064)];
        }
		
    }
	
    [UIView commitAnimations];
     
    
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
	
}
- (void) showTabBar {
	//[[UIApplication sharedApplication] setStatusBarHidden:FALSE];
    [self makeTabBarHidden:FALSE];
    
    
   // NSLog(@"Tab bar inside sir please check it");
    int yPos = 975;
    if ([self isLandscape]) {
        yPos = 719;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, yPos, 
                                      view.frame.size.width, 
                                      view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 
                                      view.frame.origin.y, 
                                      view.frame.size.width, yPos)];
        }
    }
    [UIView commitAnimations]; 
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[[webView subviews] objectAtIndex:0] touchesBegan:touches withEvent:event];
    
}

- (void) PostMessage:(id)sender {
    //0,45,700,600
    
    if(![self checkInternet]) 
        return;
    [categoryBrowseView setHidden:TRUE];
    //[categoryBrowsView removeFromSuperview];
    [commentsListView removeFromSuperview];
    [self hideThumbScrollView];
    if(![self.view.subviews containsObject:postMsgView])
    {
        postMsgView = [[UITextView alloc] initWithFrame:CGRectMake	(34, 100,700, 650)];
        [postMsgView setBackgroundColor:[UIColor blackColor]];
        // For the border and rounded corners
        [[postMsgView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[postMsgView layer] setBorderWidth:5];
        //[[commentsListView layer] setCornerRadius:15];
        [postMsgView setClipsToBounds: YES];    //[[postMsgView layer] setCornerRadius:15];
        [postMsgView setClipsToBounds: YES];
        [self.view addSubview:postMsgView];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,55)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[cmntHeader layer] setBorderWidth:5];
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [commentTitle setTitle:@"உங்கள் கருத்து" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(280, 10, 130, 25);
        [cmntHeader addSubview:commentTitle];
        [postMsgView addSubview:cmntHeader];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 700,615)] autorelease];
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;
        UIWebView *postWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,700, 615)] ;
        postWebView.scalesPageToFit = NO;
        postWebView.delegate = self;
        
        [postMsgView  addSubview:postView];
        [postView addSubview:postWebView];
        
        NSMutableString *postUrl = [NSMutableString stringWithString:KIPADPOSTCMNTSURL];//[urlMore appendString:categoryID];
        [postUrl appendString:articleID];
        
        NSMutableString *uriString = [[NSMutableString alloc] initWithString:[logMod getDeviceDetails:postUrl]];
        NSURL* url = [[NSURL alloc] initWithString:uriString];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        [postWebView loadRequest:urlRequest];
        [postWebView setNeedsLayout];
    }
}
- (void) closepost_clicked:(id )sender {
    [postMsgView removeFromSuperview];
    [postMsgView release];
}
- (void) LoadingIcon {
    
    //Initialize activity window for loading process
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//Set position
	//[spinner setCenter:CGPointMake(self.view.bounds.size, self.view.frame.size.height/2.0)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    //CGRect bounds = self.view.bounds;
    //spinner.center = CGPointMake(bounds.size.width / 2, bounds.size.height / 2);
    spinner.center = self.view.center;
	//Add the loading spinner to view
	[self.view addSubview:spinner];
	//Start animating
	[spinner startAnimating];
}
- (void) StopLoading {
    [spinner stopAnimating];
	[spinner removeFromSuperview];
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), toInterfaceOrientation);
#endif
    NSLog(@"Teo");
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), interfaceOrientation);
#endif
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d to %d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), fromInterfaceOrientation, self.interfaceOrientation);
#endif
    NSLog(@"Four");
    [categoryBrowseView setHidden:TRUE];
    scrollCategoryView.frame = CGRectMake(scrollCategoryView.frame.origin.x, 
                                          scrollCategoryView.frame.origin.y, 
                                          self.view.bounds.size.width, 
                                          self.view.bounds.size.height);
    
    //Recent update
    [self HideCategoryBrowse];
    /*
     CGRect mainContainer = CGRectMake(-5, 10, self.view.bounds.size.width, self.view.bounds.size.height);
     scrollCategoryView = [[UIScrollView alloc] initWithFrame:mainContainer];
     scrollCategoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
     */
}
- (BOOL) connectedToNetwork
{
    
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:KVIKSERVERURL]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
} 

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
		[self _showAlert:KINTERNETNOTAVBL];
		return NO;
	}
	else {
		return YES;
	}
}

//Release used objects
- (void)didReceiveMemoryWarning {
    
	
	//[responseData release];
	/*
     [scrollCategoryView release];
     [scrollArticleView release];
     [tempScrollView release];
     //UI activity window with spinner
     [spinner release];
     [headerImageView release];
     [detailNewsView release];
     //[back release];
     [webView release];
     [refreshImageView release];
     [commentsImageView release];
     //[close release];
     [newsDetailTitle release];
     [headerView release];
     //[closeView release];
     [thumbTitle release];
     [categoryBrowseView release];
     [fontView release];
     [cmntCntTxt release];
     [moreViewPlaceHolder release];
     // [offset release];
     //[urlMore release];
     */
    [super didReceiveMemoryWarning];    
    // Release any cached data, images, etc that aren't in use.
	
}
//nullify the view objects
- (void)viewDidUnload {
	
	responseData = nil;
	scrollCategoryView = nil;
	scrollArticleView = nil;
	tempScrollView = nil;
	//UI activity window with spinner
	spinner = nil;
	headerImageView = nil;
	detailNewsView = nil;
	back = nil;
	webView = nil;
	refreshImageView = nil;
	commentsImageView = nil;
    close = nil;
    newsDetailTitle = nil;
    headerView = nil;
    closeView = nil;
    thumbTitle = nil;
    categoryBrowseView = nil;
    fontView = nil;
    moreViewPlaceHolder = nil;
    urlMore = nil;
    logMod = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
