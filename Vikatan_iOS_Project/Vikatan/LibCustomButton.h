//
//  LibCustomButton.h
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 16/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface LibCustomButton : UIButton
{
    UIImageView * _wrapperimage;
}

@property (nonatomic, retain) UIImage * image;
@property (nonatomic, copy) NSString * libcontentId;
@property (nonatomic, copy) NSString * libcontentpath;



@end
