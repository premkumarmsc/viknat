//
//  LibCustomButton.m
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 16/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import "LibCustomButton.h"

@implementation LibCustomButton

@synthesize image, libcontentpath, libcontentId;


- (id) initWithFrame: (CGRect) frame {
    
    self = [super initWithFrame: frame ];
    if ( self == nil )
        return ( nil );
    
    _wrapperimage = [[UIImageView alloc] initWithFrame: CGRectMake(0,0,150,212)];
    
    [_wrapperimage setBackgroundColor:[UIColor darkGrayColor]];
    [_wrapperimage.layer setMasksToBounds:YES];
    [_wrapperimage.layer setCornerRadius:5.0f];
    [_wrapperimage.layer setBorderWidth:0.0f];
    _wrapperimage.backgroundColor = [UIColor lightGrayColor];
    _wrapperimage.layer.shadowColor = [UIColor blackColor].CGColor;
    _wrapperimage.layer.shadowOpacity = 0.6f;
    _wrapperimage.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    _wrapperimage.layer.shadowRadius = 4.0f;
    _wrapperimage.layer.masksToBounds = NO;
    
    [self addSubview: _wrapperimage];

    return ( self );
}


- (UIImage *) image
{
    
    return ( _wrapperimage.image );
}

- (void) setImage: (UIImage *) anImage
{
    _wrapperimage.image = anImage;
    [self setNeedsLayout];
    
}

- (NSString *) libcontentId
{
    return (libcontentId);
}

- (void) setContentId: (NSString *) contentIdval
{
    libcontentId = contentIdval;
}

- (NSString *) libcontentpath
{
    return (libcontentpath);
}

- (void) setlibcontentpath: (NSString *) contentpathval
{
    libcontentpath = contentpathval;
}

@end
