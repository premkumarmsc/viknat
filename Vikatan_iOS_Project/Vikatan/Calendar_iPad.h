//
//  Calendar_iPad.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 14/10/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagazineStore_iPad.h"
@class MagazineStore_iPad;
@interface Calendar_iPad : UIViewController <UIPickerViewDelegate>{
    
    MagazineStore_iPad *magStore;
    NSInteger curYear, curMonth;
    NSString* issValidFrm;
}


- (id)initWithMag:(MagazineStore_iPad *) magController:(NSString*)validFrom;
@end
