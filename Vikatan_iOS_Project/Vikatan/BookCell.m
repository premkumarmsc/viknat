
#import "BookCell.h"
#import "Book_ipad.h"
#import "ASIHTTPRequest.h"
#import "ZipArchive.h"
#import "EpubViewControlleriPAD.h"
#import "EPubViewController.h"
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import "ASIFormDataRequest.h"

@implementation BookCell

@synthesize asihttprequest, selectedPrdIndt, content_price;
@synthesize author, action_button,imgV, custombutton, contentid, zipcontentpath;
@synthesize responseData, filesize, downloadprogress, downloadpath, cachePath,parent;
@synthesize delegate=_delegate;
@synthesize catid, catname, contentdate, contentname, contentformat, authorname;
@synthesize btnStatus, price,contdesc, epubDocPath, downloadurl, 
transactionStatus,lblPrice,lblAuthor,lblCurrency,contentDesc,popoverController, allowVersion;

@synthesize downloadManagerVersion;

@synthesize downloadOperation, httpClient, cancelDownload;

@synthesize totalBytesExpectedToReadFromUrl, totalBytesReadFromUrl, pauseBtn, cancelBtn, resumeBtn, downloadLblBtn;


- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    error = nil;
    fileManager = [NSFileManager defaultManager];
    
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.layer setMasksToBounds:YES];
    [self.layer setCornerRadius:10.0f];
    [self.layer setBorderWidth:0.0f];
    //self.backgroundColor = [UIColor lightGrayColor];
   
    int contentWidth = self.contentView.frame.size.width , 
    xPos = 140,yTitlePos = 20,yAuthorPos=45,
    yDescPos = 80,nLines = 6,titleFontSize = 17,authorFontSize= 14;
    NSString *bgName = @"bookcontainer-iphone.png";

    if(IsIpad()){
        imgV = [[HJManagedImageV alloc] initWithFrame:CGRectMake(15,35,150,190)];
        xPos = 170;
        bgName = @"bookcontainer.png";
    }
    else {
        imgV = [[HJManagedImageV alloc] initWithFrame:CGRectMake(10,35,150/1.2,  168)];
        xPos = 140;
        yTitlePos = 20,yAuthorPos=65,yDescPos = 90;
        nLines = 4;
        titleFontSize = 15;
        authorFontSize = 12;
    }

    UIImage *rowImage = [UIImage imageNamed:bgName];
    UIImageView *rowImageView = [[UIImageView alloc] initWithFrame:self.contentView.frame];
    rowImageView.image = rowImage;
    rowImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:rowImageView];
    [self sendSubviewToBack:rowImageView];

    [imgV.layer setMasksToBounds:YES];
    [imgV.layer setCornerRadius:5.0f];
    [imgV.layer setBorderWidth:0.0f];

    pauseBtn = [[CustomUIButton alloc] init];
    pauseBtn.frame = CGRectMake(contentWidth- 80,210, 28, 28);
    [pauseBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [pauseBtn addTarget:self action:@selector(pausedownload:) forControlEvents:UIControlEventTouchDown];
    [self.contentView addSubview:pauseBtn];
    
    resumeBtn = [[CustomUIButton alloc] init];
    resumeBtn.frame = CGRectMake(contentWidth- 80,210, 28, 28);
    [resumeBtn setBackgroundImage:[UIImage imageNamed:@"resume.png"] forState:UIControlStateNormal];
    [resumeBtn addTarget:self action:@selector(resumedownload:) forControlEvents:UIControlEventTouchDown];
    [self.contentView addSubview:resumeBtn];

    cancelBtn = [[CustomUIButton alloc] init];
    cancelBtn.frame = CGRectMake(contentWidth- 45,210, 28, 28);
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(canceldownload:) forControlEvents:UIControlEventTouchDown];
    [self.contentView addSubview:cancelBtn];

    cancelBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    pauseBtn.hidden = TRUE;

    downloadprogress = [[UIProgressView alloc] initWithFrame:CGRectMake(contentWidth- 200,220, 110, 40)] ;
    downloadprogress.progressViewStyle=UIProgressViewStyleBar;

    custombutton = [[CustomUIButton alloc] initWithFrame: CGRectMake(contentWidth- 120,210,100,30)];
    [custombutton addTarget:self action:@selector(action_button:) forControlEvents:UIControlEventTouchDown];
    
    downloadLblBtn = [[CustomUIButton alloc] initWithFrame: CGRectMake(contentWidth- 120,210,100,30)];
    [downloadLblBtn setBackgroundImage:[UIImage imageNamed:STORE_BTN_ADDTOLIB] forState:UIControlStateNormal];

    _title = [[UILabel alloc] initWithFrame: CGRectMake(xPos,yTitlePos,contentWidth - 150,40)];
    _title.font = [UIFont boldSystemFontOfSize: titleFontSize];
    _title.textColor = [UIColor whiteColor];
    _title.adjustsFontSizeToFitWidth = YES;
    _title.minimumFontSize = titleFontSize;
    _title.textAlignment = UITextAlignmentLeft;
    _title.lineBreakMode = UILineBreakModeWordWrap;
    _title.numberOfLines = 4; 
    _title.backgroundColor = [UIColor clearColor];
    
    _author = [[UILabel alloc] initWithFrame: CGRectMake(xPos,yAuthorPos, contentWidth - 200, 40)];
    _author.font = [UIFont boldSystemFontOfSize: authorFontSize];
    _author.textColor = [UIColor whiteColor];
    _author.backgroundColor = [UIColor clearColor];
    _author.adjustsFontSizeToFitWidth = YES;
    _author.minimumFontSize = authorFontSize;
    _author.textAlignment = UITextAlignmentLeft;
    _author.numberOfLines = 3;
    _author.lineBreakMode = UILineBreakModeWordWrap;
    _author.backgroundColor = [UIColor clearColor];
    
    contentDesc = [[UILabel alloc] initWithFrame: CGRectMake(xPos,yDescPos, contentWidth - 190, 120)];
    contentDesc.font = [UIFont boldSystemFontOfSize: 14.0];
    contentDesc.textColor = [UIColor lightGrayColor];
    contentDesc.backgroundColor = [UIColor clearColor];
    contentDesc.adjustsFontSizeToFitWidth = YES;
    contentDesc.minimumFontSize = 14.0;
    contentDesc.textAlignment = UITextAlignmentLeft;
    contentDesc.numberOfLines = nLines;
    contentDesc.lineBreakMode = UILineBreakModeWordWrap;
    contentDesc.backgroundColor = [UIColor clearColor];
    
    lblCurrency = [[UILabel alloc] initWithFrame: CGRectMake(contentWidth- 60,140, 10, 40)];
    lblCurrency.font = [UIFont boldSystemFontOfSize: 12.0];
    lblCurrency.textColor = [UIColor whiteColor];
    lblCurrency.backgroundColor = [UIColor clearColor];
    lblCurrency.adjustsFontSizeToFitWidth = YES;
    lblCurrency.minimumFontSize = 10.0;
    lblCurrency.textAlignment = UITextAlignmentLeft;
    lblCurrency.numberOfLines = 3;
    lblCurrency.lineBreakMode = UILineBreakModeWordWrap;
    lblCurrency.backgroundColor = [UIColor clearColor];
    lblCurrency.text = @"$";
    
    lblPrice = [[UILabel alloc] initWithFrame: CGRectMake(contentWidth- 115,140, 130, 40)];
    lblPrice.font = [UIFont boldSystemFontOfSize: 14.0];
    lblPrice.textColor = [UIColor blueColor];
    lblPrice.backgroundColor = [UIColor clearColor];
    lblPrice.adjustsFontSizeToFitWidth = YES;
    lblPrice.minimumFontSize = 14.0;
    lblPrice.textAlignment = UITextAlignmentLeft;
    lblPrice.numberOfLines = 3;
    lblPrice.lineBreakMode = UILineBreakModeWordWrap;
    lblPrice.backgroundColor = [UIColor clearColor];
    lblPrice.text = @"Price";
    
    _price = [[UILabel alloc] initWithFrame: CGRectMake(contentWidth- 170,200, 200, 50)];
    _price.font = [UIFont boldSystemFontOfSize: 14.0];
    _price.textColor = [UIColor lightGrayColor];
    _price.backgroundColor = [UIColor clearColor];
    _price.adjustsFontSizeToFitWidth = YES;
    _price.minimumFontSize = 14.0;
    _price.textAlignment = UITextAlignmentLeft;
    _price.numberOfLines = 3;
    _price.lineBreakMode = UILineBreakModeWordWrap;
    _price.backgroundColor = [UIColor clearColor];
    [self.contentView setBackgroundColor:[UIColor clearColor]];

    [self.contentView addSubview: _title];
    [self.contentView addSubview:_author];
    [self.contentView addSubview:contentDesc];
    [self.contentView addSubview:_price];
    [self.contentView addSubview:imgV];
    [self.contentView addSubview:custombutton];
    [self.contentView addSubview:downloadLblBtn];
    [self.contentView addSubview:downloadprogress];

    downloadprogress.hidden = TRUE;
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    downloadLblBtn.hidden = TRUE;
    
    return ( self );
}

- (UIColor *) colorWithHexString: (NSString *) hex  
{  
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
    
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return [UIColor grayColor];  
    
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
    
    if ([cString length] != 6) return  [UIColor grayColor];  
    
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
    
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
    
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
    
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
    
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
} 


- (NSString *) title
{
    return ( _title.text );
}

- (void) setTitle: (NSString *) title
{
    _title.text = title;
    [self setNeedsLayout];
}

- (NSString *) author
{
    return ( _author.text );
}

- (void) setAuthor: (NSString *) authorval
{
    _author.text = authorval;
    [self setNeedsLayout];
}


- (NSString *) price
{
    return ( _price.text );
}

- (void) setPrice: (NSString *) priceval
{
    _price.text = priceval;
    [self setNeedsLayout];
}
- (NSString *) contdesc
{
    return ( contentDesc.text );
}

- (void) setContdesc: (NSString *) contdescval
{
    contentDesc.text = contdescval;
    [self setNeedsLayout];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
}

-(IBAction) action_button:(id)sender {

    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if([[standardUserDefaults objectForKey:CHECK_DOWNLOAD_PROGRESS] isEqualToString:@"FALSE"]) {
        CustomUIButton *catbtn = (CustomUIButton *)sender; 
        
        for(UIView *subview in [self.contentView subviews]) 
        {
            if([subview isKindOfClass:[UIButton class]]) 
            {
                if(catbtn.tag == subview.tag){
                    catid = catbtn.categoryid; // NSLog(@"this check:%@",catbtn.categoryname);
                    catname = catbtn.categoryname;
                    contentid = catbtn.contentId;
                    contentname = catbtn.contentName;
                    authorname = catbtn.authorName;
                    contentdate = catbtn.contentDate;
                    contentformat = catbtn.contentFormat;
                    selectedPrdIndt = catbtn.prod_identifier;
                    content_price = catbtn.price;
                    // downloadurl = catbtn.downloadurl;
                    cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];

                    downloadpath = [cachePath stringByAppendingPathComponent:contentid];

                    [downloadpath retain];
                    [contentid retain];
                    [downloadurl retain];
                    [selectedPrdIndt retain];
                    [content_price retain];
                    [catname retain];
                    
                   // NSLog(@"Perform download action %d",catbtn.readStatus);
                    
                    [self performButtonAction:catbtn.readStatus];
                    
                }
            }
        } 
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                        message:@"Do you want to cancel?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
        
        
        //[alert show];
        [alert release];
        
        [self _showAlert:@"Download inprogress, please wait."];
    }
}

- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
   // NSLog(@"buttonIndex is %d", buttonIndex);
    if (buttonIndex == 1) {
    
        [self cancelProgressingDownload];
    }
}

-(void)showInAppList {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"bookpurccontinue" object:nil];
    if([selectedPrdIndt isEqualToString:@""] ||selectedPrdIndt == NULL||selectedPrdIndt == Nil)
    {
        [self  _showAlert:KINVALIDPRDTIDNT];
    } 
    else
    {
       // NSLog(@"selectedPrdIndt and content id is %@  %@" , selectedPrdIndt, contentid);
        NSMutableArray *ar = [[NSMutableArray alloc] init];
        [ar addObject:selectedPrdIndt];
        [self productRequesting:ar];
        [ar retain];
    }
}

- (void) closePops {
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"purccontinue" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(closePops) name:@"popviewclose" object: nil];
    [self.popoverController dismissPopoverAnimated:FALSE];
}
- (void)closeModalView {
    [self.parent dismissModalViewControllerAnimated:YES];
}
-(void) performButtonAction:(int) id
{
    switch (id) {
        case 0:
        {
            Login *loginMod = [[Login alloc] init];
            
            
            if (![loginMod isUserDetailsExists]) {
                 [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(closePops) name:@"popviewclose" object: nil];
                [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(showInAppList) name:@"bookpurccontinue" object: nil];
                loginMod.fromInfo = FALSE;
                //[loginMod showLoginViewAndAuthenticateUser:self.view];
                
                LoginView *ct = [[LoginView alloc] init];
                ct.fromBookStore = TRUE;
                [ct.view setBackgroundColor:[UIColor clearColor]];
                
                if(IsIpad()) {
                    UIPopoverController *popoverControll = [[UIPopoverController alloc] initWithContentViewController:ct] ;
                    ///[popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                    popoverControll.delegate = self;
                    CGRect popoverRect = [self convertRect:[self frame] 
                                                       fromView:[self superview]];    
                    popoverRect.size.width = MIN(popoverRect.size.width, 100);
                    CGSize size = CGSizeMake(768, 450); // size of view in popover
                    popoverControll.popoverContentSize = size;
                    [popoverControll 
                     presentPopoverFromRect:popoverRect 
                     inView:self 
                     permittedArrowDirections:UIPopoverArrowDirectionAny 
                     animated:YES];
                    self.popoverController = popoverControll;
                } else {
                    
                    
                    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:ct] autorelease];
                    ct.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closeModalView)] autorelease];
                    //controller.navigationItem.title = self.document.title;
                    [self.parent presentModalViewController:navController animated:NO];
                }
            transactionStatus = FALSE;
            } else {
                if([selectedPrdIndt isEqualToString:@""] ||selectedPrdIndt == NULL||selectedPrdIndt == Nil)
                {
                    [self  _showAlert:KINVALIDPRDTIDNT];
                } 
                else
                {
                   // NSLog(@"selectedPrdIndt and content id is %@  %@" , selectedPrdIndt, contentid);
                    NSMutableArray *ar = [[NSMutableArray alloc] init];
                    [ar addObject:selectedPrdIndt];
                    [self productRequesting:ar];
                    //[ar retain];
                }
            }            
            break;
        }
        case 1:
        {
            if([self checkInternet] == YES)
            {
                if (![fileManager fileExistsAtPath:downloadpath]) {
                    
                    [fileManager createDirectoryAtPath:downloadpath withIntermediateDirectories:NO attributes:nil 
                                                 error:&error]; // To Create folder with Content ID
                    
                }

                zipcontentpath = [NSString stringWithFormat:@"%@/book_%@.zip", downloadpath, contentid];
                
                [zipcontentpath retain];
                
                NSLog(@"ZIP PATH:%@",zipcontentpath);
                
                
                [self startdownloadrequest: downloadurl];
            }
            else
            {
                return;
            }
            break;
        }
        case 2:
        {
            
            epubDocPath = [NSString stringWithFormat:@"%@%@%@%@", downloadpath, @"/book_", contentid, @".epub"];
            
            NSString * pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@", downloadpath, @"/book_", contentid, @".pdf"];
            
            if ([fileManager fileExistsAtPath:epubDocPath]) {
               // NSLog(@"EPUB file exists in cache directory");
                [self openbook:epubDocPath];
            }
            else if ([fileManager fileExistsAtPath:pdfDocPath]) {
               
                /*  // commented for disabling pdf reader
                MagazineLib *magLib = [[MagazineLib alloc] init];
                PSPDFDocument *document = [PSPDFDocument PDFDocumentWithUrl:[NSURL fileURLWithPath:pdfDocPath]];
                document.searchEnabled = FALSE;
                document.annotationsEnabled = FALSE;
                document.outlineEnabled = FALSE;
                document.twoStepRenderingEnabled = FALSE;
                document.aspectRatioEqual = TRUE;
                document.title = @"Vikatan";
                
                MVPDFViewController *pdfController = [[MVPDFViewController alloc] initWithDocument:document] ;
                pdfController.preview = FALSE;
                pdfController.custTitle = @"Book";
                pdfController.navigationItem.title = @"";
                
                if(PSIsIpad())
                    pdfController.custTitle = @"Back";
                else
                    pdfController.custTitle = @"<";
                
                UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:pdfController] autorelease];
                pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                [self.parent presentModalViewController:pdfNavController animated:NO];
                [pdfController release];
                [magLib release];
                 */
                

            }
            break;
        }
        default:
            break;
    }
}


/***************In-App purchase Function SKProductsRequestDelegate methods***********************/

- (void) productRequesting:(NSArray*)ar {
    
    NSLog(@"Inside productRequesting");
    int i;
    
    if([ar count] >0) {

        for(i=0; i<[ar count]; i++) {
            NSLog(@"product identifire ar count is %@", [ar objectAtIndex:i]);
    
        }
    } else {
        NSLog(@"product identifire count is zero");
    }

    if ([SKPaymentQueue canMakePayments]) {
        NSLog(@"Can make payemnts price: %@",content_price);
        //[self _showAlert:@"Can make payemnts."];
        [self requestProductData:ar];
    } else {
        NSLog(@"Cannot proceed");
        [self _showAlert:@"Check your setting, in app purchase is restricted."];
    }
}

- (void)requestProductData:(NSArray *) ar
{
     NSLog(@"Inside requestProductData");

    NSSet *productIdentifiers = [NSSet setWithArray:ar ];
    //NSSet *productIdentifiers = [NSSet setWithObject:@"com.viduthalai.books.dravidaraariyar" ];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    request.delegate = self;
    [request start];
    //[ar release];

    // we will release the request object in the delegate callback
}

#pragma mark -
#pragma mark SKProductsRequestDelegate methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSLog(@"Product Response is found to be:%d",response.products.count);
    
   /* if([response.products count] > 0 )
    {
        for (NSString *invalidProductId in response.invalidProductIdentifiers)
        {
            NSLog(@"Invalid product id: %@" , invalidProductId);
        }

        // finally release the reqest we alloc/init’ed in requestProductData
        [productsRequest release];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        SKPayment *payment = [SKPayment paymentWithProductIdentifier:selectedPrdIndt ];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        selectedPrdIndt = @"";

    }*/
    
    NSArray *products = response.products;
    proUpgradeProduct = [products count] == 1 ? [[products objectAtIndex:0] retain] : nil;
    if (proUpgradeProduct)
    {
        /*NSLog(@"Product title: %@" , proUpgradeProduct.localizedTitle);
        NSLog(@"Product description: %@" , proUpgradeProduct.localizedDescription);
        NSLog(@"Product price: %@" , proUpgradeProduct.price);
        NSLog(@"Product id: %@" , proUpgradeProduct.productIdentifier);*/
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        SKPayment *payment = [SKPayment paymentWithProductIdentifier:selectedPrdIndt ];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        selectedPrdIndt = @"";
        [productsRequest release];

    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers)
    {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }

}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error1  
{  
    NSLog(@"Failed to connect with error: %@", [error1 localizedDescription]);  
}

/***************In-App purchase Function***********************/

//
// kick off the upgrade transaction
//
- (void)purchaseProUpgrade
{
    NSLog(@"Inside purchaseProUpgrade");
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchaseProUpgradeProductId];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

#pragma -
#pragma Purchase helpers

//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Inside recordTransaction");
    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"proUpgradeTransactionReceipt" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
    NSLog(@"Inside provideContent %@",productId);
    if ([productId isEqualToString:kInAppPurchaseProUpgradeProductId])
    {
        // enable the pro features
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isProUpgradePurchased" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    NSLog(@"Inside finishTransaction");
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful)
    {
        NSLog(@"Inside finishTransaction wasSuccessful");
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionSucceededNotification object:self userInfo:userInfo];
        
        NSString *receiptData = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];

        Login *logMod = [[Login alloc]init];
        
        NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];
        
       // NSLog(@"finishTransaction contentid, emailID, userID, sessionID, password %@==>%@ ==>%@==>%@==>%@", contentid, [getUrlAndParams objectForKey:@"email_id"], [getUrlAndParams objectForKey:@"userID"], [getUrlAndParams objectForKey:@"session_id"], [getUrlAndParams objectForKey:@"password"]);
        

        NSURL *requestURL = [NSURL URLWithString:KMAGPAYMENTSUCESS];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
        
               
        [request setPostValue:[getUrlAndParams objectForKey:@"password"] forKey:REQUEST_PASSWORD];
        [request setPostValue:[getUrlAndParams objectForKey:@"userID"] forKey:RESPONSE_USER_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"session_id"] forKey:RESPONSE_SESSION_ID];
        [request setPostValue:[getUrlAndParams objectForKey:@"email_id"] forKey:RESPONSE_EMAIL_ID];

        [request setPostValue:[NSString stringWithFormat:@"%@",contentid] forKey:@"book_id"];
        [request setPostValue:[NSString stringWithFormat:@"%@",content_price] forKey:@"amount"];
        [request setPostValue:[NSString stringWithFormat:@"%d",0] forKey:@"trans_status"];
        [request setPostValue:[NSString stringWithFormat:@"%@",receiptData] forKey:@"receipt-data"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"Apple"] forKey:@"bank"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"ios"] forKey:@"platform"];
        [request setPostValue:[NSString stringWithFormat:@"%@",@"ipad"] forKey:@"device"];
        [request setPostValue:[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueIdentifier]] forKey:@"udid"];
        
       // [request setPostValue:@"mobileveda" forKey:@"debug"];

        if(transactionStatus)
            [request setPostValue:[NSString stringWithFormat:@"%@",@"restore"] forKey:@"request_type"];
        else
            [request setPostValue:[NSString stringWithFormat:@"%@",@""] forKey:@"request_type"];
        
        [request startSynchronous];
        NSError *error1 = [request error];
        NSLog(@"error log token %@", [error localizedDescription]);
        if (!error1) {
            //_issueToDownload = [NSString stringWithFormat:@"%d",issueID];
            
            NSString *res = [request responseString];

            if(![res isEqualToString:KMAGPAYMENTDONE])
            {
                NSLog(@"Response failed found to be  is being the %@", res);
                // [self _showAlert:KSERVERPAYMENTFAILED];
                
            } else {
                NSLog(@"Response is found to be  is being the %@", res);
                [self _showAlert:@"Book Purchase Sucessfull"];
                custombutton.title = @"Add To Library";
                custombutton.readStatus = 1;
                [custombutton setBackgroundImage:[UIImage imageNamed:STORE_BTN_ADDTOLIB] forState:UIControlStateNormal];
                if (!transactionStatus) 
                {
                    NSLog(@"after success token is found to be %@", res);
                   //  [self _showAlert:KPURCHASESCUCCS]; 
                     
                }
                //[networkQueue cancelAllOperations];
            }
            
        } {
            
        }
                
    }
    else
    {
        NSLog(@"Inside finishTransaction was not Successful");
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerTransactionFailedNotification object:self userInfo:userInfo];
    }
    
    //remove the observer after transaction
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self]; // To avoid previous payment requests to server and to enable request for current request.
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Inside completeTransaction");
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
     NSLog(@"Inside restoreTransaction");
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
     NSLog(@"Inside failedTransaction");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        // error!
        NSLog(@"Inside fKDEVICEPAYMENTFAILED");
        [self _showAlert:KDEVICEPAYMENTFAILED]; 
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {   NSLog(@"Inside the user just cancelled");
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
    
    //remove the observer after transaction failed
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self]; // To avoid previous payment requests to server and to enable request for current request.
}

#pragma mark -
#pragma mark SKPaymentTransactionObserver methods

//
// called when the transaction status is updated
//
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"Inside paymentQueue");
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                 NSLog(@"purchased");
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"failed");

                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"restore");

                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

/***************ASIHTTPRequest download request Function***********************/
-(void) updateCheckDownloadProgress: (NSString *) setValue
{
    
    NSLog(@"setValue is %@", setValue);


    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:setValue forKey: CHECK_DOWNLOAD_PROGRESS];
}


- (void)startdownloadrequest: (NSString *) downloadurlstring {

    Login *logMod = [[Login alloc]init];
    NSMutableDictionary * getUrlAndParams = [logMod getDeviceDetails];
    
    
    allowVersion = TRUE;
    downloadprogress.hidden = FALSE;
    cancelBtn.hidden = FALSE;
    custombutton.hidden = TRUE;

    [self updateCheckDownloadProgress:@"TRUE"];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    downloadManagerVersion = [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY];

    NSComparisonResult order = [[UIDevice currentDevice].systemVersion compare: @"5.0.0" options: NSNumericSearch];

    if (order == NSOrderedSame || order == NSOrderedDescending) { // OS version Greater than or equal 5
        allowVersion = TRUE;
        
    }  else { // OS version less than 5
        allowVersion = FALSE;
        downloadManagerVersion = SEL_DOWNLOAD_MANAGER; // Overriding Download Manager Version for progress view support
    }

    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(setPauseMode) name:@"setPauseMode" object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(cancelProgressingDownload) name:@"cancelBookDownloadOnRefresh" object: nil];

    if(![downloadManagerVersion isEqualToString:@"v2"]) { //ASIHTTP Download Manager
        
        NSLog(@"Download url String in ASIHTTP %@",[NSString stringWithFormat:@"%@&%@", downloadurlstring, [getUrlAndParams objectForKey:@"userDet"]] );
        

        asihttprequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@&%@", downloadurlstring, [getUrlAndParams objectForKey:@"userDet"]]]];
        
        
        NSLog(@"DOWNLOAD URL:%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@&%@", downloadurlstring, [getUrlAndParams objectForKey:@"userDet"]]]);
        
        [asihttprequest setDelegate:self];
        [asihttprequest setAllowCompressedResponse:YES];
        [asihttprequest setDownloadDestinationPath:zipcontentpath];
        [asihttprequest setDownloadProgressDelegate:downloadprogress];
       
       // [asihttprequest appendPostData:[[NSString stringWithFormat:@"&%@", [getUrlAndParams objectForKey:@"userDet"]] dataUsingEncoding:NSUTF8StringEncoding]];
        //[asihttprequest setRequestMethod:@"POST"];

        [asihttprequest startAsynchronous];
        
    } else { //AFNETWORK Download Manager

        pauseBtn.hidden = FALSE;
        NSLog(@"Download url String in AFNETWORK %@",downloadurlstring );
        
        
        httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:downloadurlstring]];
        [httpClient defaultValueForHeader:@"Accept"];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                [getUrlAndParams objectForKey:REQUEST_EMAIL_ID], REQUEST_EMAIL_ID,
                                [getUrlAndParams objectForKey:REQUEST_PASSWORD], REQUEST_PASSWORD,
                                [getUrlAndParams objectForKey:RESPONSE_USER_NAME], RESPONSE_USER_NAME,
                                [getUrlAndParams objectForKey:RESPONSE_SOCIAL_FLAG], RESPONSE_SOCIAL_FLAG,
                                nil];

        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                                path:downloadurlstring
                                                          parameters:params];
        
        
        
        NSLog(@"DOWNLOAD:%@",downloadurlstring);

        // AFHTTPRequestOperation download functions

        downloadOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        downloadOperation.outputStream = [NSOutputStream outputStreamToFileAtPath:zipcontentpath append:NO];
        cancelBtn.dnwlOperation = downloadOperation;
        cancelBtn.downloadPath = zipcontentpath;
        
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];

        [downloadOperation start];

        [downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

            if(totalBytesExpectedToReadFromUrl == -1 ) {
                [self downloadFailedMethod];
                downloadOperation=nil, [downloadOperation release];
            } else {

                [self unzipDownloadedFile];
                downloadOperation=nil, [downloadOperation release];
            }

        } failure:^(AFHTTPRequestOperation *operation, NSError *errors) {

            if(totalBytesReadFromUrl > totalBytesExpectedToReadFromUrl) {
                [self unzipDownloadedFile];
                downloadOperation=nil, [downloadOperation release];
            } else {    
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Msg" message:errors.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
                
                [self downloadFailedMethod];
                downloadOperation=nil, [downloadOperation release];
            }
        }];
        
        if (allowVersion) {

            [downloadprogress setProgress:0.0 animated:YES];
            [downloadOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
                
                float percentDone = totalBytesRead/(float)totalBytesExpectedToRead;
                [downloadprogress setProgress:percentDone animated:YES];
                
                totalBytesExpectedToReadFromUrl = totalBytesExpectedToRead;
                totalBytesReadFromUrl = totalBytesRead;
            }];
        } else {
           
            int contentWidth = self.contentView.frame.size.width;
            downloadLblBtn.frame =  CGRectMake(contentWidth- 160,210,100,30);

            pauseBtn.hidden = TRUE;
            resumeBtn.hidden = TRUE;
            custombutton.hidden = TRUE;
            downloadprogress.hidden = TRUE;
            downloadLblBtn.hidden = FALSE;
            downloadLblBtn.title = [NSString stringWithFormat:@"Downloading"];
        }
        
        params=nil, request=nil, request=nil;
    }
    
    getUrlAndParams=nil;
    logMod=nil, [logMod release];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self unzipDownloadedFile];

}

- (void)requestFailed:(ASIHTTPRequest *)asihttprequest
{
    //NSError *error = [asihttprequest error];
    [self downloadFailedMethod];
}


//AFNetworking functions

-(IBAction) pausedownload: (id) sender {
    
    [self setPauseMode];
}

-(void) setPauseMode  {

    [downloadOperation pause];

    pauseBtn.hidden = TRUE;
    resumeBtn.hidden=FALSE;
}

-(IBAction) resumedownload: (id) sender {
    
    if([self checkInternet] == YES) {
    
        pauseBtn.hidden = FALSE;
        resumeBtn.hidden=TRUE;

        [downloadOperation resume];

    }
}

-(IBAction) canceldownload: (id) sender {
    
    //[self cancelProgressingDownload];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                    message:@"Do you want to cancel Download?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok",nil];
    
    
    [alert show];
    [alert release];

}

-(void) cancelProgressingDownload {
    
    if(![downloadManagerVersion isEqualToString:@"v2"]) {
        if(asihttprequest) {
            
            [asihttprequest cancel];
            [asihttprequest clearDelegatesAndCancel];
            asihttprequest=nil;
        }
        
    } else {
        [downloadOperation cancel];
        downloadOperation=nil, [downloadOperation release];
    }

    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    downloadprogress.hidden = TRUE;
    downloadLblBtn.hidden = TRUE;
    custombutton.hidden = FALSE;
    
    if(!allowVersion) {
        int contentWidth = self.contentView.frame.size.width;
        custombutton.frame = CGRectMake(contentWidth- 120,210,100,30);
    }
    
    custombutton.title = @"Add to Library";
    [self updateCheckDownloadProgress:@"FALSE"];
}

//AFNetworking functions

-(void) downloadFailedMethod {
    
    [self updateCheckDownloadProgress:@"FALSE"];
    [self _showAlert:@"Download failed, Please check your Internet Connection"];
    
    downloadprogress.hidden = TRUE;
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    downloadLblBtn.hidden = TRUE;
    custombutton.hidden = FALSE;
    custombutton.title = @"Add to Library";
    custombutton.color = [UIColor whiteColor];
    custombutton.readStatus = 1;
    [custombutton setBackgroundImage:[UIImage imageNamed:STORE_BTN_ADDTOLIB] forState:UIControlStateNormal];
    
    downloadOperation=nil, [downloadOperation release];

}

-(void) unzipDownloadedFile {

    ZipArchive *za = [[ZipArchive alloc] init];
   // NSLog(@"unzipDownloadedFile zipcontentpath %@", zipcontentpath);
    NSLog(@"unzipDownloadedFile downloadpath %@", downloadpath);

    if ([za UnzipOpenFile: zipcontentpath]) {
        
        NSString *zipcontentpathval = [NSString stringWithFormat:@"%@%@",@"/", downloadpath];
        
        BOOL ret = [za UnzipFileTo:zipcontentpathval overWrite: YES];
        
        if (NO == ret){
           // NSLog(@"Failed");
            [za UnzipCloseFile];
        } else {
            NSLog(@"success");
            [za UnzipCloseFile];
            [fileManager removeItemAtPath:zipcontentpath error:NULL];
        }
    }
    [za release];
    
    [self saveData];
    
    [self updateCheckDownloadProgress:@"FALSE"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Download completed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    downloadprogress.hidden = TRUE;
    pauseBtn.hidden = TRUE;
    resumeBtn.hidden = TRUE;
    cancelBtn.hidden = TRUE;
    downloadLblBtn.hidden = TRUE;
    
    if(!allowVersion) {
        int contentWidth = self.contentView.frame.size.width;
        custombutton.frame =  CGRectMake(contentWidth- 120,210,100,30);
    }

    custombutton.hidden = FALSE;
    custombutton.title = @"ReadNow";
    custombutton.color = [UIColor blackColor];
    custombutton.readStatus = 2;
    [custombutton setBackgroundImage:[UIImage imageNamed:STORE_BTN_READNOW] forState:UIControlStateNormal];
    
    downloadOperation=nil, [downloadOperation release];

}

/***************ASIHTTPRequest download request Function***********************/

/***************ASIHTTPRequest download request Function***********************/

-(void) openbook:(NSString *) epubDocPathval
{
    EpubViewControlleriPAD *epubView = [[EpubViewControlleriPAD alloc] init]; //EpubViewController
    [epubView loadEpub:[NSURL fileURLWithPath:epubDocPathval] contentid:contentid type:@"main"];
    epubView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    /* UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:epubView] autorelease];
     pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
     pdfNavController.navigationController.toolbar.hidden=FALSE;
     [parent presentModalViewController:pdfNavController animated:YES];*/
    
    //MV modification
    [parent presentModalViewController:epubView animated:YES];
    [parent dismissModalViewControllerAnimated:NO];

}

- (void) saveData
{
   
    DBClass *dbObject = [[DBClass alloc] init];
    
    NSString *queryValues = [NSString stringWithFormat:@"%@,%@,\"%@\",\"%@\",\"%@\",\"%@\"",contentid, catid, catname, contentname, contentDesc.text, authorname];

    if ([dbObject insertMagazineFields:@"book_id, cat_id, cat_name, book_name, book_desc, author_name" values:queryValues tableName:@"lib_book"]) 
    {
       // NSLog(@"book inserting  Successfully %@",queryValues);
        
    }
    [dbObject release];
    dbObject = nil, queryValues=nil;
}

// Check internet connection status delegate


-(BOOL) checkInternet
{
    NSLog(@"Inter not available");
    //Make sure we have internet connectivity
    if([self connectedToNetwork] != YES)
    {
       // NSLog(@"Internet not available");
        [self _showAlert:KINTERNETNOTAVBL];
        return NO;
    }
    else {
        NSLog(@"connectedToNetwork == yes");
        return YES;
    }
}


- (BOOL) connectedToNetwork
{
   // NSLog(@"connectedToNetwork");
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        NSLog(@"Error. Could not recover network reachability flags");
        return 0;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
    BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:KIPADBIMGPATH]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}

-(void) _showAlert:(NSString *) title
{
    
    Login *logMod = [[Login alloc]init];
    BOOL st = logMod.isUserDetailsExists;
    
    if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
        logMod.username = @"Guest";
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]  message:title  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
    [logMod release],logMod = nil;
    
}

@end
