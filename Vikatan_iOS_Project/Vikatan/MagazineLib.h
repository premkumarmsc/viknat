//
//  MagazineLib.h
//  SQL
//
//  Created by svp on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#define DATABASE_NAME @"vikatan.sqlite"
#define TABLE_NAME @"lib_magazine"

#define FN_LIBMAGAZINEID @"lib_magazine_id"
#define FN_MAGAZINEID @"magazine_id"
#define FN_MAGAZINENAME @"lib_magazine_name"
#define FN_ISSUEID @"issue_id"
#define FN_ISSUEDATE @"issue_date"
#define FN_SUPPLEMENTARY @"supplementary"
#define BOOK_LIB_TABLE   @"lib_book"

@interface MagazineLib : NSObject {
    NSInteger magazinePrimaryID;
    NSInteger magazineID;
    NSInteger issueID;
    NSString *issueDate;
    BOOL supplementary;
}

@property(nonatomic) NSInteger magazinePrimaryID;
@property(nonatomic) NSInteger magazineID;
@property(nonatomic) NSInteger issueID;
@property(nonatomic, retain) NSString *issueDate;
@property(nonatomic) BOOL supplementary;

-(NSString *) magazineFieldNameOfLIBID;
-(NSString *) magazineFieldNameOfMagazineID;
-(NSString *) magazineFieldNameOfIssueID;
-(NSString *) magazineFieldNameOfIssueDate;
-(NSString *) magazineFieldNameOfSupplementary;


-(BOOL) copyDatabaseIfNeeded;
-(NSString *) getDBPath;
-(BOOL) openDatabase;
-(BOOL) closeDatabase;

-(BOOL) insertMagazine;

-(BOOL) insertMagazineFields:(NSString *)fields values:(NSString *)values;

-(BOOL) updateMagazineWithFieldAndValues:(NSString *)fieldAndValues when:(NSString *)condition;
-(BOOL) deleteMagazineWhen:(NSString *)condition;
-(BOOL) deleteAllMagazines;
-(BOOL) deleteBookWhen:(NSString *)condition;

-(NSMutableArray *) selectMagazineField:(NSArray *)fields when:(NSString *)condition;
-(NSMutableArray *) selectMagazineField:(NSArray *)fields when:(NSString *)condition order:(NSString *)order recordsCount:(NSInteger)limit startWith:(NSInteger)offset;
-(NSMutableArray *) selectValues:(NSArray *)fields when:(NSString *)condition tableName:(NSString *)table;

-(NSInteger) countMagazinesWhen:(NSString*) condition;
-(NSInteger) getLatestMag;
-(NSInteger) getLatestBook;
-(NSString*) generate_device_access:(NSString *)title contentid:(NSString *)contid magazineid:(NSString *)magid;
//-(void) deleteEpubUnzippath ;
-(void) deleteEpubUnzippath:(NSString*)deleteFolder;
@end
