//
//  LoginView.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 03/12/11.
//  Copyright (c) 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialNetworkWebViewViewController.h"
#import "Information_iPad.h"


//Internet check
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@interface LoginView : UIViewController<UITextFieldDelegate> {

    UIView *loginView;
    UILabel *messageLabel;
    UILabel *usernameLabel;
    UILabel *passwordLabel;
    UITextField *userTxt;
    UITextField *passTxt,*userTxtEmail,*cPassTxt;
    BOOL fromInfo;
    
    UIViewController *parent;
}
@property(nonatomic)BOOL fromInfo,fromBookStore, fromSocialLogin;

- (BOOL) connectedToNetwork;

-(BOOL) checkInternet;
- (void) magStorereferesh;
- (void) _showAlert:(NSString*)title;
-(void) slideFrame:(BOOL) up;
-(void) closePopViewContrl;
- (void) showlog;
- (void)closeModalView;
@end
