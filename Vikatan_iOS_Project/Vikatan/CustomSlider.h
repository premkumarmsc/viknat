//
//  CustomSlider.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 25/08/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MNESliderValuePopupView;


@interface CustomSlider : UISlider {
    
    MNESliderValuePopupView *valuePopupView; 
}

@property (nonatomic, readonly) CGRect thumbRect;

@end
