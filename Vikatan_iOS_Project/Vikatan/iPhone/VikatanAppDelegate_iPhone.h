//
//  VikatanAppDelegate_iPhone.h
//  Vikatan
//
//  Created by MobileVeda on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
// 10971050024962

#import <UIKit/UIKit.h>
#import "VikatanAppDelegate.h"
#import "News_iPhone.h"
#import "Information_iPhone.h"
#import "Library_iPad.h"
#import "Information_iPad.h"
#import "MagazineStore_iPhone.h"
#import "Library_iPhone.h"
#import "FeedBackForm.h"
#import "SHKConfiguration.h"
#import "Library_Book_iPAD.h"

#define DATABASE_NAME @"vikatan.sqlite"

@interface VikatanAppDelegate_iPhone : VikatanAppDelegate {
    
    UITabBarController *tabBarController;
    NSNumber *globalCount;
    //Current PDF Document Path
    NSString* pdfDocPath;
    
    NSString* issValidFrom;
}
- (BOOL) copyDatabaseIfNeeded;
@property (nonatomic,copy)  NSNumber *globalCount;
@property (nonatomic,copy)  NSString* pdfDocPath;
@property (nonatomic,copy)  NSString *issValidFrom;
- (NSString *) getDBPath;
@end
