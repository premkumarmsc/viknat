//
//  News_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 13/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "News_iPhone.h"

@implementation News_iPhone


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}



#pragma mark - View lifecycle

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
*/


@synthesize headerImageView, detailNewsView, back, webView,scrollCategoryView,
            scrollArticleView, tempScrollView, newsDetailTitle ,
            headerView, refreshImageView, commentsImageView, spinner, close,
            closeView, thumbTitle, categoryBrowseView, fontView, cmntCntTxt,
            categoryBrowsView, moreScrollView, pageControl, moreViewPlaceHolder,
            urlMore, offset, tempBtn, postMsgView, postView, shareView, commentsListView,
            titleView,titleViewDetPage,socBtn,post,increase,decrease;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self setUIPosOnOrientation];
}

-(void)setUIPosOnOrientation {
    
    if([self isLandscape])
    {
        refreshImageView.frame =  CGRectMake(360, 0, 32, 32);
    }
    else 
    {
        refreshImageView.frame =  CGRectMake(275, 0, 32, 32);
    }
    categoryBrowsView.hidden = FALSE;
    tempScrollView.frame = CGRectMake(0,0,0,0);
}
- (BOOL)isLandscape {
           return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
//Initialize View with header and category view
- (void)viewDidLoad {
	
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    titleView = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.frame];
    titleViewDetPage = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.frame];
    self.navigationItem.titleView = titleView;
    //Add Header Image Control
	UIImage *header = [UIImage imageNamed:@"iphone_vikatan_logo_ver_1.png"];
	headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(75, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
    //add header image to titleview
    [titleView addSubview:headerImageView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(75+header.size.width, 10, 75, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"செய்திகள்";
    lblTitle.textAlignment= UITextAlignmentLeft;
    [titleView addSubview:lblTitle];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(75+header.size.width, 10, 75, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:16];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"செய்திகள்";
    lblTitle.textAlignment= UITextAlignmentLeft;
    [titleViewDetPage addSubview:lblTitle];
    
    [ self showShare];
    [self showPostComment];
    
    //Add Referesh Image Control
   	refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
	refreshImageView.frame =  CGRectMake(275, 5, 32, 32);
    
	[refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:refreshImageView];
	
    //Add Comments Image Control
	commentsImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[commentsImageView setImage:[UIImage imageNamed:@"iphone_comment_box_ver_2.png"] forState:UIControlStateNormal];
	commentsImageView.frame =  CGRectMake(245, -30, 0, 0);
	[commentsImageView addTarget:self action:@selector(show_comments_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [titleViewDetPage addSubview:commentsImageView];
	
	//Add Back Button but hidden
	back = [UIButton buttonWithType:UIButtonTypeCustom];
	back.frame =  CGRectMake(5, 0, 0, 0);
    [titleViewDetPage addSubview:back];
    [back addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    closeView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,580,0,0)];
    closeView.backgroundColor = [UIColor darkGrayColor];
    
        
    //font size view
    fontView = [[UIView alloc] initWithFrame:CGRectMake(40,0,65,32)];
    UIButton *increase = [UIButton buttonWithType:UIButtonTypeCustom];
	[increase setImage:[UIImage imageNamed:@"A+_32.png"] forState:UIControlStateNormal];
	increase.frame =  CGRectMake(0, 0, 32, 32);
    increase.tag = 2;
	[increase addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
    [fontView addSubview:increase];
    
    UIButton *decrease = [UIButton buttonWithType:UIButtonTypeCustom];
	[decrease setImage:[UIImage imageNamed:@"A-_32.png"] forState:UIControlStateNormal];
	decrease.frame =  CGRectMake(34, 0, 32, 32);
    decrease.tag = 1;
	[decrease addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
    [fontView addSubview:decrease];
    [titleViewDetPage addSubview:fontView];
    //Pclaeholder of Thumbnail view of News article in news detail page
    categoryBrowseView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,738,KIPADWIDTH + 100,0)];
    scrollCategoryView.userInteractionEnabled = TRUE;
    self.navigationController.navigationBar.layer.shadowColor = [ [UIColor whiteColor ] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0,1.0);//, <#CGFloat height#>)
    self.navigationController.navigationBar.layer.shadowOpacity = 0.40;
	
    logMod = [[Login alloc] init];
    [self setUIPosOnOrientation];
    articleTitle = [[NSString alloc]init]; 
    //Send request to get news
	[self NewsGetRequest];
    [super viewDidLoad];
    
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}


// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //Stop loading Icon
    [ self StopLoading];
    
    //Show Alert on Network failure
    [self _showAlert:KINTERNETNOTAVBL];
    
}
/*
 On Successfull Completion of News Json Downloaded
 */
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    
    [self StopLoading];
    //get Response message
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
   // NSLog(@"PREM:%@",responseString);
    
    //
    
   // responseString = [responseString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //SHOW CATEGORY AND ARTICLES
   [self Show_CategoryView:( NSString *) responseString];
     
}

//Show Alert 
- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:title delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}

//Method to Send News Get Request to server
-(void) NewsGetRequest {
	
   // NSLog(@"@:Request");
	responseData = [[NSMutableData data] retain];
    NSURLRequest *request;
    //Stop loading process
    [self LoadingIcon];
	//Send http request
    request  = [NSURLRequest requestWithURL:[NSURL URLWithString:[logMod getDeviceDetails:KIPHNNEWSURL]]];
    
    NSLog(@"REQUEST:%@",request);
    
    
  	[[NSURLConnection alloc] initWithRequest:request delegate:self] ;
    // NSLog(KIPHNNEWSURL);
}

//On News Detail Page Load in webview 

-(void)webViewDidStartLoad:(UIWebView *) portal {
	[self StopLoading];
    [self LoadingIcon];
}
//On News Detail Page Loaded
- (void)webViewDidFinishLoad:(UIWebView *)webView {    
	
    [self StopLoading];	
    [UIView beginAnimations:@"webView" context:nil];
    webView.alpha = 1.0;
    [UIView commitAnimations];
}


//Main Method to show News Main page with Category and their articles
- (void) Show_CategoryView:(NSString*)responseString{

    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int ndx, imageWidth, titlesY = 0, categorySectionY = 10, xCoord = 0 ;
	NSString *titleString;
	NSInteger categoryid;
    UILabel *cateGoryTitle;
    //Initialize Json parser class
    SBJSON *jsonParser = [[SBJSON new] autorelease];
    //get reponse message

   	id response = [jsonParser objectWithString:responseString];
  //  NSLog(@"Rsponse %@:",responseString);
    NSLog(@"Error String %@",[jsonParser.errorTrace objectAtIndex:1] );
    NSDictionary *newsfeed = (NSDictionary *)response;
    //Load News Categories in a Array
    NSArray *categories = (NSArray *)[newsfeed valueForKey:KCATEGORY];
	NSDictionary *category;
        
    //Category Browse View in News Detail Page
    categoryBrowsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, KIPHNCATEBROWSELOC, self.view.bounds.size.width, KIPHNCATBRWBTNHGHT)];
    categoryBrowsView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [categoryBrowsView setBackgroundColor:[UIColor blackColor]];
    [categoryBrowsView setHidden:FALSE];
    //Category Main View
	CGRect mainContainer = CGRectMake(-5, 0, self.view.bounds.size.width + 5, self.view.bounds.size.height);
    [scrollCategoryView removeFromSuperview];
	scrollCategoryView = [[UIScrollView alloc] initWithFrame:mainContainer];
	scrollCategoryView.contentSize =CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height);
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
	scrollCategoryView.showsVerticalScrollIndicator = NO;
	scrollCategoryView.bounces = NO;
    
    //Article Main View
	CGRect subContainer = CGRectMake(0, 0, self.view.bounds.size.width - 58, KIPHNTHUMBIMGHEIGHT);
	scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
	scrollArticleView.contentSize = CGSizeMake(KIPHNWIDTH, KIPHNTHUMBIMGHEIGHT);
	scrollArticleView.showsHorizontalScrollIndicator = NO;
	scrollArticleView.bounces = YES;
	
	//if Empty News is returned then show error...	
    if(categories.count == 0 ) {
        [self _showAlert:KSERVERDOWN];
    }
	
	// loop over all the JSON and build news main page
	
	for (ndx = 0; ndx < categories.count ; ndx++) {
		
        subContainer = CGRectMake(17, categorySectionY , self.view.bounds.size.width- 13, KIPHNTHUMBIMGHEIGHT + 40);
        //Get Particular Category Array along with thir article details
		category = (NSDictionary *)[categories objectAtIndex:ndx];
		titleString = urlStringDecode([category valueForKey:KCATEGORYTITLE]);
		categoryid  = (NSInteger)[category valueForKey:KCATEGORYID];
        cateGoryTitle = [[[UILabel alloc] initWithFrame:CGRectMake(-32,30 , KIPHNTHUMBIMGHEIGHT - 10, 30)] autorelease];
		
		cateGoryTitle.text = titleString;cateGoryTitle.textAlignment = UITextAlignmentCenter;
        cateGoryTitle.numberOfLines = 1;cateGoryTitle.transform =CGAffineTransformMakeRotation( M_PI/-2 );// CATransform3DMakeRotation(90);//
		cateGoryTitle.backgroundColor = [UIColor clearColor];
		cateGoryTitle.textColor = [UIColor whiteColor];
		cateGoryTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
		cateGoryTitle.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
		cateGoryTitle.shadowOffset = CGSizeMake(0, -1.0);
		cateGoryTitle.tag = categoryid;
        [self addCategoryBrowse:cateGoryTitle:xCoord];
        xCoord = xCoord + 100;
        
		scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
		scrollArticleView.tag = categoryid;
		
        //Category Title Image shownin ledt
        CustomButton *titleImageView = [CustomButton buttonWithType:UIButtonTypeCustom];
        [titleImageView setImage:[UIImage imageNamed:@"iphone_category_title_bg_ver_1.png"] forState:UIControlStateNormal];
        titleImageView.frame =  CGRectMake(0, categorySectionY + 35, 25, 100);
        [titleImageView addSubview:cateGoryTitle];
        [titleImageView setCateGoryID:categoryid];
        [titleImageView setArticleCount:[category valueForKey:KARTICLECOUNT]];
        [titleImageView setCateGoryTitle:titleString];
        [titleImageView setCateGoryIDStr:[category valueForKey:KCATEGORYID]];
        [titleImageView setEnabled:FALSE];
        
        //Get Article List from Category Array
		NSArray *articles = (NSArray *)[category valueForKey:KARTICLE];
		imageWidth = 5 ;
		//Loop through individual articles in each category and build article thumb view
		for (int y = 0 ; y < articles.count; y++){
			
			NSDictionary *article = (NSDictionary *)[articles objectAtIndex:y];
            NSMutableString *urlString = [NSMutableString stringWithString:KIPADBPATH];
			CGRect frame;
			frame.size.width = KIPHNTHUMBIMGWIDTH; frame.size.height = KIPHNTHUMBIMGHEIGHT;// KIPADTHUMBIMGHEIGHT;
			frame.origin.x = imageWidth; frame.origin.y = 0;
            //Initialise Asynchronouse Imae=ge class ( Thumbview of article Image )
			AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                          initWithFrame:frame] ;
            asyncImage.moduleIndi = KCLNEWSMODL;
            asyncImage.resStatus = TRUE;
			asyncImage.tag = y;
            urlString = [NSMutableString stringWithString:KIPADBIMGPATH];
			asyncImage.categoryTitle = urlStringDecode([article valueForKey:KARTICLETITLE]);
            asyncImage.dateTime      = [article valueForKey:KARTICLEDATE];
            asyncImage.commentCount  = [article valueForKey:KARTCMNTCOUNT];
            asyncImage.articleDesc   = urlStringDecode([article valueForKey:KIPADARTICLEDESC]);
			if ([@"" isEqualToString:[article valueForKey:KARTICLEIMG]]) {
				asyncImage.imageURL = @"";
				[asyncImage showHalfArticle];
			}else {
                [urlString appendString: [article valueForKey:KARTICLEIMG]];
				asyncImage.imageURL = urlString;
			}	
            //Load Image
			[asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
			
			CustomButton *thumbNailView = [CustomButton buttonWithType:UIButtonTypeCustom];
			thumbNailView.backgroundColor = [UIColor clearColor];
			thumbNailView.frame = CGRectMake(frame.origin.x+1, frame.origin.y + 31, frame.size.width, frame.size.height + 1) ;
			thumbNailView.tag = [[article valueForKey:KARTICLEID] intValue];
            [thumbNailView setImage:[UIImage imageNamed: @"iphone_imgoutline_white_ver_1.png"] forState:UIControlStateHighlighted];
            urlString = [NSMutableString stringWithString:KIPADBPATH];
            [urlString appendString: [article valueForKey:KARTICLEURL]];
            [thumbNailView setCateGoryTitle:titleString];
			[thumbNailView setUrlString:urlString];
			[thumbNailView setCateGoryID:categoryid];
            [thumbNailView setCommentsCount:[article valueForKey:KARTCMNTCOUNT]];
            [thumbNailView setArticleID: [[article valueForKey:KARTICLEID] intValue]];
            [thumbNailView setArtWebURL:[article valueForKey:KARTICLEWEBURL]];
            [thumbNailView setArticleTitle:/*[article valueForKey:KARTICLETITLE]*/[[article valueForKey:KARTICLETITLE] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"Looping the article's: %@",[article valueForKey:KARTICLETITLE]);
            
            [thumbNailView addTarget:self action:@selector(article_clicked:) forControlEvents:UIControlEventTouchUpInside];
            [scrollArticleView addSubview:asyncImage];
			[scrollArticleView addSubview:thumbNailView];
           
			imageWidth = imageWidth + 101;
        }
         
		scrollArticleView.showsHorizontalScrollIndicator = NO;
		scrollArticleView.backgroundColor = [UIColor clearColor];
		scrollArticleView.contentSize = CGSizeMake(articles.count * 104, KIPHNTHUMBIMGHEIGHT + 10);
		scrollArticleView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        subContainer = CGRectMake(2, categorySectionY , self.view.bounds.size.width + 5  , KIPHNROWHEIGHT + 10);
        UIImage *rowImage = [UIImage imageNamed:@"iphone_form_bg_ver_1.png"];
        UIImageView *rowImageView = [[UIImageView alloc] initWithFrame:subContainer];
        rowImageView.image = rowImage;
        rowImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [scrollCategoryView addSubview:rowImageView];
        [scrollCategoryView addSubview:scrollArticleView];
        [scrollCategoryView addSubview:titleImageView];
         
		articles = nil;
        titlesY = titlesY + 130;
		categorySectionY = categorySectionY + 135 ;
    }
    
    
  
    
	scrollCategoryView.contentSize = CGSizeMake(self.view.bounds.size.width, 145*categories.count);
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
    categoryBrowsView.contentSize = CGSizeMake(102 * categories.count, 40);
    scrollCategoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:scrollCategoryView];
    
    //release used objects
	jsonParser = nil;
	[jsonParser release];
	newsfeed = nil;
	categories = nil;
	category = nil;
	responseString = nil;
    jsonParser = nil;
    [pool release];
}

// Post message View
- (void) PostMessage:(id)sender {
    
    [categoryBrowsView removeFromSuperview];
    [commentsListView removeFromSuperview];
    [self hideThumbScrollView];
    if(![self.view.subviews containsObject:postMsgView])
    {
        
        postMsgView = [[UITextView alloc] initWithFrame:CGRectMake	(5, 50,315, 350)];
        [postMsgView setBackgroundColor:[UIColor blackColor]];
        [[postMsgView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[postMsgView layer] setBorderWidth:5];
        [postMsgView setClipsToBounds: YES];
        [self.view addSubview:postMsgView];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,315,50)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        //[cmntHeader setBackgroundColor:[UIColor darkGrayColor]];
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[cmntHeader layer] setBorderWidth:5];
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];

         [commentTitle setTitle:@"உங்கள் கருத்து" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(100, 10, 130, 25);
        [cmntHeader addSubview:commentTitle];
        [postMsgView addSubview:cmntHeader];
        
        
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, 315, 325)] autorelease];
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;
        UIWebView *postWebView = [[UIWebView alloc] initWithFrame:CGRectMake(5, 0,315, 325)] ;
        postWebView.scalesPageToFit = NO;
        postWebView.delegate = self;
        
        [postMsgView  addSubview:postView];
        [postView addSubview:postWebView];
        postView.contentSize = CGSizeMake(315,325);// <#CGFloat height#>)
        
        NSMutableString *postUrl = [NSMutableString stringWithString:KIPHNPOSTCMNTSURL];//[urlMore appendString:categoryID];
        [postUrl appendString:articleID];
        
        NSMutableString *uriString = [[NSMutableString alloc] initWithString:[ logMod getDeviceDetails:postUrl]];
        
        NSLog(@"PREM URI:%@",uriString);
        
        NSURL* url = [[NSURL alloc] initWithString:uriString];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        [postWebView loadRequest:urlRequest];
        [postWebView setNeedsLayout];
        postWebView.scalesPageToFit = YES;
    }
}

//Show Comments View
- (void) show_comments_clicked:(id)sender {
    
    [postMsgView removeFromSuperview];
    [categoryBrowsView removeFromSuperview];
    [self hideThumbScrollView];
    if(![self.view.subviews containsObject:commentsListView])
    {
        commentsListView = [[UIView alloc] initWithFrame:CGRectMake(5,50,315,350)];
        [[commentsListView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[commentsListView layer] setBorderWidth:5];
        [commentsListView setClipsToBounds: YES];
        UIButton *cmnt = (UIButton*)sender;
        CommentView_iPad *viewController = [[CommentView_iPad alloc] init] ;
        
        viewController.articleID = [NSString stringWithFormat:@"%d", cmnt.tag];
        viewController.cmntCount = cmntCntTxt.text;
        viewController.resStatus = TRUE;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        UIView   *cmntBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,310,365)];
        [cmntBox addSubview:viewController.view];
            
        [commentsListView addSubview:cmntBox];
        [self.view addSubview:commentsListView];
         
    }
       // back.frame =  CGRectMake(10, 5, 0, 0);
}
- (void) close_clicked:(id)sender {

	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
	tempScrollView.frame = CGRectMake(-1300, 825,prevArticleViewFrame.size.width,prevArticleViewFrame.size.height);
    newsDetailTitle.frame = CGRectMake(0, 940, 175, 40);
    closeView.frame = CGRectMake(0,1000,175,50);
    [UIView commitAnimations];	
}


//Event on Article Clicked
- (void)article_clicked:(id)sender {
    
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [ self StopLoading];	
    commentsImageView.enabled = TRUE;
    CustomButton *urlButton = (CustomButton *)sender;
    [cmntCntTxt removeFromSuperview];
    [closeView removeFromSuperview];
    [back setImage:[UIImage imageNamed:@"arrow-back_32.png"] forState:UIControlStateNormal];
	back.frame = CGRectMake(0, 5, 32, 32);
    
    int framHght = 40;
    //if([self isLandscape])
    //    framHght = 290;
    
    if([urlButton.commentsCount intValue]>0) {
        
        commentsImageView.frame = CGRectMake(250, 0, 32, 35);
        cmntCntTxt = [[UILabel alloc] initWithFrame:CGRectMake(5, 4, 20, 20)]; //0, -1, 35, 27
        cmntCntTxt.backgroundColor = [UIColor clearColor];
        cmntCntTxt.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:9];
        cmntCntTxt.textColor = [UIColor whiteColor];
        cmntCntTxt.text = urlButton.commentsCount;
        cmntCntTxt.textAlignment= UITextAlignmentCenter;
        [commentsImageView addSubview:cmntCntTxt];
        commentsImageView.tag = urlButton.ArticleID;
        
    } else {
        commentsImageView.frame = CGRectMake(720, 10, 0, 0);
    }
       
	NSString *urlString = urlButton.UrlString;
    [urlString appendString:KIPHNDIM];
	NSMutableString *uriString = [[NSMutableString alloc] initWithString:urlString];
    
	NSURL* url = [[NSURL alloc] initWithString:[ logMod getDeviceDetails:uriString]];
    
    
    
    
	NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url]; NSLog(@"detail news url %@", urlString);
    
    NSLog(@"URL:%@",urlRequest);
    
    
	CGRect frame;
	frame.size.width = 50; frame.size.height = 25;
	frame.origin.x = 5; frame.origin.y = 2;
	if(detailView == FALSE){	
		
        detailNewsView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-framHght)] autorelease];
        detailNewsView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        //detailNewsView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, KIPHNWIDTH, 440)] autorelease];
        detailNewsView.backgroundColor = [UIColor blackColor];
		detailNewsView.userInteractionEnabled = TRUE;detailNewsView.delegate = self;
        detailNewsView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height );

		webView = [[CustomWebView alloc] initWithFrame:CGRectMake(0, 0,self.view.bounds.size.width, self.view.bounds.size.height-framHght + 100)] ;
       // webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;;
        [webView setBackgroundColor:[UIColor clearColor]];
        [webView setOpaque:NO];
        webView.scalesPageToFit = NO;
		webView.delegate = self;
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)]; 
        tgr.delegate = self;
        [webView addGestureRecognizer:tgr];
        [tgr autorelease];
		[self.view  addSubview:detailNewsView];
       // detailNewsView.contentSize = CGSizeMake(KIPHNWIDTH, KIPHNHEIGHT+50);
		[detailNewsView addSubview:webView];
        [self hideTabBarView];
        detailView = TRUE;
	}
	else {
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.7];
		tempScrollView.frame = CGRectMake(-1300, 825,prevArticleViewFrame.size.width,prevArticleViewFrame.size.height);
        closeView.frame = CGRectMake(0,1000,175,50);
		[UIView commitAnimations];		
	}
    webView.alpha = 0.5;
    [webView loadRequest:urlRequest];
	[webView setNeedsLayout];
   
	textFontSize = 120;
    [self.view addSubview:closeView];
    articleID = [[NSString alloc] initWithFormat:@"%d", 
                 urlButton.tag]; 
    //Select the particular btn
    int position = 0;
    for(UIView *subview in [categoryBrowsView subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
            if(subview.tag == urlButton.CateGoryID ) {
                [subview setBackgroundColor:[UIColor blackColor]];
                [categoryBrowsView setContentOffset:CGPointMake(position * 102,0) animated:YES];
                [self highlightCategory:subview];
            } else {
                [subview setBackgroundColor:[UIColor clearColor]];
            }
            position++;
        }
    }
    artWebURL = urlButton.artWebURL;
    articleTitle = urlButton.articleTitle;
    
    NSLog(@"Article Title is found to be: %@",articleTitle);
    
    [categoryBrowsView removeFromSuperview];
    self.navigationItem.titleView = titleViewDetPage;
     [pool release];
}

- (void)showPostComment {
   // if(![self.navigationController.navigationBar.subviews containsObject:postView])
   // {
    
    UIImage *img = [UIImage imageNamed:@"comments_post_32_ver_1.png"];

    postView = [[UIView alloc] initWithFrame:CGRectMake(280,5,32,32)];
    UIButton *post = [UIButton buttonWithType:UIButtonTypeCustom];
	[post setTitle:@"Post" forState:UIControlStateNormal];//<#(UIControlState)#>
	post.frame =  CGRectMake(0, 0, 32, 32);
    post.backgroundColor = [UIColor clearColor];
    [post setImage:img forState:UIControlStateNormal];
	[post addTarget:self action:@selector(PostMessage:) forControlEvents:UIControlEventTouchUpInside];
    [postView addSubview:post];
    [titleViewDetPage addSubview:postView];
    //[self.navigationController.navigationBar addSubview:postView];
    // [commentsListView removeFromSuperview];
   // }
    
}

- (void) showShare {
    
    //if(![self.navigationController.navigationBar.subviews containsObject:shareView])
    //{
    UIImage *img = [UIImage imageNamed:@"socialnetwork_32_ver_1.png"];
    shareView =  [[UIView alloc] initWithFrame:CGRectMake(215,0,37,32)];
    shareView.backgroundColor = [UIColor clearColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0,0,32,32);// <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    [btn setImage:img forState:UIControlStateNormal];
    [shareView addSubview:btn];
    [titleViewDetPage addSubview:shareView];
   // [self.navigationController.navigationBar addSubview:shareView];
    [btn addTarget:self action:@selector(showShare_Clicked:) forControlEvents:UIControlEventTouchUpInside];
   // [commentsListView removeFromSuperview];
    //}
    
}

- (void) showShare_Clicked:(id)sender {
    
    [self hideThumbScrollView];
    [categoryBrowsView removeFromSuperview];
    
    /*
    NSMutableString *webMainURL = [[NSMutableString alloc] initWithString:KIPADBIMGPATH];
    [webMainURL appendString:artWebURL];

    
    SHKItem *item = [SHKItem URL:[ NSURL URLWithString:webMainURL]  title:articleTitle];
    [item setShareType:SHKShareTypeURL];
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    [[SHK currentHelper] setRootViewController:self];
    [actionSheet showInView:self.view];
     */
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:SHARE_TITLE delegate:self cancelButtonTitle:CANCEL_BUTTON_TITLE destructiveButtonTitle:nil otherButtonTitles:SHARE_FACEBOOK, POST_TWITTER, SEND_EMAIL, nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    NSMutableString *webMainURL = [[NSMutableString alloc] initWithString:KIPADBIMGPATH];
    [webMainURL appendString:artWebURL];
    
    NSURL *url = [NSURL URLWithString:webMainURL];
    SHKItem *item = [SHKItem URL:url title:articleTitle
                     contentType:(SHKURLContentTypeUndefined)];
    [SHK setRootViewController:self];
    
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        if (buttonIndex == 0) { //[actionSheet destructiveButtonIndex]
            // Add Facebook Connect code here.
            [SHKFacebook shareItem:item];
        }
        if (buttonIndex == 1) {
            // Add Twitter code here.
            [SHKTwitter shareItem:item];
        }
        if (buttonIndex == 2) {
            // Add Email code here.
            
            [SHKMail shareItem:item];
        }
    }
    
    url=nil, item=nil, webMainURL=nil;
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return (interfaceOrientation == UIInterfaceOrientationPortrait);
	return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (void) HideCategoryBrowse {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    if(![self.view.subviews containsObject:categoryBrowsView])
    {
        if([self isLandscape])
            categoryBrowsView.frame = CGRectMake(0,260 ,categoryBrowsView.frame.size.width,categoryBrowsView.frame.size.height);
        else
            categoryBrowsView.frame = CGRectMake(0,screenHeight-80 ,categoryBrowsView.frame.size.width,categoryBrowsView.frame.size.height);
        [self.view addSubview:categoryBrowsView]; //400
        categoryBrowsView.hidden = FALSE;
    } else {
       // NSLog(@"else");
        
            categoryBrowsView.frame = CGRectMake(0,screenWidth ,categoryBrowsView.frame.size.width,categoryBrowsView.frame.size.height); //320
            [categoryBrowsView removeFromSuperview];
        
    }

}
- (void) hideThumbScrollView {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    tempScrollView.frame = CGRectMake(0, 1500,tempScrollView.frame.size.width,tempScrollView.frame.size.height);
    
    [UIView commitAnimations];
}
- (void) tap:(id)sender {
    // NSLog(@"position %f",categoryBrowsView.bounds.origin.y );
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    tempScrollView.frame = CGRectMake(0, 1500,tempScrollView.frame.size.width,tempScrollView.frame.size.height);
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    [self HideCategoryBrowse];
    [UIView commitAnimations];
    
}

- (void) tapMain:(id)sender {
    [self showTabBar];    
}

- (void) highlightCategory:(UIButton*)sender {
    
    [self disableSelection:categoryBrowsView];
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed: @"ipad_category_horizontal.png"] forState:UIControlStateNormal];
        [sender setSelected:NO];
    }else {
        [sender setImage:[UIImage imageNamed: @"ipad_category_horizontal_roll.png"] forState:UIControlStateSelected];
        [sender setSelected:YES];
    }

}

- (void) hightlightArticle:(UIButton*)artBtn {
    for(UIView *subview in [artBtn.superview subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
           // NSLog(@"asdasd");
            [artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
            [artBtn setSelected:NO];
        }
    }
    
    if ([artBtn isSelected]) {
        [artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
        [artBtn setSelected:NO];
    }else {
        [artBtn setImage:[UIImage imageNamed: @"ipad_imgoutline_white.png"] forState:UIControlStateSelected];
        [artBtn setSelected:YES];
    }
     
}
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord{
    
    newsDetailTitle = [UIButton buttonWithType:UIButtonTypeCustom];
   // [newsDetailTitle setImage:[UIImage imageNamed:@"titel_header.png"] forState:UIControlStateNormal];
    [newsDetailTitle setImage:[UIImage imageNamed:@"ipad_category_horizontal.png"] forState:UIControlStateNormal];
    [newsDetailTitle setImage:[UIImage imageNamed:@"ipad_category_horizontal_roll.png"] forState:UIControlStateHighlighted];
    newsDetailTitle.alpha  =0.5;
    //[newsDetailTitle setTitle:urlButton.text forState:UIControlStateNormal];
    newsDetailTitle.backgroundColor = [UIColor clearColor];
    newsDetailTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
    newsDetailTitle.frame =  CGRectMake(xCoord, 0, 100, 40);
    newsDetailTitle.tag = urlButton.tag;
    UILabel *cateGoryTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0,0 , 100, 30)] autorelease];
    cateGoryTitle.text = urlButton.text;cateGoryTitle.textAlignment = UITextAlignmentCenter;
    cateGoryTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:12];
    cateGoryTitle.numberOfLines = 1;
    [cateGoryTitle setTextColor:[UIColor whiteColor]];
    [cateGoryTitle setBackgroundColor:[UIColor clearColor]];
    [newsDetailTitle addSubview:cateGoryTitle];
    [newsDetailTitle addTarget:self action:@selector(expand_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [categoryBrowsView  addSubview:newsDetailTitle];
    NSLog(urlButton.text);
}
//Methos to Increase font size in webview component
- (IBAction)changeTextFontSize:(id)sender
{
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    [self hideThumbScrollView];
    UIButton *btn = (UIButton*)sender; 
    btn.enabled = FALSE;
    switch ([sender tag]) {
        case 1: // A-
            textFontSize = (textFontSize > 80) ? textFontSize -20 : textFontSize;
            break;
        case 2: // A+
            textFontSize = (textFontSize < 260) ? textFontSize + 20 : textFontSize;
            //textFontSize = textFontSize + 5;
            break;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", 
                          textFontSize];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    btn.enabled = TRUE;
}

- (void)expand_clicked:(id)sender {
    
    tempScrollView.frame = prevArticleViewFrame;
    [tempScrollView setBackgroundColor:[UIColor clearColor]];
	[scrollCategoryView addSubview:tempScrollView];
    UIButton *btnCatg = (UIButton*)sender;    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
	//newsDetailTitle.frame =  CGRectMake(-350, 955, 0, 0);
    //to test the category switch
    if(scrollCategoryView!= nil)
    {
        for(UIView *subview in [scrollCategoryView subviews]) 
        {
            if([subview isKindOfClass:[UIScrollView class]]) 
            { NSLog(@"Logging in Manu baby %i:",subview.tag);
                
                if(subview.tag == btnCatg.tag)
                {
                    [subview removeFromSuperview];
                    prevArticleViewFrame = subview.frame;
                    CGRect size;
                    if([self isLandscape]) {
                        //categoryBrowsView.frame = CGRectMake(0,690,self.view.bounds.size.width,40);  
                        size = CGRectMake(0, 130,self.view.bounds.size.width,subview.bounds.size.height);
                    }
                    else {
                        //categoryBrowsView.frame = CGRectMake(0,940,self.view.bounds.size.width,40);
                        size = CGRectMake(0, screenHeight-220,self.view.bounds.size.width,subview.bounds.size.height);
                    }
                    
                   // CGRect size = CGRectMake(0, 260,KIPHNWIDTH,subview.bounds.size.height);
                   // [tempScrollView removeFromSuperview];
                    tempScrollView = [[[UIView alloc] initWithFrame:size] autorelease];
                    tempScrollView = subview;
                    tempScrollView.frame = size;
                    //[self.view  addSubview:tempScrollView];	
                    [self.view  addSubview:tempScrollView];
                    [tempScrollView setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"bg_nw_vikatan_ipad.png"]]];
                    //tempScrollView.alpha = 0.8;
                    tempScrollView.layer.cornerRadius = 8;
                    tempScrollView.layer.masksToBounds = YES;
                }  
            }
        }
    }
    [self highlightCategory:btnCatg];
    //[self disableSelection2:tempScrollView];
    //UIButton *artBtn = [tempScrollView viewWithTag:[articleID intValue]];
    [UIView commitAnimations];
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
   
}


- (void) disableSelection:(UIScrollView*)mainView {
    
    for(UIView *subview in [mainView subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
            [subview setImage:[UIImage imageNamed: @"ipad_category_horizontal.png"] forState:UIControlStateNormal];
            [subview setSelected:NO];
        }
    }
    
}
- (void) disableSelection2:(UIView*)mainView {
    
    for(UIView *subview in [mainView subviews]) 
    {
        if([subview isKindOfClass:[CustomButton class]]) 
        {
           
            [subview setImage:[UIImage imageNamed: @"ipad_imgoutline_grey.png"] forState:UIControlStateNormal];
            [subview setSelected:NO];
        }
    }
    
}
- (void)back_clicked:(id)sender {
    
    [self StopLoading];	
    [cmntCntTxt removeFromSuperview];
    closeView.frame = CGRectMake(0,770,0,0);
    [closeView removeFromSuperview];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.7];
	[detailNewsView removeFromSuperview];
    [self showTabBar];
    tempScrollView.frame = prevArticleViewFrame;
    [tempScrollView setBackgroundColor: [UIColor clearColor]];
	[scrollCategoryView addSubview:tempScrollView];
	detailView = FALSE; 
    [commentsListView removeFromSuperview];
    [postMsgView removeFromSuperview];
    [categoryBrowsView removeFromSuperview];
    self.navigationItem.titleView = titleView;
    [UIView commitAnimations];
}
- (void)refresh_clicked:(id)sender {
	
    
    for(UIView *subview in [self.view subviews]) 
    {
        [subview removeFromSuperview];
    }
    tempScrollView = nil;
   	//[self LoadingIcon];
	[self NewsGetRequest];
	
}
- (void) LoadingIcon {
    
    //Initialize activity window for loading process
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//Set position
	[spinner setCenter:CGPointMake(KIPHNWIDTH/2.0, KIPHNHEIGHT/2.0)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	//Add the loading spinner to view
	[self.view addSubview:spinner];
	//Start animating
	[spinner startAnimating];
}
- (void) StopLoading {

    [spinner stopAnimating];
	[spinner removeFromSuperview];

}
//Hide the tab bar view
-(void) hideTabBarView{
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 1064, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 1064)];
        }
		
    }
	
    [UIView commitAnimations];
	
}
- (void) showTabBar {
	
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
       // NSLog(@"%@", view);
		
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, screenHeight-48, view.frame.size.width, view.frame.size.height)];
			
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, screenHeight-48)]; //432
        }
		
		
        
    }
	
    [UIView commitAnimations]; 
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[[webView subviews] objectAtIndex:0] touchesBegan:touches withEvent:event];
    
}


- (void) closepost_clicked:(id )sender {
    
    [postMsgView removeFromSuperview];
    [postMsgView release];
       
}

//Release used objects
- (void)didReceiveMemoryWarning {
    
	
	//[responseData release];
	/*
    [scrollCategoryView release];
	[scrollArticleView release];
	[tempScrollView release];
	//UI activity window with spinner
	[spinner release];
	[headerImageView release];
	[detailNewsView release];
	//[back release];
	[webView release];
	[refreshImageView release];
	[commentsImageView release];
    //[close release];
    [newsDetailTitle release];
    [headerView release];
    //[closeView release];
    [thumbTitle release];
    [categoryBrowseView release];
    [fontView release];
    [cmntCntTxt release];
    [moreViewPlaceHolder release];
   // [offset release];
    //[urlMore release];
     */
    [super didReceiveMemoryWarning];    
    // Release any cached data, images, etc that aren't in use.
	
}
//nullify the view objects
- (void)viewDidUnload {
	
	responseData = nil;
	scrollCategoryView = nil;
	scrollArticleView = nil;
	tempScrollView = nil;
	//UI activity window with spinner
	spinner = nil;
	headerImageView = nil;
	detailNewsView = nil;
	back = nil;
	webView = nil;
	refreshImageView = nil;
	commentsImageView = nil;
    close = nil;
    newsDetailTitle = nil;
    headerView = nil;
    closeView = nil;
    thumbTitle = nil;
    categoryBrowseView = nil;
    fontView = nil;
    cmntCntTxt = nil;
    moreViewPlaceHolder = nil;
    urlMore = nil;
    offset = nil;
    logMod = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(BOOL)shouldAutorotate
{
    //I don't want to support auto rotate, but you can return any value you want here
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
