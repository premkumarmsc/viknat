//
//  News_iPad.h
//  Vikatan
//
//  Created by MobileVeda on 13/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "asyncimageview.h"
#import "JSON.h"
#import "CustomButton.h"
#import "Constant.h"
#import "CommentView_iPad.h"
#import "CustomWebView.h"
#import "SHK.h"
#import <QuartzCore/QuartzCore.h>
#import "Utilities.h"
#import "Login.h"

#import "SHKMail.h"
#import "SHKTwitter.h"
#import "SHKFacebook.h"


@interface News_iPhone : UIViewController <UIWebViewDelegate, UIScrollViewDelegate, UIActionSheetDelegate> {
    
    NSMutableData *responseData;
	UIActivityIndicatorView *spinner; 
	UIImage *image;
	NSString *imageURL;
	NSString *articleTitle, *artWebURL;
	NSString *articleID;
	NSString *categoryID;
	NSString *categoryTitle;
    NSInteger offset;
	UIScrollView *scrollArticleView1;
    //View declaration for news view
    UIImageView *headerImageView;
    UIScrollView *detailNewsView;
    CustomWebView *webView;
    UIScrollView *scrollCategoryView,
                 *scrollArticleView,
                 *categoryBrowsView;
    UIView  *tempScrollView, 
            *closeView,
            *categoryBrowseView,
            *fontView;
    UIImageView *headerView;
    UIButton *refreshImageView , 
             *commentsImageView, 
             *newsDetailTitle,
             *back, *close,
             *thumbTitle,
             *helpImageView,
             *socBtn,*post,
             *increase,*decrease;
    
    UILabel *cmntCntTxt;
    bool detailView;
    
    CGRect prevArticleViewFrame;
    NSUInteger textFontSize;
    UIScrollView *moreScrollView;
    BOOL moreCLicked;
    UIPageControl *pageControl;
    UIView *moreViewPlaceHolder;
    NSMutableString *urlMore;
    //temp
    UIButton *tempBtn;
    UIView   *postMsgView, *postView, *shareView, *commentsListView,*titleView,*titleViewDetPage;
    int tagID;
    
    Login *logMod;
  
}
@property (nonatomic, retain) UIActivityIndicatorView *spinner;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, retain) UIView *moreViewPlaceHolder;
@property (nonatomic, retain) NSMutableString *urlMore;

@property (nonatomic, retain) UIImageView *headerImageView;
@property (nonatomic, retain) UIScrollView *detailNewsView;
@property (nonatomic, retain) UIButton *back,*tempBtn;
@property (nonatomic, retain) CustomWebView *webView;
@property (nonatomic, retain) UIScrollView *scrollCategoryView,
                                            *categoryBrowsView,
                                            *scrollArticleView,
                                            *moreScrollView;
@property (nonatomic, retain) UIView *tempScrollView,
                                     *closeView, 
                                     *categoryBrowseView,
                                     *fontView,
                                     *titleView,
                                     *titleViewDetPage;
@property (nonatomic, retain) UIButton *newsDetailTitle, *thumbTitle ;
@property (nonatomic, retain) UIImageView *headerView;
@property (nonatomic, retain) UIButton *refreshImageView, *helpImageView,*post,*socBtn,*increase,*decrease;
@property (nonatomic, retain) UIButton *commentsImageView;
@property (nonatomic, retain) UIButton *close;
@property (nonatomic, retain) UILabel *cmntCntTxt;
@property (nonatomic,retain) UIView   *postMsgView, *postView, *shareView, *commentsListView;
@property (nonatomic)NSInteger offset;



- (void) PostMessage:(id)sender;
- (void)showPostComment;
- (void) showShare;
- (void)loadImage;
- (void) myExceptionHandler :(NSException *)exception;
- (void) NewsGetRequest;
- (void) hideTabBarView;
- (void) showTabBar;
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord;
- (void) Show_MoreView:(NSString*)responseString;
- (void) nextPageURL;
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord;
- (void) disableSelection:(UIScrollView*)mainView;
- (void) disableSelection2:(UIView*)mainView;
- (void) highlightCategory:(UIButton*)sender;
- (void) _showAlert:(NSString*)title;
- (void) hightlightArticle:(UIButton*)artBtn;
- (void) LoadingIcon;
- (void) StopLoading;
- (void) HideCategoryBrowse;
- (void) hideThumbScrollView;
- (void) Show_CategoryView:( NSString *) responseString;
- (BOOL)isLandscape;
-(void)setUIPosOnOrientation;

@end
