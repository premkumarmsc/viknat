//
//  MagPostCmnt.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/09/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "MagPostCmnt.h"
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "Login.h"

@implementation MagPostCmnt
@synthesize articleID,magID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    int xWidth = self.view.bounds.size.width,
    yHeight = self.view.bounds.size.height, 
    widthSub = 100,heightSub = 200, headerXpos = 200,fontPos = 560;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        self.contentSizeForViewInPopover = CGSizeMake(self.view.bounds.size.width - 100, self.view.bounds.size.height - 200);
        CGSize contSize = CGSizeMake(xWidth - widthSub, yHeight - heightSub);
        
        UITextView *postMsgView = [[UITextView alloc] initWithFrame:CGRectMake	(0, 0,contSize.width, contSize.height)];
        [postMsgView setBackgroundColor:[UIColor blackColor]];
        
        [self.view addSubview:postMsgView];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,contSize.width,50)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        cmntHeader.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [commentTitle setTitle:@"உங்கள் கருத்து" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(headerXpos, 10, 300, 25);
        [cmntHeader addSubview:commentTitle];
        [postMsgView addSubview:cmntHeader];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, contSize.width,contSize.height)] autorelease];
        
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;
        UIWebView *postWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,contSize.width, contSize.height)] ;
        postWebView.scalesPageToFit = NO;
        postWebView.delegate = self;
        [postMsgView  addSubview:postView];
        [postView addSubview:postWebView];
        Login *loginMod = [[Login alloc]init];
        BOOL st = loginMod.isUserDetailsExists;
        NSMutableString *postUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",KMAGIPADPOSTCMNTSURL,articleID,@"&mid=",magID,@"&email_id=",loginMod.emailID,@"&password=",loginMod.password,@"&user_name=",loginMod.username];
       
        
        NSLog(@"Comment post url is %@", postUrl);
        
        NSMutableString *uriString = [[NSMutableString alloc] initWithString:[loginMod getDeviceDetails:postUrl]];
        NSURL* url = [[NSURL alloc] initWithString:uriString];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        
        [postWebView loadRequest:urlRequest];
        [postWebView setNeedsLayout];
        
      
    } else {
      //  xWidth = 320;
        yHeight = 440;
        widthSub = 0;
        heightSub = 0;
        headerXpos = 10;
        fontPos = 210;
        
        CGSize contSize = CGSizeMake(xWidth - widthSub, yHeight - heightSub);
        
        UITextView *postMsgView = [[UITextView alloc] initWithFrame:CGRectMake	(0, 0,contSize.width, contSize.height)];
        [postMsgView setBackgroundColor:[UIColor blackColor]];
        postMsgView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:postMsgView];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,contSize.width,50)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        cmntHeader.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        commentTitle.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [commentTitle setTitle:@"உங்கள் கருத்து" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(headerXpos, 10, 300, 25);
        [cmntHeader addSubview:commentTitle];
        [postMsgView addSubview:cmntHeader];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, contSize.width,contSize.height)] autorelease];
        postView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;
        UIWebView *postWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,contSize.width, contSize.height)] ;
        postWebView.scalesPageToFit = NO;
        postWebView.delegate = self;
        postWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [postMsgView  addSubview:postView];
        [postView addSubview:postWebView];
        
        Login *loginMod = [[Login alloc]init];
        BOOL st = loginMod.isUserDetailsExists;
        NSMutableString *postUrl = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",KMAGIPADPOSTCMNTSURL,articleID,@"&mid=",magID,@"&email_id=",loginMod.emailID,@"&password=",loginMod.password,@"&user_name=",loginMod.username];
       // NSLog(@"Comment post url is %@", postUrl);
        
        NSMutableString *uriString = [[NSMutableString alloc] initWithString:[loginMod getDeviceDetails:postUrl]];
        NSURL* url = [[NSURL alloc] initWithString:uriString];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        
        [postWebView loadRequest:urlRequest];
        [postWebView setNeedsLayout];
        
    }

       [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
@end
