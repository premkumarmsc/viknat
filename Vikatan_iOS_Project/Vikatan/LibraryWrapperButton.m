//
//  LibraryWrapperButton.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 19/08/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "LibraryWrapperButton.h"


@implementation LibraryWrapperButton
@synthesize MagID, IssueID,gridInd, issueDate, issueFolderPath;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
