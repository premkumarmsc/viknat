//
//  CustomUIButton.h
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 11/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperation.h"

@interface CustomUIButton : UIButton 
{

    UILabel *_btntitle;

}

@property (nonatomic, copy) UILabel * btntitle;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString *contentId;
@property (nonatomic, copy) NSString *downloadurl;
@property (nonatomic, copy) NSString *contentName;
@property (nonatomic, copy) NSString *contentDesc;
@property (nonatomic, copy) NSString *authorName;
@property (nonatomic, copy) NSString *categoryid;
@property (nonatomic, copy) NSString *categoryname;
@property (nonatomic, copy) NSString *contentDate;
@property (nonatomic, copy) NSString *contentFormat;
@property (nonatomic, copy) NSString *prod_identifier;
@property (nonatomic, copy) NSString *price, * downloadPath;
@property (nonatomic, copy) UIColor * color;
@property (nonatomic) int readStatus;
@property (nonatomic, retain) AFHTTPRequestOperation *dnwlOperation;


@end
