//
//  MagazineStore_iPad.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "Constant.h"
#import "asyncimageview.h"
#import "JSON.h"
#import "CustomButton.h"
#import "Utilities.h"
#import "DownloadControl.h"
#import "Preview_iPad.h"
#import "Login.h"
#import "CustomButton.h"
#import "Calendar_iPad.h"
#import "InappView.h"
#import <QuartzCore/QuartzCore.h>
//Coverflow
#import "TKCoverflowView.h"
#import "TKCoverflowCoverView.h"
#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>
//GridView
#import "AQGridView.h"
#import "ImageDemoCellChooser.h"
#import "ImageDemoGridViewCell.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "CustomUIButton.h"
#import "TestPDFReader.h"

#include "LoginView.h"

//Internet check
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"
#define kInAppPurchaseProUpgradeProductId @"com.vikatan.reader.avalsubscription"


@class ASINetworkQueue;


@interface MagazineStore_iPad : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver,LoginDelegate,UITextFieldDelegate,UIAlertViewDelegate, TKCoverflowViewDelegate,TKCoverflowViewDataSource,UIPopoverControllerDelegate,UIScrollViewDelegate,UINavigationControllerDelegate,LogDelegate,UIAlertViewDelegate,AQGridViewDelegate, AQGridViewDataSource,UIWebViewDelegate> {
    
    ASINetworkQueue *networkQueue;
    
    UIImageView *imageView1;
	UIImageView *imageView2;
	UIImageView *imageView3;
	UIProgressView *progressIndicator;
	UISwitch *accurateProgress;
	UIProgressView *imageProgressIndicator1;
	UIProgressView *imageProgressIndicator2;
	UIProgressView *imageProgressIndicator3;
	UILabel *imageLabel1;
	UILabel *imageLabel2;
	UILabel *imageLabel3;
	BOOL failed;
    DownloadControl *tmpDwnlBtn;
    
    NSMutableData *responseData;
    UIScrollView *scrollCategoryView,
                *scrollArticleView,
                *scrollArchCategoryView,
                *scrollArchArticleView;
    
    ///Login Authentication
    Login *loginMod;
    UIView *loginView;
    NSMutableArray *magzineList,
                   *magazineIDS,
                   *magProdIdnt,
                   *singleIssueState,
                   *singleIssuePrice,
                   *sub_promoList;

    BOOL magArchieve;
    
    int magazineID, 
    issueID,subid;
    NSString *subprice;
        
    NSInteger selYear;
    NSInteger selMonth;
    
    UIActivityIndicatorView *spinner, *dwnldSpinner,*webSpinner;
    
    //Coverflow view
    TKCoverflowView *coverflow; 
	NSMutableArray *covers, *coverImgList; // album covers images
    NSMutableDictionary *articleImgUrl;
    UIScrollView *belView;
	BOOL collapsed;
    
    UIView *inappView, *downloadView;
    UIScrollView *dwnldListView;
    
    UIScrollView *container;
    
    int maxDowncount;
    NSMutableArray *currentDwnldIDS; 
    UIView *titleView;
    UIButton *dwnldMgrBtn,*tempbtn;
    UIScrollView *subScriptionbtnView;
    UIView *serachView;
    InappView *productIdentView,*productIdentView1;
    
    UIPopoverController *popController;
    
    //Downlaod
    BOOL downLoadInProgress;
    BOOL singleSelected;
    
    NSString *selectedPrdIndt;
    
   ASIHTTPRequest *dwnRequest;
    
    UIScrollView *hidView;
    
    int currentCoverIndex, tempCurrentCoverIndex;
    NSString *issueDwnlTime, 
             *issueDate,
             *magazineName,
             *issValidFrm,
             *downldUrlString;
    
    
    ///Subs Detaisl
    NSMutableDictionary *singleIssDic;
    NSMutableDictionary *subIssDic;
    UIPopoverController *popoverController;
    UIView *disableView;
    
    UIButton *btn;
    
    //Grid View
    AQGridView * gridView;
    NSMutableArray * _imageNames, *showSub;
    CGSize thumbnailSize;// = CGSizeMake(162.f, 200.f);
    CGSize thumbnailMargin;//_ = CGSizeMake(10.f, 10.f);
    
    BOOL onLoad, transactionStatus,
                showsubStrip,isLogin, restoreallpurc;
    DownloadControl *downloadBtntmp;
    
    AFHTTPRequestOperation *downloadOperation;
    AFHTTPClient * apiClient;
    
    UIProgressView *imageProgressIndicator;
}

@property (nonatomic)NSInteger selYear;
@property (nonatomic)NSInteger selMonth;
@property (nonatomic)BOOL magArchieve, allowDownloadVersion;
@property (nonatomic, retain)ASIHTTPRequest *dwnRequest;
@property (nonatomic, retain)NSMutableDictionary *singleIssDic,*subIssDic;

@property (nonatomic, retain) UIScrollView *scrollCategoryView,
                                            *scrollArticleView,
                                            *scrollArchCategoryView,
                                            *scrollArchArticleView;

@property (nonatomic, retain) UIView *loginView,*disableView;
@property (nonatomic, retain) InappView *productIdentView;
@property(nonatomic, retain) UIPopoverController *popoverController;
//coverflow
@property (retain,nonatomic) TKCoverflowView *coverflow; 
@property (retain,nonatomic) NSMutableArray *covers;
@property (retain,nonatomic) NSString *selectedPrdIndt, *downloadManagerVersion;
//coverflow

@property(nonatomic, assign) CGSize thumbnailSize;

/// change margin around thumbnails. Defaults to 10x10
@property(nonatomic, assign) CGSize thumbnailMargin;

@property (retain, nonatomic) AFHTTPRequestOperation *downloadOperation;
@property (retain, nonatomic) AFHTTPClient *apiClient;
@property(nonatomic) int totalBytesExpectedToReadFromUrl, totalBytesReadFromUrl;

@property (nonatomic, retain) CustomUIButton * resumeBtn, * cancelBtn, * pauseBtn;

@property (nonatomic, retain) UIProgressView *imageProgressIndicator;

@property (nonatomic, retain) UILabel *DownloadTitle;


- (void)imageDownloadSart:(ASIHTTPRequest *)request;

- (void)imageFetchComplete:(ASIHTTPRequest *)request;

- (void)imageFetchFailed:(ASIHTTPRequest *)request;

- (void)getBooksMagaList;

- (void)buildStorePage:(NSString *)responseString;

- (void)startDownload;

- (void) _showAlert:(NSString*)title;

- (void) magazineOnLoad;

- (void) buildMagArchievesPage:(NSString *)responseString;

- (void) LoadingIcon;

- (void) StopLoading;

- (void) downldLoading ;

- (void) stopDownlLoading;

-(void) setImageToCovers:(int)highLightIss;

//In app purchase module
- (void) requestProductData:(NSArray*)ar;

- (void)purchaseProUpgrade;

- (void) LoadImageData:(NSString *)url;

- (void) productRequesting:(NSArray*)ar;

- (void) _showAlert:(NSString*)title;

- (void) buildDwnlMgr;

- (BOOL) findCurrentIssueDwnld:(NSInteger)issID;

-(void) removeObjectsOnTranFail;

- (void ) buildSearchView;

- (void) callBackArchive;
- (void) restorealltran;
- (void) callInapp;
//unzipContent:(ASIHTTPRequest *)request;

- (void) sendTransactionReceipt:(NSString*)data;

- (void) getProductList:(int)magID;

- (void) resetNetwork;

- (void)disableAction;

- (void) hideInappView;

-(void)showInAppList:(id)sender;

//GridView
- (void)initializeGridView;
@property (nonatomic, retain)  AQGridView * gridView;

- (BOOL) connectedToNetwork;

-(BOOL) checkInternet;

- (BOOL)isLandscape ;

- (void) showDwnldView;

- (void) createMagRootFolder;

- (void) closePops;

- (void) showLogin;

-(void) AFNetworkUnZipFile:(NSString *) destinationPath;

-(void) initiateAFDownloadRequest:(NSString *) downloadUrl destinyPath:(NSString *) destinyPath;

-(void) cancelProgressingDownload;

@end
