//
//  SocialNetworkWebViewViewController.m
//  Vikatan
//
//  Created by mobileveda on 03/02/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import "SocialNetworkWebViewViewController.h"

@interface SocialNetworkWebViewViewController ()

@end

@implementation SocialNetworkWebViewViewController

@synthesize webView, requestPath, email_id, socialFlag, l_status, popoverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*
     
     Social login form : http://api.vikatan.com/app_slogin.php
     
     Success login link : http://api.vikatan.com/app_slogin.php?email_id=dotcomproject@vikatan.com&source=vinoth%20kumar&l_status=success
     
     After success login call this link : http://api.vikatan.com/app_slogin.php?email_id=test@gmail.com&login_type=social
     
     */
    

   int xWidth = 768,headerHeight = 50, newUsrXpos = 175, headerYPos = 10;

    UIView   *cmntHeader;
    UIButton   *commentTitle;

    cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,headerHeight)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];
    [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[cmntHeader layer] setBorderWidth:5];
    [cmntHeader setClipsToBounds: YES];
    
    commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:@"Vikatan Social Login" forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(newUsrXpos, headerYPos, 300, 25);
    commentTitle.enabled = FALSE;
    

    if(IsIpad()) {
        [cmntHeader addSubview:commentTitle];
        [self.view addSubview:cmntHeader];
        commentTitle=nil, cmntHeader=nil;
    }

    webView =[[UIWebView alloc] init];
    webView.delegate =self;

    requestPath = SOCIAL_LOGIN_FORM;
    
    NSLog(@"REQUEST PATH:%@",requestPath);
    
    
    NSURL * url = [NSURL URLWithString:requestPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    
    load_back = [[UIView alloc] init];
    [load_back.layer setMasksToBounds:YES];
    [load_back.layer setCornerRadius:5.0f];
    [load_back.layer setBorderWidth:0.0f];
    load_back.backgroundColor = [UIColor blackColor];
    load_back.layer.shadowColor = [UIColor blackColor].CGColor;
    load_back.layer.shadowOpacity = 0.6f;
    load_back.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    load_back.layer.shadowRadius = 4.0f;
    load_back.layer.masksToBounds = NO;
    load_back.alpha = 0.5;
    [webView addSubview:load_back];
    load_back.hidden = TRUE;

    url=nil, [url release];
    request=nil, [request release];

    closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.backgroundColor = [UIColor redColor];
    closeBtn.titleLabel.text = @"Close";
    closeBtn.titleLabel.textColor = [UIColor whiteColor];
    [closeBtn addTarget:self action:@selector(closeWindow:) forControlEvents:UIControlEventTouchDown];
    
    
    if(IsIpad()) {
        closeBtn.frame = CGRectMake(self.view.frame.size.width-120, 10, 100, 40);
        webView.frame = CGRectMake(0.0, headerHeight+2, xWidth, 450);
        load_back.frame = CGRectMake(webView.frame.size.width/2-70, webView.frame.size.height/2-100, 140, 140);
    } else {
        closeBtn.frame = CGRectMake(5, 0, 100, 40);
        webView.frame = CGRectMake(0.0, 0, self.view.frame.size.width, self.view.frame.size.height-20);
        load_back.frame = CGRectMake(webView.frame.size.width/2-30, webView.frame.size.height/2-40, 100, 100);
    }

}

-(void) viewDidUnload {
    
    modalView = nil, [modalView release];
    webView = nil, [webView release];
    closeBtn=nil, [closeBtn release];
    email_id=nil, [email_id release];
    socialFlag=nil, [socialFlag release];
    l_status=nil, [l_status release];
    popoverController=nil, [popoverController release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)closeWindow:(id)sender {

    [self closeSocialViewController];
   
}

/*  Web view delegates  */


-(void) webViewDidStartLoad:(UIWebView *)webView {
    
    [self startLoading];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView {
    [self stopLoading];
}


// PREM WEB VIEW CONDITION //

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
        
    requestPath = [[request URL] absoluteString];
    
    NSMutableArray * requestURLArray = [[requestPath componentsSeparatedByString:@"?"] mutableCopy];
    
    NSLog(@"Request URL Array:%@",requestURLArray);
    
    
    if([requestURLArray [0] isEqualToString:@"http://api.vikatan.com/app_slogin.php"]) {
    
        URLParser *parser = [[[URLParser alloc] initWithURLString:requestPath] autorelease];
        
        NSLog(@"Par:%@",parser);
        
        
        email_id = [parser valueForVariable:@"email_id"];
        socialFlag = [parser valueForVariable:@"source"];
        l_status = [parser valueForVariable:@"l_status"];
        
        NSLog(@"EMAIL:%@",email_id);
        NSLog(@"FLAG:%@",socialFlag);
        NSLog(@"STATUS:%@",l_status);
        

        if([l_status isEqualToString:@"success"]) {
            if((![email_id isEqualToString:@""] && ![email_id isEqualToString:@"(null)"] && email_id != NULL) && (![socialFlag isEqualToString:@""] && socialFlag !=NULL) && (![l_status isEqualToString:@""] || l_status!=NULL)) {
                
                [email_id retain];
                [socialFlag retain];
                [l_status retain];
                [self closeSocialViewController];
            }
        }
        else if (![l_status isEqualToString:@"success"] && l_status != NULL && ![l_status isEqualToString:@""]) {
            
            NSString * msg = [parser valueForVariable:@"msg"];

            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Dear Guest" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            msg = nil, [msg release];
            [self dismissModalViewControllerAnimated:NO];
        }
    }

    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
       
        return YES;
    }
    return YES;
    
}

-(void) closeSocialViewController {
    
    Login *loginMod = [[Login alloc]init];
    LoginView * loginview = [[LoginView alloc] init];
    
    if(email_id!=NULL) {

       /* NSLog(@"email from Success URL is id %@", email_id);
        NSLog(@"socialFlag from Success URL is %@", socialFlag);
        NSLog(@"l_status from Success URL is %@", l_status);*/

        if( [loginMod startServerAuthentication:email_id:@"":socialFlag:@"social"]) {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"inforeferesh" object: nil];
            [loginview magStorereferesh];
            
            [[self navigationController] popViewControllerAnimated: YES];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Login Success" message:@"Login Success"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
            [loginview closePopViewContrl];
            
            if(IsIpad()) {
                [self.popoverController dismissPopoverAnimated:FALSE];
            } else {
            
                loginview.fromSocialLogin = TRUE;
                [loginview dismissModalViewControllerAnimated:TRUE];
            }
        } else {
            NSLog(@"startServerAuthentication not success");
        }
    }

    modalView = nil, [modalView release];
    webView = nil, [webView release];
    closeBtn=nil, [closeBtn release];
    email_id=nil, [email_id release];
    socialFlag=nil, [socialFlag release];
    l_status=nil, [l_status release];
    
    loginview.fromSocialLogin = TRUE;
    [self dismissModalViewControllerAnimated:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
    [self stopLoading];
}

/*  Starting of spinner loading delegates*/

-(void) startLoading {
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    spinner.frame = CGRectMake(load_back.frame.size.width/2-20, load_back.frame.size.height/2-10, 30, 30);
    spinner.activityIndicatorViewStyle =  UIActivityIndicatorViewStyleWhiteLarge;
    load_back.hidden =FALSE;
    [load_back addSubview:spinner];
    [webView bringSubviewToFront:load_back];
    [spinner startAnimating];
}

-(void) stopLoading {
    if([load_back.subviews containsObject:spinner]) {
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        [spinner setHidesWhenStopped:YES];
        load_back.hidden = TRUE;
    }
    [spinner release], spinner = nil;
}
/*  End of spinner loading delegates*/
@end
