//
//  Information_iPhone.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 21/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Login.h"
#import "MagazineLib.h"
#import "FeedBackForm.h"
#import "SHKMail.h"
#import "SHKTwitter.h"
#import "SHKSafari.h"
#import "SHKFacebook.h"

@interface Information_iPhone : UITableViewController<LoginDelegate,UIAlertViewDelegate, UIActionSheetDelegate> {
    
    NSMutableArray *listOfItems;
    UIView *loginView;
    UITextField *userTxt;
    UITextField *passTxt;
    BOOL restore, likeApp;
}

-(void) showLoginViewAndAuthenticateUser;
- (void) deletAllIssues;
- (void)closeModalView;
@end
