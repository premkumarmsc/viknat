//
//  Login.m
//  LoginAPI
//
//  Created by Saravanan Nagarajan on 28/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Login.h"
#import "Constant.h"
#import "NSData+Base64.h"

@implementation Login

@synthesize  delegate;
@synthesize logdelegate;
@synthesize fromInfo;
@synthesize username, password, 
sessionID, downLaoding,
emailID,userID,
UID, socialFlag;

-(BOOL) isUserDetailsExists {
    
    
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"UserDetails.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"UserDetails" ofType:@"plist"];
    }
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        return FALSE;
        
    } else {
        
        [self setUsername:[temp objectForKey:RESPONSE_USER_NAME]];
        [self setPassword:[temp objectForKey:RESPONSE_USER_PASSWORD]];
        [self setUserID:[temp objectForKey:RESPONSE_USER_ID]];
        [self setSessionID:[temp objectForKey:RESPONSE_SESSION_ID]];
        [self setEmailID:[temp objectForKey:RESPONSE_EMAIL_ID]];
        [self setSocialFlag:[temp objectForKey:RESPONSE_SOCIAL_FLAG]];

        //|| self.password == nil || [self.password isEqualToString:@""]

        if (self.username == nil || [self.username isEqualToString:@""] ) {
            return FALSE;
        }
        return TRUE;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
 
   // NSLog(@"viewDidAppear is coming inside of the login.m");
}

-(void) showLoginViewAndAuthenticateUser:(UIView*) currentView {
    
    int xWidth = 700, yWidth = 350, yPos = 50,
    txtUsrYPos = 100,txtUsrXPos = 145,txtPassYPos=170,txtPassXPos= 145,
    lblUsrYpos = 115,lblPassYpos = 185,loginYView = 34,loginXPos = 200
    ,txtlength = 300,newUsrXpos = 230;
    if (!PSIsIpad()) {
        xWidth = 310, yWidth = 350, yPos = 10,txtUsrYPos = 135,
        txtUsrXPos = 20,txtPassYPos=215,txtPassXPos= 20,lblUsrYpos = 100,
        lblPassYpos = 180,loginYView = 5,loginXPos = 100,txtlength = 250, 
        newUsrXpos = 100;
    }
    parentView = currentView;
    
    if(![parentView.subviews containsObject:loginView]) {
        
        loginView = [[UIView alloc] initWithFrame:CGRectMake(loginYView,yPos,xWidth,yWidth)];
        [loginView setBackgroundColor:[UIColor whiteColor]];
        [[loginView layer] setBorderColor:[[UIColor blackColor] CGColor]];
        [[loginView layer] setBorderWidth:5];
        [loginView setClipsToBounds: YES];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,50)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
        [[cmntHeader layer] setBorderWidth:5];
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        [commentTitle setTitle:@"Login" forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(290, 15, 100, 25);
        commentTitle.enabled = FALSE;
        [cmntHeader addSubview:commentTitle];
        [loginView addSubview:cmntHeader];
        [loginView addSubview:cmntHeader];
        
        UILabel *userLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,lblUsrYpos , 100, 30)] autorelease];
        userLbl.text = @"User Id";
        userLbl.textAlignment = UITextAlignmentLeft;
        userLbl.backgroundColor = [UIColor clearColor];
        userLbl.textColor = [UIColor darkGrayColor];
        userLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        [loginView addSubview:userLbl];
        
        userTxt = [[UITextField alloc] initWithFrame:CGRectMake	(txtUsrXPos, txtUsrYPos, txtlength, 43)];
        [[userTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[userTxt layer] setBorderWidth:2];
        [userTxt setBackgroundColor:[UIColor clearColor]];
        [userTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
        userTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        userTxt.textAlignment = UITextAlignmentCenter;
        //userTxt.placeholder = @"User Id";
        [userTxt setKeyboardType:UIKeyboardTypeEmailAddress];
        userTxt.returnKeyType = UIReturnKeyNext;
        userTxt.delegate = self;
        userTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
        userTxt.autocorrectionType = UITextAutocorrectionTypeNo;
        userTxt.keyboardType = UIKeyboardTypeDefault;  // type of the keyboard
        userTxt.returnKeyType = UIReturnKeyDone;  //
        [userTxt setClipsToBounds: YES];
        [userTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [loginView addSubview:userTxt];
        
        UILabel *passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,lblPassYpos , 100, 30)] autorelease];
        passLbl.text = @"Password";
        passLbl.textAlignment = UITextAlignmentLeft;
        passLbl.backgroundColor = [UIColor clearColor];
        passLbl.textColor = [UIColor darkGrayColor];
        passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        
        passTxt = [[UITextField alloc]initWithFrame:CGRectMake(txtPassXPos,txtPassYPos,txtlength,43)]; 
        [[passTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[passTxt layer] setBorderWidth:2];
        [passTxt setBackgroundColor:[UIColor clearColor]];
        [passTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
        passTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        passTxt.textAlignment = UITextAlignmentCenter;
        [passTxt setSecureTextEntry:YES];
        [passTxt setKeyboardType:UIKeyboardTypeDefault];
        passTxt.returnKeyType = UIReturnKeyDone;
       // passTxt.placeholder = @"Password";
        passTxt.delegate = self;
        passTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
        passTxt.autocorrectionType = UITextAutocorrectionTypeNo;
        passTxt.keyboardType = UIKeyboardTypeDefault;  // type of the keyboard
        passTxt.returnKeyType = UIReturnKeyDone;  // 
        [passTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [passTxt setClipsToBounds: YES];
        [loginView addSubview:passTxt];
        
        [loginView addSubview:passLbl];
        
        UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        loginBtn.backgroundColor = UIColorFromRGB(0x3399FF);//[UIColor darkGrayColor];
        loginBtn.frame = CGRectMake(loginXPos,260 , 100, 30) ;
        [loginBtn setTitle:@"Submit" forState:UIControlStateNormal];
        [loginBtn addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [loginView addSubview:loginBtn];
        
        UIButton *newUser = [[[UIButton alloc] initWithFrame:CGRectMake(newUsrXpos,310 , 300, 30)] autorelease];
        
        newUser.backgroundColor = [UIColor clearColor];
        UILabel *newUsrLabl = [[[UILabel alloc] initWithFrame:CGRectMake(0,0 , 300, 30)] autorelease];
        newUsrLabl.text = @"New User? Get registered here...";
        newUsrLabl.textColor = [UIColor darkGrayColor];
        [newUser addSubview:newUsrLabl];
        [newUser addTarget:self action:@selector(newUser:) forControlEvents:UIControlEventTouchUpInside];
        [loginView addSubview:newUser];
        
        UIButton   *loginClose = [UIButton buttonWithType:UIButtonTypeCustom];
        loginClose.frame =  CGRectMake(xWidth - 90, 15, 130, 25);
        [loginClose setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
        [loginClose addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [loginView addSubview:loginClose];
        
        [parentView addSubview:loginView];
        [parentView bringSubviewToFront:loginView];
        
    }
}

-(IBAction) loginButtonClicked:(id) sender {
    
    
    NSLog(@"LOGIN CLICK");
    
    if(![self checkInternet]) 
        return;
    activityIndicator.hidden = NO;
    username = userTxt.text;
    password = passTxt.text;
    
    if (username == nil || password == nil || [username isEqualToString:@""] || [password isEqualToString:@""] ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Invalid user, Please Try Again.." message:@"Authentication Failed"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        activityIndicator.hidden = YES;
    } else {
        [self startServerAuthentication:username:password:@"":@"main"];
    }
}

-(void)newUser:(id)sender
{

    [loginView removeFromSuperview];
    loginView = [[UIView alloc] initWithFrame:CGRectMake(34,50,700,350)];
    [loginView setBackgroundColor:[UIColor whiteColor]];
    [[loginView layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[loginView layer] setBorderWidth:5];
    [loginView setClipsToBounds: YES];
    
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,50)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[cmntHeader layer] setBorderWidth:5];
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:@"User Registration" forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(280, 10, 200, 25);
    commentTitle.enabled = FALSE;
    [cmntHeader addSubview:commentTitle];
    [loginView addSubview:cmntHeader];
    [loginView addSubview:cmntHeader];
    
    UILabel *userLblEmail = [[[UILabel alloc] initWithFrame:CGRectMake(20,80 , 130, 30)] autorelease];
    userLblEmail.text = @"User Email";
    userLblEmail.textAlignment = UITextAlignmentLeft;
    userLblEmail.backgroundColor = [UIColor clearColor];
    userLblEmail.textColor = [UIColor darkGrayColor];
    userLblEmail.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    userLblEmail.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    userLblEmail.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:userLblEmail];
    
    userTxtEmail = [[UITextField alloc] initWithFrame:CGRectMake	(200, 65, 300, 43)];
    [[userTxtEmail layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[userTxtEmail layer] setBorderWidth:2];
    [userTxtEmail setBackgroundColor:[UIColor clearColor]];
    [userTxtEmail setFont:[UIFont boldSystemFontOfSize:16.0]];
    userTxtEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTxtEmail.textAlignment = UITextAlignmentCenter;
    //userTxtEmail.placeholder = @"Email";
    [userTxtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    userTxtEmail.returnKeyType = UIReturnKeyNext;
    userTxtEmail.delegate = self;
    userTxtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    userTxtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    [userTxtEmail setClipsToBounds: YES];
    [userTxtEmail addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [loginView addSubview:userTxtEmail];
    
    
    UILabel *userLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,140 , 110, 30)] autorelease];
    userLbl.text = @"User Name";
    userLbl.textAlignment = UITextAlignmentLeft;
    userLbl.backgroundColor = [UIColor clearColor];
    userLbl.textColor = [UIColor darkGrayColor];
    userLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    userLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    userLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:userLbl];
    
    userTxt = [[UITextField alloc] initWithFrame:CGRectMake	(200, 125, 300, 43)];
    [[userTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[userTxt layer] setBorderWidth:2];
    [userTxt setBackgroundColor:[UIColor clearColor]];
    [userTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    userTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTxt.textAlignment = UITextAlignmentCenter;
    //userTxt.placeholder = @"User Name";
    [userTxt setKeyboardType:UIKeyboardTypeEmailAddress];
    userTxt.returnKeyType = UIReturnKeyNext;
    userTxt.delegate = self;
    userTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    userTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [userTxt setClipsToBounds: YES];
    [userTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [loginView addSubview:userTxt];
    
    UILabel *passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,200 , 100, 30)] autorelease];
    passLbl.text = @"Password";
    passLbl.textAlignment = UITextAlignmentLeft;
    passLbl.backgroundColor = [UIColor clearColor];
    passLbl.textColor = [UIColor darkGrayColor];
    passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    passLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    passLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:passLbl];
    
    passTxt = [[UITextField alloc]initWithFrame:CGRectMake(200,185,300,43)]; 
    [[passTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[passTxt layer] setBorderWidth:2];
    [passTxt setBackgroundColor:[UIColor clearColor]];
    [passTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    passTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    passTxt.textAlignment = UITextAlignmentCenter;
    [passTxt setSecureTextEntry:YES];
    [passTxt setKeyboardType:UIKeyboardTypeDefault];
    passTxt.returnKeyType = UIReturnKeyDone;
    //passTxt.placeholder = @"Password";
    passTxt.delegate = self;
    passTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [passTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [passTxt setClipsToBounds: YES];
    [loginView addSubview:passTxt];
    
    //Confirm Password
    passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,260 , 180, 30)] autorelease];
    passLbl.text = @"Confirm Password";
    passLbl.textAlignment = UITextAlignmentLeft;
    passLbl.backgroundColor = [UIColor clearColor];
    passLbl.textColor = [UIColor darkGrayColor];
    passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    passLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    passLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:passLbl];
    
    passTxt = [[UITextField alloc]initWithFrame:CGRectMake(200,245,300,43)]; 
    [[passTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[passTxt layer] setBorderWidth:2];
    [passTxt setBackgroundColor:[UIColor clearColor]];
    [passTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    passTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    passTxt.textAlignment = UITextAlignmentCenter;
    [passTxt setSecureTextEntry:YES];
    [passTxt setKeyboardType:UIKeyboardTypeDefault];
    passTxt.returnKeyType = UIReturnKeyDone;
    //passTxt.placeholder = @"Confirm Password";
    passTxt.delegate = self;
    passTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [passTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [passTxt setClipsToBounds: YES];
    [loginView addSubview:passTxt];

    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.backgroundColor = UIColorFromRGB(0x3399FF);//[UIColor darkGrayColor];
    loginBtn.frame = CGRectMake(200,300 , 100, 30) ;
    [loginBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(regButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginView addSubview:loginBtn];

    UIButton   *loginClose = [UIButton buttonWithType:UIButtonTypeCustom];
    loginClose.frame =  CGRectMake(610, 15, 130, 25);
    [loginClose setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [loginClose addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginView addSubview:loginClose];
    
    [parentView addSubview:loginView];
    
}

-(void)regButtonClicked:(id)sender
{

    NSLog(@"PREM REGISTER:");
    
    
    NSURL *requestURL = [NSURL URLWithString:@"http://api.vikatan.com/?module=reg"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
    [request setPostValue:userTxt.text forKey:REQUEST_FIRST_NAME];
    [request setPostValue:passTxt.text forKey:REQUEST_PASSWORD];
    [request setPostValue:userTxtEmail.text forKey:@"emailid"];
    [request setPostValue:userTxtEmail.text forKey:@"c_password"];
    [request startSynchronous];
    NSError *error = [request error];
    if (!error) {
        NSString *res = [request responseString];
       // NSLog(@"Response string is %@", res);
    }    
    
    
}

-(void)closeClicked:(id)sender
{
    
    [loginView removeFromSuperview];
}

-(IBAction)textFieldReturn:(id)sender {
    [sender resignFirstResponder];
}

-(BOOL) startServerAuthentication:(NSString *)reqUsername:(NSString *)reqPassword:(NSString *)reqSocialFlag :(NSString *) loginType {

    [self setUsername:reqUsername];
    [self setPassword:reqPassword];
    [self setSocialFlag:reqSocialFlag];

    BOOL flag = FALSE;
    NSString *responseString = [self getUserDetailsFromServer:loginType];
    
   // NSLog(@"Parse json %@", responseString);
    
    if (![responseString isEqualToString:@""]) {
        
        if ( [self parseResponseJSON:responseString])
        {
            [self storeUserDetailsInPList];
            
            flag = TRUE;

            return TRUE;
        } else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Authentication Failed" message:logErroMessage  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
            return FALSE;
            
        }
    } else {
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Authentication Failed" message:logErroMessage  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return FALSE;
    }
}

-(NSString *) getUserDetailsFromServer:(NSString *) loginType {

    /*NSLog(@"user name is %@", username);
    NSLog(@"password is %@", password);
    NSLog(@"socail flag is %@", socialFlag);
    NSLog(@"loginType is %@", loginType);*/

    NSURL *requestURL;

    NSString * deviceType = @"iphone";
    
    if(IsIpad()) {
        deviceType=@"ipad";
    }
    
    NSInteger wrapWidth = 20;
    NSData *inputData = [password dataUsingEncoding:NSUTF8StringEncoding];
    
    //encode
    NSString *encodedPassword = [inputData base64EncodedStringWithWrapWidth:wrapWidth];
    NSLog(@"encodedPassword is AA %@",encodedPassword);
    ASIFormDataRequest *request;
    
    NSLog(@"PREM:LOGIN:");
    
    
    if([loginType isEqualToString:@"main"]) {
        requestURL = [NSURL URLWithString:SERVER_AUTH_URL];
        request = [ASIFormDataRequest requestWithURL:requestURL];
        [request setPostValue:encodedPassword forKey:REQUEST_PASSWORD];
        [request setPostValue:username forKey:REQUEST_EMAIL_ID];
        
        NSLog(@"SERVER:%@",SERVER_AUTH_URL);
        NSLog(@"SERVER:%@",REQUEST_PASSWORD);
        NSLog(@"SERVER:%@",REQUEST_EMAIL_ID);
        
        
    }
    else
    {
        
        
        NSLog(@"SSOCIAL LOGIN:%@",SOCIAL_LOGIN_URL);
        NSLog(@"REQUEST_EMAIL_ID :%@",REQUEST_EMAIL_ID);
        NSLog(@"username:%@",username);
        
        
        NSString * socialLoginUrl = [NSString stringWithFormat:@"%@&%@=%@", SOCIAL_LOGIN_URL, REQUEST_EMAIL_ID, username];
        
        NSLog(@"PREM SOCIAL:%@",socialLoginUrl);
        
        
        requestURL = [NSURL URLWithString:socialLoginUrl];
        request = [ASIFormDataRequest requestWithURL:requestURL];
    }

    [request setPostValue:@"ios" forKey:@"platform"];
    [request setPostValue:KAPPVERSIONNO forKey:@"v"];
    [request setPostValue:deviceType forKey:@"device"];
    [request setPostValue:[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueIdentifier]] forKey:@"udid"];
    
    
    
    NSLog(@"UDIT:%@",[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueIdentifier]] );
    NSLog(@"UDIT:%@",deviceType );
     NSLog(@"UDIT:%@",KAPPVERSIONNO );

    [request startSynchronous];
    
    inputData=nil, [inputData release];
    encodedPassword=nil, [encodedPassword release];
    
    NSError *error = [request error];
    if (!error) {
        NSString *res = [request responseString];
       NSLog(@"Response string %@", res);
        return res;
    }    
    return @"";
}

-(BOOL)parseResponseJSON:(NSString *)responseString {
    
    if(responseString !=@"" || responseString !=nil)
    {
        SBJSON *jsonParser = [[SBJSON new] autorelease];
        id response = [jsonParser objectWithString:responseString];
        NSDictionary *newsfeed = (NSDictionary *)response;
        
        
        NSLog(@"NEWS FEED:%@",newsfeed);
        
        NSMutableArray *categories = (NSMutableArray *)[newsfeed valueForKey:@"udetail"];
        NSString *subString = [NSString stringWithFormat:@"%@",[newsfeed valueForKey:@"err"]];
        
        NSLog(@"Login error value: %@",subString);

        if(!categories || categories.count==0){
            logErroMessage = subString;
            NSLog(@"Login error buddy: %@",[newsfeed valueForKey:@"udetail"]);
            return FALSE;
        }
        NSDictionary *object = (NSDictionary *)[categories objectAtIndex:0];
        if([object count]>0 ) {
            
            NSInteger wrapWidth = 20;
            NSData *inputData = [[object objectForKey:RESPONSE_USER_PASSWORD] dataUsingEncoding:NSUTF8StringEncoding];
            
            //encode
            NSString *encodedPassword = [inputData base64EncodedStringWithWrapWidth:wrapWidth];
           // NSLog(@"encodedPassword in login.m file is %@", encodedPassword);
    
            [self setUsername:[object objectForKey:RESPONSE_USER_NAME]];
            [self setSessionID:[object objectForKey:RESPONSE_SESSION_ID]];   
            [self setUserID:[object objectForKey:RESPONSE_USER_ID]];
            [self setPassword:encodedPassword];
            [self setEmailID:[object objectForKey:RESPONSE_EMAIL_ID]];
            [self setSocialFlag:@""]; //[object objectForKey:RESPONSE_Social]
            
            encodedPassword=nil, [encodedPassword release];
            inputData=nil, [inputData release];

            
        } else {
            downLaoding = TRUE;
        }
    } else {
        downLaoding = TRUE;
    }
    return  TRUE;
}
-(void) storeUserDetailsInPList{
        
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"UserDetails.plist"];
    NSDictionary *plistDict = [NSDictionary dictionaryWithObjects:
                               [NSArray arrayWithObjects: self.username, self.password,  self.emailID, self.sessionID, self.userID, self.socialFlag, nil]
                                                          forKeys:[NSArray arrayWithObjects: RESPONSE_USER_NAME, RESPONSE_USER_PASSWORD,  RESPONSE_EMAIL_ID, RESPONSE_SESSION_ID,RESPONSE_USER_ID, RESPONSE_SOCIAL_FLAG, nil]];
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
        NSLog(@"PList updated successfully");
    }
    else {
       // NSLog(@"Error while writing PList : %@", error);
        [error release];
    }
}

-(BOOL) logout {
    
    [self setUsername:@""];
    [self setPassword:@""];
    [self setEmailID:@""];
    [self setUserID:@""];
    [self setSessionID:@""];
    [self setSocialFlag:@""];
    [self storeUserDetailsInPList];
    [self magStorereferesh];
    return TRUE;
}

- (void) magStorereferesh {
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"magazinereferesh" object: nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"magazinerefereshiphone" object: nil];
    }
}
- (BOOL) connectedToNetwork
{
    
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.vikatan.com/"]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
} 

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service unavailable" message:@"Check your connection, network unavailable."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return NO;
	}
	else {
		return YES;
	}
}

-(NSString*) getDeviceDetails:(NSString*) url {
    
    [self isUserDetailsExists];  NSLog(@"before URL REQUEST %@", url);
    NSString *devName = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] name]];
    NSString *devOs = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] name]];  
    NSString *userName = [[self username] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    devName= (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                 NULL,
                                                                 (CFStringRef)[[UIDevice currentDevice] name],
                                                                 NULL,
                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]%",
                                                                 kCFStringEncodingUTF8 );
    devOs= (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                               NULL,
                                                               (CFStringRef)[[UIDevice currentDevice] systemName],
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]%",
                                                               kCFStringEncodingUTF8 );
    
    NSString *userDet = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                                                    @"&email_id=",[self emailID],
                                                    @"&password=",[self password],
                                                    @"&user_name=",userName,
                                                    @"&social=", [self socialFlag]];
    
    NSString *devDet = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@",url,@"&platform=ios&v=1.4",userDet,
                        @"&udid=",
                        [[UIDevice currentDevice] uniqueIdentifier],
                        @"&devicename=",
                        devName,@"&deviceos=",
                        devOs];
   
    devName = nil, devOs = nil, userName = nil;

    NSLog(@"FINAL URL REQUEST %@", devDet);
    return devDet;//
    
}

-(NSMutableDictionary *) getDeviceDetails {
    
    [self isUserDetailsExists];  //NSLog(@"before URL REQUEST %@", url);

    NSString *devName = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] name]];
    NSString *devOs = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] name]];
    NSString *userName = [[self username] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    devName= (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                 NULL,
                                                                 (CFStringRef)[[UIDevice currentDevice] name],
                                                                 NULL,
                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]%",
                                                                 kCFStringEncodingUTF8 );
    devOs= (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                               NULL,
                                                               (CFStringRef)[[UIDevice currentDevice] systemName],
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]%",
                                                               kCFStringEncodingUTF8 );
    
    NSString *userDet = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",@"email_id=",[self emailID],
                         @"&password=",[self password],
                         @"&user_name=",userName,
                         @"&social=", [self socialFlag],
                         @"&user_id=", [self userID],
                         @"&session_id=",[self sessionID]];

    NSString *devDet = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",
                        @"&platform=ios&v=1.4",
                       
                        @"&udid=", [[UIDevice currentDevice] uniqueIdentifier],
                        @"&devicename=", devName,
                        @"&deviceos=",devOs];
    

    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setValue:userDet forKey:@"userDet"];
    [dict setValue:devDet forKey:@"devDet"];
    [dict setValue:[self emailID] forKey:@"email_id"];
    [dict setValue:[self password] forKey:@"password"];
    [dict setValue:userName forKey:@"user_name"];
    [dict setValue:[self socialFlag] forKey:@"social"];
    [dict setValue:[self userID] forKey:@"userID"];
    [dict setValue:[self sessionID] forKey:@"session_id"];

    devName = nil, devOs = nil, userName = nil;
    
    return dict;
}


@end
