//
//  FeedBackForm.m
//  Vikatan
//
//  Created by mobileveda on 09/02/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import "FeedBackForm.h"

@interface FeedBackForm ()

@end

@implementation FeedBackForm

@synthesize userName, eMail, feedBack, popoverController, isPresentedFromPopOver;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
   	UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
	UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:
                                    CGRectMake(0, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-20, 5, 100, 20)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"விகடன்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];   
    CGRect screenBound1 =  [[UIScreen mainScreen] applicationFrame];

    NSLog(@"rect3: %@", NSStringFromCGRect(screenBound));
    NSLog(@"rect4: %@", NSStringFromCGRect(screenBound1));

    UIView   *cmntHeader;
    UIButton   *commentTitle;
    int xWidth = screenBound.size.width,headerHeight = 50, newUsrXpos = screenBound.size.width/2-150, headerYPos = 10;

    cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,headerHeight)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];
    [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[cmntHeader layer] setBorderWidth:5];
    [cmntHeader setClipsToBounds: YES];
    cmntHeader.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:@"App Feedback" forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(newUsrXpos, headerYPos, 300, 25);
    commentTitle.enabled = FALSE;
    commentTitle.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin+UIViewAutoresizingFlexibleRightMargin;

    if(IsIpad()) {
        [cmntHeader addSubview:commentTitle];
        [self.view addSubview:cmntHeader];
        commentTitle=nil, cmntHeader=nil;
        
         self.navigationItem.titleView = titleView;
    }
}

-(void) viewDidAppear:(BOOL)animated {
     [self setFeedBackForm];
}

-(void) viewDidUnload {

    formView =nil, [formView release];
    userName=nil, [userName release];
    eMail=nil, [eMail release];
    feedBack=nil, [feedBack release];
    submitBtn = nil, [submitBtn release];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    int xWidth = screenBound.size.width;
    
    int txtlength, loginbtn; 
    int mesglblheigth = 20; //100
    
    int userNametxtXpos =180, emailtxtXpos = 180, feedBacktxtXpos=180;    
    int userNamelblYpos=mesglblheigth;
    int userNamelblheight = 30, userNametxtYpos = userNamelblYpos;
    int userNametxtheight = 43;
    
    int emaillblYpos = mesglblheigth + userNamelblheight + userNametxtheight;
    
    int emailtxtYpos = emaillblYpos;
    int emailtxtheigth = 43;
    
    int feedbacklblYpos = emailtxtYpos+emailtxtheigth+20;
    
    int feedBacktxtYpos = feedbacklblYpos;
    int feedbacktxtheight = 300;
    
    loginbtn = feedBacktxtYpos+feedbacktxtheight+15;
    
    if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight){
        // set contents' frame for landscape
        
        txtlength = xWidth-180;

        userName.frame = CGRectMake(userNametxtXpos, userNametxtYpos, txtlength, userNametxtheight);
        eMail.frame = CGRectMake(emailtxtXpos, emailtxtYpos, txtlength, emailtxtheigth);
        feedBack.frame = CGRectMake(feedBacktxtXpos,feedBacktxtYpos,txtlength,feedbacktxtheight);
        submitBtn.frame = CGRectMake(emailtxtXpos + txtlength/2 - 50,loginbtn , 100, 30) ;
    }
    
    else {
        txtlength = xWidth-380;
        
        userName.frame = CGRectMake(userNametxtXpos, userNametxtYpos, txtlength, userNametxtheight);
        eMail.frame = CGRectMake(emailtxtXpos, emailtxtYpos, txtlength, emailtxtheigth);
        feedBack.frame = CGRectMake(feedBacktxtXpos,feedBacktxtYpos,txtlength,feedbacktxtheight);
        submitBtn.frame = CGRectMake(emailtxtXpos + txtlength/2 - 50,loginbtn , 100, 30);
        
    }
    
    // Return YES for supported orientations
	return YES;
}

-(void) setFeedBackForm {
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    int xWidth = screenBound.size.width;

    int yWidth = screenBound.size.height-100, yPos = 50, xStartPos = 20,
    loginYView = 0,loginXPos = 240, txtlength = xWidth-380,loginbtn = 350;
    
    int mesglblheigth = 20; //100
    
    int userNamelblXpos= 50, emaillblXpos = 20, feedbacklblXpos=20;
    int userNametxtXpos =180, emailtxtXpos = 180, feedBacktxtXpos=180;
    
    int userNamelblYpos=mesglblheigth;
    int userNamelblwidth = 100, userNamelblheight = 30, 
        userNametxtYpos = userNamelblYpos;
    int userNametxtheight = 43;

    int emaillblYpos = mesglblheigth + userNamelblheight + userNametxtheight;
    int emaillblwidth  = 100, emaillblheigth = 30;

    int emailtxtYpos = emaillblYpos;
    int emailtxtheigth = 43;
    
    int feedbacklblYpos = emailtxtYpos+emailtxtheigth+20;
    int feedbacklblwidth  = 100, feedbacklblheigth = 30;
    
    int feedBacktxtYpos = feedbacklblYpos;
    int feedbacktxtheight = 300, msgfontsize = 16, msglbl=30;

    loginbtn = feedBacktxtYpos+feedbacktxtheight+15;

    if (!([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)) {

        xWidth = screenBound.size.width, yWidth = screenBound.size.height, yPos = 0, loginYView = 0,loginXPos = 100,txtlength = 270, xStartPos = 20;

        userNamelblXpos = userNametxtXpos = emaillblXpos = emailtxtXpos = feedBacktxtXpos = feedbacklblXpos= 20;
        userNamelblYpos =mesglblheigth-10;
        
        userNametxtYpos= mesglblheigth+userNamelblheight;

        emaillblYpos =userNametxtYpos+userNametxtheight;
        emailtxtYpos= emaillblYpos + emaillblheigth;

        feedbacklblYpos = emailtxtYpos+emailtxtheigth;
        feedbacklblwidth  = 100, feedbacklblheigth = 30;

        feedBacktxtYpos = feedbacklblYpos+feedbacklblheigth;
        feedbacktxtheight = 100, msgfontsize = 15, msglbl=0;

        loginbtn = feedBacktxtYpos+feedbacktxtheight+10;
    } 

    formView = [[UIView alloc] initWithFrame:CGRectMake(loginYView,yPos,xWidth,yWidth)];
    [formView setBackgroundColor:[UIColor whiteColor]];
    [formView setClipsToBounds: YES];

    /*UILabel *mesg = [[[UILabel alloc] initWithFrame:CGRectMake(userNamelblXpos, 0, xWidth-30, mesglblheigth)] autorelease];
    mesg.text = KLOGMSG;
    mesg.textAlignment = UITextAlignmentLeft;
    mesg.backgroundColor = [UIColor clearColor];
    mesg.textColor = [UIColor darkGrayColor];
    mesg.lineBreakMode = UILineBreakModeWordWrap;
	mesg.numberOfLines = 4;
    mesg.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:msgfontsize];
    [formView addSubview:mesg];*/

    UILabel *userNamelbl = [[[UILabel alloc] initWithFrame:CGRectMake(userNamelblXpos,userNamelblYpos, userNamelblwidth, userNamelblheight)] autorelease];
    userNamelbl.text = @"User Name";
    userNamelbl.textAlignment = UITextAlignmentLeft;
    userNamelbl.backgroundColor = [UIColor clearColor];
    userNamelbl.textColor = [UIColor darkGrayColor];
    userNamelbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    userNamelbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    userNamelbl.shadowOffset = CGSizeMake(0, -1.0);

    userName = [[UITextField alloc] initWithFrame:CGRectMake(userNametxtXpos, userNametxtYpos, txtlength, userNametxtheight)];
    [[userName layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[userName layer] setBorderWidth:2];
    [userName setBackgroundColor:[UIColor clearColor]];
    [userName setFont:[UIFont boldSystemFontOfSize:16.0]];
    userName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userName.textAlignment = UITextAlignmentLeft;
    userName.placeholder = @"";
    [userName setKeyboardType:UIKeyboardTypeEmailAddress];
    userName.returnKeyType = UIReturnKeyNext;
    userName.delegate = self;
    //userName.clearButtonMode = UITextFieldViewModeWhileEditing;
    userName.autocorrectionType = UITextAutocorrectionTypeNo;
    [userName setClipsToBounds: YES];
    userName.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    [userName addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];

    UILabel *emailLbl = [[[UILabel alloc] initWithFrame:CGRectMake(userNamelblXpos,emaillblYpos , emaillblwidth, emaillblheigth)] autorelease];
    emailLbl.text = @"Email Id";
    emailLbl.textAlignment = UITextAlignmentLeft;
    emailLbl.backgroundColor = [UIColor clearColor];
    emailLbl.textColor = [UIColor darkGrayColor];
    emailLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    emailLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    emailLbl.shadowOffset = CGSizeMake(0, -1.0);

    eMail = [[UITextField alloc] initWithFrame:CGRectMake(emailtxtXpos, emailtxtYpos, txtlength, emailtxtheigth)];
    [[eMail layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[eMail layer] setBorderWidth:2];
    [eMail setBackgroundColor:[UIColor clearColor]];
    [eMail setFont:[UIFont boldSystemFontOfSize:16.0]];
    eMail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    eMail.textAlignment = UITextAlignmentLeft;
    eMail.placeholder = @"";
    [eMail setKeyboardType:UIKeyboardTypeEmailAddress];
    eMail.returnKeyType = UIReturnKeyNext;
    eMail.delegate = self;
    //eMail.clearButtonMode = UITextFieldViewModeWhileEditing;
    eMail.autocorrectionType = UITextAutocorrectionTypeNo;
    [eMail setClipsToBounds: YES];
    eMail.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    [eMail addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];

    UILabel *feedbackLbl = [[[UILabel alloc] initWithFrame:CGRectMake(userNamelblXpos,feedbacklblYpos , feedbacklblwidth, feedbacklblheigth)] autorelease];
    feedbackLbl.text = @"Feedback";
    feedbackLbl.textAlignment = UITextAlignmentLeft;
    feedbackLbl.backgroundColor = [UIColor clearColor];
    feedbackLbl.textColor = [UIColor darkGrayColor];
    feedbackLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    feedbackLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    feedbackLbl.shadowOffset = CGSizeMake(0, -1.0);
    

    feedBack = [[UITextView alloc] initWithFrame:CGRectMake(feedBacktxtXpos,feedBacktxtYpos,txtlength,feedbacktxtheight)];
    [[feedBack layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[feedBack layer] setBorderWidth:2];
    [feedBack setBackgroundColor:[UIColor clearColor]];
    [feedBack setFont:[UIFont boldSystemFontOfSize:16.0]];
    feedBack.textAlignment = UITextAlignmentLeft;
    [feedBack setSecureTextEntry:NO];
    [feedBack setKeyboardType:UIKeyboardTypeDefault];
    feedBack.returnKeyType = UIReturnKeyDone;
    feedBack.delegate = self;
    feedBack.autocorrectionType = UITextAutocorrectionTypeNo;
    feedBack.layer.sublayerTransform = CATransform3DMakeTranslation(3, 0, 0);
    //[feedBack addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [feedBack setClipsToBounds: YES];

    submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
    submitBtn.frame = CGRectMake(xWidth/2 - 60,loginbtn , 100, 30) ;
    [submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [submitBtn.layer setMasksToBounds:YES];
    [submitBtn.layer setCornerRadius:5.0f];
    [submitBtn.layer setBorderWidth:1.0f];
    
    Login *loginMod = [[Login alloc]init];

    if(loginMod.isUserDetailsExists)
    {
        userName.text = loginMod.username;
        eMail.text =  loginMod.emailID;
    }

    if(!isPresentedFromPopOver && IsIpad()) {
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight){
            
            txtlength = xWidth-180;
            
            userName.frame = CGRectMake(userNametxtXpos, userNametxtYpos, txtlength, userNametxtheight);
            eMail.frame = CGRectMake(emailtxtXpos, emailtxtYpos, txtlength, emailtxtheigth);
            feedBack.frame = CGRectMake(feedBacktxtXpos,feedBacktxtYpos,txtlength,feedbacktxtheight);
            submitBtn.frame = CGRectMake(xWidth/2 + 30,loginbtn , 100, 30) ;
        }
        
        else {
            txtlength = xWidth-380;
            
            userName.frame = CGRectMake(userNametxtXpos, userNametxtYpos, txtlength, userNametxtheight);
            eMail.frame = CGRectMake(emailtxtXpos, emailtxtYpos, txtlength, emailtxtheigth);
            feedBack.frame = CGRectMake(feedBacktxtXpos,feedBacktxtYpos,txtlength,feedbacktxtheight);
            
        }
    }

    //[formView setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    [formView addSubview:userNamelbl];
    [formView addSubview:userName];
    [formView addSubview:emailLbl];
    [formView addSubview:eMail];
    [formView addSubview:feedbackLbl];
    [formView addSubview:feedBack];
    [formView addSubview:submitBtn];
    [self.view addSubview:formView];

    
    emailLbl=nil, [emailLbl release];
    userNamelbl=nil, [userNamelbl release];
    feedbackLbl=nil, [feedbackLbl release];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)submitButtonClicked:(id)sender
{
    if(![self checkInternet])
        return;
    
    NSString  *usernameString = userName.text;
    NSString  *emailIdString = eMail.text;
    NSString  *feedbackString = feedBack.text;
    
    NSLog(@"usernameString %@, emailIdString %@, feedbackString %@", usernameString, emailIdString, feedbackString);
    
    if (usernameString == nil || emailIdString == nil || [usernameString isEqualToString:@""] || [emailIdString isEqualToString:@""] || feedbackString==nil || [feedbackString isEqualToString:@""]) {

        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Invalid user, Please Try Again.." message:@"Please provide User Name, Email-Id and Feedback"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];

    } else {
        NSURL *requestURL = [NSURL URLWithString:FEEDBACK_URL];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
        [request setPostValue:userName.text forKey:REQUEST_FEEDBACK_FIRST_NAME];
        [request setPostValue:eMail.text forKey:REQUEST_FEEDBACK_EMAIL_ID];
        [request setPostValue:feedBack.text forKey:REQUEST_FEEDBACK];
        [request startSynchronous];

        NSError *error = [request error];
        
        if (!error) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Thank you for your valuable feedback"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        } else {
            NSLog(@"feedback error occured %@", error);
        }
        
        if(IsIpad()) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"infopopviewclose" object: nil];
        } else {
            [self dismissModalViewControllerAnimated:NO];
            
        }
    }
}

-(IBAction)textFieldReturn:(id)sender {
    [sender resignFirstResponder];
}

- (BOOL) connectedToNetwork
{
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.vikatan.com/"]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
        [self _showAlert:KINTERNETNOTAVBL];
		//[self _showAlert:@"Check your connection, network unavailable."];
		return NO;
	}
	else {
		return YES;
	}
}

- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:title delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}

@end
