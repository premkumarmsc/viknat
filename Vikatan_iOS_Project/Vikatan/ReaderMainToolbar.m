//
//	ReaderMainToolbar.m
//	Reader v2.0.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011 Julius Oklamcak. All rights reserved.
//
//	This work is being made available under a Creative Commons Attribution license:
//		«http://creativecommons.org/licenses/by/3.0/»
//	You are free to use this work and any derivatives of this work in personal and/or
//	commercial products and projects as long as the above copyright is maintained and
//	the original author is attributed.
//

#import "ReaderConstants.h"
#import "ReaderMainToolbar.h"

#import <MessageUI/MessageUI.h>

@implementation ReaderMainToolbar

#pragma mark Constants

#define TITLE_Y 8.0f
#define TITLE_X 12.0f
#define TITLE_HEIGHT 28.0f

#define DONE_BUTTON_WIDTH 56.0f
#define PRINT_BUTTON_WIDTH 44.0f
#define EMAIL_BUTTON_WIDTH 44.0f

#pragma mark Properties

@synthesize delegate;

#pragma mark ReaderMainToolbar instance methods

- (id)initWithFrame:(CGRect)frame
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if ((self = [super initWithFrame:frame]))
	{
		//self.translucent = YES;
		self.barStyle = UIBarStyleBlack;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth;

		NSMutableArray *toolbarItems = [NSMutableArray new]; NSUInteger buttonCount = 0;

		CGFloat titleX = TITLE_X; CGFloat titleWidth = (self.bounds.size.width - (titleX * 2.0f));

#if (READER_STANDALONE == FALSE) // Option

		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"நூலகம்", @"button")
										style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonTapped:)];

		[toolbarItems addObject:doneButton]; [doneButton release]; buttonCount++; titleX += DONE_BUTTON_WIDTH; titleWidth -= DONE_BUTTON_WIDTH;

#endif // end of READER_STANDALONE Option

		UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];

		[toolbarItems addObject:flexSpace]; [flexSpace release];

        
        
        UIImage *buttonImage = [UIImage imageNamed:@"socialnetwork_32.png"];
        
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        aButton.tag = 1;
    
        [aButton setImage:buttonImage forState:UIControlStateNormal];
        
        aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        [aButton addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        share = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        [buttonImage release];[aButton release];
        
        [toolbarItems addObject:share]; [share release]; buttonCount++; titleWidth -= EMAIL_BUTTON_WIDTH;
        
        
        buttonImage = [UIImage imageNamed:@"iphone_comment_box_ver_1.png"];
        
        aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
         aButton.tag = 2;
        
        [aButton setImage:buttonImage forState:UIControlStateNormal];
        
        aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        [aButton addTarget:self action:@selector(viewCmntButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        viewComment = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        [buttonImage release];[aButton release];
        
        [toolbarItems addObject:viewComment]; [viewComment release]; buttonCount++; titleWidth -= EMAIL_BUTTON_WIDTH;
        
        buttonImage = [UIImage imageNamed:@"comments_post_32.png"];
        
        aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        aButton.tag = 3;
        
        [aButton setImage:buttonImage forState:UIControlStateNormal];
        
        aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        [aButton addTarget:self action:@selector(postCmntButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        postComment = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        [buttonImage release];[aButton release];
        
        [toolbarItems addObject:postComment]; [postComment release]; buttonCount++; titleWidth -= EMAIL_BUTTON_WIDTH;
		

        buttonImage = [UIImage imageNamed:@"text_view_32.png"];
        
        aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        aButton.tag = 5;
        
        [aButton setImage:buttonImage forState:UIControlStateNormal];
        
        aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        [aButton addTarget:self action:@selector(textButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        textButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        [buttonImage release];[aButton release];

        [toolbarItems addObject:textButton]; [textButton release]; buttonCount++; titleWidth -= EMAIL_BUTTON_WIDTH;
        
        
        buttonImage = [UIImage imageNamed:@"toc_32.png"];
        
        aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        aButton.tag = 4;
        
        [aButton setImage:buttonImage forState:UIControlStateNormal];
        
        aButton.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
        
        [aButton addTarget:self action:@selector(tocButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        tocButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        [buttonImage release];[aButton release];
        
        [toolbarItems addObject:tocButton]; [tocButton release]; buttonCount++; titleWidth -= PRINT_BUTTON_WIDTH;
        
        
        
		if (buttonCount > 0) [self setItems:toolbarItems animated:NO]; [toolbarItems release];
        
        UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
        UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(293, 5, header.size.width, header.size.height)];
        headerImageView.image = header;
        [self addSubview:headerImageView];
        
        CGRect titleRect = CGRectMake(290 + header.size.width, TITLE_Y, 100, TITLE_HEIGHT);
        
		theTitleLabel = [[UILabel alloc] initWithFrame:titleRect];
        
		theTitleLabel.textAlignment = UITextAlignmentCenter;
		theTitleLabel.font = [UIFont systemFontOfSize:20.0f];
		theTitleLabel.textColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
		theTitleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
		theTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		theTitleLabel.backgroundColor = [UIColor clearColor];
		theTitleLabel.adjustsFontSizeToFitWidth = YES;
		theTitleLabel.minimumFontSize = 14.0f;
        
		[self addSubview:theTitleLabel];
	}

	return self;
}

- (void)dealloc
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

    /*
	[theTitleLabel release], theTitleLabel = nil;
    [textButton release], textButton = nil;
    [tocButton release], tocButton = nil;
    [share release], share = nil;
    [postComment release], postComment = nil;
    [viewComment release], viewComment = nil;
*/
    [theTitleLabel release], theTitleLabel = nil;
	[super dealloc];
}

- (void)setToolbarTitle:(NSString *)title
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[theTitleLabel setText:title];
}

- (void)hideToolbar
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if (self.hidden == NO)
	{
		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.alpha = 0.0f;
			}
			completion:^(BOOL finished)
			{
				self.hidden = YES;
			}
		];
	}
}

- (void)showToolbar
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if (self.hidden == YES)
	{
		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.hidden = NO;
				self.alpha = 1.0f;
			}
			completion:NULL
		];
	}
}

#pragma mark UIBarButtonItem action methods

- (void)doneButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[delegate tappedInToolbar:self doneButton:button];
}

- (void)tocButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[delegate tappedInToolbar:self tocButton:button];
}

- (void)textButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[delegate tappedInToolbar:self textButton:button];
}

- (void)shareButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[delegate tappedInToolbar:self shareButton:button];
}
- (void)viewCmntButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[delegate tappedInToolbar:self viewCmntButton:button];
}
- (void)postCmntButtonTapped:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[delegate tappedInToolbar:self postCmntButton:button];
}

@end
