//    File: PDFScrollView.m
//Abstract: UIScrollView subclass that handles the user input to zoom the PDF page.  This class handles swapping the TiledPDFViews when the zoom level changes.
// Version: 1.0
//
//Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
//Inc. ("Apple") in consideration of your agreement to the following
//terms, and your use, installation, modification or redistribution of
//this Apple software constitutes acceptance of these terms.  If you do
//not agree with these terms, please do not use, install, modify or
//redistribute this Apple software.
//
//In consideration of your agreement to abide by the following terms, and
//subject to these terms, Apple grants you a personal, non-exclusive
//license, under Apple's copyrights in this original Apple software (the
//"Apple Software"), to use, reproduce, modify and redistribute the Apple
//Software, with or without modifications, in source and/or binary forms;
//provided that if you redistribute the Apple Software in its entirety and
//without modifications, you must retain this notice and the following
//text and disclaimers in all such redistributions of the Apple Software.
//Neither the name, trademarks, service marks or logos of Apple Inc. may
//be used to endorse or promote products derived from the Apple Software
//without specific prior written permission from Apple.  Except as
//expressly stated in this notice, no other rights or licenses, express or
//implied, are granted by Apple herein, including but not limited to any
//patent rights that may be infringed by your derivative works or by other
//works in which the Apple Software may be incorporated.
//
//The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
//MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
//THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
//FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
//OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
//
//IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
//OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
//MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
//AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
//STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
//POSSIBILITY OF SUCH DAMAGE.
//
//Copyright (C) 2010 Apple Inc. All Rights Reserved.
//

#import "PDFScrollView.h"
#import "TiledPDFView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PDFScrollView
@synthesize index, pdfDocPath;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        VikatanAppDelegate_iPad *appDelegate  = [[UIApplication sharedApplication] delegate];
        pdfPage =  [ appDelegate.globalCount intValue] + 1;
        //pdfDocPath = appDelegate.pdfDocPath;
        NSLog(@"This time moing fast " );
		//Set up the UIScrollView
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
		[self setBackgroundColor:[UIColor grayColor]];
		self.maximumZoomScale = 2.0;
		self.minimumZoomScale = 1.0;
        [self setZoomScale:1];
		pageCOUNT = 0;//, pdfPage = 1;
        
       
       
        pdfURL = (CFURLRef)[[NSBundle mainBundle] URLForResource:@"aval.pdf" withExtension:nil];
        pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
        
        CGPDFDocumentRetain(pdf);
        [self MoveNext];
        
        /*
        CGPDFPageRelease(page);
        CGPDFDocumentRelease(pdf);
        pdfURL = nil;
        
        [pdfView.layer release];
        pdfView.layer.contents = nil;
        pdfView.layer.delegate = nil;
        [pdfView release];
        */
        
        NSLog(@"After Move Next is called");
    }
    return self;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSUInteger numTaps = [[touches anyObject] tapCount];
    
    if (numTaps== 2)
    {
        
        /*
        [oldPDFView removeFromSuperview];
        [oldPDFView release];
        
        // Set the current TiledPDFView to be the old view.
        oldPDFView = pdfView;
        [self addSubview:oldPDFView];
         */
        
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSUInteger numTaps = [[touches anyObject] tapCount];
   
    if (numTaps== 2) 
    {
        
        /*
        //some operations here
        NSLog(@"double tap ");
        pdfScale *= 1.5; //scale;
        
        // Calculate the new frame for the new TiledPDFView
        CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
        
        // Create a new TiledPDFView based on new frame and scaling.
        pdfView = [[TiledPDFView alloc] initWithFrame:pageRect andScale:pdfScale];
        [pdfView setPage:page];
        
        // Add the new TiledPDFView to the PDFScrollView.
        [self addSubview:pdfView];
         */
        UIImageView* imageView = [self.subviews objectAtIndex:0];
        
        [UIView beginAnimations:nil context:nil];
        imageView.frame = CGRectMake(0, 0, imageView.image.size.width, imageView.image.size.height);
        self.contentSize = imageView.image.size;
        self.minimumZoomScale = 320.0f/imageView.image.size.width;
        
        //self.contentOffset = location;
        self.maximumZoomScale = 1;
        [UIView commitAnimations];
        
        //zoomed = YES;
    }
    
}
// Double Tap Zoom
- (void)doubleTapZoom:(NSNotification *)notification
{
    
    /*
    // scroll view passed as object, get content subview
    UIScrollView * scrollView = [notification object];
    UIView * content = [[scrollView subviews] objectAtIndex:0];
    
    // get dictionary holding event, and event
    NSDictionary * dict = [notification userInfo];
    UIEvent * event = [dict objectForKey:@"event"];
    
    // get touch from set of touches for view
    UITouch * touch = [[event touchesForView:content] anyObject];
    
    // need scroll frame, content frame and content offset
    CGRect scrollFrame = scrollView.frame;
    CGRect contentFrame;
    CGPoint contentOffset;
    
    // zoom in if not at max size, other zoom out to fit to page
    float scalingFactor = 0.0;
    float currentWidth = [content frame].size.width;
    if( currentWidth < kMaxWidth )
    {
        contentFrame = CGRectMake(0, 0, scrollFrame.size.width*2, scrollFrame.size.height*2);
        scalingFactor = kMaxWidth / currentWidth;
        CGPoint touchPoint = [touch locationInView:content];
        contentOffset = CGPointMake(touchPoint.x*scalingFactor/2, touchPoint.y*scalingFactor/2);
    }
    else
    {
        scalingFactor = kMinWidth / currentWidth;
        contentFrame = CGRectMake(0, 0, scrollFrame.size.width, scrollFrame.size.height);
        contentOffset = CGPointMake(0,0);
    }
    
    // dump the old views
    [content removeFromSuperview];
    [scrollView removeFromSuperview];
    
    // create scroll view and content view
    [self createScrollView:scrollFrame contentOffset:contentOffset contentFrame:contentFrame scalingFactor:1.0/scalingFactor];
     */
    
    NSLog(@"double tap ");
}
-(void) swipeleft:(id)sender 
{
    
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        [[self layer] addAnimation:animation forKey:@"SwitchToView1"];
        //[self MoveNext];
       
}

-(void) swiperight:(id)sender
{
   
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.4];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[self layer] addAnimation:animation forKey:@"SwitchToView1"];
        
        pageCOUNT = pageCOUNT - 1;
        //[self MoveNext];
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)sender 
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.frame.size.width;
    int page = floor((self.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
}

-(void) addPDF {
    
    [pdfView removeFromSuperview];pdfView = nil;
    [backgroundImageView removeFromSuperview];backgroundImageView = nil;
    
    //CGPDFDocumentRelease(pdf);//CGContextRelease(context);
    backgroundImageView = [[UIImageView alloc] initWithImage:[_contentArray objectAtIndex:0]];
    backgroundImageView.frame = self.bounds;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:backgroundImageView];
    [self sendSubviewToBack:backgroundImageView];
    
    // Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
    pdfView = [[TiledPDFView alloc] initWithFrame: self.bounds andScale:pdfScale];
    [pdfView setPage:page];
    [self addSubview:pdfView];
    
}
-(void) MoveNext {
    
     
    //[_contentArray release];_contentArray = nil;
    [pdfView removeFromSuperview];
    [backgroundImageView removeFromSuperview];
    backgroundImageView = nil;
       
    UIGraphicsEndImageContext();
    page = CGPDFDocumentGetPage(pdf, pdfPage); 
    //CGPDFPageRetain(page);
    // determine the size of the PDF page
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    pdfScale = self.frame.size.width/pageRect.size.width;
    pageRect.size =  CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
    NSLog(@"Page Number %f",pageRect.size.width);
    // Create a low res image representation of the PDF page to display before the TiledPDFView
    // renders its content.
    UIGraphicsBeginImageContext(pageRect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context,pageRect);
    
    CGContextSaveGState(context);
    // Flip the context so that the PDF page is rendered
    // right side up.
    CGContextTranslateCTM(context, 0.0, pageRect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh); 
    CGContextSetRenderingIntent(context, kCGRenderingIntentDefault);
    // Scale the context so that the PDF page is rendered 

    // Scale the context so that the PDF page is rendered 
    // at the correct size for the zoom level.
    CGContextScaleCTM(context, pdfScale,pdfScale);	
    CGContextDrawPDFPage(context, page);
    CGContextRestoreGState(context);
    
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();//CGContextEndPage (context);
    
    backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
    backgroundImageView.frame = pageRect;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:backgroundImageView];
    [self sendSubviewToBack:backgroundImageView];
    
    // Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
    pdfView = [[TiledPDFView alloc] initWithFrame:pageRect andScale:pdfScale];
    [pdfView setPage:page];
    
    [self addSubview:pdfView];
    //CGPDFDocumentRelease(pdf);
   
}
- (void)didReceiveMemoryWarning {
    CGPDFPageRelease(page);
    CGPDFDocumentRelease(pdf);
    pdfURL = nil;
    
    [pdfView.layer release];
    pdfView.layer.contents = nil;
    pdfView.layer.delegate = nil;
    [pdfView release];
}
- (void)dealloc
{
    
	// Clean up
    pdfView.layer.contents = nil;
    pdfView.layer.delegate=nil;
    [pdfView removeFromSuperlayer];
    [pdfView release];
	[backgroundImageView release];
	CGPDFPageRelease(page);
	CGPDFDocumentRelease(pdf);
    [super dealloc];
}
- (void)setIndex:(NSUInteger)i {
    index = i;
    pdfPage = i;
    NSLog(@"Index value od pdf is %d",i);
    //indexLabel.text = [NSString stringWithFormat:@"%i", i];
}
#pragma mark -
#pragma mark Override layoutSubviews to center content

// We use layoutSubviews to center the PDF page in the view
- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    // center the image as it becomes smaller than the size of the screen
	
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = pdfView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    pdfView.frame = frameToCenter;
	backgroundImageView.frame = frameToCenter;
    
	// to handle the interaction between CATiledLayer and high resolution screens, we need to manually set the
	// tiling view's contentScaleFactor to 1.0. (If we omitted this, it would be 2.0 on high resolution screens,
	// which would cause the CATiledLayer to ask us for tiles of the wrong scales.)
	//pdfView.contentScaleFactor = 1.0;
}




#pragma mark -
#pragma mark UIScrollView delegate methods

// A UIScrollView delegate callback, called when the user starts zooming. 
// We return our current TiledPDFView.
/*
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
       UIView *view = nil;
    if (scrollView == self) {
        view = [self viewWithTag:100];
    }
    
    //return view;

    return pdfView;
}
*/
// A UIScrollView delegate callback, called when the user stops zooming.  When the user stops zooming
// we create a new TiledPDFView based on the new zoom level and draw it on top of the old TiledPDFView.
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
     NSLog(@"latest..");
	// set the new scale factor for the TiledPDFView
	pdfScale *=scale;
	
	// Calculate the new frame for the new TiledPDFView
	CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
	pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
	
	// Create a new TiledPDFView based on new frame and scaling.
	pdfView = [[TiledPDFView alloc] initWithFrame:pageRect andScale:pdfScale];
	[pdfView setPage:page];
	
	// Add the new TiledPDFView to the PDFScrollView.
	[self addSubview:pdfView];
}

// A UIScrollView delegate callback, called when the user begins zooming.  When the user begins zooming
// we remove the old TiledPDFView and set the current TiledPDFView to be the old view so we can create a
// a new TiledPDFView when the zooming ends.
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
	// Remove back tiled view.
	[oldPDFView removeFromSuperview];
	[oldPDFView release];
	
	// Set the current TiledPDFView to be the old view.
	oldPDFView = pdfView;
	[self addSubview:oldPDFView];
}


@end
