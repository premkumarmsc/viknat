//
//  DBClass.h
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 15/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

#import "sqlite3.h"
static sqlite3 *database = nil;
static sqlite3_stmt *deleteStmt = nil;
static sqlite3_stmt *addStmt = nil;
@interface DBClass : UIViewController {

 sqlite3 * database;

}

-(BOOL) openDatabase;
- (BOOL) copyDatabaseIfNeeded;
@property(nonatomic) NSInteger magazinePrimaryID;
-(NSMutableArray *) selectMagazineField:(NSArray *)fields tblname :(NSString *)tablename when:(NSString *)condition order:(NSString *)order recordsCount:(NSInteger)limit startWith:(NSInteger)offset;

- (NSString *) getDBPath;
-(void) closeDBconnection;
-(NSString *) openDBconnection:(NSString *) documentsDir;
-(int) deleteContentId:(NSString *) contentID;
-(BOOL) deleteAllMagazines:(NSString *)table;
-(BOOL) insertMagazineFields:(NSString *)fields values:(NSString *)values tableName:(NSString *)table;
-(NSInteger) getLatestBook;

-(BOOL) updateMagazineWithFieldAndValues:(NSString *)fieldAndValues tblname:(NSString *)tblname when:(NSString *)condition;

@end
