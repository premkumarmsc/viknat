//
//  bookCategory.h
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 11/06/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "DBClass.h"

@interface bookStoreCommon : UIView {
    NSString * cachePath;
    NSFileManager *fileManager;

}

@property (nonatomic, retain) NSString * cachePath;


-(NSString *) returnButtonTitle:(int) id;
-(NSString *) returnButtonColor:(int) id;
-(UIColor *) returnButtonTitleColor:(int) id;
-(int) returnbtnStatus: (NSString *) downloadable contentid:(NSString *) contentid;
-(int) checkfilepath: (NSString *) contentid;
-(int) checkDownloadDB: (NSString *) contentid;

@end
