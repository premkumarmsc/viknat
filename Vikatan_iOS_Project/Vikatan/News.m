//
//  News.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "News.h"
#import <QuartzCore/QuartzCore.h>

@implementation News

@synthesize headerImageView, detailNewsView, back, webView,scrollCategoryView, scrollArticleView, tempScrollView, newsDetailTitle , headerView, refreshImageView, commentsImageView;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    //	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	self.wantsFullScreenLayout = YES;
	//Set view background color to black
	[self.view setBackgroundColor:[UIColor blackColor]];
	//Initialize activity window for loading process
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//Set position
	[spinner setCenter:CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height/2.0)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	//Add the loading spinner to view
	[self.view addSubview:spinner];
	//Start animating
	[spinner startAnimating];
	NSLog(@"before url call %@", @"ffff"); 
	
	headerView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.bounds.size.width, 80)];
	headerView.backgroundColor = [UIColor darkGrayColor];
	//headerView.opaque = 0.8;
	//headerView.alpha = 0.5;
	headerView.userInteractionEnabled = YES;
	//headerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
	//headerView.layer.borderWidth = 1.0f;
	
	
	UILabel *hdTitle = [[[UILabel alloc] initWithFrame:CGRectMake(120,8 , 150, 30)] autorelease];
	hdTitle.textColor = [UIColor whiteColor];
	hdTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:28];
	[hdTitle setText:@"News"];
	hdTitle.backgroundColor = [UIColor clearColor];
	[headerView addSubview:hdTitle];
	//Add Header Image Control
	UIImage *header = [UIImage imageNamed:@"vikatan_icon_32.png"];
	headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7.0, header.size.width, header.size.height)];
	headerImageView.image = header;
	[headerView addSubview:headerImageView];
	//UIView *headerView;
	//headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
	
	refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
	refreshImageView.frame =  CGRectMake(285, 7, 32, 32);
	[refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
	[headerView addSubview:refreshImageView];
	
	commentsImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[commentsImageView setImage:[UIImage imageNamed:@"comments_norm_32.png"] forState:UIControlStateNormal];
	commentsImageView.frame =  CGRectMake(0, 0, 0, 0);
	[commentsImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
	[headerView addSubview:commentsImageView];
	
	//Add Back Button but hidden
	back = [UIButton buttonWithType:UIButtonTypeCustom];
	[back setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
	back.frame =  CGRectMake(10, 2, 0, 0);
	[back addTarget:self action:@selector(back_clicked:) forControlEvents:UIControlEventTouchUpInside];
	[headerView addSubview:back];
	[self.view addSubview:headerView];
    
	scrollCategoryView.userInteractionEnabled = TRUE;
	//Send request to get news
	[self NewsGetRequest];
	
    [super viewDidLoad];
	//[pool drain];
	
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	//label.text = [NSString stringWithFormat:@"Connection failed: %@", [error description]];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
	[self Show_CategoryView:( NSString *) responseString];
    //	[responseData release];
	
    
}

-(void) NewsGetRequest{
	
	responseData = [[NSMutableData data] retain];
	//Send http request
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.mobileveda.com/r_d/mcps/news.json"]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self] ;
	
}
//Load Category view 

-(void)webViewDidStartLoad:(UIWebView *) portal {
	
	
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	//Set position
	[spinner setCenter:CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height/2.0)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
	//Add the loading spinner to view
	[self.view addSubview:spinner];
	//Start animating
	[spinner startAnimating];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {    
	[spinner stopAnimating];
	[spinner removeFromSuperview];
	
	/*m_activity.hidden= TRUE;     
	 [m_activity stopAnimating];  
	 NSLog(@"Web View started loading...");
	 */
}
- (void) Show_CategoryView:(NSString*)responseString{
	
	[scrollCategoryView removeFromSuperview];
    
    //	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	//Local variables declaration
	int ndx;
	int imageWidth;
	int titlesY = 15;
	int categorySectionY = 35;
	NSString *titleString;
	NSInteger categoryid;
	//For Category Scroll View
	CGRect mainContainer = CGRectMake(-5, 40, self.view.frame.size.width, self.view.frame.size.height);
	scrollCategoryView = [[UIScrollView alloc] initWithFrame:mainContainer];
	scrollCategoryView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
	scrollCategoryView.showsVerticalScrollIndicator = NO;
	scrollCategoryView.bounces = NO;
	//scrollCategoryView.delaysContentTouches = NO;
	//For news scroll view
	CGRect subContainer = CGRectMake(0, 0, self.view.frame.size.width, 100);
	scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
	scrollArticleView.contentSize = CGSizeMake(self.view.frame.size.width, 100);
	scrollArticleView.showsHorizontalScrollIndicator = NO;
	scrollArticleView.bounces = YES;
	
	//JSON Parser declaration
	SBJSON *jsonParser = [[SBJSON new] autorelease];
	
	id response = [jsonParser objectWithString:responseString];
	
	NSDictionary *newsfeed = (NSDictionary *)response;
    
	// get the array of "stream" from the feed and cast to NSArray
	NSArray *categories = (NSArray *)[newsfeed valueForKey:@"categories"];
	NSDictionary *category;
	// loop over all the stream objects and print their titles
	
	for (ndx = 0; ndx < categories.count ; ndx++) {
		
		category = (NSDictionary *)[categories objectAtIndex:ndx];
		titleString = [category valueForKey:@"title"];
		categoryid  = [category valueForKey:@"id"];
		NSLog(@"Category Title: %@", [category valueForKey:@"title"]); 
		
		UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(5,titlesY , 100, 20)];
		titleView.backgroundColor = [UIColor clearColor];
		titleView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"news_title_norm.png"]];
		UILabel *cateGoryTitle = [[[UILabel alloc] initWithFrame:CGRectMake(3,0 , 95, 20)] autorelease];
		subContainer = CGRectMake(5, categorySectionY, 320, 110);
		
		UIButton *moreView = [UIButton buttonWithType:UIButtonTypeCustom];
		moreView.backgroundColor = [UIColor blackColor];
		moreView.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:28];
		[moreView setTitle:@"..." forState:UIControlStateNormal];
		//[moreView setImage:[UIImage imageNamed:@"more_norm.png"] forState:UIControlStateNormal];
		moreView.frame =  CGRectMake(250,titlesY-5, 100, 15);
		moreView.alpha = 0.5;
		//[moreView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
		[scrollCategoryView addSubview:moreView];
		//moreView = nil;
		titlesY = titlesY + 140;
		categorySectionY = categorySectionY + 140 ;
		cateGoryTitle.text = titleString;
		cateGoryTitle.backgroundColor = [UIColor clearColor];
		cateGoryTitle.textColor = [UIColor whiteColor];
		cateGoryTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
		cateGoryTitle.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
		cateGoryTitle.shadowOffset = CGSizeMake(0, -1.0);
		
		[titleView addSubview:cateGoryTitle];
		[scrollCategoryView addSubview:titleView];
		
		//rowYAxis = rowYAxis + 18;
		scrollArticleView = [[UIScrollView alloc] initWithFrame:subContainer];
		scrollArticleView.tag = ndx;
		//Browse Articles in the category and add artcile summary
		//NSArray *articles = (NSArray *)[category valueForKey:@"articles"];
		NSArray *articles = (NSArray *)[category valueForKey:@"articles"];
		imageWidth = 2 ;
		//Loop through individual articles in each category
		for (int y = 0 ; y < articles.count; y++){
			
			NSDictionary *article = (NSDictionary *)[articles objectAtIndex:y];			
			CGRect frame;
			frame.size.width = 100; frame.size.height = 100;
			frame.origin.x = imageWidth; frame.origin.y = 5;
			
			AsyncImageView* asyncImage = [[AsyncImageView alloc]
                                          initWithFrame:frame] ;
			asyncImage.tag = y;
			asyncImage.categoryTitle = [article valueForKey:@"title"];
			if ([@"" isEqualToString:[article valueForKey:@"image"]]) {
				asyncImage.imageURL = @"";
				asyncImage.showHalfArticle;
				NSLog(@"It is empty!");
			}else {
				asyncImage.imageURL = [article valueForKey:@"image"];
			}			
			[asyncImage loadImageFromURL:[NSURL URLWithString:asyncImage.imageURL ]];
			
			CustomButton *thumbNailView = [CustomButton buttonWithType:UIButtonTypeCustom];
			thumbNailView.opaque = 0.3;
			thumbNailView.alpha = 0.1;
			thumbNailView.backgroundColor = [UIColor clearColor];
			thumbNailView.frame = frame;
			thumbNailView.tag = y;
           // thumbNailView.UrlString = [article valueForKey:@"url"];
			//[thumbNailView setUrlString:[article valueForKey:@"url"]];
			//[thumbNailView setCateGoryID:ndx];
			//[thumbNailView setCateGoryTitle:titleString];
			//NSString *url = [NSString stringWithFormat: @"http://%@", [article valueForKey:@"url"]];
			//[thumbNailView setTitle:@"manu" forState:UIControlStateNormal];
			[thumbNailView addTarget:self action:@selector(article_clicked:) forControlEvents:UIControlEventTouchUpInside];
			[scrollArticleView addSubview:asyncImage];
			[scrollArticleView addSubview:thumbNailView];
			imageWidth = imageWidth +105;
            //	thumbNailView  = nil;
            //	asyncImage = nil;
            //	article = nil;
		}
		scrollArticleView.showsHorizontalScrollIndicator = NO;
		scrollArticleView.backgroundColor = [UIColor lightGrayColor];
		scrollArticleView.contentSize = CGSizeMake(imageWidth, 100);
		[scrollCategoryView addSubview:scrollArticleView];
        //	scrollArticleView = nil;
		//Add More option 
		[spinner stopAnimating];
		[spinner removeFromSuperview];
		articles = nil;
		//[spinner removeFromSuperview];
	}
	scrollCategoryView.contentSize = CGSizeMake(325, 145*categories.count);
	scrollCategoryView.showsHorizontalScrollIndicator = NO;
	[self.view addSubview:scrollCategoryView];
	jsonParser = nil;
	[jsonParser release];
	newsfeed = nil;
	categories = nil;
	category = nil;
	
    //	[pool release];
}
- (void) handleTapFrom{
	UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Network error" message: @"Error sending your info to the server" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
	
	//[someError show];
	//[someError release];
}
//Event on Article Clicked
- (void)article_clicked:(id)sender {
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	back.frame =  CGRectMake(10, 2, 60, 40);
	
	[self.view addSubview:back];
	
	CustomButton *urlButton = (CustomButton *)sender;
	NSString *urlString = urlButton.UrlString;
	NSMutableString *uriString = [[NSMutableString alloc] initWithString:urlString];
	NSURL* url = [[NSURL alloc] initWithString:uriString];
	NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
	
	CGRect frame;
	frame.size.width = 50; frame.size.height = 25;
	frame.origin.x = 5; frame.origin.y = 2;
	
	if(detailView == FALSE){	
		
		detailNewsView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
		detailNewsView.backgroundColor = [UIColor blackColor];
		detailNewsView.userInteractionEnabled = TRUE;
		webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)] ;
		webView.delegate = self;
        //		webView.userInteractionEnabled = TRUE;
		NSLog(@"Sending web request to %@", urlString);	
		[self.view  addSubview:detailNewsView];
		[detailNewsView addSubview:webView];
        //	self.hideTabBarView;
		
		if(scrollCategoryView!= nil){
			for(UIView *subview in [scrollCategoryView subviews]) {
				if([subview isKindOfClass:[UIScrollView class]]) {
                    
                    NSLog(@"Tag Id is equal to %i:",subview.tag);
					if(subview.tag == urlButton.CateGoryID){
                        
                        [subview removeFromSuperview];
                        //prevArticleViewFrame = subview.frame;
                        CGRect size = CGRectMake(0, 370,subview.bounds.size.width,subview.bounds.size.height);
                        tempScrollView = [[[UIView alloc] initWithFrame:size] autorelease];
                        tempScrollView = subview;
                        tempScrollView.frame = size;
                        [self.view  addSubview:tempScrollView];	
                        [UIView beginAnimations:nil context:NULL];
                        [UIView setAnimationDuration:0.5];
                        tempScrollView.frame = CGRectMake(-350, 380,subview.bounds.size.width,subview.bounds.size.height);
                        back.frame =  CGRectMake(0, -90, 0, 0);
                        refreshImageView.frame =  CGRectMake(0, 0, 0, 0);
                        commentsImageView.frame =  CGRectMake(285, 7, 32, 32);
                        [UIView commitAnimations];
						
                        newsDetailTitle = [UIButton buttonWithType:UIButtonTypeCustom];
                        newsDetailTitle.layer.shadowColor = [[UIColor blackColor] CGColor];
                        newsDetailTitle.layer.shadowOffset = CGSizeMake(10.0f, 10.0f);
                        newsDetailTitle.layer.shadowOpacity = 1.0f;
                        newsDetailTitle.layer.shadowRadius = 10.0f;
                        newsDetailTitle.layer.shouldRasterize = YES;
                        [newsDetailTitle setTitle:urlButton.CateGoryTitle forState:UIControlStateNormal];
                        newsDetailTitle.backgroundColor = [UIColor darkGrayColor];
                        newsDetailTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
                        newsDetailTitle.frame =  CGRectMake(0, 450, 120, 30);
                        headerView.frame = CGRectMake(0.0, -100, self.view.bounds.size.width, 50);
                        [headerView removeFromSuperview];
                        [headerImageView removeFromSuperview];
                        [newsDetailTitle addTarget:self action:@selector(expand_clicked:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [self.view  addSubview:newsDetailTitle];
                        [self.view  addSubview:tempScrollView];	
                        //tempScrollView.backgroundColor = [UIColor blackColor];
                        detailView = TRUE;	
						
					}else {
                        //[subview removeFromSuperview];
					}
                    
				} else {
                    // Do nothing - not a UIButton or subclass instance
				}
                //[[[scrollCategoryView subviews] objectAtIndex:0] removeFromSuperview];
                
			}
		}
	}
	else {
		
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.5];
		tempScrollView.frame = CGRectMake(-350, 380,320,100);
		newsDetailTitle.frame = CGRectMake(0, 450, 120, 30);
		headerView.frame = CGRectMake(0.0, -100, self.view.bounds.size.width, 50);
		back.frame =  CGRectMake(10, 2, 0, 0);
		//[headerView removeFromSuperview];
		[UIView commitAnimations];		
	}
	[webView loadRequest:urlRequest];
	[webView setNeedsLayout];
	//newsWebView1 = nil;
    
	[pool release];
	
}

- (void)expand_clicked:(id)sender {
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
	tempScrollView.frame = CGRectMake(0, 380,320,100);
	newsDetailTitle.frame =  CGRectMake(-350, 450, 0, 0);
	[self.view addSubview:headerView];
	headerView.frame = CGRectMake(0.0, 0, self.view.bounds.size.width, 50);
	back.frame =  CGRectMake(10, 2, 60, 40);
	[headerView addSubview:back];
	[UIView commitAnimations];	
}
- (void)back_clicked:(id)sender {
	
	[detailNewsView removeFromSuperview];
	[newsDetailTitle removeFromSuperview];
	back.frame =  CGRectMake(0, 0, 0, 0);
	//[back removeFromSuperview];
	//self.showTabBar;
	//tempScrollView.frame = prevArticleViewFrame;
	[scrollCategoryView addSubview:tempScrollView];
	refreshImageView.frame = CGRectMake(285, 7, 32, 32);
	commentsImageView.frame =  CGRectMake(0, 0, 0, 0);
    [headerView addSubview:headerImageView];
	//headerView.frame = CGRectMake(0.0, 0, self.view.bounds.size.width, 50);
	detailView = FALSE; 
	//	self.ShowArticleBrowse;
}
- (void)refresh_clicked:(id)sender {
	
	[spinner startAnimating];
	[self NewsGetRequest];
	
}

//Hide the tab bar view
-(void) hideTabBarView{
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
		
    }
	
    [UIView commitAnimations];
	
}
- (void) showTabBar {
	
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        NSLog(@"%@", view);
		
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
			
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
        }
		
		
    }
	
    [UIView commitAnimations]; 
}

//Show Statu bar on tap for navigation http:\/\/new.vikatan.com\/news.php?nid=1626

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

//Release used objects
- (void)didReceiveMemoryWarning {
    
	
	[responseData release];
	[scrollCategoryView release];
	[scrollArticleView release];
	[tempScrollView release];
	[spinner release];
	[headerImageView release];
	[detailNewsView release];
	[back release];
	//[newsWebView release];
	[refreshImageView release];
	[commentsImageView release];
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];    
    // Release any cached data, images, etc that aren't in use.
	
}
//nullify the view objects
- (void)viewDidUnload {
	
	responseData = nil;
	scrollCategoryView = nil;
	scrollArticleView = nil;
	tempScrollView = nil;
	//UI activity window with spinner
	spinner = nil;
	headerImageView = nil;
	detailNewsView = nil;
	back = nil;
	//newsWebView = nil;
	refreshImageView = nil;
	commentsImageView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
