//
//  ChapterList.h
//  EpubDownload
//
//  Created by Macminiserver on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPubViewController.h"
#import "Constant.h"
#import "DBClass.h"

@interface BookmarkList : UITableViewController{
    EpubViewController* epubViewController;
    
    UIView *titleView;
    
    NSMutableArray *bid;
    NSMutableArray *chap_no;
    NSMutableArray *chap_page;
    NSMutableArray *desc;
    NSMutableArray *font_size;
    
    NSString * content_id;
    
}
@property(nonatomic, assign) EpubViewController * epubViewController;
@property (nonatomic, retain) NSString * content_id;


- (void) loadBookmark:(NSString *) contentid;
@end
