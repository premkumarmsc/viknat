//
//  EPubViewController.m
//  EpubDownload
//
//  Created by Macminiserver on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EpubViewControlleriPAD.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+SearchWebView.h"
#import "Chapter.h"
#import "ChapterList.h"
#import "BookmarkList.h"
#import <QuartzCore/QuartzCore.h>


@interface EpubViewControlleriPAD()

//Spine
- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;

- (int) getGlobalPageCount;
- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;


@end

@implementation EpubViewControlleriPAD

@synthesize loadedEpub, toolbar, webView; 
@synthesize chapterListButton,decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, searching;
@synthesize currentSearchResult;
@synthesize loadingView, content_id, chap_page, chap_no, font_size;

@synthesize bookName;

- (void) loadEpub:(NSURL*) epubURL contentid: (NSString *) contentid type:(NSString *) viewtype
{
    isBarShow = YES;
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
	searching = NO;
    epubLoaded = NO;
    isLoadFirstTime = TRUE;
    
    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path] contentid:contentid type:viewtype];
    epubLoaded = YES;
    content_id = contentid;
    
    if([viewtype isEqualToString:@"main"]) {
        DBClass * dbObject = [[DBClass alloc] init];
        NSArray *fields     = [[NSArray alloc] initWithObjects:@"chap_no",@"chap_page", @"font_size", nil];
        NSString *condition = [NSString stringWithFormat:@"book_id = %@", content_id];

        NSMutableArray *mutableArr = [dbObject selectMagazineField:fields tblname: BOOK_LIB_TABLE when:condition order:@""  recordsCount:100 startWith:0];
        
        if([mutableArr count] > 0) {
            NSDictionary *bookfeed = (NSDictionary *)mutableArr;
            chap_no = (NSMutableArray *)[bookfeed valueForKey:@"chap_no"];
            chap_page = (NSMutableArray *)[bookfeed valueForKey:@"chap_page"];
            font_size = (NSMutableArray *)[bookfeed valueForKey:@"font_size"];
        }

        [dbObject release], dbObject=nil;
    
        if([chap_no count] > 0 && [chap_page count] > 0) {
            currentSpineIndex = [[chap_no objectAtIndex:0] intValue];
            currentPageInSpineIndex = [[chap_page objectAtIndex:0] intValue];
            currentTextSize = [[font_size objectAtIndex:0] intValue];

            if(currentTextSize == 0) {
            currentTextSize = 150;
            }
        }

        [chap_no retain];
        [chap_page retain];
        [font_size retain];
    }

   [self performSelector:@selector(updatePagination) withObject:nil afterDelay:0.0];

}

- (void) chapterDidFinishLoad:(Chapter *)chapter
{   
   // NSLog(@"coming inside chapterDidFinishLoad and page count is %d", chapter.pageCount);
    
    [currentPageLabel setText:[NSString stringWithFormat:@""]];
    if(isLoadFirstTime == TRUE) {
        [self stratRolling];
        totalPagesCount = chapter.pageCount;
        if([chap_no count] > 0 && [chap_page count] > 0) 
        {
            isLoadFirstTime = FALSE; NSLog(@"coming inside in isLoadFirstTime");
            
        } 
    } else {
             totalPagesCount = chapter.pageCount;
    
    }
    paginating = NO;

    [self performSelector:@selector(stopRolling) withObject:nil afterDelay:15.0];
    [self setbOokNightMode];
    [self setcurrenttextsize];

}

-(void) setcurrenttextsize
{
    currentTextSize = [CURRENT_TEXT_SIZE intValue];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *value = (NSString *)[standardUserDefaults objectForKey:CURRENT_TEXT_SIZE];
    int i = [value intValue];
    
    if(i > 150 )
    {   
        currentTextSize = i;
    }
    
}

- (int) getGlobalPageCount{
	int pageCount = 0;
	for(int i=0; i<currentSpineIndex; i++){
		pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount]; 
	}
	pageCount+=currentPageInSpineIndex+1;
	return pageCount;
}

- (int) getPreviousChapterPageCount{
	int pageCount = 0;
	pageCount+= [[loadedEpub.spineArray objectAtIndex:currentSpineIndex-1] pageCount]; 
	pageCount+=currentPageInSpineIndex+1;
	return pageCount;
}


- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
    
	[self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
    
    [self stratRolling];
	webView.hidden = YES;	
	self.currentSearchResult = theResult;
    
    if(spineIndex < [loadedEpub.spineArray count]) {
        NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];    

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url]; 
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [self.webView loadRequest:request];

    
        currentPageInSpineIndex = pageIndex;
        currentSpineIndex = spineIndex;
        if(!paginating) {
        
            //[currentPageLabel setText:[NSString stringWithFormat:@"%d / %d", currentPageInSpineIndex+1,totalPagesCount]];
        
            //[currentPageLabel setText:[NSString stringWithFormat:@"%d / %d",[self getGlobalPageCount], totalPagesCount]];
        
            //[currentPageLabel setText:[NSString stringWithFormat:@"%d / chapter - %d",currentPageInSpineIndex+1, currentSpineIndex]];
        }
    } 
}

/** Started changes made  */
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex fontsize:(int)fontsize
{
    //[loadingIndicator startAnimating];
	webView.hidden = YES;	
	//self.currentSearchResult = theResult;
    
    //iphone popView
	//[chaptersPopover dismissPopoverAnimated:YES];
    
    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];    
    //webView loading
     //webView.frame = CGRectMake(0,50,320,480);
    
    // startes changes
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url]; 
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [self.webView loadRequest:request];
    
     // Ended changes
   
    //[webView loadRequest:[NSURLRequest requestWithURL:url]]; //uncommennt after test
    
    //[self performSelector:@selector(updatePaginationforBookmark) withObject:nil afterDelay:0.0];
    
    currentTextSize = fontsize;
	currentPageInSpineIndex = pageIndex;
	currentSpineIndex = spineIndex;

    [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];

    //[self performSelector:@selector(updatePaginationforBookmark) withObject:nil afterDelay:0.0];

    if(!paginating)
    {
        NSLog(@"loadSpine:(int)spineIndex atPageIndex:(int)pageIndex fontsize:(int)fontsize is %@", [[loadedEpub.spineArray objectAtIndex: currentSpineIndex] title]);
        [currentPageLabel setText:[NSString stringWithFormat:@"%d", currentPageInSpineIndex+1]];
        
        //[currentPageLabel setText:[NSString stringWithFormat:@"%d / %d", currentPageInSpineIndex+1, totalPagesCount]]; //chapter total pages
	}
}

/** Ended changes made  **/

- (void) gotoPageInCurrentSpine:(int)pageIndex{
	if(pageIndex>=pagesInCurrentSpineCount){
		pageIndex = pagesInCurrentSpineCount - 1;
		currentPageInSpineIndex = pagesInCurrentSpineCount - 1;	
	}
	
	float pageOffset = pageIndex*webView.bounds.size.width;
    
	NSString *goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
	NSString *goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
	
	[webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
	[webView stringByEvaluatingJavaScriptFromString:goTo];
	
	if(!paginating){
        NSLog(@"gotoPageInCurrentSpine:(int)pageIndex and title is %@", [[loadedEpub.spineArray objectAtIndex: currentSpineIndex] title]);

        if([[loadedEpub.spineArray objectAtIndex: currentSpineIndex] title]!=nil && [[loadedEpub.spineArray objectAtIndex: currentSpineIndex] title]!=@"Un Named Chapters") {
            //[currentPageLabel setText:[NSString stringWithFormat:@"%d / %d", currentPageInSpineIndex+1, totalPagesCount]]; //chapter total pages
            
            [currentPageLabel setText:[NSString stringWithFormat:@"%d", currentPageInSpineIndex+1]];
            [currentChaptitle setText:[NSString stringWithFormat:@"%@", [[loadedEpub.spineArray objectAtIndex: currentSpineIndex] title]]];
        }
	}
	webView.hidden = NO;
}

- (void) gotoNextSpine {
	if(!paginating){
		if(currentSpineIndex+1<[loadedEpub.spineArray count]){
			[self loadSpine:++currentSpineIndex atPageIndex:0];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];

          //  [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		}
	}
   
}

//  To Navigate to previous chapter

- (void) gotoPrevSpine {
    if(!paginating){
        if(currentSpineIndex-1>=0){
			[self loadSpine:--currentSpineIndex atPageIndex:10000];
		}	
	}
}

//  To navigate to next page

- (void) gotoNextPage {
	if(!paginating){
        if(currentPageInSpineIndex+1<pagesInCurrentSpineCount){
			[self gotoPageInCurrentSpine:++currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];
           // [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		} 
        else {
            [currentPageLabel setText:[NSString stringWithFormat:@""]];
			[self gotoNextSpine];
		}
    }
}

//  To navigate to previous page

- (void) gotoPrevPage {
	if (!paginating) {
        if(currentPageInSpineIndex-1>=0){
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageUnCurl"];
            [transition setSubtype:@"fromRight"];
           // [self.webView.layer addAnimation:transition forKey:@"UnCurlAnim"];
		} 
        else {
            [currentPageLabel setText:[NSString stringWithFormat:@""]];
            [self gotoPrevSpine];
            
			
		}
	}
}

// To increase font size by 25 from 150
- (IBAction) increaseTextSizeClicked:(id)sender{
	if(!paginating){
		if(currentTextSize+25<=225){
			currentTextSize+=25;
             [self updateTextSize];
			[self updatePagination];

			if(currentTextSize == 225){
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
	}
}

//  To decrease font size by 25 from 225

- (IBAction) decreaseTextSizeClicked:(id)sender{
    if(!paginating){
        
		if(currentTextSize-25>125){
			currentTextSize-=25;
            [self updateTextSize];
			[self updatePagination];
            
			if(currentTextSize==125){
				[decTextSizeButton setEnabled:NO];
			}
			[incTextSizeButton setEnabled:YES];
		}
	}
}

// Chapter list TOC

- (IBAction)showChapterIndex:(id)sender{
    ChapterList* chapterListView = [[ChapterList alloc] initWithNibName:nil bundle:[NSBundle mainBundle]];
    [chapterListView setEpubViewController:self];
    chapterListView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:chapterListView] autorelease];
    chapterListView.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closetoc:)] autorelease];
    navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self presentModalViewController:navController animated:NO ];
    [chapterListView release];
}

- (IBAction) closetoc:(id)sender {
    [self dismissModalViewControllerAnimated:TRUE];
}

- (IBAction) backButton:(id)sender {
    isLoadFirstTime = TRUE;
   
    DBClass *dbObject = [[DBClass alloc] init];
    NSString *fieldAndValues = [NSString stringWithFormat:@"chap_no = %d, chap_page = %d , font_size = %d", currentSpineIndex, currentPageInSpineIndex, currentTextSize];
    NSString * condition = [NSString stringWithFormat:@"book_id = %d", [content_id intValue]];

    //[dbObject updateMagazineWithFieldAndValues:fieldAndValues when:condition tableName:BOOK_LIB_TABLE]; //KBOOKLIBTABL
    
    [dbObject updateMagazineWithFieldAndValues:fieldAndValues tblname:BOOK_LIB_TABLE when:condition];
    [dbObject release], dbObject=nil;

    self.toolbar = nil;
    self.webView = nil;
    [self.webView release];
    
    [chap_no release], chap_no=nil;
    [chap_page release], chap_page=nil;
    [font_size release], font_size=nil;
    [chapter_title_toc release], chapter_title_toc=nil;
    [currentChaptitle release], currentChaptitle=nil;
    [back release], back=nil;
    [btnlabel release], btnlabel=nil;
    [toc release], toc=nil;
    [increase release], increase=nil;
    [decrease release], decrease=nil;
    [dayBtn release], dayBtn=nil;
    
    [self dismissModalViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:NO];
}


- (IBAction) addBookmark:(id)sender
{
   /*DBClass *dbObject = [[DbManager alloc] init];
    
    NSString *queryValues = [NSString stringWithFormat:@"%@,%d,%d,'%@',%d,%d",content_id, currentSpineIndex, currentPageInSpineIndex, @"", 1, currentTextSize];
    
    NSLog(@"queryValues are %@", queryValues);
    
    if ([dbObject insertMagazineFields:@"content_id, chap_no, chap_page, desc, status, font_size" values:queryValues tableName:EPUB_BOOKMARK_TABLE]) 
    {
       // NSLog(@"Bookmark added  Successfully %@",queryValues);
        
    }
    queryValues=nil;
    [dbObject release], dbObject = nil;*/
}


- (IBAction)showBookmark:(id)sender{
    
   
    BookmarkList* BookmarkListView = [[BookmarkList alloc] init];
    
    UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:BookmarkListView] autorelease];
    pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    pdfNavController.navigationController.toolbar.hidden=FALSE;
    
    [BookmarkListView loadBookmark:content_id];
    [BookmarkListView setEpubViewController:self];
    BookmarkListView.modalTransitionStyle = UIModalPresentationFullScreen; 

    [self presentModalViewController:pdfNavController animated:YES];
    [BookmarkListView release];
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    [[[webView subviews] lastObject] setScrollEnabled:NO];
    
	NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
	
	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
	"if (mySheet.addRule) {"
	"mySheet.addRule(selector, newRule);"								// For Internet Explorer
	"} else {"
	"ruleIndex = mySheet.cssRules.length;"
	"mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
	"}"
	"}";
	
    
	NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", self.webView.frame.size.height, self.webView.frame.size.width];
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
	NSString *setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:auto;')", self.webView.frame.size.width *0.75];

   // NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'background-color: black; color:white;')", self.webView.frame.size.width *0.75];
    
    //NSString *bookFrame = [NSString stringWithFormat:@"addCSSRule('body', 'background: -webkit-gradient(linear, left top, right bottom, from(#FFF), to(#DDD)); -webkit-box-shadow: -3px 0 3px #311; -moz-box-shadow: -3px 0 3px #311;-webkit-border-top-right-radius: 3px;-webkit-border-bottom-right-radius: 3px; -moz-border-radius-topright: 3px;-moz-border-radius-bottomright: 3px;background-color: #F7F7F7;')", self.webView.frame.size.width *0.75];

    
   // NSString *changeFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:Bradley Hand;font-size:80;color:green;')"];
    // src:'/MVF.ttf'

    /*NSString * cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)   objectAtIndex:0];
    NSString * ttfpath = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:MVF; src:url(\"%@%@\")')", cachePath,@"/MVF.ttf"];

    NSLog(@"TTF Path is %@", ttfpath);*/
    
    NSString *changeFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:MVF;')"];
  //  NSString *changeFontTTF = [NSString stringWithFormat:@"addCSSRule('body', 'src:url(../../MVF.ttf);')"];

	[webView stringByEvaluatingJavaScriptFromString:varMySheet];
	[webView stringByEvaluatingJavaScriptFromString:addCSSRule];
	[webView stringByEvaluatingJavaScriptFromString:insertRule1];
	[webView stringByEvaluatingJavaScriptFromString:insertRule2];
	[webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
	[webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
	[webView stringByEvaluatingJavaScriptFromString:setImageRule];
   // [webView stringByEvaluatingJavaScriptFromString:changeFont];
    //[webView stringByEvaluatingJavaScriptFromString:changeFontTTF];

    if(currentSearchResult!=nil){
        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
	}

	int totalWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
	pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
    
    NSLog(@"pagesInCurrentSpineCount %i",pagesInCurrentSpineCount);
	NSLog(@"currentPageInSpineIndex %i",currentPageInSpineIndex);

    [self gotoPageInCurrentSpine:currentPageInSpineIndex];
    
    [self setbOokNightMode];
    [self stopRolling];
   
}

- (void) setbOokNightMode {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"NIGHT"] )
    {
        NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'color:white;')", self.webView.frame.size.width *0.75]; //
        [webView stringByEvaluatingJavaScriptFromString:setNightMode];
        
    }
    else
    {
        NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'color:black;')", self.webView.frame.size.width *0.75];
        [webView stringByEvaluatingJavaScriptFromString:setNightMode];

    }
}

- (void) updatePagination {
	if(epubLoaded){
        if(!paginating){
            paginating = YES;
            totalPagesCount=0;
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            [[loadedEpub.spineArray objectAtIndex:currentSpineIndex] setDelegate:self];
            [[loadedEpub.spineArray objectAtIndex:currentSpineIndex] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [self removeAllHighlights];
        }
    }
}

- (void) updatePaginationforBookmark{
	if(epubLoaded){
        if(!paginating){
            paginating = YES;
            totalPagesCount=0;
           // [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            //[[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
            //[[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [currentPageLabel setText:@"... / ..."];
            
            NSLog(@"pagination count is %d", totalPagesCount);
            [self removeAllHighlights];
            
            //int resultCount = [self highlightAllOccurencesOfString:@"child"];
        }
        
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    UIMenuItem *markHighlightedString = [[[UIMenuItem alloc] initWithTitle:@"markHighlightedString" action:@selector(markHighlightedString:)] autorelease];
    UIMenuItem *getHighlightedString = [[[UIMenuItem alloc] initWithTitle:@"getHighlightedString" action:@selector(getHighlightedString:)] autorelease];
    [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:markHighlightedString, getHighlightedString, nil]];

}

/*         Changes made starts here     */
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[UIMenuController sharedMenuController] setMenuItems:nil];
}

- (void)controlValueDidChange:(float)value sender:(id)sender
{
    //	NSLog(@"Slider Value: %f", value);
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    currentTextSize = 150;
    isLoadFirstTime = TRUE;

    [self setcurrenttextsize];

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.toolbar setHidden:TRUE];
    [webView setOpaque:NO];
    [webView setDelegate:self];
    webView.backgroundColor=[UIColor clearColor];    

	UIScrollView* sv = nil;
	for (UIView* v in  webView.subviews) {
		if([v isKindOfClass:[UIScrollView class]]){
			sv = (UIScrollView*) v;
			sv.scrollEnabled = NO;
			sv.bounces = NO;
		}
	}

    
    //Webview
	UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] ;
	[rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	
	UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] ;
	[leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.view addGestureRecognizer:rightSwipeRecognizer];
	[self.view addGestureRecognizer:leftSwipeRecognizer];
    [self performSelector:@selector(stratRolling)];
    
    back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    chapter_title_toc = [UIButton buttonWithType:UIButtonTypeCustom];
    [chapter_title_toc addTarget:self action:@selector(showChapterIndex:) forControlEvents:UIControlEventTouchUpInside];
   
    toc = [UIButton buttonWithType:UIButtonTypeCustom];
	[toc addTarget:self action:@selector(showChapterIndex:) forControlEvents:UIControlEventTouchUpInside];
    
    add_bookmark = [UIButton buttonWithType:UIButtonTypeCustom];
	[add_bookmark addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
    
    show_bookmark = [UIButton buttonWithType:UIButtonTypeCustom];
	[show_bookmark addTarget:self action:@selector(showBookmark:) forControlEvents:UIControlEventTouchUpInside];

    dayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[dayBtn addTarget:self action:@selector(dayBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    increase = [UIButton buttonWithType:UIButtonTypeCustom];
    increase.tag = 2;
	[increase addTarget:self action:@selector(increaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside];//increaseTextSizeClicked

    decrease = [UIButton buttonWithType:UIButtonTypeCustom];
    decrease.tag = 1;
	[decrease addTarget:self action:@selector(decreaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside]; //decreaseTextSizeClicked
    
    load_back = [[UIView alloc] init];

    if(IsIpad())
    {
        load_back.frame         =   CGRectMake(self.view.frame.size.width/2-40, self.view.frame.size.height/2-40, 80, 80);
        webView.frame           =   CGRectMake(self.view.frame.size.width-723,self.view.frame.size.height-923,656,830);
        back.frame              =   CGRectMake(self.view.frame.size.width-668, self.view.frame.size.height-984, 60, 32);
        toc.frame               =   CGRectMake(self.view.frame.size.width-578, self.view.frame.size.height-984, 36, 32);
        add_bookmark.frame      =   CGRectMake(self.view.frame.size.width-518, self.view.frame.size.height-984, 36, 32);
        show_bookmark.frame     =   CGRectMake(self.view.frame.size.width-400, self.view.frame.size.height-984, 36, 32);
        dayBtn.frame            =   CGRectMake(self.view.frame.size.width-268, self.view.frame.size.height-984, 32, 32);
        increase.frame          =   CGRectMake(self.view.frame.size.width-168, self.view.frame.size.height-984, 32, 32);
        decrease.frame          =   CGRectMake(self.view.frame.size.width-118, self.view.frame.size.height-984, 32, 32);
        chapter_title_toc.frame  =   CGRectMake(30, self.view.frame.size.height-60, self.view.frame.size.width-118, 32);
    }
    else
    {   float x = self.view.frame.size.width/2-64;
        float y = self.view.frame.size.height/2-32;

        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        
        load_back.frame             =   CGRectMake(x-200, y-280, 100, 100);
        webView.frame               =   CGRectMake(x-300,y-420,280,screenHeight-80);
        currentPageLabel.frame      =   CGRectMake(x-197, screenHeight-40, 75, 21);
        
        back.frame                  =   CGRectMake(x-300, y-460, 40, 28);
        toc.frame                   =   CGRectMake(x-250, y-460, 28, 28);
        add_bookmark.frame          =   CGRectMake(x-200, y-460, 36, 32);
        show_bookmark.frame         =   CGRectMake(x-160, y-460, 36, 32);
        dayBtn.frame                =   CGRectMake(x-120, y-460, 28, 28);
        increase.frame              =   CGRectMake(x-80, y-460, 28, 28);
        decrease.frame              =   CGRectMake(x-40, y-460, 28, 28);
    }

    UIButton * fbsharebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [fbsharebtn setTitle:@"Share" forState:UIControlStateNormal];
    fbsharebtn.frame = CGRectMake(self.view.frame.size.width-400,self.view.frame.size.height-984, 80 ,30);
    [fbsharebtn setImage:[UIImage imageNamed:@"button_blue.png"] forState:UIControlStateNormal];
    [fbsharebtn addTarget:self action:@selector(FBpublish:) forControlEvents:UIControlStateNormal];
    //[self.view addSubview:fbsharebtn];

    [self.view addSubview:chapter_title_toc];
    [self.view addSubview:back];
    [self.view addSubview:toc];
    [self.view addSubview:dayBtn]; 
    [self.view addSubview:increase];
    [self.view addSubview:decrease];
    //[self.view addSubview:add_bookmark];
    //[self.view addSubview:show_bookmark];
    [self.view addSubview:load_back];
    [self.view addSubview:currentChaptitle];

    btnlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, back.frame.size.width-1, back.frame.size.height-1)];
    btnlabel.text = @"Back";
    btnlabel.font = [UIFont boldSystemFontOfSize: 15.0];
    btnlabel.adjustsFontSizeToFitWidth = YES;
    btnlabel.minimumFontSize = 10.0;
    btnlabel.textAlignment = UITextAlignmentCenter;
    btnlabel.backgroundColor = [UIColor clearColor];    
    [back addSubview:btnlabel];
    
    currentChaptitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, chapter_title_toc.frame.size.width-1, chapter_title_toc.frame.size.height-1)];
   // currentChaptitle.font = [UIFont fontWithName:@"MVF" size:20.0];
    currentChaptitle.adjustsFontSizeToFitWidth = YES;
    currentChaptitle.textAlignment = UITextAlignmentLeft;
    currentChaptitle.backgroundColor = [UIColor clearColor];
    [chapter_title_toc addSubview:currentChaptitle];
    
    loadingIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingIndicator.center = CGPointMake(load_back.frame.size.width/2, load_back.frame.size.height/2);
    [loadingIndicator startAnimating];
    toolbar.alpha = 0.8;

    [load_back addSubview:loadingIndicator];
    load_back.hidden = TRUE;
    
    
    [load_back.layer setMasksToBounds:YES];
    [load_back.layer setCornerRadius:5.0f];
    [load_back.layer setBorderWidth:0.0f];
    load_back.backgroundColor = [UIColor clearColor];
    load_back.layer.shadowColor = [UIColor blackColor].CGColor;
    load_back.layer.shadowOpacity = 0.6f;
    load_back.layer.shadowOffset = CGSizeMake(7.0f, 7.0f);
    load_back.layer.shadowRadius = 4.0f;
    load_back.layer.masksToBounds = NO;
    load_back.alpha = 0.5;
    
    [self setbookmenu];
}

- (void) setbookmenu {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"NIGHT"] )
    {
        [self nightmenu];
        
    }
    else
    {        
        [self daymenu];
    }
}


/* Day night functions*/

-(IBAction) dayBtnClicked:(id)sender {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"NIGHT"] ) {
        [self updateBookNightMode:@"DAY"];
        [self daymenu];
    }
    else {   
        [self updateBookNightMode:@"NIGHT"];
        [self nightmenu];
    }
}

-(void) updateBookNightMode:(NSString *) bookReadMode {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:bookReadMode forKey:BOOK_NIGHT_MODE];    
    [self setbOokNightMode];
}

-(void) updateTextSize {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setInteger: currentTextSize forKey: CURRENT_TEXT_SIZE];
}

-(void) nightmenu {
    if(IsIpad()) {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipadbookbg_night.png"]]];
    } else {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"iphonebookbg_night.png"]]];
    }

    load_back.backgroundColor = [UIColor lightGrayColor];
    [back setImage:[UIImage imageNamed:@"night_library_back.png"] forState:UIControlStateNormal];
    [toc setImage:[UIImage imageNamed:@"night_chaptertoc.png"] forState:UIControlStateNormal];
    [dayBtn setImage:[UIImage imageNamed: @"night.png"] forState:UIControlStateNormal]; //done
    [increase setImage:[UIImage imageNamed:@"nightA+.png"] forState:UIControlStateNormal];
    [decrease setImage:[UIImage imageNamed:@"nightA-.png"] forState:UIControlStateNormal];

    //[show_bookmark setImage:[UIImage imageNamed:@"night_chaptertoc.png"] forState:UIControlStateNormal];
    //[add_bookmark setImage:[UIImage imageNamed:@"nightA+.png"] forState:UIControlStateNormal];

    btnlabel.textColor = [UIColor blackColor];
    currentChaptitle.textColor = [UIColor whiteColor];
}

-(void) daymenu {

    if(IsIpad()) {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipadbookbg_day.png"]]];
    } else {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"iphonebookbg_day.png"]]];
    }

    load_back.backgroundColor = [UIColor blackColor];
    [back setImage:[UIImage imageNamed:@"day_library_back.png"] forState:UIControlStateNormal];
    [toc setImage:[UIImage imageNamed:@"day_chaptertoc.png"] forState:UIControlStateNormal];
    [dayBtn setImage:[UIImage imageNamed: @"day.png"] forState:UIControlStateNormal]; //edit
    [increase setImage:[UIImage imageNamed:@"dayA+.png"] forState:UIControlStateNormal];
    [decrease setImage:[UIImage imageNamed:@"dayA-.png"] forState:UIControlStateNormal];
    btnlabel.textColor = [UIColor whiteColor];
     currentChaptitle.textColor = [UIColor blackColor];

    //[show_bookmark setImage:[UIImage imageNamed:@"day_chaptertoc.png"] forState:UIControlStateNormal];
    //[add_bookmark setImage:[UIImage imageNamed:@"dayA+.png"] forState:UIControlStateNormal];
}

/* Day night functions*/

//  Epub Loading animation spinner

- (void)stratRolling {
    load_back.hidden = FALSE;
    [loadingIndicator startAnimating];
    currentPageLabel.hidden=TRUE;
    currentChaptitle.hidden=TRUE;
}

- (void)stopRolling {
    [loadingIndicator stopAnimating];
    [loadingIndicator setHidesWhenStopped:YES];
    load_back.hidden = TRUE;
    currentPageLabel.hidden=FALSE;
    currentChaptitle.hidden=FALSE;
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return NO;
 }
*/

- (NSInteger)highlightAllOccurencesOfString:(NSString*)str {
    // The JS File  
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"UIWebViewSearch" ofType:@"js" inDirectory:@""];
    NSData *fileData    = [NSData dataWithContentsOfFile:filePath];
    NSString *jsString  = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    // The JS Function
    NSString *startSearch   = [NSString stringWithFormat:@"uiWebview_HighlightAllOccurencesOfString('%@')",str];
    [webView stringByEvaluatingJavaScriptFromString:startSearch];
    
    // Search Occurence Count
    // uiWebview_SearchResultCount - is a javascript var
    NSString *result        = [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_SearchResultCount"];
    return [result integerValue];
}

- (void)removeAllHighlights {
    // calls the javascript function to remove html highlights
    [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_RemoveAllHighlights()"];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload  {
    [super viewDidUnload];
    self.toolbar = nil;
	self.webView = nil;
    [self.webView release];
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.currentPageLabel = nil;
	[loadedEpub release];
	//[chaptersPopover release];
	[currentSearchResult release];
    
    [content_id release], content_id=nil;
   // [add_bookmark release], add_bookmark=nil;
    //[show_bookmark release], show_bookmark=nil;
    [chap_no release], chap_no=nil;
    [chap_page release], chap_page=nil;
    
    [currentChaptitle release] , currentChaptitle=nil;
    [chapter_title_toc release], chapter_title_toc=nil;
    
    [back release], back=nil;
    [btnlabel release], btnlabel=nil;
    [toc release], toc=nil;
    [increase release], increase=nil;
    [decrease release], decrease=nil;
    [dayBtn release], dayBtn=nil;
}


- (void)dealloc {
    self.toolbar = nil;
	self.webView = nil;
    [self.webView release];
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.currentPageLabel = nil;
	[loadedEpub release];
	//[chaptersPopover release];
	[currentSearchResult release];

    [content_id release], content_id=nil;
    [add_bookmark release], add_bookmark=nil;
    [show_bookmark release], show_bookmark=nil;
    [chap_no release], chap_no=nil;
    [chap_page release], chap_page=nil;
    
    [currentChaptitle release] , currentChaptitle=nil;
    [chapter_title_toc release], chapter_title_toc=nil;
    
    [back release], back=nil;
    [btnlabel release], btnlabel=nil;
    [toc release], toc=nil;
    [increase release], increase=nil;
    [decrease release], decrease=nil;
    [dayBtn release], dayBtn=nil;
    [super dealloc];
}


/*******START OF WEBVIEW HIGHLIGHT*********/


- (IBAction)markHighlightedString:(id)sender {
    
    // The JS File   
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"HighlightedString" ofType:@"js" inDirectory:@""];
    NSData *fileData    = [NSData dataWithContentsOfFile:filePath];
    NSString *jsString  = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    // The JS Function
    NSString *startSearch   = [NSString stringWithFormat:@"stylizeHighlightedString()"];
    [webView stringByEvaluatingJavaScriptFromString:startSearch];
    
    //NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", startPoint.x, startPoint.y];
    //NSString *urlToSave = [self.wvBrowser stringByEvaluatingJavaScriptFromString:imgURL];
}

- (IBAction)getHighlightedString:(id)sender {

    // The JS File   
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"HighlightedString" ofType:@"js" inDirectory:@""];
    NSData *fileData    = [NSData dataWithContentsOfFile:filePath];
    NSString *jsString  = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    // The JS Function
    NSString *startSearch   = [NSString stringWithFormat:@"getHighlightedString()"];
    [webView stringByEvaluatingJavaScriptFromString:startSearch];
    
    NSString *selectedText   = [NSString stringWithFormat:@"selectedText"];
    NSString * highlightedString = [webView stringByEvaluatingJavaScriptFromString:selectedText];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Highlighted String" 
                                                    message:highlightedString
                                                   delegate:nil 
                                          cancelButtonTitle:@"Oh Yeah" 
                                          otherButtonTitles:nil];
    [alert show];
    //[alert release]; // not required anymore because of ARC
}


/*******END OF WEBVIEW HIGHLIGHT*********/

// MV modifications started

-(BOOL)shouldAutoRotate
{
    return YES;
    
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

// MV modifications completed

@end
