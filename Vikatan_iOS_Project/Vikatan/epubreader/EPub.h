//
//  EPubParser.h
//  AePubReader
//
//  Created by Federico Frappi on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TouchXML.h"
		

@interface EPub : NSObject {
	NSArray* spineArray;
	NSString* epubFilePath;
    NSString * content_id;
    NSString * viewType;
}

@property(nonatomic, retain) NSArray* spineArray;

@property(nonatomic, retain) NSString * content_id;

//- (id) initWithEPubPath:(NSString*)path;
- (id) initWithEPubPath:(NSString *)path contentid:(NSString *)contentid type:(NSString *) viewtype;
- (void) parseEpub:(NSString *) contentid;
- (NSString *) getcontentPath;

@end
