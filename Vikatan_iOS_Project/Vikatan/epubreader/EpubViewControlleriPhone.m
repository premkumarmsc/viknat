//
//  EPubViewController.m
//  EpubDownload
//
//  Created by Macminiserver on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EpubViewControlleriPhone.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+SearchWebView.h"
#import "Chapter.h"
#import "ChapterList.h"
#import <QuartzCore/QuartzCore.h>


@interface EpubViewControlleriPhone()

//Spine 
- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;

- (int) getGlobalPageCount;
- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;


@end

@implementation EpubViewControlleriPhone

@synthesize loadedEpub, toolbar, webView; 
@synthesize chapterListButton,decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, searching;
@synthesize currentSearchResult;
@synthesize loadingView;

@synthesize bookName;

- (void) loadEpub:(NSURL*) epubURL{
    isBarShow = YES;
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
	searching = NO;
    epubLoaded = NO;
    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path]];
    epubLoaded = YES;
    [self performSelector:@selector(updatePagination) withObject:nil afterDelay:0.0];
}

- (void) chapterDidFinishLoad:(Chapter *)chapter{
    totalPagesCount+= chapter.pageCount;
	if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
		[currentPageLabel setText:[NSString stringWithFormat:@" ... /%d", totalPagesCount]];
	} 
    else {
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
		paginating = NO;
        [self performSelector:@selector(stopRolling) withObject:nil afterDelay:0.0];
	}
}

- (int) getGlobalPageCount{
	int pageCount = 0;
	for(int i=0; i<currentSpineIndex; i++){
		pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount]; 
	}
	pageCount+=currentPageInSpineIndex+1;
	return pageCount;
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
	[self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
    
    [loadingIndicator startAnimating];
	webView.hidden = YES;	
	self.currentSearchResult = theResult;
    
    //iphone popView
	//[chaptersPopover dismissPopoverAnimated:YES];
    
    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];    
    //html webView loading
    
    //webView.frame = CGRectMake(0,50,320,480);
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
	currentPageInSpineIndex = pageIndex;
	currentSpineIndex = spineIndex;
	if(!paginating){
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
	}
}

- (void) gotoPageInCurrentSpine:(int)pageIndex{
	if(pageIndex>=pagesInCurrentSpineCount){
		pageIndex = pagesInCurrentSpineCount - 1;
		currentPageInSpineIndex = pagesInCurrentSpineCount - 1;	
	}
	
	float pageOffset = pageIndex*webView.bounds.size.width;
    
	NSString *goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
	NSString *goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
	
	[webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
	[webView stringByEvaluatingJavaScriptFromString:goTo];
	
	if(!paginating){
		//[currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
        [currentPageLabel setText:[NSString stringWithFormat:@"%d",[self getGlobalPageCount]]];
	}
	webView.hidden = NO;
}

- (void) gotoNextSpine {
	if(!paginating){
		if(currentSpineIndex+1<[loadedEpub.spineArray count]){
			[self loadSpine:++currentSpineIndex atPageIndex:0];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];
          //  [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		}
	}
}

- (void) gotoPrevSpine {
    if(!paginating){
        if(currentSpineIndex-1>=0){
			[self loadSpine:--currentSpineIndex atPageIndex:0];
		}	
	}
}

- (void) gotoNextPage {
	if(!paginating){
        if(currentPageInSpineIndex+1<pagesInCurrentSpineCount){
			[self gotoPageInCurrentSpine:++currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];
           // [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		} 
        else {
			[self gotoNextSpine];
		}
    }
}

- (void) gotoPrevPage {
	if (!paginating) {
        if(currentPageInSpineIndex-1>=0){
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageUnCurl"];
            [transition setSubtype:@"fromRight"];
           // [self.webView.layer addAnimation:transition forKey:@"UnCurlAnim"];
		} 
        else {
			if(currentSpineIndex!=0){
                CATransition *transition = [CATransition animation];
                [transition setDelegate:self];
                [transition setDuration:0.5f];
                [transition setType:@"pageUnCurl"];
                [transition setSubtype:@"fromRight"];
               // [self.webView.layer addAnimation:transition forKey:@"UnCurlAnim"];
				int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
				[self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
			}
		}
	}
}

- (IBAction) increaseTextSizeClicked:(id)sender{
	if(!paginating){
		if(currentTextSize+25<=210){
			currentTextSize+=25;
			[self updatePagination];
			if(currentTextSize == 210){
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
	}
}

- (IBAction) decreaseTextSizeClicked:(id)sender{
    if(!paginating){
       
		if(currentTextSize-25>=50){
			currentTextSize-=25;
			[self updatePagination];
            
			if(currentTextSize==50){
				[decTextSizeButton setEnabled:NO];
			}
			[incTextSizeButton setEnabled:YES];
		}
	}
}

//Iphone UIPopoverController，Crash
- (IBAction)showChapterIndex:(id)sender{
    ChapterList* chapterListView = [[ChapterList alloc] initWithNibName:nil bundle:[NSBundle mainBundle]];
    [chapterListView setEpubViewController:self];
    chapterListView.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    
    [self presentModalViewController:chapterListView animated:YES];
    [chapterListView release];
}

- (IBAction) backButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}


- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
	
    [[[webView subviews] lastObject] setScrollEnabled:NO];
    
	NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
	
	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
	"if (mySheet.addRule) {"
	"mySheet.addRule(selector, newRule);"								// For Internet Explorer
	"} else {"
	"ruleIndex = mySheet.cssRules.length;"
	"mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
	"}"
	"}";
	
    
	NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", self.webView.frame.size.height, self.webView.frame.size.width];
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
	NSString *setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:auto;')", self.webView.frame.size.width *0.75];
    
    
    
    
    NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'background-color: black; color:white;')", self.webView.frame.size.width *0.75];
    
    NSString *bookFrame = [NSString stringWithFormat:@"addCSSRule('body', 'background: -webkit-gradient(linear, left top, right bottom, from(#FFF), to(#DDD)); -webkit-box-shadow: -3px 0 3px #311; -moz-box-shadow: -3px 0 3px #311;-webkit-border-top-right-radius: 3px;-webkit-border-bottom-right-radius: 3px; -moz-border-radius-topright: 3px;-moz-border-radius-bottomright: 3px;background-color: #F7F7F7;')", self.webView.frame.size.width *0.75];
                          
    
    NSString *changeFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:Bradley Hand;font-size:80;')"];
    
	[webView stringByEvaluatingJavaScriptFromString:varMySheet];
	[webView stringByEvaluatingJavaScriptFromString:addCSSRule];
	[webView stringByEvaluatingJavaScriptFromString:insertRule1];
	[webView stringByEvaluatingJavaScriptFromString:insertRule2];
	[webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
	[webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
	[webView stringByEvaluatingJavaScriptFromString:setImageRule];
   // [webView stringByEvaluatingJavaScriptFromString:setNightMode];
//    [webView stringByEvaluatingJavaScriptFromString:changeFont];
	
    if(currentSearchResult!=nil){
        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
	}
	
	int totalWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
	pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
     
    [self gotoPageInCurrentSpine:currentPageInSpineIndex];
    [loadingIndicator stopAnimating];
    // If no occurences of string, show alert message
   
}


- (void) updatePagination{
	if(epubLoaded){
        if(!paginating){
            paginating = YES;
            totalPagesCount=0;
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
            [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [currentPageLabel setText:@".../..."];
            
            [self removeAllHighlights];
            
            int resultCount = [self highlightAllOccurencesOfString:@"child"];
        }
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */
- (void)controlValueDidChange:(float)value sender:(id)sender
{
    //	NSLog(@"Slider Value: %f", value);
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES animated:YES];

    [self.toolbar setHidden:TRUE];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"bookbg.png"]]];
    //CGRect rt = CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height-50);
    //webView = [[UIWebView alloc] initWithFrame:self.view.bounds]; 
    [webView setOpaque:NO];
    webView.backgroundColor=[UIColor clearColor];
  //  [self.view addSubview:webView];
    
    loadingIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loadingIndicator.center = CGPointMake(toolbar.frame.size.width/2 ,self.view.frame.size.height/2);
    [loadingIndicator startAnimating];
    toolbar.alpha = 0.8;
    [self.view addSubview:loadingIndicator];
    
    [webView setDelegate:self];
    
	UIScrollView* sv = nil;
	for (UIView* v in  webView.subviews) {
		if([v isKindOfClass:[UIScrollView class]]){
			sv = (UIScrollView*) v;
			sv.scrollEnabled = NO;
			sv.bounces = NO;
		}
	}
	currentTextSize = 125;
	
    //Webview
	UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] ;
	[rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	
	UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] ;
	[leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    
    
  //  [webView addGestureRecognizer:rightSwipeRecognizer];
	//[webView addGestureRecognizer:leftSwipeRecognizer];
    
    [self.view addGestureRecognizer:rightSwipeRecognizer];
	[self.view addGestureRecognizer:leftSwipeRecognizer];
    
    [self performSelector:@selector(stratRolling)];
    
    
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
	back.frame =  CGRectMake(100, 40, 79, 41);
    [back setImage:[UIImage imageNamed:@"libraryback.png"] forState:UIControlStateNormal];
	[back addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:back];
    
    UIButton* toc = [UIButton buttonWithType:UIButtonTypeCustom];
	toc.frame =  CGRectMake(190, 40, 79, 41);
    [toc setImage:[UIImage imageNamed:@"chaptertoc.png"] forState:UIControlStateNormal];
	[toc addTarget:self action:@selector(showChapterIndex:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:toc];
    
    
    
    //fontView = [[UIView alloc] initWithFrame:CGRectMake(570,0,100,48)];A+_32
    UIButton* increase = [UIButton buttonWithType:UIButtonTypeCustom];
	[increase setImage:[UIImage imageNamed:@"Font_Inc.png"] forState:UIControlStateNormal];
	increase.frame =  CGRectMake(600, 35, 48, 48);
    increase.tag = 2;
	[increase addTarget:self action:@selector(increaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:increase];
    
    UIButton* decrease = [UIButton buttonWithType:UIButtonTypeCustom];
	[decrease setImage:[UIImage imageNamed:@"Font_Dec.png"] forState:UIControlStateNormal];
	decrease.frame =  CGRectMake(650, 35, 48, 48);
    decrease.tag = 1;
	[decrease addTarget:self action:@selector(decreaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside];
    

    /*
    UILabel *bookTitle = [[[UILabel alloc] initWithFrame:CGRectMake(350,40 , 200, 30)] autorelease];
    bookTitle.text = @"BOOk NAME";
    bookTitle.backgroundColor = [UIColor clearColor];
    bookTitle.textColor = [UIColor grayColor];
    [self.view addSubview:bookTitle];
     */
    
    [self.view addSubview:decrease];

}

//Epub Loading
- (void)stratRolling{
    [loadingIndicator startAnimating];
}

- (void)stopRolling{
    [loadingIndicator stopAnimating];
    [loadingIndicator setHidesWhenStopped:YES];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return NO;
 }
 */
- (NSInteger)highlightAllOccurencesOfString:(NSString*)str
{
    // The JS File   
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"UIWebViewSearch" ofType:@"js" inDirectory:@""];
    NSData *fileData    = [NSData dataWithContentsOfFile:filePath];
    NSString *jsString  = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    // The JS Function
    NSString *startSearch   = [NSString stringWithFormat:@"uiWebview_HighlightAllOccurencesOfString('%@')",str];
    [webView stringByEvaluatingJavaScriptFromString:startSearch];
    
    // Search Occurence Count
    // uiWebview_SearchResultCount - is a javascript var
    NSString *result        = [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_SearchResultCount"];
    return [result integerValue];
}

- (void)removeAllHighlights
{
    // calls the javascript function to remove html highlights
    [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_RemoveAllHighlights()"];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.currentPageLabel = nil;
	[loadedEpub release];
	//[chaptersPopover release];
	[currentSearchResult release];
    [super dealloc];
}

@end
