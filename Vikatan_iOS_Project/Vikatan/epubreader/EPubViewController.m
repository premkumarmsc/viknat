//
//  EPubViewController.m
//  EpubDownload
//
//  Created by Macminiserver on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EpubViewController.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+SearchWebView.h"
#import "Chapter.h"
#import "ChapterList.h"
#import <QuartzCore/QuartzCore.h>


@interface EpubViewController()

//Spine 
- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;

- (int) getGlobalPageCount;
- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;


@end

@implementation EpubViewController

@synthesize loadedEpub, toolbar, webView; 
@synthesize chapterListButton,decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, searching;
@synthesize currentSearchResult;
@synthesize loadingView,bg;

@synthesize bookName;

- (void) loadEpub:(NSURL*) epubURL{
    isBarShow = YES;
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
	searching = NO;
    epubLoaded = NO;
    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path]];
    epubLoaded = YES;
    [self performSelector:@selector(updatePagination) withObject:nil afterDelay:0.0];
}

- (void) chapterDidFinishLoad:(Chapter *)chapter
{
    totalPagesCount+= chapter.pageCount;
	if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){

        [self performSelector:@selector(stratRolling) withObject:nil afterDelay:0.0];
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
		[[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
		[currentPageLabel setText:[NSString stringWithFormat:@" ... / %d", totalPagesCount]];
	} 
    else {
        [currentPageLabel setText:[NSString stringWithFormat:@"%d / %d",[self getGlobalPageCount], totalPagesCount]];
		paginating = NO;
        [self performSelector:@selector(stopRolling) withObject:nil afterDelay:0.0];
	}
    
    
    [self setbOokNightMode];
    [self setcurrenttextsize];
    
    // [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"bookbg.png"]]];
    //[self.view setBackgroundColor:[UIColor blackColor]];
}

- (int) getGlobalPageCount{
	int pageCount = 0;
	for(int i=0; i<currentSpineIndex; i++){
		pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount]; 
	}
	pageCount+=currentPageInSpineIndex+1;
	return pageCount;
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
    // UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Load spine" message:@"Load spine" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //[alert show];
    
	[self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
    
    [loadingIndicator startAnimating];
	webView.hidden = YES;	
	self.currentSearchResult = theResult;
    
    //iphone popView
	//[chaptersPopover dismissPopoverAnimated:YES];
    
    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];    
    //html webView loading
    
    //webView.frame = CGRectMake(0,50,320,480);
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
	currentPageInSpineIndex = pageIndex;
	currentSpineIndex = spineIndex;
	if(!paginating){
        [currentPageLabel setText:[NSString stringWithFormat:@"%d / %d",[self getGlobalPageCount], totalPagesCount]];
	}
}

/** Started changes made  */
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex fontsize:(int)fontsize{
    
    [loadingIndicator startAnimating];
	webView.hidden = YES;	
	//self.currentSearchResult = theResult;

    
    //iphone popView
	//[chaptersPopover dismissPopoverAnimated:YES];
    
    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];    
    //html webView loading
    
    //webView.frame = CGRectMake(0,50,320,480);
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    currentTextSize = fontsize;
	currentPageInSpineIndex = pageIndex;
	currentSpineIndex = spineIndex;
	if(!paginating){
        [currentPageLabel setText:[NSString stringWithFormat:@"%d / %d",[self getGlobalPageCount], totalPagesCount]];
	}
}

/** Started changes made  **/

- (void) gotoPageInCurrentSpine:(int)pageIndex{
	if(pageIndex>=pagesInCurrentSpineCount){
		pageIndex = pagesInCurrentSpineCount - 1;
		currentPageInSpineIndex = pagesInCurrentSpineCount - 1;	
	}
	
	float pageOffset = pageIndex*webView.bounds.size.width;
    
	NSString *goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
	NSString *goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
	
	[webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
	[webView stringByEvaluatingJavaScriptFromString:goTo];
	
	if(!paginating){
		[currentPageLabel setText:[NSString stringWithFormat:@"%d / %d",[self getGlobalPageCount], totalPagesCount]];
       // [currentPageLabel setText:[NSString stringWithFormat:@"%d",[self getGlobalPageCount]]];
	}
	webView.hidden = NO;
}

- (void) gotoNextSpine {
	if(!paginating){
		if(currentSpineIndex+1<[loadedEpub.spineArray count]){
			[self loadSpine:++currentSpineIndex atPageIndex:0];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];
            //  [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		}
	}
}

- (void) gotoPrevSpine {
    if(!paginating){
        if(currentSpineIndex-1>=0){
			[self loadSpine:--currentSpineIndex atPageIndex:0];
		}	
	}
}

- (void) gotoNextPage {
	if(!paginating){
        if(currentPageInSpineIndex+1<pagesInCurrentSpineCount){
			[self gotoPageInCurrentSpine:++currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageCurl"];
            [transition setSubtype:@"fromRight"];
            // [self.webView.layer addAnimation:transition forKey:@"CurlAnim"];
		} 
        else {
			[self gotoNextSpine];
		}
    }
}

- (void) gotoPrevPage {
	if (!paginating) {
        if(currentPageInSpineIndex-1>=0){
			[self gotoPageInCurrentSpine:--currentPageInSpineIndex];
            CATransition *transition = [CATransition animation];
            [transition setDelegate:self];
            [transition setDuration:0.5f];
            [transition setType:@"pageUnCurl"];
            [transition setSubtype:@"fromRight"];
            // [self.webView.layer addAnimation:transition forKey:@"UnCurlAnim"];
		} 
        else {
			if(currentSpineIndex!=0){
                CATransition *transition = [CATransition animation];
                [transition setDelegate:self];
                [transition setDuration:0.5f];
                [transition setType:@"pageUnCurl"];
                [transition setSubtype:@"fromRight"];
                // [self.webView.layer addAnimation:transition forKey:@"UnCurlAnim"];
				int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
				[self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
			}
		}
	}
}

- (IBAction) increaseTextSizeClicked:(id)sender{
	if(!paginating){
		if(currentTextSize+25<=210){
			currentTextSize+=25;
            [self updateTextSize];
			[self updatePagination];
            
			if(currentTextSize == 210){
				[incTextSizeButton setEnabled:NO];
			}
			[decTextSizeButton setEnabled:YES];
		}
	}
}

- (IBAction) decreaseTextSizeClicked:(id)sender{
    if(!paginating){
       
		if(currentTextSize-25>=50){
			currentTextSize-=25;
            [self updateTextSize];
			[self updatePagination];
            
			if(currentTextSize==50){
				[decTextSizeButton setEnabled:NO];
			}
			[incTextSizeButton setEnabled:YES];
		}
	}
}

//Iphone UIPopoverController Crash
- (IBAction)showChapterIndex:(id)sender{
    ChapterList* chapterListView = [[ChapterList alloc] initWithNibName:nil bundle:[NSBundle mainBundle]];
    [chapterListView setEpubViewController:self];
    chapterListView.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    
    [self presentModalViewController:chapterListView animated:YES];
    [chapterListView release];
}

- (IBAction) backButton:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}


- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
	
    [[[webView subviews] lastObject] setScrollEnabled:NO];
    
	NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
	
	NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
	"if (mySheet.addRule) {"
	"mySheet.addRule(selector, newRule);"								// For Internet Explorer
	"} else {"
	"ruleIndex = mySheet.cssRules.length;"
	"mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
	"}"
	"}";
	
    
	NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", self.webView.frame.size.height, self.webView.frame.size.width];
	NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
	NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
	NSString *setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
    NSString *setImageRule = [NSString stringWithFormat:@"addCSSRule('img', 'max-width: %fpx; height:auto;')", self.webView.frame.size.width *0.75];
    
    
    // NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'background-color: black; color:white;')", self.webView.frame.size.width *0.75];
    
    //NSString *bookFrame = [NSString stringWithFormat:@"addCSSRule('body', 'background: -webkit-gradient(linear, left top, right bottom, from(#FFF), to(#DDD)); -webkit-box-shadow: -3px 0 3px #311; -moz-box-shadow: -3px 0 3px #311;-webkit-border-top-right-radius: 3px;-webkit-border-bottom-right-radius: 3px; -moz-border-radius-topright: 3px;-moz-border-radius-bottomright: 3px;background-color: #F7F7F7;')", self.webView.frame.size.width *0.75];
    
    
    // NSString *changeFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:Bradley Hand;font-size:80;color:green;')"];
    // src:'/MVF.ttf'
    
    NSString * cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)   objectAtIndex:0];
    NSString * ttfpath = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:MVF; src:url(\"%@%@\")')", cachePath,@"/MVF.ttf"];
    
    NSLog(@"TTF Path is %@", ttfpath);
    
    // NSString *changeFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:MVF;')"];
    //  NSString *changeFontTTF = [NSString stringWithFormat:@"addCSSRule('body', 'src:url(../../MVF.ttf);')"];
    
	[webView stringByEvaluatingJavaScriptFromString:varMySheet];
	[webView stringByEvaluatingJavaScriptFromString:addCSSRule];
	[webView stringByEvaluatingJavaScriptFromString:insertRule1];
	[webView stringByEvaluatingJavaScriptFromString:insertRule2];
	[webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
	[webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
	[webView stringByEvaluatingJavaScriptFromString:setImageRule];
    //[webView stringByEvaluatingJavaScriptFromString:changeFont];
    //[webView stringByEvaluatingJavaScriptFromString:changeFontTTF];
	
    if(currentSearchResult!=nil){
        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
	}
	
	int totalWidth = [[self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
	pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
    
    NSLog(@"pagesInCurrentSpineCount %i",pagesInCurrentSpineCount);
	NSLog(@"currentPageInSpineIndex %i",currentPageInSpineIndex);
    
    [self gotoPageInCurrentSpine:currentPageInSpineIndex];
    [loadingIndicator stopAnimating];
    
    [self setbOokNightMode];
    
    
    // [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"bookbg.png"]]];
    //[self.view setBackgroundColor:[UIColor blackColor]];
    
    // If no occurences of string, show alert message
    
}

- (void) setbOokNightMode {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"TRUE"] )
    {
        NSLog(@"BOOK_NIGHT_MODE == TRUE");
        NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'color:white;')", self.webView.frame.size.width *0.75]; //
        [webView stringByEvaluatingJavaScriptFromString:setNightMode];
        
        //[self updatePagination];
        
    }
    else
    {
        NSLog(@"BOOK_NIGHT_MODE == FALSE");
        
        NSString *setNightMode = [NSString stringWithFormat:@"addCSSRule('body', 'color:black;')", self.webView.frame.size.width *0.75];
        [webView stringByEvaluatingJavaScriptFromString:setNightMode];
        
        
        // [self updatePagination];
    }
}

- (void) updatePagination{
	if(epubLoaded){
        if(!paginating){
            paginating = YES;
            totalPagesCount=0;
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
            [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [currentPageLabel setText:@"... / ..."];
            
            [self removeAllHighlights];
            
            int resultCount = [self highlightAllOccurencesOfString:@"child"];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */
- (void)controlValueDidChange:(float)value sender:(id)sender
{
    //	NSLog(@"Slider Value: %f", value);
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setcurrenttextsize];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.toolbar setHidden:TRUE];
    [webView setOpaque:NO];
    [webView setDelegate:self];
    webView.backgroundColor=[UIColor clearColor];    
    
	UIScrollView* sv = nil;
	for (UIView* v in  webView.subviews) {
		if([v isKindOfClass:[UIScrollView class]]){
			sv = (UIScrollView*) v;
			sv.scrollEnabled = NO;
			sv.bounces = NO;
		}
	}
	
	
    loadingIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loadingIndicator.center = self.view.center;
    [loadingIndicator startAnimating];
    toolbar.alpha = 0.8;
    [self.view addSubview:loadingIndicator];
    
    //Webview
	UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] ;
	[rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	
	UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] ;
	[leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.view addGestureRecognizer:rightSwipeRecognizer];
	[self.view addGestureRecognizer:leftSwipeRecognizer];
    [self performSelector:@selector(stratRolling)];
    
    back = [UIButton buttonWithType:UIButtonTypeCustom];
	back.frame =  CGRectMake(20, 15, 40, 28);
	[back addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    btnlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, back.frame.size.width, back.frame.size.height)];
    btnlabel.text = @"Back";
    btnlabel.font = [UIFont boldSystemFontOfSize: 12.0];
    btnlabel.adjustsFontSizeToFitWidth = YES;
    btnlabel.minimumFontSize = 10.0;
    btnlabel.textAlignment = UITextAlignmentCenter;
    btnlabel.backgroundColor = [UIColor clearColor];    
    [back addSubview:btnlabel];
    
    toc = [UIButton buttonWithType:UIButtonTypeCustom];
	toc.frame =  CGRectMake(70, 15, 28, 28);
	[toc addTarget:self action:@selector(showChapterIndex:) forControlEvents:UIControlEventTouchUpInside];
    
    dayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	dayBtn.frame =  CGRectMake(190, 15, 28, 28);
    dayBtn.tag = 2;
	[dayBtn addTarget:self action:@selector(dayBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    increase = [UIButton buttonWithType:UIButtonTypeCustom];
	increase.frame =  CGRectMake(230, 15, 28, 28);
    increase.tag = 2;
	[increase addTarget:self action:@selector(increaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    decrease = [UIButton buttonWithType:UIButtonTypeCustom];
	decrease.frame =  CGRectMake(270, 15, 28, 28);
    decrease.tag = 1;
	[decrease addTarget:self action:@selector(decreaseTextSizeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:back];
    [self.view addSubview:toc];
    [self.view addSubview:dayBtn]; 
    [self.view addSubview:increase];
    [self.view addSubview:decrease];
    
    [self setbookmenu];
    
}

- (void) setbookmenu {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"TRUE"] )
    {
        [self nightmenu];
        
    }
    else
    {        
        [self daymenu];
    }
}


/* Day night functions*/

-(IBAction) dayBtnClicked:(id)sender
{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if([[standardUserDefaults objectForKey:BOOK_NIGHT_MODE] isEqualToString:@"TRUE"] )
    {
        [self daymenu];
        [self updateBookNightMode:@"FALSE"];
        
    }
    else
    {   
        [self nightmenu];
        [self updateBookNightMode:@"TRUE"];
    }
    
}

-(void) updateBookNightMode:(NSString *) bookReadMode
{
    totalPagesCount = 0;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:bookReadMode forKey:BOOK_NIGHT_MODE];    
    
    [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
    [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
    [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
    
}


-(void) nightmenu {
    //[bg removeFromSuperview];
     //bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"iphonebookbg_night.png"]];
    //bg.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //[self.view addSubview:bg];[self.view sendSubviewToBack:bg];

    btnlabel.textColor = [UIColor blackColor];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"iphonebookbg_night.png"]]];
    [back setImage:[UIImage imageNamed:@"day_library_back.png"] forState:UIControlStateNormal];
    [toc setImage:[UIImage imageNamed:@"night_chaptertoc.png"] forState:UIControlStateNormal];
    [dayBtn setImage:[UIImage imageNamed: @"night.png"] forState:UIControlStateNormal]; //done
    [increase setImage:[UIImage imageNamed:@"nightA+.png"] forState:UIControlStateNormal];
    [decrease setImage:[UIImage imageNamed:@"nightA-.png"] forState:UIControlStateNormal];
    
}

-(void) daymenu {
    //[bg removeFromSuperview];
    //bg= [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"iphonebookbg_day.png"]];
    //bg.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //[self.view addSubview:bg];[self.view sendSubviewToBack:bg];
   // self.view.contentMode = UIViewContentModeScaleAspectFit;

    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"iphonebookbg_day.png"]]];
    
    btnlabel.textColor = [UIColor whiteColor];
    [back setImage:[UIImage imageNamed:@"night_library_back.png"] forState:UIControlStateNormal];
    [toc setImage:[UIImage imageNamed:@"day_chaptertoc.png"] forState:UIControlStateNormal];
    [dayBtn setImage:[UIImage imageNamed: @"day.png"] forState:UIControlStateNormal]; //edit
    [increase setImage:[UIImage imageNamed:@"dayA+.png"] forState:UIControlStateNormal];
    [decrease setImage:[UIImage imageNamed:@"dayA-.png"] forState:UIControlStateNormal];
    
}

-(void) setcurrenttextsize
{
    
    currentTextSize = [CURRENT_TEXT_SIZE intValue];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *value = (NSString *)[standardUserDefaults objectForKey:CURRENT_TEXT_SIZE];
    int i = [value intValue];
    
    if(i > 150 )
    {   
        currentTextSize = i;
    }
    
}

-(void) updateTextSize
{
    NSLog(@"updateTextSize");
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setInteger: currentTextSize forKey: CURRENT_TEXT_SIZE];
    
}

/* Day night functions*/

//Epub Loading
- (void)stratRolling{
    [loadingIndicator startAnimating];
}

- (void)stopRolling{
    [loadingIndicator stopAnimating];
    [loadingIndicator setHidesWhenStopped:YES];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return NO;
 }
 */
- (NSInteger)highlightAllOccurencesOfString:(NSString*)str
{
    // The JS File   
    NSString *filePath  = [[NSBundle mainBundle] pathForResource:@"UIWebViewSearch" ofType:@"js" inDirectory:@""];
    NSData *fileData    = [NSData dataWithContentsOfFile:filePath];
    NSString *jsString  = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    // The JS Function
    NSString *startSearch   = [NSString stringWithFormat:@"uiWebview_HighlightAllOccurencesOfString('%@')",str];
    [webView stringByEvaluatingJavaScriptFromString:startSearch];
    
    // Search Occurence Count
    // uiWebview_SearchResultCount - is a javascript var
    NSString *result        = [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_SearchResultCount"];
    return [result integerValue];
}

- (void)removeAllHighlights
{
    // calls the javascript function to remove html highlights
    [webView stringByEvaluatingJavaScriptFromString:@"uiWebview_RemoveAllHighlights()"];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc 
{
    self.toolbar = nil;
	self.webView = nil;
	self.chapterListButton = nil;
	self.decTextSizeButton = nil;
	self.incTextSizeButton = nil;
	self.currentPageLabel = nil;
	[loadedEpub release];
	//[chaptersPopover release];
	[currentSearchResult release];
    
    [back release], back=nil;
    [btnlabel release], btnlabel=nil;
    [toc release], toc=nil;
    [increase release], increase=nil;
    [decrease release], decrease=nil;
    [dayBtn release], dayBtn=nil;
    [super dealloc];
}

@end
