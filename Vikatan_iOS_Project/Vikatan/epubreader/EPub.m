//
//  EPub.m
//  AePubReader
//
//  Created by Federico Frappi on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EPub.h"
#import "ZipArchive.h"
#import "Chapter.h"

@interface EPub()

- (void) parseEpub;
- (void) unzipAndSaveFileNamed:(NSString*)fileName;
- (NSString*) applicationDocumentsDirectory;
- (NSString*) parseManifestFile;
- (void) parseOPF:(NSString*)opfPath;

@end

@implementation EPub

@synthesize spineArray, content_id;

- (id) initWithEPubPath:(NSString *)path contentid:(NSString *)contentid type:(NSString *) viewtype {
	if((self=[super init])){
		epubFilePath = [path retain];
		spineArray = [[NSMutableArray alloc] init];
        content_id = contentid;
        viewType = viewtype;
		[self parseEpub];
	}
	return self;
}

- (NSString *) getcontentPath {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    if([viewType isEqualToString:@"preview"]) {
        return [documentsDir stringByAppendingPathComponent:@"preview"];
    } else {
        return [documentsDir stringByAppendingPathComponent:content_id];
    }
}

- (void) parseEpub {
    
   // NSLog(@"strPath %@", epubFilePath);

    /* 
    NSString *strPath=[NSString stringWithFormat:@"%@/vikatanbook",[self getcontentPath]];
    NSFileManager *filemanager=[[NSFileManager alloc] init];
    if (![filemanager fileExistsAtPath:strPath]) {
        NSError *error;
        [self unzipAndSaveFileNamed:epubFilePath];
    }
    */
    
    [self unzipAndSaveFileNamed:epubFilePath];
	NSString* opfPath = [self parseManifestFile];
   // NSLog(@"parseManifestFile is %@", opfPath);

	[self parseOPF:opfPath];
}


- (void)unzipAndSaveFileNamed:(NSString*)fileName{
	ZipArchive* za = [[ZipArchive alloc] init];
//	NSLog(@"%@", fileName);
//	NSLog(@"unzipping %@", epubFilePath);
    
	if( [za UnzipOpenFile:epubFilePath]){
		//NSString *strPath=[NSString stringWithFormat:@"%@/UnzippedEpub",[self applicationDocumentsDirectory]];
        NSString *strPath=[NSString stringWithFormat:@"%@/vikatanbook",[self getcontentPath]];
		NSLog(@"strPath %@", strPath);
		//Document/UnzippedEpub
        
		NSFileManager *filemanager=[[NSFileManager alloc] init];
		if ([filemanager fileExistsAtPath:strPath]) {
			NSError *error;
			[filemanager removeItemAtPath:strPath error:&error];
		}
		[filemanager release];
		filemanager=nil;

		BOOL ret = [za UnzipFileTo:[NSString stringWithFormat:@"%@/",strPath] overWrite:YES];
		if( NO==ret ){
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Error"
														  message:@"Error while unzipping the epub"
														 delegate:self
												cancelButtonTitle:@"OK"
												otherButtonTitles:nil];
			[alert show];
			[alert release];
			alert=nil;
		}
		[za UnzipCloseFile];
	}					
	[za release];
}

//Document
- (NSString *)applicationDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

//META-INF
- (NSString*) parseManifestFile{
	NSString* manifestFilePath = [NSString stringWithFormat:@"%@/vikatanbook/META-INF/container.xml", [self getcontentPath]];
//	NSLog(@"%@", manifestFilePath);
	NSFileManager *fileManager = [[NSFileManager alloc] init];

	if ([fileManager fileExistsAtPath:manifestFilePath]) {
		NSLog(@"Valid epub");
		CXMLDocument* manifestFile = [[[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:manifestFilePath] options:0 error:nil] autorelease];
		CXMLNode* opfPath = [manifestFile nodeForXPath:@"//@full-path[1]" error:nil];
//		NSLog(@"%@", [NSString stringWithFormat:@"%@/UnzippedEpub/%@", [self applicationDocumentsDirectory], [opfPath stringValue]]);
		return [NSString stringWithFormat:@"%@/vikatanbook/%@", [self getcontentPath], [opfPath stringValue]];
        
	} 
    else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"MSG"
                                                      message:@"epub not found"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert show];
        [alert release];
		return nil;
	}
	[fileManager release];
}

//OPF
- (void) parseOPF:(NSString*)opfPath {
    NSLog(@"opfPath in parseOPF is %@", opfPath);
	CXMLDocument* opfFile = [[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:opfPath] options:0 error:nil];
	NSArray* itemsArray = [opfFile nodesForXPath:@"//opf:item" namespaceMappings:[NSDictionary dictionaryWithObject:@"http://www.idpf.org/2007/opf" forKey:@"opf"] error:nil];
	NSLog(@"itemsArray size: %d", [itemsArray count]);
    
    NSString* ncxFileName;
	
    NSMutableDictionary* itemDictionary = [[NSMutableDictionary alloc] init];
	for (CXMLElement *element in itemsArray) {
		[itemDictionary setValue:[[element attributeForName:@"href"] stringValue] forKey:[[element attributeForName:@"id"] stringValue]];
         
        if([[[element attributeForName:@"id"] stringValue] isEqualToString:@"ncx"]){
            ncxFileName = [[element attributeForName:@"href"] stringValue];
           // NSLog(@"ncxFileName is %@", ncxFileName);
            
            
        }
    }
    int lastSlash = [opfPath rangeOfString:@"/" options:NSBackwardsSearch].location;
	NSString* ebookBasePath = [opfPath substringToIndex:(lastSlash +1)];
    CXMLDocument* ncxToc = [[CXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", ebookBasePath, ncxFileName]] options:0 error:nil];
    
   // NSLog(@"NCX filename is %@",[NSString stringWithFormat:@"%@%@", ebookBasePath, ncxFileName]);
    NSMutableDictionary* titleDictionary = [[NSMutableDictionary alloc] init];
    
    for (CXMLElement* element in itemsArray) {
        NSString* href = [[element attributeForName:@"href"] stringValue];
        NSString* xpath = [NSString stringWithFormat:@"//ncx:content[@src='%@']/../ncx:navLabel/ncx:text", href];
        NSArray* navPoints = [ncxToc nodesForXPath:xpath namespaceMappings:[NSDictionary dictionaryWithObject:@"http://www.daisy.org/z3986/2005/ncx/" forKey:@"ncx"] error:nil];
       
        if([navPoints count]!=0){
            CXMLElement* titleElement = [navPoints objectAtIndex:0];
           [titleDictionary setValue:[titleElement stringValue] forKey:href];
        }
    }

	NSArray* itemRefsArray = [opfFile nodesForXPath:@"//opf:itemref" namespaceMappings:[NSDictionary dictionaryWithObject:@"http://www.idpf.org/2007/opf" forKey:@"opf"] error:nil];
	NSLog(@"itemRefsArray %@", itemRefsArray);
    
	NSMutableArray* tmpArray = [[NSMutableArray alloc] init];
    int count = 0;
	for (CXMLElement* element in itemRefsArray) {
        NSString* chapHref = [itemDictionary valueForKey:[[element attributeForName:@"idref"] stringValue]];
        
       // NSLog(@"chapHref in Epub.m %@", chapHref);

        Chapter* tmpChapter = [[Chapter alloc] initWithPath:[NSString stringWithFormat:@"%@%@", ebookBasePath, chapHref]
                                                       title:[titleDictionary valueForKey:chapHref] 
                                                chapterIndex:count++];
        
      //  NSLog(@"chapter title in Epub.m is %@", [titleDictionary valueForKey:chapHref]);
       // if([titleDictionary valueForKey:chapHref] != NULL) {
            [tmpArray addObject:tmpChapter];
		//}
		[tmpChapter release];
        NSLog(@"chapHref  %@",chapHref);
	}
	
    
	self.spineArray = [NSArray arrayWithArray:tmpArray]; 
    
   // NSLog(@"spineArray count is %d", [spineArray count]);
    
	NSLog(@"\n\n");
    NSLog(@"SpineArray  %@",spineArray);
    
	[opfFile release];
	[tmpArray release];
	[ncxToc release];
	[itemDictionary release];
	[titleDictionary release];
    
}

- (void)dealloc {
    [spineArray release];
	[epubFilePath release];
    [super dealloc];
}



@end
