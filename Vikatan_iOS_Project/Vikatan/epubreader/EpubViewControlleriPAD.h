//
//  EPubViewController.h
//  EpubDownload
//
//  Created by Macminiserver on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZipArchive.h"
#import "EPub.h"
#import "Chapter.h"
#import "Constant.h"
#import "DBClass.h"
#import <QuartzCore/QuartzCore.h>

@class SearchResultsViewController;
@class SearchResult;

@interface EpubViewControlleriPAD : UIViewController <UIWebViewDelegate, ChapterDelegate, UIGestureRecognizerDelegate> {
	IBOutlet UIToolbar *toolbar;
	
	UIWebView *webView;
    
    UIBarButtonItem *chapterListButton;
    
    UIBarButtonItem *decTextSizeButton;
    
	UIBarButtonItem *incTextSizeButton;
    UIBarButtonItem *backButton;
    UILabel* currentPageLabel;
    UILabel* currentChaptitle;
	
	EPub* loadedEpub;
	int currentSpineIndex;
	int currentPageInSpineIndex;
	int pagesInCurrentSpineCount;
	int currentTextSize;
	int totalPagesCount;
    
    BOOL epubLoaded;
    BOOL paginating;
    BOOL searching;
    BOOL isLoadFirstTime;
	
    UIAlertView *loadingView;
    BOOL isLoadingViewShow;
    BOOL isBarShow;
    
    UIActivityIndicatorView *loadingIndicator;
    
    NSString* bookName; 
    UIButton * dayBtn;
    UIButton* back;
    UIButton* toc;
    UIButton* increase;
    UIButton* decrease;
    UILabel *btnlabel;
    UIButton* chapter_title_toc;
    

    NSString * content_id;
    NSMutableArray * chap_page;
    NSMutableArray * chap_no;
    NSMutableArray * font_size;

    UIView *load_back;
    
    UIButton* add_bookmark;
    UIButton* show_bookmark;
}

- (IBAction) showChapterIndex:(id)sender;
- (IBAction) increaseTextSizeClicked:(id)sender;
- (IBAction) decreaseTextSizeClicked:(id)sender;
- (IBAction) backButton:(id)sender;
- (void) setbOokNightMode;
- (void) setbookmenu;
-(void) daymenu;
-(void) nightmenu;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult;


//- (void) loadEpub:(NSURL*) epubURL;
- (NSInteger)highlightAllOccurencesOfString:(NSString*)str;
- (void)removeAllHighlights;
-(void) updateBookNightMode:(NSString *) bookReadMode;
- (void) editModeDone:(NSString *) bookReadMode;
-(void) updateTextSize;
-(void) setcurrenttextsize;
- (void) loadEpub:(NSURL*) epubURL contentid: (NSString *) contentid type:(NSString *) viewtype;
- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex fontsize:(int)fontsize;
-(void) stratRolling;
-(void) stopRolling;
-(void) gettotalpage;


@property (nonatomic, retain) EPub* loadedEpub;
@property (nonatomic, retain) SearchResult* currentSearchResult;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *chapterListButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *decTextSizeButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *incTextSizeButton;
@property (nonatomic, retain) IBOutlet UILabel *currentPageLabel;
@property (nonatomic, retain)  NSString *bookName;
@property BOOL searching;
@property (nonatomic, retain)UIAlertView *loadingView;
@property (nonatomic, retain) NSString * content_id;
@property (nonatomic, retain) NSMutableArray * chap_page;
@property (nonatomic, retain) NSMutableArray * chap_no;
@property (nonatomic, retain) NSMutableArray * font_size;

@end
