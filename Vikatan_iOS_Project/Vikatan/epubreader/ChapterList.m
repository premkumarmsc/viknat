//
//  ChapterList.m
//  EpubDownload
//
//  Created by Macminiserver on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ChapterList.h"

@implementation ChapterList

@synthesize epubViewController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.contentSizeForViewInPopover = CGSizeMake(220,264.0);
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    tempArray = [[NSMutableArray alloc]init];
    spineArrayIndex = [[NSMutableDictionary alloc] init];
    int i=0;
    
    for(int y = 0; y<[epubViewController.loadedEpub.spineArray count]; y++) {
        if ([[epubViewController.loadedEpub.spineArray objectAtIndex:y] title]!=NULL) {
            
            
            [spineArrayIndex setValue:[NSString stringWithFormat:@"%d", y] forKey:[NSString stringWithFormat:@"%d", i]];
            [tempArray addObject:[epubViewController.loadedEpub.spineArray objectAtIndex:y]];
            i++;
        }
    }

    [super viewDidLoad];
    //[self.tableView setSeparatorStyle: UITableViewCellSelectionStyleGray];
     //[self.tableView setBackgroundColor:[UIColor blackColor]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    spineArrayIndex=nil, [spineArrayIndex release];
    tempArray=nil, [tempArray release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// MV modifications started

-(BOOL)shouldAutoRotate
{
    return YES;
    
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

// MV modifications completed


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [tempArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }


    //cell.textLabel.numberOfLines = 2;
    cell.textLabel.lineBreakMode = UILineBreakModeMiddleTruncation;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;

    cell.textLabel.text = [[tempArray objectAtIndex:[indexPath row]] title];
    cell.textLabel.textColor = [UIColor blackColor];

    //[cell.backgroundView setBackgroundColor:[UIColor brownColor]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPat
{
    return 50.0;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * keyVal = [spineArrayIndex valueForKey:[NSString stringWithFormat:@"%d", [indexPath row]]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [epubViewController loadSpine:[keyVal intValue] atPageIndex:0 highlightSearchResult:nil];
    [self dismissModalViewControllerAnimated:YES];
}

@end
