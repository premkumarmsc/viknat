//
//  ChapterList.m
//  EpubDownload
//
//  Created by Macminiserver on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BookmarkList.h"

@implementation BookmarkList

@synthesize epubViewController, content_id;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.contentSizeForViewInPopover = CGSizeMake(220,264.0);
        // Custom initialization
    }
    return self;
}

- (void) loadBookmark:(NSString *) contentid
{
    content_id = contentid;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DBClass * dbObject = [[DBClass alloc] init];

    NSArray *fields     = [[NSArray alloc] initWithObjects:@"bid",@"chap_no", @"chap_page", @"bookmark_date", @"font_size", nil];
    NSString *condition = [NSString stringWithFormat:@"content_id = %@", content_id];
    NSMutableArray *mutableArr = [dbObject selectMagazineField:fields tblname: @"" when:condition order:@""  recordsCount:100 startWith:0];
    
    if([mutableArr count] > 0) 
    {
        
        NSDictionary *bookfeed = (NSDictionary *)mutableArr;
        bid = (NSMutableArray *)[bookfeed valueForKey:@"bid"];
        chap_no = (NSMutableArray *)[bookfeed valueForKey:@"chap_no"];
        chap_page = (NSMutableArray *)[bookfeed valueForKey:@"chap_page"];
        desc = (NSMutableArray *)[bookfeed valueForKey:@"bookmark_date"];
        font_size = (NSMutableArray *)[bookfeed valueForKey:@"font_size"];
        
        [bid retain];
        [chap_no retain];
        [chap_page retain];
        [font_size retain];
        [desc retain];
        
    }

    [dbObject release], dbObject=nil;
    [mutableArr release], mutableArr=nil;

    titleView = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.frame];
    self.navigationItem.titleView  = titleView;
    
    UIButton * closebutton = [UIButton buttonWithType:UIButtonTypeCustom];
    closebutton.frame = CGRectMake(10, 0, 50, 40);
    [closebutton addTarget:self action:@selector(closebutton:) forControlEvents:UIControlEventTouchUpInside];
    [closebutton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [titleView addSubview:closebutton];
    
    UILabel *title = [[UILabel alloc] init];
    title.frame = CGRectMake(titleView.frame.size.width/2 -20, 0, 50, 40);
    [title setText:@"Bookmarks"];
    title.font = [UIFont boldSystemFontOfSize: 15.0];
    title.textColor = [UIColor blackColor];
    title.backgroundColor = [UIColor clearColor];
    title.adjustsFontSizeToFitWidth = YES;
    title.minimumFontSize = 10.0;
    title.textAlignment = UITextAlignmentLeft;
    title.backgroundColor = [UIColor clearColor];
    [titleView addSubview:title];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (IBAction) closebutton:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [bid release], bid=nil;
    [titleView release], titleView=nil;
    [chap_no release], chap_no=nil;
    [chap_page release], chap_page=nil;
    [font_size release], font_size=nil;
    [desc release], desc=nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [bid count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }

    cell.textLabel.numberOfLines = 2;
    cell.textLabel.lineBreakMode = UILineBreakModeMiddleTruncation;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    if (indexPath.row >0)
    {
        int chapno = [[chap_no objectAtIndex:indexPath.row] intValue];
       // int chappage = [[chap_page objectAtIndex:indexPath.row] intValue];
       
        NSString *title = [[epubViewController.loadedEpub.spineArray objectAtIndex:chapno] title];

        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", [bid objectAtIndex:indexPath.row] , [chap_page objectAtIndex:indexPath.row], 
                           [chap_no objectAtIndex:indexPath.row], [desc objectAtIndex:indexPath.row], title ];
    }
    else
    {
        cell.textLabel.text = @"Bookmarks";
    }
    
    [cell.backgroundView setBackgroundColor:[UIColor brownColor]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPat
{
    return 50.0;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row>0) 
    {
        
        int chapno = [[chap_no objectAtIndex:indexPath.row] intValue];
        int chappage = [[chap_page objectAtIndex:indexPath.row] intValue];
        int fontsize = [[font_size objectAtIndex:indexPath.row] intValue];

        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        //[epubViewController loadSpine:chapno atPageIndex:chappage highlightSearchResult:nil];
        [epubViewController loadSpine:chapno atPageIndex:chappage fontsize:fontsize];


        [self dismissModalViewControllerAnimated:YES];
    }
    else
    {
         [self dismissModalViewControllerAnimated:YES];
    }
}

@end
