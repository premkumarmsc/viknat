//
//  SocialNetworkWebViewViewController.h
//  Vikatan
//
//  Created by mobileveda on 03/02/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "Information_iPad.h"
#import "Login.h"
#import "LoginView.h"
#import <QuartzCore/QuartzCore.h>
#import "URLParser.h"


@interface SocialNetworkWebViewViewController : UIViewController <UIWebViewDelegate> {
    
    
    UIWebView * webView;
    UIActivityIndicatorView *spinner;
    UIView *modalView;
    UIButton * closeBtn;
    NSString * requestPath;
    UIView * load_back;
    UIPopoverController *popoverController;
}

@property (nonatomic, retain) UIWebView * webView;
@property(nonatomic, retain)UIPopoverController *popoverController;

@property (nonatomic, retain) NSString * requestPath, *email_id, * socialFlag, * l_status;

- (NSString *) checkUrl:(NSString *) requestUrl;
-(void) closeSocialViewController;
-(void) startLoading;
-(void) stopLoading;

@end

