//
//  Login.h
//  LoginAPI
//
//  Created by Saravanan Nagarajan on 28/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h> 

#import "JSON.h"
#import "ASIFormDataRequest.h"
#import "Constant.h"
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

#define PSIsIpad() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
@protocol LoginDelegate <NSObject>

-(void) authenticationCompleted:(BOOL) success;

@end

//Define the protocol for the delegate
@protocol LogDelegate
- (void)callBackArchive;
@end

@interface Login : NSObject <UITextFieldDelegate> {

    id <LoginDelegate> delegate;
    NSString *username;
    NSString *password;
    NSString *sessionID;
    NSString *emailID;
    NSString *userID;
    NSString *UID;
 
    
    UILabel *messageLabel;
    UILabel *usernameLabel;
    UILabel *passwordLabel;
    UITextField *userTxt;
    UITextField *passTxt,*userTxtEmail;
    
    UIActivityIndicatorView *activityIndicator;
    UIView *parentView;
    UIView *loginScreenView;
    UIView *loginView;
    
    BOOL downLaoding;
    NSString *logErroMessage;
    BOOL fromInfo;
    
    id <LogDelegate> logdelegate;
}
@property (nonatomic, assign) id  <LogDelegate> logdelegate;  
@property(nonatomic, retain) id <LoginDelegate> delegate;

@property(nonatomic, retain) NSString *username;
@property(nonatomic, retain) NSString *password;
@property(nonatomic, retain) NSString *sessionID;
@property(nonatomic, retain) NSString *emailID;
@property(nonatomic, retain) NSString *userID;
@property(nonatomic, retain) NSString *UID;
@property(nonatomic, retain) NSString *socialFlag;
@property(nonatomic) BOOL fromInfo;

@property(nonatomic) BOOL downLaoding;

//@property (nonatomic, retain) IBOutlet UITextField *usernameField;

-(BOOL) isUserDetailsExists;

-(void) showLoginViewAndAuthenticateUser:(UIView*) currentView;

-(BOOL) isPaidUser;

-(IBAction) loginButtonClicked:(id) sender;

-(IBAction)textFieldReturn:(id)sender;

-(BOOL) startServerAuthentication:(NSString *)reqUsername:(NSString *)reqPassword:(NSString *)reqSocialFlag:(NSString *)loginType;

-(NSString *) getUserDetailsFromServer:(NSString *)loginType;

-(BOOL) parseResponseJSON:(NSString *)responseString;

-(void) storeUserDetailsInPList;

-(BOOL) logout;

- (BOOL) connectedToNetwork;

-(BOOL) checkInternet;

- (void) magStorereferesh;

-(NSString*) getDeviceDetails:(NSString*) url ;

-(NSMutableDictionary *) getDeviceDetails;

@end
