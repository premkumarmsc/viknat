//
//  FeedBackForm.h
//  Vikatan
//
//  Created by mobileveda on 09/02/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <QuartzCore/QuartzCore.h>
#import "Constant.h"
#import "ASIFormDataRequest.h"
#import "Login.h"

@interface FeedBackForm : UIViewController<UITextFieldDelegate, UITextViewDelegate> {

    UIView *formView;
    UITextField * userName, * eMail;//,* feedBack;
    UIPopoverController *popoverController;
    UITextView * feedBack;
    UIButton *submitBtn;

}

@property (nonatomic, retain) UITextField * userName, * eMail; //,* feedBack

@property (nonatomic, retain) UITextView * feedBack;
@property(nonatomic, retain)UIPopoverController *popoverController;
@property (nonatomic, retain) UIButton * submitBtn;

@property (nonatomic) BOOL isPresentedFromPopOver;

-(void) setFeedBackForm;
-(BOOL) connectedToNetwork;
-(BOOL) checkInternet;
-(void) _showAlert:(NSString*)title;

@end
