//
//  CommentView_iPad.h
//  Vikatan
//
//  Created by MobileVeda on 02/06/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSON.h"
#import "Constant.h"
#import "Utilities.h"
#import "Login.h"
@interface CommentView_iPad : UITableViewController {
    
    NSString *articleID, *cmntCount, *magID;
    
    NSMutableData *responseData;
    
    NSMutableArray *listOfMessages;
    NSMutableArray *dateOfMessages;
    NSMutableArray *postedBy;
    
    UIView *postView;
    UIView *postMsgView;
    
    UIActivityIndicatorView *spinner; 
    
    int offset, xWidth;
    int buttonTag;
    
    BOOL resStatus;
    BOOL magCmnt;
    UIScrollView *pageIndexView;
    
    int xCoord;
    UILabel *pakanGal;
    
    //Login
    Login *logMod;
    
}

@property (nonatomic, retain) NSString *articleID,*cmntCount,*magID;
@property (nonatomic,retain) UIView *postView,  *postMsgView;
@property (nonatomic,retain)  UIActivityIndicatorView *spinner;
@property (nonatomic,retain)  UIScrollView *pageIndexView;
@property (nonatomic) BOOL resStatus;
@property (nonatomic) BOOL magCmnt;
- (void) LoadingIcon;
-(void) CommentsGetRequest;
- (void) BtnUnhighlight;
- (UIScrollView*)buildPagination;
- (void) _showAlert:(NSString*)title;
@end

