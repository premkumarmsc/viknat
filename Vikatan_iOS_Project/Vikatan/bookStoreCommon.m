//
//  bookCategory.m
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 11/06/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import "bookStoreCommon.h"

@implementation bookStoreCommon

@synthesize cachePath;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(NSString *) returnButtonColor:(int) id 
{
    NSString *btncolor;
    
    switch (id) {
        case 0:
        {
            btncolor = STORE_BTN_BUYNOW;
            break;
        }
        case 1:
        {
            btncolor = STORE_BTN_ADDTOLIB;
            break;
        }
        case 2:
        {
            btncolor = STORE_BTN_READNOW;
            break;
        }
        default:
            break;
    }
    return btncolor;
}

-(NSString *) returnButtonTitle:(int) id {
    
    NSString *btntitle;
    
    switch (id) {
        case 0:
        {
            btntitle = @"Buy Now";
            break;
        }
        case 1:
        {
            btntitle = @"Add to library";
            break;
        }
        case 2:
        {
            btntitle = @"Read Now";
            break;
        }
        default:
            break;
    }
    return btntitle;
}

-(UIColor *) returnButtonTitleColor:(int) id {
    
    UIColor *btncolor;
    
    switch (id) {
        case 0:
        {
            btncolor = [UIColor whiteColor];
            break;
        }
        case 1:
        {
            btncolor = [UIColor whiteColor];
            break;
        }
        case 2:
        {
            btncolor = [UIColor blackColor];
            break;
        }
        default:
            break;
    }
    return btncolor;
}


-(int) returnbtnStatus: (NSString *) downloadable contentid:(NSString *) contentid {
    
    int i=0;
    
    if([downloadable isEqualToString:@"Y"]) {
        
        i = [self checkDownloadDB:contentid];
    } 
    
    return i;
}

-(int) checkDownloadDB: (NSString *) contentid {
    
    NSLog(@"entering checkDownloadDB in ");
    
    int ret_val=1;
    
    DBClass *dbObject = [[DBClass alloc] init];
    
    NSArray *fields     = [[NSArray alloc] initWithObjects:@"book_id", nil];
    NSString * condition = [NSString stringWithFormat:@"book_id=%@",contentid];
    
    NSMutableArray * check_bookdb = [dbObject selectMagazineField:fields tblname:@"lib_book"  when:condition order:@""  recordsCount:100 startWith:0];
    
    
   // NSLog(@"check_bookdb count is %d ", [check_bookdb count]);
    
    
    if([check_bookdb count] > 0){
        ret_val = [self checkfilepath:contentid];
    }
    [dbObject release],dbObject = nil;
    fields=nil, condition=nil, check_bookdb=nil;
    return ret_val;
    
}

-(int) checkfilepath: (NSString *) contentid {
    
    cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    fileManager = [NSFileManager defaultManager];
    NSString * checkfilepath = [cachePath stringByAppendingPathComponent:contentid];
    
    if ([fileManager fileExistsAtPath:checkfilepath]) {
        NSLog(@"checkfilepath");
        NSString * fullwrapper_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"wrapper.jpg"]];
        
        if ([fileManager fileExistsAtPath:fullwrapper_path]) {
            NSLog(@"fullwrapper_path");

            NSString * epubfile_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentid, @".epub"]];
            
            NSString * pdffile_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentid, @".pdf"]];

            if ([fileManager fileExistsAtPath:epubfile_path]) {
               // NSLog(@"EPUB file exists in cache directory");
                
                pdffile_path=nil, epubfile_path=nil;
                
                return  2;
            } else if ([fileManager fileExistsAtPath:pdffile_path]) {
               // NSLog(@"PDF file exists in cache directory");
                
                pdffile_path=nil, epubfile_path=nil;
                return  2;
            }
        }
    }
    
    return  1;
}

@end
