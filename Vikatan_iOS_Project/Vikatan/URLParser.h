//
//  URLParser.h
//  Vikatan
//
//  Created by mobileveda on 07/02/13.
//  Copyright (c) 2013 MobileVeda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLParser : NSObject {
    NSArray *variables;
}

@property (nonatomic, retain) NSArray *variables;

- (id)initWithURLString:(NSString *)url;
- (NSString *)valueForVariable:(NSString *)varName;

@end

