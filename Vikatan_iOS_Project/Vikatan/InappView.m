//
//  CustomButton.m
//  Vikatan
//
//  Created by MobileVeda on 12/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "InappView.h"


@implementation InappView
@synthesize UrlString, CateGoryID, ArticleID, 
            CateGoryTitle, commentsCount, 
            articleCount, CateGoryIDStr, 
            artWebURL, articleTitle,
            productIdentifier,
            subsname, 
            subprice,
            ios_uid,
            subid,singleIssue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
