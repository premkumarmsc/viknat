//
//	ReaderMainPagebar.m
//	Reader v2.0.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011 Julius Oklamcak. All rights reserved.
//
//	This work is being made available under a Creative Commons Attribution license:
//		«http://creativecommons.org/licenses/by/3.0/»
//	You are free to use this work and any derivatives of this work in personal and/or
//	commercial products and projects as long as the above copyright is maintained and
//	the original author is attributed.
//

#import "ReaderMainPagebar.h"
#import "ReaderDocument.h"

#import <QuartzCore/QuartzCore.h>

@implementation ReaderMainPagebar

#pragma mark Constants

#define PAGE_NUMBER_WIDTH 96.0f
#define PAGE_NUMBER_HEIGHT 30.0f

#define SLIDER_WIDTH_INSET 16.0f

#pragma mark Properties

@synthesize delegate;

#pragma mark ReaderMainPagebar instance methods

- (id)initWithFrame:(CGRect)frame
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if ((self = [super initWithFrame:frame]))
	{
		self.autoresizesSubviews = YES;
		self.userInteractionEnabled = YES;
		self.contentMode = UIViewContentModeRedraw;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
		self.backgroundColor = [UIColor blackColor];//[UIColor colorWithWhite:0.0f alpha:0.64f];

        
        imageWidth = 110;
        
		CGFloat numberY = (0.0f - (PAGE_NUMBER_HEIGHT * 2.0f));
		CGFloat numberX = ((self.bounds.size.width - PAGE_NUMBER_WIDTH) / 2.0f);
		CGRect numberRect = CGRectMake(numberX, numberY, PAGE_NUMBER_WIDTH, PAGE_NUMBER_HEIGHT);

		pageNumberView = [[UIView alloc] initWithFrame:numberRect];

		pageNumberView.autoresizesSubviews = NO;
		pageNumberView.userInteractionEnabled = NO;
		pageNumberView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
		pageNumberView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];

		pageNumberView.layer.cornerRadius = 4.0f;
		pageNumberView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
		pageNumberView.layer.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.6f].CGColor;
		pageNumberView.layer.shadowPath = [UIBezierPath bezierPathWithRect:pageNumberView.bounds].CGPath;
		pageNumberView.layer.shadowRadius = 2.0f; pageNumberView.layer.shadowOpacity = 1.0f;

		CGRect labelRect = CGRectInset(pageNumberView.bounds, 4.0f, 2.0f); // Inset the text a bit

		pageNumberLabel = [[UILabel alloc] initWithFrame:labelRect];

		pageNumberLabel.autoresizesSubviews = NO;
		pageNumberLabel.autoresizingMask = UIViewAutoresizingNone;
		pageNumberLabel.textAlignment = UITextAlignmentCenter;
		pageNumberLabel.backgroundColor = [UIColor clearColor];
		pageNumberLabel.textColor = [UIColor whiteColor];
		pageNumberLabel.font = [UIFont systemFontOfSize:16.0f];
		pageNumberLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
		pageNumberLabel.shadowColor = [UIColor blackColor];
		pageNumberLabel.adjustsFontSizeToFitWidth = YES;
		pageNumberLabel.minimumFontSize = 12.0f;

		[pageNumberView addSubview:pageNumberLabel];

		[self addSubview:pageNumberView];

		CGRect sliderFrame = CGRectInset(self.bounds, SLIDER_WIDTH_INSET, 0.0f); // Inset the slider

        //Updated recently
		//thePageSlider = [[UISlider alloc] initWithFrame:sliderFrame];
        thePageSlider = [[CustomSlider alloc] initWithFrame:CGRectMake(25,numberY+PAGE_NUMBER_HEIGHT+208,718, 20)];

		thePageSlider.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		thePageSlider.minimumValue = 1.0f; thePageSlider.maximumValue = 1.0f; thePageSlider.value = 1.0f;

		[thePageSlider addTarget:self action:@selector(pageSliderTouchDown:) forControlEvents:UIControlEventTouchDown];
		[thePageSlider addTarget:self action:@selector(pageSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
		[thePageSlider addTarget:self action:@selector(pageSliderTouchUp:) forControlEvents:UIControlEventTouchUpOutside];
		[thePageSlider addTarget:self action:@selector(pageSliderTouchUp:) forControlEvents:UIControlEventTouchUpInside];
        //[thePageSlider setMinimumTrackImage:[[UIImage imageNamed:@"slidergreen.png"] stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0] forState:UIControlStateNormal];
        
      
        //CGAffineTransform transform = CGAffineTransformMakeScale(0.7f, 0.7f);
        //thePageSlider.transform = transform;
		[self addSubview:thePageSlider];
        
        
        //Add ThumbView
        thumbView = [[UIScrollView alloc]initWithFrame:CGRectMake(25,0,718, 165)];// <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
        [thumbView setBackgroundColor:[UIColor blackColor]];
        //[self bindImage];
       // [NSThread detachNewThreadSelector:@selector(bindImage) toTarget:self withObject:nil];
        thumbView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
       // thumbView.delegate = self;
        //thumbView.scrollEnabled = FALSE;
        [self addSubview:thumbView];
        
        
        //Highlight View
        highlightView = [[UIView alloc]initWithFrame:CGRectMake(0,5,0,0)]; 
        [thumbView addSubview:highlightView];
	}

	return self;
}

- (void)dealloc
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[thePageSlider release], thePageSlider = nil;
    

	[pageNumberLabel release], pageNumberLabel = nil;

	[pageNumberView release], pageNumberView = nil;

	[document release], document = nil;

	[super dealloc];
}

- (void)setReaderDocument:(ReaderDocument *)object
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[document release], document = nil; // Release first

	document = [object retain]; // Retain the document object

	thePageSlider.maximumValue = [document.pageCount integerValue];

	[self updatePageNumberDisplay]; // Update page display
    
    [NSThread detachNewThreadSelector:@selector(bindImage) toTarget:self withObject:nil];
    ///Update thumbview
   // [thumbView setContentOffset:CGPointMake([document.pageCount integerValue] * 100, 0) animated:TRUE];
   
}

- (void)updatePageNumberText:(NSInteger)pageNumber
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

    pageNumberView.alpha = 1.0;
	NSInteger pageCount = [document.pageCount integerValue];

	NSString *format = NSLocalizedString(@"%d of %d", @"format");

	NSString *numbers = [NSString stringWithFormat:format, pageNumber, pageCount];

	pageNumberLabel.text = numbers; // Update page number label text
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:2.5];
    
    pageNumberView.alpha = 0.0;
   
    [UIView commitAnimations];
    
}

- (void)updatePageNumberDisplay
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	[self updatePageNumberText:[document.pageNumber integerValue]]; // Update text

	thePageSlider.value = [document.pageNumber integerValue]; // Update slider
    
     
    
}

- (void)hidePagebar
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if (self.hidden == NO)
	{
		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.alpha = 0.0f;
			}
			completion:^(BOOL finished)
			{
				self.hidden = YES;
			}
         
         //Thumb View Show/Hide
            
         
		];
        //thumbView.frame = CGRectMake(0,0,0, 0);
        NSLog(@"Hide");
	}
}

- (void)showPagebar
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	if (self.hidden == YES)
	{
		[UIView animateWithDuration:0.25 delay:0.0
			options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
			animations:^(void)
			{
				self.hidden = NO;
				self.alpha = 1.0f;
			}
			completion:NULL
		];
	}
    //thumbView.frame = CGRectMake(25,0,718, 180);
   // thumbView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

#pragma mark UISlider action methods

- (void)pageSliderTouchDown:(UISlider *)slider
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	NSInteger pageNumber = slider.value; // Get page slider value

	if (pageNumber != [document.pageNumber integerValue]) // Only if different
	{
		[self updatePageNumberText:pageNumber]; // Update page number text
	}

	lastPageTrack = pageNumber; // Start tracking
}

- (void)pageSliderValueChanged:(UISlider *)slider
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	NSInteger pageNumber = slider.value; // Get page slider value

	if (pageNumber != lastPageTrack) // Only if the page has changed
	{
		[self updatePageNumberText:pageNumber]; // Update page number text

		lastPageTrack = pageNumber; // Update tracking
	}
}

- (void)pageSliderTouchUp:(UISlider *)slider
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif

	NSInteger pageNumber = slider.value; // Get page slider value

	if (pageNumber != lastPageTrack) // Only if the page has changed
	{
		[self updatePageNumberText:pageNumber]; // Update page number text

		thePageSlider.value = pageNumber; // Set slider to integer value
	}
    
    if(pageNumber % 2 != 0)
        pageNumber = pageNumber - 1;

	[delegate pagebar:self gotoPage:pageNumber]; // Goto the page

	lastPageTrack = 0; // Reset tracking
    
    [self updateHighLightView:pageNumber + 1];
 
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView 
{
    NSLog(@"Scrolling the Content's");
    //[self updateHighLightView:scrollView.contentOffset.x/110];
    thePageSlider.value = (int)roundf(scrollView.contentOffset.x / 121) + 2 ;//[[scrollView.contentOffset.x ]/110 integerValue];
    
    int pageNum = thePageSlider.value;
    
    if(pageNum % 2 != 0)
        pageNum = pageNum - 1;
    [delegate pagebar:self gotoPage:pageNum];
    [self updateHighLightView:thePageSlider.value ];
    
}

//ADD IMAGES TO THUMBVIEW ASYNCHRONOUSLY
-(void)bindImage {
    
    NSLog(@"Load imaging 1");
    @synchronized(thumbView){
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
     NSLog(@"Load imaging 2");
    
    [self LoadingIcon];
    
    NSString* tmp;// = (NSString*) [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"avalpreview.pdf"];
    
    NSString *jpg= @".jpg";
    
    int imgWidth = 5;
    
    for( int i = 1 ;i<=[document.pageCount integerValue] ;i++) {
    
       // imgWidth = i - 1;
        NSString *fileName = [@"tumb-" stringByAppendingString:[NSString stringWithFormat:@"%d", i-1]];
        
        fileName = [fileName stringByAppendingString:jpg];
        
        tmp = (NSString*) [[document.folderPath stringByAppendingPathComponent:@"images"] stringByAppendingPathComponent:fileName];
        
               
        UIImage *imageThumbnail = [UIImage imageWithContentsOfFile:tmp];
        
        UIButton *_imageViewPreview = [UIButton buttonWithType:UIButtonTypeCustom];_imageViewPreview.backgroundColor = [UIColor redColor];
        
       // [_imageViewPreview setImage:imageThumbnail forState:UIControlStateNormal];
        
        [_imageViewPreview setBackgroundImage:imageThumbnail forState:UIControlStateNormal];
        
        _imageViewPreview.frame =  CGRectMake(imgWidth  
                                              , 10, imageThumbnail.size.width, imageThumbnail.size.height ); 
        _imageViewPreview.imageView.contentMode = UIViewContentModeScaleToFill;
        
        if( i % 2 == 0) {
            
            imgWidth = imgWidth + imageThumbnail.size.width;
            
        }
        else {
            
             imgWidth = imageThumbnail.size.width + imgWidth + 30;
            
        }
        _imageViewPreview.tag = i ;
        
        [_imageViewPreview setBackgroundColor:[UIColor clearColor]];
        
        [_imageViewPreview addTarget:self action:@selector(thumbview_clicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [thumbView addSubview:_imageViewPreview];
       
        NSLog(@"imaging");
       
    }
    int t = 30* ([document.pageCount integerValue]/2) + 5;
    
    imageWidth =  (imgWidth - t)/[document.pageCount integerValue];
        
    thumbView.contentSize = CGSizeMake(121*[document.pageCount integerValue],165);//, <#CGFloat height#>)
    thumbView.delegate = self;
    [thumbView setNeedsDisplay];
   // thumbView.scrollEnabled = TRUE;
    [self StopLoading];
    
    [pool release];
    }
}


- (void) loadThumbnails:(UIButton*)_imageViewPreview {
    
    [thumbView addSubview:_imageViewPreview];
     [_imageViewPreview release];
}
- (void) LoadingIcon {
    
    //Initialize activity window for loading process
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//Set position
	[spinner setCenter:CGPointMake(768/2.0, 90)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	//Add the loading spinner to view
	[thumbView addSubview:spinner];
	//Start animating
	[spinner startAnimating];
    
}
- (void) StopLoading {
    
    [spinner stopAnimating];
    
	[spinner removeFromSuperview];
    
}

- (void)thumbview_clicked:(id)sender {
    
    thumbStatus = TRUE;
    
    UIButton *btn = (UIButton*)sender;
    
    [delegate pagebar:self gotoPage:btn.tag];
    
    [self updateHighLightView:btn.tag];

}

-(void) updateHighLightView:(NSInteger)page 
{
    
    if(page % 2 != 0) {
        
        page = page - 1;
        
    }
    int cnt = 2;
    if(page == 0) {
        page = 1;
       
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    
    NSLog(@"Page Count sridhar %d",page);
    
    
    if (page == [document.pageCount intValue]) {
        
        highlightView.frame = CGRectMake(page/2 * 30 + (page- 1) * imageWidth,5,(imageWidth * cnt/2) + 10,160); 
        [[highlightView layer] setBorderColor:[/*UIColorFromRGB(0xADFF2F)*/UIColorFromRGB(0x3399FF) CGColor]];
        [[highlightView layer] setBorderWidth:3];
        [highlightView setBackgroundColor:[UIColor clearColor]];  
        
    } else {
    
    switch (page) {
        case 1: // A-
            highlightView.frame = CGRectMake(page/2 * 30 + (page- 1) * imageWidth,5,(imageWidth * cnt/2) + 5,160); 
            [[highlightView layer] setBorderColor:[/*UIColorFromRGB(0xADFF2F)*/UIColorFromRGB(0x3399FF) CGColor]];
            [[highlightView layer] setBorderWidth:2];
            [highlightView setBackgroundColor:[UIColor clearColor]];  
            break;
        default: // A+
            highlightView.frame = CGRectMake(page/2 * 30 + (page- 1) * imageWidth,5,imageWidth * cnt + 10,160); 
            [[highlightView layer] setBorderColor:[ /*UIColorFromRGB(0xADFF2F)*/ UIColorFromRGB(0x3399FF) CGColor]];
            [[highlightView layer] setBorderWidth:2];
            [highlightView setBackgroundColor:[UIColor clearColor]]; 
            break;
    }
    }

    if(!thumbStatus)
        [thumbView setContentOffset:CGPointMake(page/2 * 30 + (page- 1) * imageWidth - 10, 0) animated:TRUE];
    
    [UIView commitAnimations];
    
    thumbStatus = FALSE;
}
@end
