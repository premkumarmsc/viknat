//
//  Information_iPad.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 20/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Login.h"
#import "MagazineLib.h"
#import "FeedBackForm.h"
#import "SHKMail.h"
#import "SHKTwitter.h"
#import "SHKFacebook.h"
#import "SHKSafari.h"

@interface Information_iPad : UITableViewController <LoginDelegate,UIAlertViewDelegate, UIActionSheetDelegate> {
    
    NSMutableArray *listOfItems;
    UIView *loginView;
    UITextField *userTxt;
    UITextField *passTxt;
    UIPopoverController *popoverController;
    BOOL restore, likeApp;
}
@property(nonatomic, retain)UIPopoverController *popoverController;
-(void) showLoginViewAndAuthenticateUser;
- (void) deletAllIssues;
- (void) closePops;

@end
