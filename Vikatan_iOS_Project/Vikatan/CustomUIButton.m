//
//  CustomUIButton.m
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 11/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import "CustomUIButton.h"

@implementation CustomUIButton

@synthesize btntitle, contentId, downloadurl, contentDate, authorName, prod_identifier,dnwlOperation;
@synthesize categoryid, contentDesc, contentName, categoryname, contentFormat,readStatus, price, downloadPath;

- (id) initWithFrame: (CGRect) frame {
    
    self = [super initWithFrame: frame ];
    if ( self == nil )
        return ( nil );

    _btntitle = [[UILabel alloc] initWithFrame: CGRectMake(0,5,100,20)];
    _btntitle.font = [UIFont boldSystemFontOfSize: 12.0];
    _btntitle.textColor = [UIColor whiteColor];
    _btntitle.adjustsFontSizeToFitWidth = YES;
    _btntitle.minimumFontSize = 10.0;
    _btntitle.textAlignment = UITextAlignmentCenter;
    _btntitle.backgroundColor = [UIColor clearColor];
    
    [self addSubview: _btntitle];
    return ( self );
}

- (NSString *) title
{
    return (_btntitle.text);
}

- (void) setTitle: (NSString *) title
{
    _btntitle.text = title;
}

- (NSString *) contentId
{
    return (contentId);
}

- (void) setContentId: (NSString *) contentIdval
{
    contentId = contentIdval;
}

- (NSString *) contentName
{
    return (contentName);
}

- (void) setContentName: (NSString *) contentNameval
{
    contentName = contentNameval;
}

- (NSString *) categoryid
{
    return (categoryid);
}

- (void) setcategoryid: (NSString *) categoryidval
{
    categoryid = categoryidval;
}

- (NSString *) categoryname
{
    return (categoryname);
}

- (void) setcategoryname: (NSString *) categorynameval
{
    categoryname = categorynameval;
}

- (NSString *) authorName
{
    return (authorName);
}

- (void) setauthorName: (NSString *) authorNameval
{
    authorName = authorNameval;
}


- (NSString *) contentFormat
{
    return (contentFormat);
}

- (void) setcontentFormat: (NSString *) contentFormatval
{
    contentFormat = contentFormatval;
}

- (NSString *) prod_identifier
{
    return (prod_identifier);
}

- (void) setprod_identifier: (NSString *) prod_identifierval
{
    prod_identifier = prod_identifierval;
}

- (NSString *) contentDate
{
    return (contentDate);
}

- (void) setcontentDate: (NSString *) contentDateval
{
    contentDate = contentDateval;
}

- (NSString *) downloadurl
{
    return (downloadurl);
}

- (void) setDownloadurl: (NSString *) downloadurlval
{
    downloadurl = downloadurlval;
}

- (NSString *) price
{
    return ( price );
}

- (void) setPrice: (NSString *) priceval
{
    price = priceval;
    [self setNeedsLayout];
}
- (UIColor *) color

{
    
    return (_btntitle.textColor);
    
}



- (void) setColor: (UIColor *) color

{
    
    _btntitle.textColor = color;
    
}
@end