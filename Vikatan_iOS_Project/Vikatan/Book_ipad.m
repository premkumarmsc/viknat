//
//  Book_ipad.m
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 09/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import "Book_ipad.h"
#import "BookCell.h"
#import "CustomUIButton.h"
#import "EpubViewControlleriPAD.h"

@implementation Book_ipad

@synthesize gridView=_gridView;
@synthesize responseData, loadCatBooks;
@synthesize imageManager, categories;
@synthesize cachePath,  catdetails,
refreshBtn,categName, getUrlAndParams;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void) viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:@"FALSE" forKey: CHECK_DOWNLOAD_PROGRESS];
    //[self getBooksList];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    self.imageManager = [[HJObjManager alloc] init];
    NSString *cacheDirectory = [NSHomeDirectory() stringByAppendingPathComponent:IMAGES_CACHE_PATH];
    HJMOFileCache *fileCache = [[[HJMOFileCache alloc] initWithRootPath:
                                 cacheDirectory] autorelease];
    self.imageManager.fileCache = fileCache;
    
    
    titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    
    titleView.autoresizingMask =UIViewAutoresizingNone;// UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    if(IsIpad()){
    int toolBarHght = self.navigationController.navigationBar.frame.size.height;
    UIToolbar *compoundView = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 60.f , toolBarHght)] autorelease];
    compoundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    compoundView.barStyle = UIBarStyleBlackOpaque;
    compoundView.tintColor = [UIColor blackColor];
    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil] autorelease];
    space.width = 3.f;
    NSMutableArray *compountItems = [NSMutableArray array];
    
    refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
    refreshImageView.frame =  CGRectMake(30, 5, 32, 32);
    [refreshImageView addTarget:self action:@selector(refreshLib:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *refButton = [[UIBarButtonItem alloc] initWithCustomView:refreshImageView];
    [compountItems addObjectsFromArray:[NSArray arrayWithObjects:refButton, space, nil]];
    compoundView.items = compountItems;
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:compoundView] autorelease];
    
    } else {
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(215, 0, 100, 50.0)];
        [self.navigationController.navigationBar addSubview:rightView];
        refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
        refreshImageView.frame =  CGRectMake(70, 5, 32, 32);
        [refreshImageView addTarget:self action:@selector(refreshLib:) forControlEvents:UIControlEventTouchUpInside];
        [rightView addSubview:refreshImageView];
        
    }
    UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
    logo = [[UIImageView alloc] initWithFrame:
            CGRectMake(0, 0, header.size.width, header.size.height)];
    logo.image = header;
    title = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width+2, 5, 100, 20)];
    [title setText:@"புத்தகங்கள்"];
    title.font = [UIFont boldSystemFontOfSize: 15.0];
    title.textColor = [UIColor whiteColor];
    title.backgroundColor = [UIColor clearColor];
    title.adjustsFontSizeToFitWidth = YES;
    title.minimumFontSize = 10.0;
    title.textAlignment = UITextAlignmentLeft;
    title.backgroundColor = [UIColor clearColor];
    
     [titleView addSubview:title];
    [titleView addSubview:logo];
    [titleView addSubview:refreshBtn];
     self.navigationItem.titleView  = titleView;
    [[NSNotificationCenter defaultCenter] addObserver: self selector: 
                            @selector(getBooksList) name:@"bookreferesh" object: nil];
    book_cat = [[bookStoreCommon alloc] init];

    [self initializeGridView];
    [self getBooksList];
}

- (void)initializeGridView {
    if (!self.gridView) {
        // init thumbs view
        self.gridView = [[[AQGridView alloc] initWithFrame:CGRectZero] autorelease];
        self.gridView.backgroundColor = [UIColor clearColor];
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;  
        self.gridView.scrollEnabled = TRUE;

        self.gridView.layoutDirection = AQGridViewLayoutDirectionVertical;
        
        // if we have a translucent bar, make some room
        if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent) {
            self.gridView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
        }else {
            self.gridView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        self.gridView.hidden = NO;
        self.gridView.scrollsToTop = YES;
        [self.view addSubview:self.gridView];
        CGRect frame = CGRectMake(0, 60, self.view.bounds.size.width, self.view.bounds.size.height - 60);   
        self.gridView.frame = frame;
        // add a footer view for tabbar
        if (!IsIpad()) {
            self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 50)] autorelease];
        }
    }
    
}
- (void)viewDidAppear:(BOOL)animated
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
       
    } else {
       
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {

       
    } else {

    }

    // Return YES for supported orientations
	return YES;
}

-(void) invokeGridDelegate {
    [self.gridView reloadData];
}

// Override to allow orientations other than the default portrait orientation.


- (void) viewDidUnload
{
	// Release any retained subviews of the main view.
    self.gridView = nil;
    [title release], title=nil;
    [logo release], logo=nil;
    [titleView release], titleView=nil;
    [refreshBtn release], refreshBtn=nil;
    [scroll release], scroll = nil;
    [refreshImageView release], refreshImageView=nil;
    [getUrlAndParams release],getUrlAndParams=nil;
}



-(void) refreshLib:(id) sender
{
   // NSLog(@"refresh button was selected");
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if([[standardUserDefaults objectForKey:CHECK_DOWNLOAD_PROGRESS] isEqualToString:@"FALSE"]) {     
            [self getBooksList];
    } else {
        //[self _showAlert:@"Download is in progress, please wait."];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Inprogress"
                                                        message:@"Do you want to cancel Download?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
        [alert release];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex != 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelBookDownloadOnRefresh" object: nil];
    }
    
}

//Get Book details from JSON

- (void)getBooksList {

    responseData = [[NSMutableData data] retain];
    NSMutableURLRequest *request;
    
    [self startLoading];
	//Send http request
    
    Login *logMod = [[Login alloc]init];
    getUrlAndParams = [logMod getDeviceDetails];
   
    // NSLog(@"bookONLoadReq is %@%@&%@", BOOK_JSON_URL, [getUrlAndParams objectForKey:@"devDet"], [getUrlAndParams objectForKey:@"userDet"]);

    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BOOK_JSON_URL, [getUrlAndParams valueForKey:@"devDet"]]];
    
    NSLog(@"a URL:%@",aUrl);
    
    request = [NSMutableURLRequest requestWithURL:aUrl
                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                    timeoutInterval:60.0];

    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[getUrlAndParams objectForKey:@"userDet"] dataUsingEncoding:NSUTF8StringEncoding]];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];

    refreshImageView.enabled=FALSE;
	[logMod release],logMod = nil,aUrl=nil;
}

//HTTP Request Handler to get Book/Mag list from Store

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0]; 
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
    
    NSString* responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self readBookIndex:responseString];
    
    if(responseString!=NULL) 
    {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setValue:responseString forKey:STANDARD_USER_DEFAULT];
        [standardUserDefaults synchronize];
        [self readBookIndex:responseString];
    } 
    else 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Server Not reachable" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
   
    refreshImageView.enabled=TRUE;
    [self stopLoading];
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    refreshImageView.enabled=TRUE;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if([standardUserDefaults objectForKey:STANDARD_USER_DEFAULT] !=nil )
    {
        [self readBookIndex:[standardUserDefaults objectForKey:STANDARD_USER_DEFAULT]];
    }   

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please check your internet connection status" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];

    [alert show];
    [alert release];
    [self stopLoading];
}

//Read Book JSON Content

-(void) readBookIndex:(NSString*)data {
    
    NSString *myText = data;
    
    if (myText) {
        
        DBClass *DBObject;

        SBJSON *jsonParser = [[SBJSON new] autorelease];
        id response = [jsonParser objectWithString:myText];
        NSDictionary *bookfeed = (NSDictionary *)response;

        NSArray *category = (NSArray *)[bookfeed valueForKey:CATEGORY];

        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setValue:[bookfeed valueForKey:DOWNLOAD_VERSION_KEY] forKey:DOWNLOAD_MANAGER_KEY];

        if([standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY] == NULL )
        {
            [standardUserDefaults setValue:SEL_DOWNLOAD_MANAGER forKey:DOWNLOAD_MANAGER_KEY];
        }

       // NSLog(@"SEL_DOWNLOAD_MANAGER in book store json is %@", [standardUserDefaults objectForKey:DOWNLOAD_MANAGER_KEY]);
        
        if(category == NULL ) {
            NSLog(@"Category is Null");
        } 
        else {
            [self clearTables];

            DBClass *DBObject= [[DBClass alloc] init];
            NSDictionary *catinfo;
            NSString * catqueryValues;

            for (int ndx = 0; ndx < category.count ; ndx++) {
            
                catinfo = (NSDictionary *)[category objectAtIndex:ndx];
            
                if(catinfo == NULL ) {
                      NSLog(@"catinfo is Null");
                } else {

                    catqueryValues = [NSString stringWithFormat:@"'%@',%@,'%@'",[catinfo valueForKey:CATEGORY_NAME],[catinfo valueForKey:CATEGORY_ID],@"book"];
                   
                    [DBObject insertMagazineFields:@"cat_name, cat_id,cat_type" values:catqueryValues tableName:STORE_CATEGORY_TABLE];
                }
            }

            catqueryValues=nil, catinfo=nil;

            NSArray *books = (NSArray *)[bookfeed valueForKey:BOOKS];
            NSDictionary *bookinfo;
            NSString *bookqueryValues, *metadata;
       
            for (int ndx = 0; ndx < books.count ; ndx++) {
                bookinfo = (NSDictionary *)[books objectAtIndex:ndx];
            
                if(bookinfo == NULL ) {
                    NSLog(@"bookinfo is Null");
                
                } else {              
                                        
                    metadata  = [NSString stringWithFormat:@"%@" ,[bookinfo objectForKey:CONTENT_METADATA]];

                    NSString * drmEnabled               =    @"";
                    NSString * drmFont                  =    @"";
                    NSArray *drm = (NSArray *)[bookinfo valueForKey:CONTENT_DRM];
                    if(drm != NULL ) {
                        bookinfo                        =   (NSDictionary *)[drm objectAtIndex:0];
                        drmEnabled                      =   [bookinfo valueForKey:CONTENT_DRM_ENABLED];

                        if([drmEnabled isEqualToString:@"Y"])
                            drmFont = [bookinfo valueForKey:CONTENT_DRM_FONT];
                    }

                    bookqueryValues = [NSString stringWithFormat:@"\"%@\",\"%@\",%@,%@,'%@',\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%@,\"%@\",\"%@\",\"%@\"",[bookinfo objectForKey:CONTENT_NAME],metadata,[bookinfo objectForKey:CONTENT_ID],[bookinfo valueForKey:CATEGORY_ID],[bookinfo valueForKey:CONTENT_DOWNLOADABLE],[bookinfo objectForKey:CONTENT_WRAPPER_URL],[bookinfo valueForKey:CONTENT_PUB_DATE],[bookinfo objectForKey:CONTENT_FORMAT],drmEnabled,[bookinfo objectForKey:CONTENT_DOWNLOAD_URL],[bookinfo objectForKey:CONTENT_PRICE],[bookinfo objectForKey:CONTENT_AUTHOR],[bookinfo valueForKey:CONTENT_PROD_IDENTIFIER],drmFont];
                    
                    if ([DBObject insertMagazineFields:@"book_name,content_short_desc,book_id,cat_id,downloadable,wrapper_url,pub_date,book_format,drm_enabled,download_url,price,author,ios_uid,font" values:bookqueryValues tableName:STORE_BOOK_TABLE]) 
                    {
                       // NSLog(@"book inserting  Successfully %@",queryValues);
                    }
                }
            }
            bookqueryValues=nil, metadata=nil;
            books=nil, bookinfo=nil;
        }
        DBObject = nil;
    }    
    [self loadMainCategory];
}


- (void) clearTables {
    DBClass *dbObject = [[DBClass alloc] init];
    [dbObject deleteAllMagazines:STORE_CATEGORY_TABLE];
    [dbObject deleteAllMagazines:STORE_BOOK_TABLE];
    [dbObject release];
     dbObject = nil;
}

-(void) loadMainCategory {

    DBClass *dbObject = [[DBClass alloc] init];
    
    [scroll removeFromSuperview],[scroll release], scroll = nil;
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 48)];
    scroll.backgroundColor = [UIColor lightGrayColor];
    scroll.opaque = 0.9;
	scroll.alpha = 0.9;
    scroll.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    NSArray *fields     = [[NSArray alloc] initWithObjects:@"cat_id",@"cat_name", nil];
    NSString * condition = @"cat_type='book'";
    NSMutableArray * catArray = [dbObject selectMagazineField:fields tblname: STORE_CATEGORY_TABLE when:condition order:@""  recordsCount:100 startWith:0];
    
    if([catArray count] > 0)
    {
        [[scroll subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
        int xCnt=0;
        categories = [[NSMutableDictionary alloc] init];
        for(NSDictionary *dicObj in catArray )
        {
            CustomUIButton *catBtn  = [[[CustomUIButton alloc] initWithFrame:CGRectMake(xCnt*125+8, 8, 120, 32)] autorelease];
            UILabel *btnLbl = [[UILabel alloc] initWithFrame: CGRectMake(0,5,120,20)];
            btnLbl.font = [UIFont boldSystemFontOfSize: 14.0];
            btnLbl.textColor = [UIColor whiteColor];
            btnLbl.adjustsFontSizeToFitWidth = YES;
            btnLbl.minimumFontSize = 14.0;
            btnLbl.textAlignment = UITextAlignmentCenter;
            btnLbl.lineBreakMode = UILineBreakModeWordWrap;
            btnLbl.numberOfLines = 4; 
            btnLbl.backgroundColor = [UIColor clearColor];
            btnLbl.text = [dicObj objectForKey:@"cat_name"] ;
            [catBtn addSubview:btnLbl];
            catBtn.tag =  [[dicObj objectForKey:@"cat_id"] intValue]; 
            [catBtn setBackgroundColor:[UIColor clearColor]];
            catBtn.categoryid = [dicObj objectForKey:@"cat_id"] ;
            catBtn.categoryname = [dicObj objectForKey:@"cat_name"];
            [catBtn addTarget:self action:@selector(selectedcategory:) forControlEvents:UIControlEventTouchUpInside];
            [catBtn setBackgroundImage:[UIImage imageNamed:@"button_bg.png"] forState:UIControlStateSelected];
            if(xCnt==0){
                [catBtn setSelected:TRUE];
                [catBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            
            [scroll addSubview:catBtn];
            xCnt++;
        }
        
        [scroll setContentSize:CGSizeMake(120 * [catArray count]+60, 46)];
        [self.view addSubview:scroll];
        [categories retain];
    }
    
    fields=nil, condition=nil, catArray=nil;
}


-(IBAction)selectedcategory:(id)sender {
   
    CustomUIButton *btn = (CustomUIButton*)sender;
    for(UIView *subview in [scroll subviews]) 
    {
        if([subview isKindOfClass:[CustomUIButton class]]) 
        {
            CustomUIButton *btn1 = (CustomUIButton*)subview;
            if(btn.tag == subview.tag)
                btn.selected = TRUE;
            else
                btn1.selected = FALSE;
        }
    }

    categName = btn.categoryname;
    [categName retain];
    
    DBClass *dbObject = [[DBClass alloc] init];

    NSArray *fields     = [[NSArray alloc] initWithObjects:@"book_id",@"wrapper_url",@"book_name",@"content_short_desc",@"author",
                           @"pub_date", @"cat_id",@"ios_uid",@"price",@"downloadable",@"download_url", nil];

    NSString * condition = [NSString stringWithFormat:@"cat_id=%@",btn.categoryid];

    loadCatBooks = [dbObject selectMagazineField:fields tblname: STORE_BOOK_TABLE when:condition order:@" order by book_id desc"  recordsCount:100 startWith:0];
   
    [self invokeGridDelegate];
}

#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return [loadCatBooks count];
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    Login *logMod = [[Login alloc]init];
    BOOL st = logMod.isUserDetailsExists;
    
    CGRect fram = CGRectMake(20, 5, 700, 260);
    if(IsIpad())
        fram = CGRectMake(20, 5, 700, 260);
    else
        fram = CGRectMake(20, 5, 300, 260);
    NSDictionary *dictObj = [loadCatBooks objectAtIndex:index];
    AQGridViewCell * cell = nil;
    fileManager = [NSFileManager defaultManager];
    NSString * FilledCellIdentifier = [NSString stringWithFormat:@"%@",[dictObj objectForKey:@"book_id"]];//@"FilledCellIdentifier";
    BookCell * filledCell = (BookCell *)[aGridView dequeueReusableCellWithIdentifier: FilledCellIdentifier];
    
    if ( filledCell == nil )
    {
        filledCell = [[BookCell alloc] initWithFrame: fram reuseIdentifier: FilledCellIdentifier];
    }
    
    filledCell.parent   = self;
    filledCell.parent   = self;
    filledCell.title    = [dictObj objectForKey:@"book_name"];
    filledCell.author   = [dictObj objectForKey:@"author"];
    filledCell.contdesc = [dictObj objectForKey:@"content_short_desc"];
    filledCell.price    = @"";
    filledCell.imgV.url =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KIPADBIMGPATH,[dictObj objectForKey:@"wrapper_url"]]];    
    filledCell.custombutton.price           = [dictObj objectForKey:@"price"];
    filledCell.custombutton.readStatus      = [book_cat returnbtnStatus:[dictObj objectForKey:@"downloadable"] contentid:[dictObj objectForKey:@"book_id"]];
    
    filledCell.btnStatus                    = filledCell.custombutton.readStatus;    
    filledCell.custombutton.title           = [book_cat returnButtonTitle:filledCell.custombutton.readStatus];
    filledCell.custombutton.categoryid      = [dictObj objectForKey:@"cat_id"];
    filledCell.custombutton.categoryname    = categName;     
    filledCell.custombutton.contentId       = [dictObj objectForKey:@"book_id"];
    filledCell.custombutton.contentName     = [dictObj objectForKey:@"book_name"];
    filledCell.custombutton.authorName      = [dictObj objectForKey:@"author"];
    filledCell.custombutton.contentDate     = [dictObj objectForKey:@"pub_date"];
    filledCell.custombutton.contentDesc     = [dictObj objectForKey:@"content_short_desc"];
    filledCell.custombutton.prod_identifier = [dictObj objectForKey:@"ios_uid"];
    
    
    //filledCell.downloadurl     = [logMod getDeviceDetails:[dictObj objectForKey:@"download_url"]];

    filledCell.downloadurl = [NSString stringWithFormat:@"%@%@", [dictObj objectForKey:@"download_url"], [getUrlAndParams valueForKey:@"devDet"]];
    
    NSLog(@"DOWNLOAD URL:%@",[NSString stringWithFormat:@"%@%@", [dictObj objectForKey:@"download_url"], [getUrlAndParams valueForKey:@"devDet"]]);
    
    
    filledCell.catname         = categName;
    filledCell.custombutton.color           = [book_cat returnButtonTitleColor:filledCell.custombutton.readStatus];
    ///latest mod
    filledCell.custombutton.tag = [[dictObj objectForKey:@"book_id"] integerValue];
    
    [filledCell.custombutton setBackgroundImage:[UIImage imageNamed:[book_cat returnButtonColor:filledCell.custombutton.readStatus]] 
                                       forState:UIControlStateNormal];
    
    [self.imageManager manage:filledCell.imgV];
    
    if(filledCell.custombutton.readStatus == 0) {
        filledCell.price    = [NSString stringWithFormat:@"%@ %@", @"$",[dictObj objectForKey:@"price"]];
    }
    cell = filledCell;
    cell.selectionStyle = AQGridViewCellSeparatorStyleNone;
    [logMod release], logMod = nil;
    return ( cell );

}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    CGSize fram = CGSizeMake(700, 280);
    if(IsIpad())
        fram = CGSizeMake(700, 260);
    else
        fram = CGSizeMake(300, 260);
    
    if([self isLandscape])
        return ( fram );
    else
        return ( fram );
}
- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}
#pragma mark -
#pragma mark Grid View Delegate
 
/*  Starting of spinner loading delegates*/

-(void) startLoading {

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setCenter:self.view.center];
    spinner.activityIndicatorViewStyle =  UIActivityIndicatorViewStyleWhiteLarge;//UIActivityIndicatorViewStyleWhiteLarge
    [self.view addSubview:spinner];
    [spinner startAnimating];
    [self.view bringSubviewToFront:spinner];
}

-(void) stopLoading {
    if([self.view.subviews containsObject:spinner]) {
        //[self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
        [self.view setOpaque:0.0];
        [self.view setUserInteractionEnabled:TRUE];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
    }
    [spinner release], spinner = nil;
}
-(void) _showAlert:(NSString *) title
{
    
    Login *logMod = [[Login alloc]init];
    BOOL st = logMod.isUserDetailsExists;
    
    if([logMod.username isEqualToString:@""] || logMod.username == nil || logMod.username == NULL )
        logMod.username = @"Guest";
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@%@",@"Dear ",logMod.username]  message:title  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
    [logMod release],logMod = nil;
    
}
@end

