//
//  AsyncImageView.m
//  Postcard
//
//  Created by markj on 2/18/09.
//  Copyright 2009 Mark Johnson. You have permission to copy parts of this code into your own projects for any use.
//  www.markj.net
//

#import "AsyncImageView.h"
#import "Constant.h"

// This class demonstrates how the URL loading system can be used to make a UIView subclass
// that can download and display an image asynchronously so that the app doesn't block or freeze
// while the image is downloading. It works fine in a UITableView or other cases where there
// are multiple images being downloaded and displayed all at the same time. 

@implementation AsyncImageView
@synthesize categoryTitle, imageURL,
            dateTime, commentCount, 
            articleDesc, resStatus,moduleIndi;

- (void)dealloc {
	[connection cancel]; //in case the URL is still downloading
	[connection release];
	[data release]; 
    [super dealloc];
}


- (void)loadImageFromURL:(NSURL*)url {
	if (connection!=nil) { [connection release]; } //in case we are downloading a 2nd image
			
	if (data!=nil) { [data release]; }
	
	if (imageURL!=nil) {
		NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
		connection = [[NSURLConnection alloc] initWithRequest:request delegate:self]; //notice how delegate set to self object
		//TODO error handling, what if connection is nil?
	}
	
}
	
-(void)showHalfArticle {
  
    // Point Variables
    
    int xWidth = KIPADTHUMBIMGWIDTH , yWidth = KIPADTHUMBIMGHEIGHT, nLines = 5, catTitleSize = 15, catDesc = 12, descHeight = 90, xComment = 125;
    NSString *stateNormal = @"ipad_imgoutline_grey.png", *commentBox = @"iphone_comment_box_ver_2.png" , *postedTime = @"posted_time_bg.png";
    
    if (resStatus) {
        
        xWidth = KIPHNTHUMBIMGWIDTH , yWidth = KIPHNTHUMBIMGHEIGHT, nLines = 4, catTitleSize = 11.5, catDesc = 10, descHeight = 40, xComment = 75;
        stateNormal = @"iphone_outline_grey_ver_1.png", commentBox = @"iphone_comment_box_ver_2.png", postedTime = @"iphone_posted_time_bg_ver_1.png";
        
    }
        
        
    
	UILabel *theText = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, 70, 20)];
	UIView *infoView,*backView;
	infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, xWidth, yWidth)];
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, xWidth, 60)];
    theText = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, xWidth - 10, 60)];
    theText.lineBreakMode = UILineBreakModeWordWrap;
	theText.numberOfLines = 4;
	theText.text = categoryTitle;
	theText.textColor = [UIColor whiteColor];
    theText.backgroundColor =  [UIColor clearColor];
	theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:catTitleSize];
    [backView setBackgroundColor:[UIColor darkGrayColor]];
    [backView addSubview:theText];
    [infoView addSubview:backView];
    
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, xWidth, 90)];
    theText = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, xWidth - 10, descHeight)];
	//nLines = 5;
	theText.lineBreakMode = UILineBreakModeWordWrap;
	theText.numberOfLines = nLines;
    theText.text = articleDesc;
	theText.textColor = [UIColor whiteColor];
	theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:catDesc];
	theText.backgroundColor =  [UIColor clearColor];
	infoView.backgroundColor = [UIColor clearColor];
    [backView setBackgroundColor:[UIColor blackColor]];
	[backView addSubview:theText];
    [infoView addSubview:backView];
    
	infoView.alpha = 0.7;
	[self addSubview:infoView];
    
    UIImage *thumbImage = [UIImage imageNamed:postedTime];
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(-2, 5, 85, 22);
	[self addSubview:imageView];
   
    theText = [[UILabel alloc] initWithFrame:CGRectMake(5, 8, 85, 15)];
    theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:11];
    theText.text = dateTime;
    theText.backgroundColor = [UIColor clearColor];
    theText.textColor = UIColorFromRGB(0x3399FF); 
    [imageView addSubview:theText]; 
	
     if([commentCount intValue]> 0) {
        UIButton *commentsImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [commentsImageView setImage:[UIImage imageNamed:commentBox] forState:UIControlStateNormal];
        commentsImageView.frame =  CGRectMake(xComment, -22, 25, 20);
        commentsImageView.tag = 2;
        [infoView addSubview:commentsImageView];
        theText = [[UILabel alloc] initWithFrame:CGRectMake(0, -2, 26, 20)];
        theText.backgroundColor = [UIColor clearColor];
        theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:8];
        theText.textColor = [UIColor whiteColor];
        theText.text = commentCount;
         theText.textAlignment = UITextAlignmentCenter;
        [commentsImageView addSubview:theText];
         [theText release];
     }
    [infoView setNeedsLayout];
    
    thumbImage = [UIImage imageNamed:stateNormal];
    imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(0, 30, xWidth, yWidth + 3);
	[self addSubview:imageView];
}
//the URL connection calls this repeatedly as data arrives
- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (data==nil) { data = [[NSMutableData alloc] initWithCapacity:2048]; } 
	[data appendData:incrementalData];
}


//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	//so self data now has the complete image 
	[connection release];
	connection=nil;
	if ([[self subviews] count]>0) {
		//then this must be another image, the old one is still in subviews
		[[[self subviews] objectAtIndex:0] removeFromSuperview]; //so remove it (releases it also)
	}
    
    switch(moduleIndi)
    {
        case KCLNEWSMODL: // general, network-related error
        {
            [self showNewsThumbView];
        }
            break;
        case KCLMAGMODL:{
            [self showMagaStoreThumb];
            
        }
            break;
        case KCLMAGMODLIPAD:{
            [self showMagaStoreiPad];
            
        }
            break;
            
        case KCLBOOKMODL:{
            [self showBookStoreThumb];
        }
            break;
            
        case 5:{
            [self showMagaStoreCover];
        }
            break;
        case 6:{
            [self showMagaStoreCoveriPhone];
        }
        case 7:{
            [self showMagaStoreIssuesiPhone];
        }
            break;
        case 8:{
            [self showPreviewPage];
        }
            break;
        default:
        {
           
        }
            break;
    }
        
    [data release]; //don't need this any more, its in the UIImageView now CGRectMake(0, 0, 224/1.75, 300/1.75)
	data=nil;
}

//just in case you want to get the image directly, here it is in subviews
- (UIImage*) image {
	UIImageView* iv = [[self subviews] objectAtIndex:0];
	return [iv image];
}


- (void)showNewsThumbView {
    
    int xWidth = KIPADTHUMBIMGWIDTH, yWidth = KIPADTHUMBIMGHEIGHT, nLines = 5, yTitle = 132, xComment = 125, catTitleSize = 15;
    NSString *stateNormal = @"ipad_imgoutline_grey.png", *commentBox = @"iphone_comment_box_ver_2.png" , *postedTime = @"posted_time_bg.png";
    
    if (resStatus) {
        
        xWidth = KIPHNTHUMBIMGWIDTH, yWidth = KIPHNTHUMBIMGHEIGHT, nLines = 4, yTitle = 90, xComment = 75;
        stateNormal = @"iphone_outline_grey_ver_1.png", commentBox = @"iphone_comment_box_ver_2.png", postedTime = @"iphone_posted_time_bg_ver_1.png";
        catTitleSize = 14;
    }
    
	UIImage *thumbImage = [UIImage imageWithData:data];
	UILabel *theText = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, xWidth, 20)];
	UIView *infoView;
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
	[self addSubview:imageView];
	imageView.frame = CGRectMake(self.bounds.origin.x + 1, self.bounds.origin.y + 31, xWidth, yWidth) ;
	[imageView setNeedsLayout];
	infoView = [[UIView alloc] initWithFrame:CGRectMake(0, yTitle , xWidth, 50)];
	nLines = 3;
	theText.lineBreakMode = UILineBreakModeWordWrap;
	theText.numberOfLines = 2;
	theText.frame = CGRectMake(4,-10, xWidth, 65);
	
    
	//Set Article title
	//theText = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, 100, 25*nLines)];
	theText.text = categoryTitle;
	theText.textColor = [UIColor whiteColor];
	theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:catTitleSize];
	theText.backgroundColor =  [UIColor clearColor];
	//infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, thumbImage.size.width, 40)];
	infoView.backgroundColor = [UIColor blackColor];
	infoView.opaque = 0.8;
	[infoView addSubview:theText];
	infoView.alpha = 0.7;
    
    NSLog(@"heelo2 %@",categoryTitle);
    thumbImage = [UIImage imageNamed:postedTime];
    imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(-2, 5, 85, 22);
	[self addSubview:imageView];
    
    
    theText = [[UILabel alloc] initWithFrame:CGRectMake(5, 8, 88, 15)];
    theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:11];
    theText.text = dateTime;
    theText.backgroundColor = [UIColor clearColor];
    theText.textColor = UIColorFromRGB(0x3399FF);
   
    [imageView addSubview:theText];
	[self addSubview:infoView];
	[infoView setNeedsLayout];
	
    if([commentCount intValue]> 0) {
        
        UIButton *commentsImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [commentsImageView setImage:[UIImage imageNamed:commentBox] forState:UIControlStateNormal];
        commentsImageView.frame =  CGRectMake(xComment, 7, 25, 20);
        commentsImageView.tag = 2;
        [infoView addSubview:commentsImageView];
        theText = [[UILabel alloc] initWithFrame:CGRectMake(0, -2, 26, 20)];
        theText.backgroundColor = [UIColor clearColor];
        theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:8];
        theText.textColor= [UIColor whiteColor];
        theText.textAlignment = UITextAlignmentCenter;
        theText.text = commentCount;
        [commentsImageView addSubview:theText];
        [self addSubview:commentsImageView];
        [theText release];
    }
    
    thumbImage = [UIImage imageNamed:stateNormal];
    imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(0, 30, xWidth + 3, yWidth + 3);
	[self addSubview:imageView];
    
    [infoView setNeedsLayout];
    
	[self setNeedsLayout];

}

- (void)showMagaStoreThumb {
    UIImage *thumbImage = [UIImage imageWithData:data];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(0, 0, 140/2, 185/2);// CGRectMake(0, 0, 108, 140);
	[self addSubview:imageView];
}
- (void)showMagaStoreiPad {
    
    //UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    UIImage *thumbImage = [UIImage imageWithData:data];
    //btn.frame = CGRectMake(0, 0, 150, 212);
    //[btn setImage:thumbImage forState:UIControlStateNormal];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    //imageView.frame = CGRectMake(0, 0, 162, 224);// CGRectMake(0, 0, 108, 140);
    imageView.frame = CGRectMake(0, 0, 150, 212);// CGRectMake(0, 0, 108, 140);
	[self addSubview:imageView];
}
- (void)showMagaStoreCover {
    UIImage *thumbImage = [UIImage imageWithData:data];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(0, 0,204, 270);// CGRectMake(0, 0, 108, 140);150+75+37, 424
	[self addSubview:imageView];
}

-(void) showMagaStoreCoveriPhone {
    UIImage *thumbImage = [UIImage imageWithData:data];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = CGRectMake(0, 0,224/1.75, 300/1.75);// CGRectMake(0, 0, 108, 140);150+75+37, 424
	[self addSubview:imageView];
    
}
- (void) showMagaStoreIssuesiPhone {
    //UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[self setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    UIImage *thumbImage = [UIImage imageWithData:data];
    //btn.frame = CGRectMake(0, 0, 150, 212);
    //[btn setImage:thumbImage forState:UIControlStateNormal];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    //imageView.frame = CGRectMake(0, 0, 162, 224);// CGRectMake(0, 0, 108, 140);
    imageView.frame = CGRectMake(0, 0, 150/1.2, 212/1.2);// CGRectMake(0, 0, 108, 140);
	[self addSubview:imageView];

}

-(void) showPreviewPage {
    UIImage *thumbImage = [UIImage imageWithData:data];
	UIImageView* imageView = [[[UIImageView alloc] initWithImage:thumbImage] autorelease];
    imageView.frame = self.bounds;
	[self addSubview:imageView];
}
@end