//
//  Library_iPad.h
//  Vikatan
//
//  Created by MobileVeda on 15/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Magazine_iPad.h"
#import "Constant.h"
#import "VikatanAppDelegate_iPad.h"
#import "PDFScrollView.h"
#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5
#import "ReaderViewController.h"
#import "LibraryCloseButton.h"

#import "ImageDemoCellChooser.h"
#import "ImageDemoGridViewCell.h"

@class PDFScrollView;
@class VikatanAppDelegate_iPad;

@interface Library_iPhone : UIViewController<UIScrollViewDelegate,UIWebViewDelegate,ReaderViewControllerDelegate,UIAlertViewDelegate,AQGridViewDelegate, AQGridViewDataSource,ImageDemoCellChooserDelegate> {
    
    UIImageView *headerImageView;
    
    UIScrollView *mainView;
    UIView *magazineView;
    UIScrollView *catPlacHolder, *magShelfRowview;
    
    int tmpIssueID, magID, itemIndex,xClosePos;
    NSString *tmpIssueFldPath;
    
    //AQGridViewCell
    AQGridView * gridView;
    NSArray * _orderedImageNames;
    NSMutableArray * _imageNames;
    UIView *noIssues;
    
    NSMutableArray *Issueid,*issueDate;
    
    BOOL editMode, pdfMode;
    UIButton *editBtn;
    
    CGSize thumbnailSize;// = CGSizeMake(162.f, 200.f);
    CGSize thumbnailMargin;//_ = CGSizeMake(10.f, 10.f);
    
    
}

@property (nonatomic,retain) UIImageView *headerImageView;

@property (nonatomic,retain) UIScrollView *mainView, 
*catPlacHolder,
*magShelfRowview;
@property (nonatomic, retain)  AQGridView * gridView;
@property (nonatomic, retain)  UIButton *editBtn;

@property (nonatomic,retain)NSString *tmpIssueFldPath;

@property(nonatomic, assign) CGSize thumbnailSize;

/// change margin around thumbnails. Defaults to 10x10
@property(nonatomic, assign) CGSize thumbnailMargin;

- (void) LoadMagazines;

- (void) hideTabBarView;

- (void) showTabBar;

- (void) LoadMagazines:(int)magazineID;

- (void) loadMagCateg;

-(BOOL) deletIssue;

- (void)applyShinyBackgroundWithColor:(UIColor *)color;

//Customize GridView
- (void)initializeGridView;

- (void) editModeDone;

- (BOOL)isLandscape;

- (void) reloadLibView;
@end
