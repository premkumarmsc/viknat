//
//  Magazine_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 13/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Magazine_iPad.h"
#import <QuartzCore/QuartzCore.h>
#import "JSON.h"

@implementation Magazine_iPad

#pragma mark Constants

#define SAMPLE_DOCUMENT @"magazine.pdf"

#define DEMO_VIEW_CONTROLLER_PUSH FALSE

#pragma mark Properties

//@synthesize ;

#pragma mark UIViewController methods
@synthesize lastIndexPreviewed = _lastIndexPreviewed;
@synthesize pageNum;
@synthesize imageView, imageScrollView, 
    _masterView,
    _thumbPlaceHolderView,
    pdfImage,
    spinner,
    _textView,
    _pageNOView,
    textViewButton,
    _loadingView,
    pageNO,
    pdfDocPath;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
   
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:TRUE];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
       
    ReaderDocument *document = [ReaderDocument unarchiveFromFileName:SAMPLE_DOCUMENT];
    
	if (document == nil) // Create a brand new ReaderDocument object the first time we run
	{
		NSString *filePath = [[NSBundle mainBundle] pathForResource:SAMPLE_DOCUMENT ofType:nil];
        
		document = [[[ReaderDocument alloc] initWithFilePath:filePath password:nil] autorelease];
	}
    
	ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
    
	readerViewController.delegate = self; // Set the ReaderViewController delegate to self
	readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    
	[self presentModalViewController:readerViewController animated:YES];
    
	[readerViewController release]; // Release the ReaderViewController
     
}

- (void)back_clicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)dealloc {
    
    appDelegate = nil;
    [super dealloc];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    //[self.tabBarController.tabBar setHidden:YES];
    
}
 
- (CGRect)frameForPageAtIndex:(NSInteger)index {
    CGSize pageSize = pagingView.bounds.size;
    return CGRectMake(index *pageSize.width, 0,
                      pageSize.width, pageSize.height);
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
   // [self.parentViewController dismissModalViewControllerAnimated:TRUE];
	//[self dismissModalViewControllerAnimated:YES];
    //[self.navigationController dismissModalViewControllerAnimated:YES];
    //[self.view popViewControllerAnimated:YES];
    
    
//#endif
}

@end
