//
//  LoginView.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 03/12/11.
//  Copyright (c) 2011 MobileVeda. All rights reserved.
//

#import "LoginView.h"
#include "Constant.h"
#include "Login.h"
#import "NSData+Base64.h"

@implementation LoginView
@synthesize fromInfo,fromBookStore, fromSocialLogin;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [self showlog];
    [super viewDidLoad];
    
    fromSocialLogin = FALSE;
    // Do any additional setup after loading the view from its nib.

}

- (void)viewDidAppear:(BOOL)animated
{
    
    NSLog(@"viewDidAppear is coming inside of the loginview.m");
    
    if(fromSocialLogin)
    {
        NSLog(@"from social login if statemen");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissModalViewSocialLogin" object: nil];
    } else {
        NSLog(@"from social login else statement");
    }
}

- (void) showlog {
    
    int inc = 20;
    int xWidth = 700, yWidth = 350+165, yPos = 0,loginTitle = 200,
    txtUsrYPos = 100+15,txtUsrXPos = 145,txtPassYPos=170+15,txtPassXPos= 145,
    lblUsrYpos = 115+15 ,lblPassYpos = 185+15,loginYView = 0,loginXPos = 200
    ,txtlength = 300,newUsrXpos = 350,msglbl = 30,loginbtn = 240,lblx = 50,lblux = 70,
    mesgfontsize = 16,newUsrYpos = 240,orlblxpos = 230   ;
    
    if (!([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)) {
        xWidth = 320, yWidth = 420, yPos = 0,txtUsrYPos = 80 + inc+10 /*135*/,
        txtUsrXPos = 20,txtPassYPos=215-55+inc,txtPassXPos= 20,lblUsrYpos = 55+inc+10,
        lblPassYpos = 135+inc,loginYView = 0,loginXPos = 20,txtlength = 250, 
        loginTitle = 20,msglbl = 0,loginbtn = 250,lblx = 20,
        lblux = 20,mesgfontsize = 15,newUsrXpos = 130,newUsrYpos = 250,orlblxpos = 55;
    }

    loginView = [[UIView alloc] initWithFrame:CGRectMake(loginYView,yPos,xWidth,yWidth)];
    [loginView setBackgroundColor:[UIColor whiteColor]];
    [loginView setClipsToBounds: YES];
    
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,50)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[cmntHeader layer] setBorderWidth:5];
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:KLOGINHD forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(loginTitle, 15, 250, 25);
    commentTitle.enabled = FALSE;
    [cmntHeader addSubview:commentTitle];
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) 
        [loginView addSubview:cmntHeader];
    
    
    UILabel *mesg = [[[UILabel alloc] initWithFrame:CGRectMake(20,msglbl , xWidth - 50, 100)] autorelease];
    mesg.text = KLOGMSG;
    mesg.textAlignment = UITextAlignmentLeft;
    mesg.backgroundColor = [UIColor clearColor];
    mesg.textColor = [UIColor darkGrayColor];
    mesg.lineBreakMode = UILineBreakModeWordWrap;
	mesg.numberOfLines = 4;
    
    mesg.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:mesgfontsize];
    [loginView addSubview:mesg];
    
    UILabel *userLbl = [[[UILabel alloc] initWithFrame:CGRectMake(lblux,lblUsrYpos , 100, 30)] autorelease];
    userLbl.text = @"User Id";
    userLbl.textAlignment = UITextAlignmentLeft;
    userLbl.backgroundColor = [UIColor clearColor];
    userLbl.textColor = [UIColor darkGrayColor];
    userLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    [loginView addSubview:userLbl];
    
    userTxt = [[UITextField alloc] initWithFrame:CGRectMake	(txtUsrXPos, txtUsrYPos, txtlength, 43)];
    userTxt.borderStyle = UITextBorderStyleBezel;
    
    [userTxt setBackgroundColor:[UIColor clearColor]];
    [userTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    userTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTxt.textAlignment = UITextAlignmentCenter;
    //userTxt.placeholder = @"User Id";
    [userTxt setKeyboardType:UIKeyboardTypeEmailAddress];
    userTxt.returnKeyType = UIReturnKeyNext;
    userTxt.delegate = self;
    userTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    userTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [userTxt setClipsToBounds: YES];
    [userTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [loginView addSubview:userTxt];
    
    UILabel *passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(lblx,lblPassYpos , 100, 30)] autorelease];
    passLbl.text = @"Password";
    passLbl.textAlignment = UITextAlignmentLeft;
    passLbl.backgroundColor = [UIColor clearColor];
    passLbl.textColor = [UIColor darkGrayColor];
    passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    
    passTxt = [[UITextField alloc]initWithFrame:CGRectMake(txtPassXPos,txtPassYPos,txtlength,43)]; 
    passTxt.borderStyle = UITextBorderStyleBezel;
    [passTxt setBackgroundColor:[UIColor clearColor]];
    [passTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    passTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    passTxt.textAlignment = UITextAlignmentCenter;
    [passTxt setSecureTextEntry:YES];
    [passTxt setKeyboardType:UIKeyboardTypeDefault];
    passTxt.returnKeyType = UIReturnKeyDone;
    //passTxt.placeholder = @"Password";
    passTxt.delegate = self;
    passTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [passTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [passTxt setClipsToBounds: YES];
    [loginView addSubview:passTxt];
    
    [loginView addSubview:passLbl];
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
    loginBtn.frame = CGRectMake(loginXPos,loginbtn , 100, 30) ;
    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:5.0f];
    [loginBtn.layer setBorderWidth:1.0f];
    [loginView addSubview:loginBtn];
    
    UIButton *newUser = [[[UIButton alloc] initWithFrame:CGRectMake(newUsrXpos,newUsrYpos , 300, 30)] autorelease];
    
    newUser.backgroundColor = [UIColor clearColor];
    UILabel *newUsrLabl = [[[UILabel alloc] initWithFrame:CGRectMake(0,0 , 190, newUser.frame.size.height+20)] autorelease];
    newUsrLabl.text = @"New User? Get registered here...";//New User? Get registered here...
    newUsrLabl.textColor = [UIColor darkGrayColor];
    
    //
    newUsrLabl.adjustsFontSizeToFitWidth = YES;
    newUsrLabl.minimumFontSize = 14.0;
    newUsrLabl.textAlignment = UITextAlignmentLeft;
    newUsrLabl.lineBreakMode = UILineBreakModeWordWrap;
    newUsrLabl.numberOfLines = 2;
    //
    [newUser addSubview:newUsrLabl];
    [newUser addTarget:self action:@selector(newUser:) forControlEvents:UIControlEventTouchUpInside];
    [loginView addSubview:newUser];

    //Adding For Social Network Login
    
    UILabel *orlbl = [[[UILabel alloc] initWithFrame:CGRectMake(orlblxpos,loginbtn+35 , 100, 30)] autorelease];
    orlbl.text = @"OR";
    orlbl.textAlignment = UITextAlignmentLeft;
    orlbl.backgroundColor = [UIColor clearColor];
    orlbl.textColor = [UIColor blackColor];
    orlbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
    [loginView addSubview:orlbl];

    UIButton *loginBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn1 setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
    loginBtn1.frame = CGRectMake(loginXPos,loginbtn+70 , 290, 30) ;
    [loginBtn1 setTitle:@"Via Social Login" forState:UIControlStateNormal];
    [loginBtn1 addTarget:self action:@selector(openSocialNetworkView:) forControlEvents:UIControlEventTouchUpInside];//
    [loginBtn1.layer setMasksToBounds:YES];
    [loginBtn1.layer setCornerRadius:5.0f];
    [loginBtn1.layer setBorderWidth:1.0f];
    [loginView addSubview:loginBtn1];

    //Added For Social Network Login

    UILabel *orlabell = [[[UILabel alloc] initWithFrame:CGRectMake(orlblxpos,loginbtn+110 , 100, 30)] autorelease];
    orlabell.text = @"OR";
    orlabell.textAlignment = UITextAlignmentLeft;
    orlabell.backgroundColor = [UIColor clearColor];
    orlabell.textColor = [UIColor blackColor];
    orlabell.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
    [loginView addSubview:orlabell];

    UIButton *socialBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [socialBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
    socialBtn.frame = CGRectMake(loginXPos,loginbtn+140 , 290, 30) ;
    [socialBtn setTitle:@"Continue Without Registering" forState:UIControlStateNormal];
    [socialBtn addTarget:self action:@selector(nologinButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [socialBtn.layer setMasksToBounds:YES];
    [socialBtn.layer setCornerRadius:5.0f];
    [socialBtn.layer setBorderWidth:1.0f];
    [loginView addSubview:socialBtn];

    [self.view addSubview:loginView];
}
-(IBAction)textFieldReturn:(id)sender {
    [sender resignFirstResponder];
}
-(IBAction) loginButtonClicked:(id) sender {
    
    if(![self checkInternet]) 
        return;
    
    NSString  *username = userTxt.text;
    NSString  *password = passTxt.text;
    
    if (username == nil || password == nil || [username isEqualToString:@""] || [password isEqualToString:@""] ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Invalid user, Please Try Again.." message:@"Please provide Valid User Name and Password"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
       
    } else {
         Login *loginMod = [[Login alloc]init];
        if( [loginMod startServerAuthentication:username:password:@"":@"main"]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"inforeferesh" object: nil];
            [self magStorereferesh];
            
            [[self navigationController] popViewControllerAnimated: YES];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Login Success" message:@"Login Success"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
            [self closePopViewContrl];
            [self dismissModalViewControllerAnimated:TRUE];
           
        }
    }
}
-(IBAction)nologinButtonClicked:(id) sender {
    if(![self checkInternet]) 
        return;
    [self closePopViewContrl];
    [self dismissModalViewControllerAnimated:TRUE];
    
    if (fromBookStore) {

       [[NSNotificationCenter defaultCenter] postNotificationName:@"bookpurccontinue" object: nil];        
    } else {
        
        if(!fromInfo) {
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"purccontinue" object: nil];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"purccontinueiphone" object: nil];
            }
        }
    }
}

-(IBAction)openSocialNetworkView:(id)sender {

    if(![self checkInternet])
        return;
    
     SocialNetworkWebViewViewController *SocialNetworkViewController;

    if(IsIpad()) {
        
        SocialNetworkViewController = [[SocialNetworkWebViewViewController alloc] initWithNibName:@"SocialNetworkWebViewViewController" bundle:nil];
        
        [SocialNetworkViewController.view setBackgroundColor:[UIColor clearColor]];
        
        SocialNetworkViewController.popoverController = [[UIPopoverController alloc] initWithContentViewController:SocialNetworkViewController] ;
        
        //CGRect popoverRect = [self.view convertRect:[sender frame] fromView:[sender superview]];
        
        CGRect popoverRect = CGRectMake(0, 0, 10, 10);
        popoverRect.size.width = MIN(popoverRect.size.width, 100);
        CGSize size = CGSizeMake(768, 450); // size of view in popover
        SocialNetworkViewController.popoverController.popoverContentSize = size;
        [SocialNetworkViewController.popoverController
         presentPopoverFromRect:popoverRect
         inView:self.view
         permittedArrowDirections:UIPopoverArrowDirectionAny
         animated:YES];
    } else {
       SocialNetworkViewController = [[SocialNetworkWebViewViewController alloc] initWithNibName:@"SocialNetworkWebViewViewController_iphone" bundle:nil];        
        
        UINavigationController *NavController = [[[UINavigationController alloc] initWithRootViewController:SocialNetworkViewController] autorelease];
        
        SocialNetworkViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(closeModalView)] autorelease];
        
        NavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        NavController.navigationController.toolbar.hidden=FALSE;
        [self presentModalViewController:NavController animated:YES];
    }
    
    SocialNetworkViewController=nil, [SocialNetworkViewController release];
    
}

- (void)closeModalView {
    
    [self dismissModalViewControllerAnimated:YES];
    fromSocialLogin = TRUE;
}

- (void) magStorereferesh {
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"magazinereferesh" object: nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bookreferesh" object: nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"magazinerefereshiphone" object: nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bookreferesh" object: nil];
    }
}

- (void) closePopViewContrl {
    
    if(fromInfo) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"infopopviewclose" object: nil];
    } else {
       [[NSNotificationCenter defaultCenter] postNotificationName:@"popviewclose" object: nil];
    }
}
-(void)newUser:(id)sender
{
    
    int xWidth = 700, yWidth = 450, yPos = 50,loginTitle = 290,xStartPos = 180,
    txtUsrYPos = 65,txtUsrXPos = 200,txtPassYPos=170,txtPassXPos= 145,
    lblUsrYpos = 115,lblPassYpos = 185,loginYView = 34,loginXPos = 280,headerYPos = 10
    ,txtlength = 300,newUsrXpos = 175,lblY = 60,headerHeight = 50,loginViewStartPos = 180,loginbtn = 300;
    if (!([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)) {
        xWidth = 320, yWidth = 420, yPos = 0,txtUsrYPos = 35,
        txtUsrXPos = 20,txtPassYPos=215,txtPassXPos= 20,lblUsrYpos = 100,
        lblPassYpos = 180,loginYView = 0,loginXPos = 100,txtlength = 250, headerYPos = 5,
        newUsrXpos = 10,loginTitle = 100, xStartPos = 10,headerHeight = 30,loginViewStartPos = 0,loginbtn = 250 ;
    }
    

    [loginView removeFromSuperview];
    loginView = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,yWidth)];
    [loginView setBackgroundColor:[UIColor whiteColor]];
    [loginView setClipsToBounds: YES];
    
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth,headerHeight)];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    [[cmntHeader layer] setBorderColor:[[UIColor clearColor] CGColor]];
    [[cmntHeader layer] setBorderWidth:5];
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:@"Vikatan Account - Registration" forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(newUsrXpos, headerYPos, 300, 25);
    commentTitle.enabled = FALSE;
    [cmntHeader addSubview:commentTitle];
    [loginView addSubview:cmntHeader];
    [loginView addSubview:cmntHeader];
    

    UILabel *userLblEmail = [[[UILabel alloc] initWithFrame:CGRectMake(20,txtUsrYPos +5 , 130, 30)] autorelease];
    userLblEmail.text = @"User Email";
    userLblEmail.textAlignment = UITextAlignmentLeft;
    userLblEmail.backgroundColor = [UIColor clearColor];
    userLblEmail.textColor = [UIColor darkGrayColor];
    userLblEmail.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    userLblEmail.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    userLblEmail.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:userLblEmail];
    
    userTxtEmail = [[UITextField alloc] initWithFrame:CGRectMake	(xStartPos, txtUsrYPos, 300, 43)];
    [[userTxtEmail layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[userTxtEmail layer] setBorderWidth:2];
    [userTxtEmail setBackgroundColor:[UIColor clearColor]];
    [userTxtEmail setFont:[UIFont boldSystemFontOfSize:16.0]];
    userTxtEmail.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTxtEmail.textAlignment = UITextAlignmentCenter;
    //userTxtEmail.placeholder = @"Email";
    [userTxtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    userTxtEmail.returnKeyType = UIReturnKeyNext;
    userTxtEmail.delegate = self;
    userTxtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    userTxtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    [userTxtEmail setClipsToBounds: YES];
    [userTxtEmail addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [loginView addSubview:userTxtEmail];
    
    UILabel *userLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,txtUsrYPos + 55 , 110, 30)] autorelease];
    userLbl.text = @"User Name";
    userLbl.textAlignment = UITextAlignmentLeft;
    userLbl.backgroundColor = [UIColor clearColor];
    userLbl.textColor = [UIColor darkGrayColor];
    userLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    userLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    userLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:userLbl];
    
    userTxt = [[UITextField alloc] initWithFrame:CGRectMake	(xStartPos, txtUsrYPos + 50, 300, 43)];
    [[userTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[userTxt layer] setBorderWidth:2];
    [userTxt setBackgroundColor:[UIColor clearColor]];
    [userTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    userTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userTxt.textAlignment = UITextAlignmentCenter;
    //userTxt.placeholder = @"User Name";
    [userTxt setKeyboardType:UIKeyboardTypeEmailAddress];
    userTxt.returnKeyType = UIReturnKeyNext;
    userTxt.delegate = self;
    userTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    userTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [userTxt setClipsToBounds: YES];
    [userTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [loginView addSubview:userTxt];
    
    UILabel *passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,txtUsrYPos + 105 , 100, 30)] autorelease];
    passLbl.text = @"Password";
    passLbl.textAlignment = UITextAlignmentLeft;
    passLbl.backgroundColor = [UIColor clearColor];
    passLbl.textColor = [UIColor darkGrayColor];
    passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    passLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    passLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:passLbl];
    
    passTxt = [[UITextField alloc]initWithFrame:CGRectMake(xStartPos,txtUsrYPos + 100,300,43)]; 
    [[passTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[passTxt layer] setBorderWidth:2];
    [passTxt setBackgroundColor:[UIColor clearColor]];
    [passTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    passTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    passTxt.textAlignment = UITextAlignmentCenter;
    [passTxt setSecureTextEntry:YES];
    [passTxt setKeyboardType:UIKeyboardTypeDefault];
    passTxt.returnKeyType = UIReturnKeyDone;
    //passTxt.placeholder = @"Password";
    passTxt.delegate = self;
    passTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [passTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [passTxt setClipsToBounds: YES];
    [loginView addSubview:passTxt];
    
    //Confirm Password
    passLbl = [[[UILabel alloc] initWithFrame:CGRectMake(20,txtUsrYPos + 155 , 180, 30)] autorelease];
    passLbl.text = @"Confirm Password";
    passLbl.textAlignment = UITextAlignmentLeft;
    passLbl.backgroundColor = [UIColor clearColor];
    passLbl.textColor = [UIColor darkGrayColor];
    passLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    passLbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
    passLbl.shadowOffset = CGSizeMake(0, -1.0);
    [loginView addSubview:passLbl];

    cPassTxt = [[UITextField alloc]initWithFrame:CGRectMake(xStartPos,txtUsrYPos + 150,300,43)]; 
    [[cPassTxt layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[cPassTxt layer] setBorderWidth:2];
    [cPassTxt setBackgroundColor:[UIColor clearColor]];
    [cPassTxt setFont:[UIFont boldSystemFontOfSize:16.0]];
    cPassTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    cPassTxt.textAlignment = UITextAlignmentCenter;
    [cPassTxt setSecureTextEntry:YES];
    [cPassTxt setKeyboardType:UIKeyboardTypeDefault];
    cPassTxt.returnKeyType = UIReturnKeyDone;
    //cPassTxt.placeholder = @"Confirm Password";
    cPassTxt.delegate = self;
    cPassTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    cPassTxt.autocorrectionType = UITextAutocorrectionTypeNo;
    [cPassTxt addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [cPassTxt setClipsToBounds: YES];
    [loginView addSubview:cPassTxt];
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
    loginBtn.frame = CGRectMake(loginXPos - 5,loginbtn , 100, 30) ;
    [loginBtn setTitle:@"Register" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(regButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [loginBtn.layer setMasksToBounds:YES];
    [loginBtn.layer setCornerRadius:5.0f];
    [loginBtn.layer setBorderWidth:1.0f];
    [loginView addSubview:loginBtn];
    
    [self.view addSubview:loginView];
    
}
-(void)regButtonClicked:(id)sender
{
    if(![self checkInternet]) 
        return;

    NSString  *username = userTxt.text;
    NSString  *password = passTxt.text;
    //NSString  *password = passTxt.text;
    
    
    if (username == nil || password == nil || [username isEqualToString:@""] || [password isEqualToString:@""] ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Invalid user, Please Try Again.." message:@"Please provide Valid User Name and Password"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        
    } else {
        
        NSInteger wrapWidth = 20;
        NSData *inputData = [passTxt.text dataUsingEncoding:NSUTF8StringEncoding];
        
        //encode
        NSString *encodedPassword = [inputData base64EncodedStringWithWrapWidth:wrapWidth];
       // NSLog(@"encodedPassword is %@", encodedPassword);

    NSURL *requestURL = [NSURL URLWithString:@"http://api.vikatan.com/?module=reg"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
    [request setPostValue:userTxt.text forKey:REQUEST_FIRST_NAME];
    [request setPostValue:encodedPassword forKey:REQUEST_PASSWORD];
    [request setPostValue:userTxtEmail.text forKey:@"emailid"];
    [request setPostValue:cPassTxt.text forKey:@"c_password"];
    [request startSynchronous];
    NSError *error = [request error];
    if (!error) {
        NSString *res = [request responseString];
        SBJSON *jsonParser = [[SBJSON new] autorelease];
        id response = [jsonParser objectWithString:res];

        NSDictionary *newsfeed = (NSDictionary *)response;
        NSString *subString = [NSString stringWithFormat:@"%@",[newsfeed valueForKey:@"resp"]];
        if([subString isEqualToString:KSUCCESS])
        {
            Login *loginMod = [[Login alloc]init];
            [loginMod setUsername:userTxt.text];
            [loginMod setSessionID:@""];   
            [loginMod setUserID:userTxtEmail.text];
            [loginMod setPassword:passTxt.text];
            [loginMod setEmailID:userTxtEmail.text];
            [loginMod storeUserDetailsInPList];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Registration" message:@"Registered Successfully"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"inforeferesh" object: nil];
            [self magStorereferesh]; 
            [self closePopViewContrl];
             [self dismissModalViewControllerAnimated:TRUE];
        }
        else {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Registration" message:subString  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        }
       // [self showlog];
         
        
    }
        inputData=nil, [inputData release];
        encodedPassword=nil, [encodedPassword release];
    
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL) connectedToNetwork
{
    
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags");
		return 0;
	}
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
	BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.vikatan.com/"]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[[NSURLConnection alloc] initWithRequest:testRequest delegate:nil] autorelease]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
} 

-(BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{
        [self _showAlert:KINTERNETNOTAVBL];
		//[self _showAlert:@"Check your connection, network unavailable."];
		return NO;
	}
	else {
		return YES;
	}
}
- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:title delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}
@end
