//
//  DownloadControl.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 28/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface DownloadControl : UIButton {
    
    NSString *downldUrlString,
             *issueDate,
             *productIdentifier,
             *magazineName,*issuName,*issuePrice, * wrapperUrl;
   
    int IssueID, MagID, index;
       
    ASIHTTPRequest *dwnRequest;
}
@property (nonatomic, retain) NSString *downldUrlString,
                                        *issueDate,
                                        *productIdentifier,
                                        *magazineName,*issuName,*issuePrice, * wrapperUrl;

@property (nonatomic) int IssueID,MagID, index ;
@property (nonatomic, retain)ASIHTTPRequest *dwnRequest;
@end
