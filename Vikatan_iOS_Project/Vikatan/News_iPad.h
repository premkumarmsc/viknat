//
//  News_iPad.h
//  Vikatan
//
//  Created by MobileVeda on 13/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "asyncimageview.h"
#import "JSON.h"
#import "CustomButton.h"
#import "Constant.h"
#import "CommentView_iPad.h"
#import "CustomWebView.h"
#import "SHK.h"
#import <QuartzCore/QuartzCore.h>
#import "Utilities.h"
#import "Login.h"
//Internet check
#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

#import "SHKMail.h"
#import "SHKTwitter.h"
#import "SHKFacebook.h"

@interface News_iPad : UIViewController <UIWebViewDelegate, UIScrollViewDelegate, UIActionSheetDelegate> {
    
    NSMutableData *responseData;
	UIActivityIndicatorView *spinner; 
	UIImage *image;
	NSString *imageURL, *artWebURL;
	NSString *articleTitle;
	NSString *articleID;
	NSString *categoryID;
	NSString *categoryTitle;
    NSInteger offset;
	UIScrollView *scrollArticleView1;
    //View declaration for news view
    UIImageView *headerImageView;
    UIScrollView *detailNewsView;
    CustomWebView *webView;
    UIScrollView *scrollCategoryView,
                 *scrollArticleView,
                 *categoryBrowsView;
    UIView  *tempScrollView, 
            *closeView,
            *categoryBrowseView,
            *fontView;
    UIImageView *headerView;
    UIButton *refreshImageView , 
             *commentsImageView, 
             *newsDetailTitle,
             *back, *close,
             *thumbTitle, 
            *socBtn,*post,
            *increase,*decrease;
    UILabel *cmntCntTxt;
    bool detailView;
    
    CGRect prevArticleViewFrame;
    NSUInteger textFontSize;
    UIScrollView *moreScrollView;
    BOOL moreCLicked, isLoading;
    UIPageControl *pageControl;
    UIView *moreViewPlaceHolder;
    NSMutableString *urlMore;
    //temp
    UIButton *tempBtn;
    UIView   *postMsgView, *postView,
             *shareView, *commentsListView,
             *categListView,*titleView,
             *titleViewDetPage;
    int tagID,articleBtnTagID;
    
    BOOL isShowingLandscapeView;
    
    Login *logMod;
}
@property (nonatomic, retain) UIActivityIndicatorView *spinner;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, retain) UIView *moreViewPlaceHolder;
@property (nonatomic, retain) NSMutableString *urlMore;

@property (nonatomic, retain) UIImageView *headerImageView;
@property (nonatomic, retain) UIScrollView *detailNewsView;
@property (nonatomic, retain) UIButton *back,*tempBtn;
@property (nonatomic, retain) CustomWebView *webView;
@property (nonatomic, retain) UIScrollView *scrollCategoryView,
                                            *categoryBrowsView,
                                            *scrollArticleView,
                                            *moreScrollView;
@property (nonatomic, retain) UIView *tempScrollView,
                                     *closeView, 
                                     *categoryBrowseView,
                                     *fontView;
@property (nonatomic, retain) UIButton *newsDetailTitle, *thumbTitle ;
@property (nonatomic, retain) UIImageView *headerView;
@property (nonatomic, retain) UIButton *refreshImageView,*post,*socBtn,*increase,*decrease;
@property (nonatomic, retain) UIButton *commentsImageView;
@property (nonatomic, retain) UIButton *close;
@property (nonatomic, retain) UILabel *cmntCntTxt;
@property (nonatomic,retain) UIView   *postMsgView, *postView, 
                                       *shareView, *commentsListView,
                                        *categListView,*titleView,
                                        *titleViewDetPage;
@property (nonatomic)NSInteger offset;

- (void) PostMessage:(id)sender;
- (void)showPostComment;
- (void) showShare;
- (void)loadImage;
- (void) myExceptionHandler :(NSException *)exception;
- (void) NewsGetRequest;
- (void) hideTabBarView;
- (void) showTabBar;
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord;
- (void) Show_MoreView:(NSString*)responseString;
- (void) nextPageURL;
- (void) addCategoryBrowse:(UILabel*)urlButton :(NSInteger) xCoord;
- (void) disableSelection:(UIScrollView*)mainView;
- (void) disableSelection2:(UIView*)mainView;
- (void) highlightCategory:(UIButton*)sender;
- (void) _showAlert:(NSString*)title;
- (void) hightlightArticle:(UIButton*)artBtn;
- (void) LoadingIcon;
- (void) StopLoading;
- (void) HideCategoryBrowse;
- (void) hideThumbScrollView;
- (void)Show_CategoryView:( NSString *) responseString;

-(void) rotateView:(UIDeviceOrientation)orientation;
- (BOOL)isLandscape;
- (BOOL) connectedToNetwork;
-(BOOL) checkInternet;

-(void)makeTabBarHidden:(BOOL)hide;


@end
