//
//	ReaderViewController.h
//	Reader v2.0.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011 Julius Oklamcak. All rights reserved.
//
//	This work is being made available under a Creative Commons Attribution license:
//		«http://creativecommons.org/licenses/by/3.0/»
//	You are free to use this work and any derivatives of this work in personal and/or
//	commercial products and projects as long as the above copyright is maintained and
//	the original author is attributed.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>

#import "ReaderDocument.h"
#import "ReaderContentView.h"
#import "ReaderMainToolbar.h"
#import "ReaderMainPagebar.h"

#import "JSON.h"

#import "QuoteCell.h"
#import "HighlightingTextView.h"
#import "SectionInfo.h"
#import "SectionHeaderView.h"

#import "Play.h"
#import "Quotation.h"

#import "CustomBezier.h"

#import "CommentView_iPad.h"

#import "ReaderContentPage.h"

@class QuoteCell;

@class ReaderViewController;
@class ReaderMainToolbar;
@class ReaderScrollView;

@class ReaderContentPage;

@protocol ReaderViewControllerDelegate <NSObject>

@required // Delegate protocols

- (void)dismissReaderViewController:(ReaderViewController *)viewController;



@end

@interface ReaderViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource, SectionHeaderViewDelegate,
													ReaderMainToolbarDelegate, ReaderMainPagebarDelegate, ReaderContentViewDelegate,SectionHeaderViewDelegate>
{
@private // Instance variables

	ReaderDocument *document;

	ReaderMainToolbar *mainToolbar;

	ReaderMainPagebar *mainPagebar;

	ReaderScrollView *theScrollView;

	NSMutableDictionary *contentViews;

	UIPrintInteractionController *printInteraction;

	NSInteger currentPage;

	NSDate *lastHideTime;

	BOOL isVisible;
    
    BOOL isLandscape;
    
    CGRect prevContSize;
    
    NSDictionary *magDIC, *articleDIC,
                 *artRefDIC;
    
    //Text View
    
    UIView *_textView, *_tocView;
    
    NSMutableArray *arrCategory,*arrCategoryID, *arrArticles, 
                    *arrArticlePage, *arrArticleWebURl;
    
    UITableView *table;//TOC TABLE VIEW
    
    //NSMutableArray* sectionInfoArray;    
    //TOC View
    
    CustomBezier *tocView;
    
    
    NSMutableArray *playsArray;
    
    NSString  *artWebURL, 
              *articleTitle,
              *magID,
              *articleID,
              *magazineName;
    
    UIWebView *textHTML;
    
    NSUInteger textFontSize;
    
    UIView *commentsListView, *postMsgView;
    
    
    
   
}

@property (nonatomic, assign, readwrite) id <ReaderViewControllerDelegate> delegate;

- (id)initWithReaderDocument:(ReaderDocument *)object;

- (void) readMagazinIndex;

-(NSString *)readFile:(NSString *)fileName;

-(NSString *) returnDocumentDirectory:(NSString *)subDir;

@property (nonatomic, retain) NSMutableArray* sectionInfoArray;

@property (nonatomic, retain) NSArray* plays;

@property (nonatomic, assign) IBOutlet QuoteCell *quoteCell;

@property (nonatomic, assign) NSInteger openSectionIndex;

@property (nonatomic, assign) NSString *magazineName;

- (void) buildTOCPage ;

- (void) hidePOPS;



@end
