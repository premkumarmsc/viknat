//
//  DBClass.m
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 15/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

#import "DBClass.h"
#import "Library_ipad.h"

@implementation DBClass
@synthesize magazinePrimaryID;


-(id) init {
    self = [super init];
    [self openDatabase];
    return self;
}

-(BOOL) openDatabase {
    
    if (sqlite3_open([ [self getDBPath] UTF8String], &database) == SQLITE_OK) {
        NSLog(@"Opened Succesfuly Chnaged to check whether coming here not ");
        return TRUE;
    } else if ([self copyDatabaseIfNeeded]) {  NSLog(@"Error Opening");
        if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK) {
            return TRUE;
        }
    }
    return FALSE;
    
}
- (BOOL) copyDatabaseIfNeeded {
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath]; 
	
    NSLog(@"Database path is to be : %@",dbPath);
    
	if(!success) { 
        
        NSFileManager *fmngr = [[NSFileManager alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:DATABASE_NAME ofType:nil];
        NSError *error;
        if(![fmngr copyItemAtPath:filePath toPath:[NSString stringWithFormat:KAPPDBPATH, NSHomeDirectory()] error:&error]) {
            // handle the error
            NSLog(@"Error creating the database: %@", [error description]);
            
        }
        [fmngr release];
        
    }	
    return FALSE;
}


- (NSString *) getDBPath {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory , NSUserDomainMask, YES);
	NSString *documentsDir = [paths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:DATABASE_NAME];
}

-(NSMutableArray *) selectMagazineField:(NSArray *)fields tblname:(NSString *)tblname when:(NSString *)condition order:(NSString *)order recordsCount:(NSInteger)limit startWith:(NSInteger)offset {

    sqlite3_stmt *selectstmt;    

    NSString *query = @"";    
    
    NSString *commaSepFields = @"";

    BOOL isFirstTime = TRUE;
    
    for (NSString *field in fields) {
        
       // NSLog(@"%@", field);
        
        if (isFirstTime) {
            
            commaSepFields = [commaSepFields stringByAppendingFormat:@" %@", field];
            
            isFirstTime = FALSE;
            
        } else {
            
            commaSepFields = [commaSepFields stringByAppendingFormat:@", %@", field];
            
        }
        
    }
    
    NSLog(@"Select Fields : %@", commaSepFields);

    if ([condition isEqualToString:@""]) {
        
        query = [NSString stringWithFormat:@"select %@ from %@", commaSepFields, tblname];
        
    } else {
        
        query = [NSString stringWithFormat:@"select %@ from %@ where %@", commaSepFields, tblname, condition];        
        
    }

    if (![order isEqualToString:@""] || ![order isEqual:nil]) {
        
        query = [query stringByAppendingString:order];
        
    }

    if (limit) {
        
        query = [query stringByAppendingFormat:@" limit %d", limit];
        
        if (offset) {
            
            query = [query stringByAppendingFormat:@" offset %d", offset];
            
        }
        
    }

    NSLog(@"Select Query : %@ ", query);
    
    const char *sql = [query UTF8String];
    
    NSDictionary *selectDict;

    NSMutableArray *resultArray = [[NSMutableArray alloc] init];

    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
 
        while(sqlite3_step(selectstmt) == SQLITE_ROW) {
  
            NSMutableArray *innerArray = [[NSMutableArray alloc] init];
            
            for (int i = 0; i < [fields count]; i++) {

                if(sqlite3_column_text(selectstmt, i) !=NULL)
                    
                {
                    
                    [innerArray addObject:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectstmt, i)]];
                    
                } else {
                    
                    [innerArray addObject:@""];
                    
                }
                
            }
            
           // NSLog(@"Inner Array Length : %d", [innerArray count]);

            selectDict = [[NSDictionary alloc] initWithObjects:innerArray forKeys:fields];
            
            [innerArray release];
            [resultArray addObject:selectDict];
            
        }        
        
    } else {
        
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        
    }
    
    sqlite3_reset(selectstmt);

    return resultArray;
    
}

-(BOOL) updateMagazineWithFieldAndValues:(NSString *)fieldAndValues tblname:(NSString *)tblname when:(NSString *)condition {
    BOOL flag = FALSE;
    
    NSString *queryStr = [NSString stringWithFormat:@"update %@ set %@", tblname, fieldAndValues ];
    
    if (![condition isEqualToString:@""] && ![condition isEqual:nil]) {
        queryStr = [queryStr stringByAppendingFormat:@" where %@", condition];
        
       // NSLog(@"Prepare Query %@", queryStr);
        
        const char *query = [queryStr UTF8String];
        
        if(sqlite3_prepare_v2(database, query, -1, &addStmt, NULL) == SQLITE_OK) {
            
            if (sqlite3_step(addStmt) == SQLITE_DONE) {
                flag = TRUE;
            } else {
                NSLog(@"DB ERR :: Cant update data");
            }
        } else {
            NSLog(@"DB ERR :: Cant create prepare statement");
           // NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        
        sqlite3_reset(addStmt);
    }
    
    return FALSE;
}

-(NSInteger) getLatestBook {
    NSInteger magCount = 0;
    
    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    
    query = [NSString stringWithFormat:@"select book_id from %@ order by download_date desc ", BOOK_LIB_TABLE];        
    const char *sql = [query UTF8String];
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        if(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            magCount = sqlite3_column_int(selectstmt, 0);
        } else {
            NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        }
        
    } else {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }
    
    
    sqlite3_reset(selectstmt);
    
    return magCount;
}
-(BOOL) insertMagazineFields:(NSString *)fields values:(NSString *)values tableName:(NSString *)table{
    BOOL flag = FALSE;
    
    NSString *queryStr = [NSString stringWithFormat:@"insert into %@ (%@) values (%@)", table, fields, values];
   // NSLog(@"Insert for issues Query %@", queryStr);
    
    const char *query = [queryStr UTF8String];
    
    if(sqlite3_prepare_v2(database, query, -1, &addStmt, NULL) == SQLITE_OK) {
        if (sqlite3_step(addStmt) == SQLITE_DONE) {
            [self setMagazinePrimaryID:sqlite3_last_insert_rowid(database)];
            flag = TRUE;
        } else {
            NSLog(@"DB ERR :: Cant insert data");
        }
    } else {
        NSLog(@"DB ERR :: Cant create prepare statement");
        NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        
        NSLog(@"Insert for issues Query %@", queryStr);
    }
    
	sqlite3_reset(addStmt);
    
    return flag;    
}
-(BOOL) deleteAllMagazines:(NSString *)table{
    BOOL flag = FALSE;
    
    NSString *queryStr = [NSString stringWithFormat:@"delete from %@", table];
    
    NSLog(@"Delete Statement %@",queryStr);
    
    const char *sql = [queryStr UTF8String];
    if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK) {
        if (sqlite3_step(deleteStmt) == SQLITE_DONE) {
            flag = TRUE;
        } else {
            NSLog(@"DB ERR :: Cant delete. '%s'", sqlite3_errmsg(database));
        }
    } else {
        NSLog(@"DB ERR :: cant create delete statement. '%s'", sqlite3_errmsg(database));
    }
    
	sqlite3_reset(deleteStmt);
    
    return flag;
}
-(int) deleteContentId:(NSString *) contentID
{
   // NSLog(@"enetring deleteContentId is ");

    int i = 0;
    
    NSString * documentsDir = [self getDBPath];
    
    NSString * openconnection = [self openDBconnection:documentsDir];
    
    if (openconnection == @"TRUE")
    {
        sqlite3_stmt *deletestmt;    
        
        NSString *delquery = [NSString stringWithFormat:@"%@%@%@%@%@%@", @"delete from ", BOOK_LIB_TABLE, @" where ", CONTENT_ID, @"=", contentID];
        
        NSLog(@"delquery is %@", delquery);
        //@"delete from Coffee where coffeeID = ?";  
        
        if(sqlite3_prepare_v2(database, [delquery UTF8String], -1, &deletestmt, NULL) == SQLITE_OK) {
            
           // NSLog(@"inside delete prepare statement");
            
            if (SQLITE_DONE == sqlite3_step(deletestmt))
            {
                 i= 1;
            
            }
        }
        
        [self closeDBconnection];
    
    }

    
    return i;
}

-(NSString *) openDBconnection:(NSString *) documentsDir {
    NSString * result;
    
    if (sqlite3_open([documentsDir UTF8String], &database) == SQLITE_OK)
    {
        result= @"TRUE";
    } 
    else
    {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        result=  [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)];
    }
    return result;

}

-(void) closeDBconnection {

    sqlite3_close(database);
}

@end
