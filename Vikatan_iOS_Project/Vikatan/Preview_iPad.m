//
//  Preview_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 28/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Preview_iPad.h"


@implementation Preview_iPad
@synthesize prevwDnldURl, magID, issueID;
@synthesize magName, issueDate,tempImg, wrapperUrl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [pagingView release];
    [networkQueue release], networkQueue = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use. 
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    int xWidth = 980;
    if(![self isLandscape])
    {
        if(PSIsIpad())
            xWidth = 748;
        else
            xWidth = 300;
    }

    UIView *titleView = [[UIView alloc] initWithFrame:self.navigationController.navigationBar.frame];
    self.navigationItem.titleView = titleView;

    //Add Header Image Control
    UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(293, 5, header.size.width, header.size.height)];
    headerImageView.image = header;
    [titleView addSubview:headerImageView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(290+header.size.width, 10, 75, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @" இதழ்கள்";
    lblTitle.textAlignment= UITextAlignmentLeft;
    [titleView addSubview:lblTitle];

     [self createMagRootFolder];
    //[ self.view setBackgroundColor:[UIColor blackColor]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* filePath = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",documentsDirectory,@"/",KMAGPREVROOTPATH,@"/",magID,@"_",issueID,
                            @"_Preview/preview_",magID,@"_",issueID,@".pdf"];
    if([fileManager fileExistsAtPath:filePath])
    {
        [self loadPDF];
        
    } 
    else {
        
        if (!networkQueue) {
            networkQueue = [[ASINetworkQueue alloc] init];	
        }
        
        failed = NO;
        [networkQueue reset];
        [networkQueue setRequestDidStartSelector:@selector(imageDownloadSart:)];
        [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
        [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
        [networkQueue setShowAccurateProgress:TRUE];
        [networkQueue setDelegate:self];

       // NSLog(@"wrapperUrl is %@", wrapperUrl);
        
        NSURL *imageurl = [NSURL URLWithString:wrapperUrl];
        NSData *imagedata = [[NSData alloc]initWithContentsOfURL:imageurl];
        UIImage *image = [UIImage imageWithData: imagedata];
        
        UIImageView * wrapperImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 -262,75, 262, 369)];//150+75+37, 212+106+53
        wrapperImage.image = image;
        [self.view addSubview:wrapperImage];

        progressIndicator = [[[UIProgressView alloc] initWithFrame:CGRectMake(20 ,490 ,xWidth-20,20)] autorelease];
        progressIndicator.backgroundColor = [UIColor clearColor];
        [progressIndicator setHidden:TRUE];
        [self.view addSubview:progressIndicator];

        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(130, 525, 375, 20)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.text = @"Please wait while Preview Downloading.... ";
        lblTitle.textAlignment= UITextAlignmentLeft;
        
        [self.view addSubview:lblTitle];
        
        if(!IsIpad()) {
            progressIndicator.frame = CGRectMake(10, 230, xWidth, 20);
            lblTitle.frame = CGRectMake(10, 260, 300, 40);
            lblTitle.numberOfLines = 2;
            wrapperImage.frame = CGRectMake(80,10, 150, 212);
        }
        
        ASIHTTPRequest *request;
        request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:prevwDnldURl]];
        
        NSLog(@"Download url %@", prevwDnldURl);
        
        [request setDownloadDestinationPath:[[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"]
                                             stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@%@%@%@",KMAGPREVROOTPATH,@"/",magID,@"_",issueID,@"_Preview.zip" ]]];
        
        NSLog(@"URL PATH:%@",[NSString stringWithFormat:@"%@%@%@%@%@%@",KMAGPREVROOTPATH,@"/",magID,@"_",issueID,@"_Preview.zip" ]);
        NSLog(@"URL NAME:%@",prevwDnldURl);
        
        [request setDownloadProgressDelegate:progressIndicator];
        [request setUserInfo:[NSDictionary dictionaryWithObject:@"request1" forKey:@"name"]];
        [networkQueue addOperation:request];
        [networkQueue go];
        
        
        NSLog(@"REQUEST PREM:%@",request);
        
        NSLog(@"Downloaded path %@", [request downloadDestinationPath]);
        
        imageurl=nil, imagedata=nil, image=nil, wrapperImage=nil;
    }
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    // Do any additional setup after loading the view from its nib.
}

- (void)imageDownloadSart:(ASIHTTPRequest *)request
{
    [request.downloadProgressDelegate setHidden:FALSE];
}

- (void)imageFetchComplete:(ASIHTTPRequest *)request
{
   
    NSString* destiny =  [request downloadDestinationPath];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    NSString *destinyPath = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",[paths objectAtIndex:0],
                             @"/",KMAGPREVROOTPATH,@"/",magID ,
                             @"_",issueID ,@"_Preview"];
    
   // NSLog(@"Destiny path is found to be %@", destiny);
    
    NSString* unZipFolder = destinyPath;
    ZipArchive *za = [[ZipArchive alloc] init];
    if ([za UnzipOpenFile: destiny]) {
        NSLog(@"inside ziping");
        BOOL ret = [za UnzipFileTo:unZipFolder overWrite: YES];
        if (NO == ret){} [za UnzipCloseFile];
    }
    [za release];
    [request.downloadProgressDelegate setHidden:TRUE];
    [request updateDownloadProgress];
    [self loadPDF];
}
- (void) createMagRootFolder {
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *newDir;
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, 
                                                   NSUserDomainMask, YES);

    docsDir = [dirPaths objectAtIndex:0];
    
    newDir = [docsDir stringByAppendingPathComponent:KMAGPREVROOTPATH];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:newDir])
    {
        
        if ([filemgr createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error: NULL] == NO)
        {
           // NSLog(@"Failed to create directoy");
            // Failed to create directory
        } else {
           // NSLog(@"Sucess");
        }
    }
    [filemgr release];
}

-(void)thumbClicked:(id)sender
{
    UIButton *btn  = (UIButton*)sender;
    
    [pagingView setContentOffset:CGPointMake((btn.tag  * 600), 0)];
}
- (void) loadPDF {
     
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@",documentsDirectory,@"/",KMAGPREVROOTPATH,@"/",magID,@"_",issueID,
                            @"_Preview/preview_",magID,@"_",issueID,@".pdf"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:pdfDocPath]) 
    {
        /* //commented for disabling pdf reader
        pdfOpen = TRUE;
        PSPDFDocument *document = [PSPDFDocument PDFDocumentWithUrl:[NSURL fileURLWithPath:pdfDocPath]];
        document.searchEnabled = FALSE;
        document.annotationsEnabled = FALSE;
        document.outlineEnabled = FALSE;
        document.twoStepRenderingEnabled = FALSE;
        document.title = [NSString stringWithFormat:@"%@%@%@", magName,@" - ",issueDate];
        
       // PSPDFViewController *pdfController = [[PSPDFViewController alloc] initWithDocument:document] ;
        
        MVPDFViewController *pdfController =[ [[MVPDFViewController alloc] initWithDocument:document] autorelease];
        pdfController.preview = TRUE;
        pdfController.magID = [NSString stringWithFormat:@"%@", magID];
        //pdfController.custTitle = @"Preview";
        if(PSIsIpad())
            pdfController.custTitle = @"இதழ்கள்";
        else
            pdfController.custTitle = @"<";
        UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:pdfController] autorelease];
        pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:pdfNavController animated:NO];
                //[self.navigationController pushViewController:pdfNavController.visibleViewController animated:NO];
         */
    }
    
}
- (void)imageFetchFailed:(ASIHTTPRequest *)request
{
	if (!failed) {
		if ([[request error] domain] != NetworkRequestErrorDomain || [[request error] code] != ASIRequestCancelledErrorType) {
			UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Download failed" message:@"Failed to download Magazine" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
			[alertView show];
		}
		failed = YES;
	}
}
- (void) viewDidAppear:(BOOL)animated{
    if(pdfOpen)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
	[super viewDidAppear:animated];
    pdfOpen = false;
	//[coverflow bringCoverAtIndexToFront:[covers count] animated:NO];
}
- (void)viewDidUnload
{
    networkQueue = nil;
    wrapperUrl=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

@end
