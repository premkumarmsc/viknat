//
//  MagTextView.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 25/09/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "MagTextView.h"


@implementation MagTextView

@synthesize articleURLPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    articleURLPath = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    int xWidth = self.view.bounds.size.width,
    yHeight = self.view.bounds.size.height, 
    widthSub = 100,heightSub = 250, headerXpos = 200,fontPos = 560;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        self.contentSizeForViewInPopover = CGSizeMake(self.view.bounds.size.width - 100 , self.view.bounds.size.height - 200);
        textFontSize = 120;
        UIView  *textView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,xWidth,yHeight)];
        [textView setBackgroundColor:[UIColor clearColor]];
        //[[textView layer] setBorderColor:[[UIColor blackColor] CGColor]];
        //[[textView layer] setBorderWidth:5];
        [textView setClipsToBounds: YES];    //[[postMsgView layer] setCornerRadius:15];
        [textView setClipsToBounds: YES];
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth - widthSub,50)];
        [headerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];     
        [textView addSubview:headerView];   
        
        UIButton   *textTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        [textTitle setTitle:@"எழுத்து வடிவம்" forState:UIControlStateNormal];
        textTitle.frame =  CGRectMake(headerXpos, 10, 300, 25);
        textTitle.enabled = FALSE;
        [headerView addSubview:textTitle];
        
        UIView *fontView = [[UIView alloc] initWithFrame:CGRectMake(fontPos,0,100,48)];
        
        UIButton *increase = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [increase setImage:[UIImage imageNamed:@"A+_white_32.png"] forState:UIControlStateNormal];
        
        increase.frame =  CGRectMake(0, 0, 48, 48);
        
        increase.tag = 2;
        
        [increase addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        [fontView addSubview:increase];
        
        UIButton *decrease = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [decrease setImage:[UIImage imageNamed:@"A-_white_32.png"] forState:UIControlStateNormal];
        
        decrease.frame =  CGRectMake(50, 0, 48, 48);
        
        decrease.tag = 1;
        
        [decrease addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        [fontView addSubview:decrease];
        
        [headerView addSubview:fontView];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, xWidth,yHeight)] autorelease];
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;    
        
        textHTML = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,xWidth - widthSub, yHeight - heightSub)] ;
        textHTML.scalesPageToFit = NO;
        
        [postView addSubview:textHTML];
        [textView addSubview:postView];
        [self.view addSubview:textView];
        //NSString *urlFilePath = [self returnDocumentDirectory:document.folderName];
        //NSString *urlFilePath = [NSString stringWithFormat:@"%@%@%@%@",urlFilePath,@"/",val,@".html"];
        
        [textHTML loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:articleURLPath]]];
    } else {
       // xWidth = 320;
        //yHeight = 440;
        widthSub = 0;
        heightSub = 0;
        headerXpos = 10;
        fontPos = 220;
        
        textFontSize = 120;
        UIView  *textView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,xWidth,yHeight)];
        [textView setBackgroundColor:[UIColor clearColor]];
        textView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
       
        [textView setClipsToBounds: YES];    //[[postMsgView layer] setCornerRadius:15];
        [textView setClipsToBounds: YES];
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,xWidth - widthSub,50)];
        [headerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];     
        [textView addSubview:headerView];   
        headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        UIButton   *textTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        [textTitle setTitle:@"எழுத்து வடிவம்" forState:UIControlStateNormal];
        textTitle.frame =  CGRectMake(headerXpos, 10, xWidth, 25);
        textTitle.enabled = FALSE;
        [headerView addSubview:textTitle];
        
        UIView *fontView = [[UIView alloc] initWithFrame:CGRectMake(fontPos,0,100,48)];
        
        UIButton *increase = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [increase setImage:[UIImage imageNamed:@"A+_white_32.png"] forState:UIControlStateNormal];
        
        increase.frame =  CGRectMake(0, 0, 48, 48);
        
        increase.tag = 2;
        
        [increase addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        [fontView addSubview:increase];
        
        UIButton *decrease = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [decrease setImage:[UIImage imageNamed:@"A-_white_32.png"] forState:UIControlStateNormal];
        
        decrease.frame =  CGRectMake(50, 0, 48, 48);
        
        decrease.tag = 1;
        
        [decrease addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
        
        [fontView addSubview:decrease];
        
        [headerView addSubview:fontView];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, xWidth,yHeight)] autorelease];
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;    
        postView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        textHTML = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,xWidth - widthSub, yHeight - heightSub)] ;
        textHTML.scalesPageToFit = NO;
        textHTML.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        
        [postView addSubview:textHTML];
        [textView addSubview:postView];
        [self.view addSubview:textView];
        //NSString *urlFilePath = [self returnDocumentDirectory:document.folderName];
        //NSString *urlFilePath = [NSString stringWithFormat:@"%@%@%@%@",urlFilePath,@"/",val,@".html"];
        
        [textHTML loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:articleURLPath]]];
    }
    
    
     
    [super viewDidLoad];
}

- (IBAction)changeTextFontSize:(id)sender
{
    UIButton *btn = (UIButton*)sender; 
    btn.enabled = FALSE;
    switch ([sender tag]) {
        case 1: // A-
            textFontSize = (textFontSize > 80) ? textFontSize -20 : textFontSize;
            break;
        case 2: // A+
            textFontSize = (textFontSize < 260) ? textFontSize + 20 : textFontSize;
            //textFontSize = textFontSize + 5;
            break;
    }

    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", 
                          textFontSize];
    [textHTML stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    btn.enabled = TRUE;
}

- (void)viewDidUnload
{
    [textHTML release];
    
    textHTML = nil;
    articleURLPath = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return  (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 /*/
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
   // return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}

@end
