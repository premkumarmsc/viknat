//
//  CustomBezier.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 26/08/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomBezier : UIView {
    
}
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) UIFont *font;
@end
