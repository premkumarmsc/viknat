//
//  Book_ipad.h
//  Viduthalai
//
//  Created by Saravanan Nagarajan on 09/05/12.
//  Copyright (c) 2012 MobileVeda. All rights reserved.
//

/*
 Epub Orientation for iPad.
 IPhone Version Orientation bug.
 Facebook Share - Sharekit (Both sharing and Refer A friend)
 
 
 Add cancel button for old download manager and display pause, resume and cancel button based on Download version. (Done)
 Dismissing Login view after successful social network login
 Post All values in request instead of sending in GET method.
 Payment Test
 Login with Social Network failure condition.

 */

#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "JSON.h"
#import <QuartzCore/QuartzCore.h>
#import "HJObjManager.h"
#import "HJManagedImageV.h"
#import "HJMOFileCache.h"
#import "CustomUIButton.h"
#import "Constant.h"
#import "DBClass.h"
#import "bookStoreCommon.h"

@class BookCell;
@protocol Book_ipad_protocol

-(void) CoverSelected:(BookCell *) custbutton;

@end

@interface Book_ipad :  UIViewController <AQGridViewDelegate, AQGridViewDataSource, Book_ipad_protocol, UIScrollViewDelegate>
{
    AQGridView * _gridView;
    sqlite3 * database;
    
    bookStoreCommon *book_cat;
    
    UIActivityIndicatorView *spinner;
    
    NSMutableArray * loadCatBooks;
    
    NSMutableDictionary *categories, *getUrlAndParams;
    
    HJObjManager* imageManager;
    NSString * cachePath;
    NSFileManager *fileManager;
    NSMutableDictionary * catdetails;
    
    UIView *titleView;
    UIButton *refreshBtn;
    UIImageView *logo;
    UILabel *title;
    UIScrollView *scroll;
    
    UIButton *refreshImageView;
}

@property (nonatomic, retain) AQGridView * gridView;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) NSMutableArray * loadCatBooks;
@property (nonatomic, retain) NSMutableDictionary * categories, *getUrlAndParams;

@property (nonatomic, retain) HJObjManager* imageManager;
@property (nonatomic, retain) NSString * cachePath;
@property (nonatomic, retain)  NSDictionary * catdetails;
@property (nonatomic, retain)  UIButton *refreshBtn;
@property (nonatomic, retain) NSString * categName;
- (BOOL)isLandscape;

- (void) clearTables;
- (void) buildTOCPage;
- (void)getBooksList;
-(void) readBookIndex:(NSString*)data;
-(void) invokeGridDelegate;
-(void) startLoading;
-(void) stopLoading;
- (void) loadMainCategory;
-(NSString *) returnButtonTitle:(int) id;
-(NSString *) returnButtonColor:(int) id;
-(UIColor *) returnButtonTitleColor:(int) id;

-(int) returnbtnStatus: (NSString *) downloadable contentid:(NSString *) contentid;
-(int) checkfilepath: (NSString *) contentid;
-(int) checkDownloadDB: (NSString *) contentid;
- (void)initializeGridView;
-(void) _showAlert:(NSString *) title;

@end