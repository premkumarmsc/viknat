//
//  CommentView_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 02/06/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "CommentView_iPad.h"
#import <QuartzCore/QuartzCore.h>
#import "Login.h"

@implementation CommentView_iPad
@synthesize articleID, cmntCount, magID;;
@synthesize postView, postMsgView, spinner, resStatus,pageIndexView;
@synthesize magCmnt;

static CGFloat const  kAccountDetailFontSize = 12.0;
static int const POSTEDBY_TAG = 13;
static int const DATELABEL_TAG = 1;
static int const MESSAGELABEL_TAG = 2;
static int const IMAGEVIEW_TAG_1 = 3;
static int const IMAGEVIEW_TAG_2 = 4;
static int const IMAGEVIEW_TAG_3 = 5;
static int const IMAGEVIEW_TAG_4 = 6;
static int const IMAGEVIEW_TAG_5 = 7;
static int const IMAGEVIEW_TAG_6 = 8;
static int const IMAGEVIEW_TAG_7 = 9;
static int const IMAGEVIEW_TAG_8 = 10;
static int const IMAGEVIEW_TAG_9 = 11;

int bubbleFragment_width, bubbleFragment_height;
int bubble_width;
int bubble_x, bubble_y;
int labelsize;
int fontsize;

- (void)viewDidLoad {
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) 
    self.contentSizeForViewInPopover = CGSizeMake(self.view.bounds.size.width - 100 , self.view.bounds.size.height - 200);
    [self.view setBackgroundColor: [UIColor whiteColor]/*[UIColor colorWithPatternImage: [UIImage imageNamed: @"bg_nw_vikatan_ipad.png"]]*/];
    
    UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,45)];
    //[cmntHeader setBackgroundColor:[UIColor darkGrayColor]];
    [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
    //[[cmntHeader layer] setCornerRadius:15];
    [cmntHeader setClipsToBounds: YES];   
    UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
    [commentTitle setTitle:@"கருத்துகள்" forState:UIControlStateNormal];
    //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
    commentTitle.frame =  CGRectMake(7, 10, 100, 25);
    commentTitle.enabled = FALSE;
    [cmntHeader addSubview:commentTitle];
    
    [self.view addSubview:cmntHeader];
    
    pakanGal = [[UILabel alloc] init] ;
    pakanGal.font = [UIFont systemFontOfSize:14];		
    pakanGal.textAlignment = UITextAlignmentLeft;
    pakanGal.textColor = [UIColor blackColor];
    pakanGal.numberOfLines = 0; //---display multiple lines---
    pakanGal.backgroundColor = [UIColor clearColor];
    pakanGal.lineBreakMode = UILineBreakModeWordWrap;		
    pakanGal.text = @"பக்கங்கள்:";

    xWidth = 650;
    int indexPos = 200;
    xCoord = 30;
    if (!resStatus) {
       
        labelsize = 18;
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.view.frame = CGRectMake(0, 0 ,700,660);
        pageIndexView = [[UIScrollView alloc] initWithFrame:CGRectMake(190,9,600,xCoord-2)];
        } else {
             NSLog(@"%@",@"iphone cooments");
           // self.view.frame = CGRectMake(0, 0 ,300,660);xWidth = 300;
            self.view.frame = CGRectMake(0, 0 ,self.view.bounds.size.width,660);xWidth = self.view.bounds.size.width-10;
            //pageIndexView = [[UIScrollView alloc] initWithFrame:CGRectMake(190,9,320,xCoord-2)];
            pageIndexView = [[UIScrollView alloc] initWithFrame:CGRectMake(190,9,self.view.bounds.size.width,xCoord-2)];
            pageIndexView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
           // commentTitle.frame =  CGRectMake(7, 10, self.view.bounds.size.width, 25);
           // commentTitle.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
            self.view.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
            cmntHeader.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
            
        }
        pakanGal.frame = CGRectMake(130,7,100,xCoord-2);
        fontsize = 13;

    } else {
        labelsize = 12;
        //self.view.frame = CGRectMake(0, 0 ,self.view.bounds.size.width,660);xWidth = self.view.bounds.size.width;
        self.view.frame = CGRectMake(0, 0 ,320,350);xWidth= 290;
        indexPos = 100;
        xCoord = 30;
        pageIndexView = [[UIScrollView alloc] initWithFrame:CGRectMake(190,5,225,xCoord-2)];
        pakanGal.frame = CGRectMake(120,5,100,xCoord-2);
        fontsize = 10;

    }
   //<#CGFloat width#>, <#CGFloat height#>)
	//---location to display the bubble fragment--- 
	bubble_x = 0;
	bubble_y = 20;
	
	//---size of the bubble fragment---
	bubbleFragment_width = 224;//56;
	bubbleFragment_height = 80;//32
	
	//---width of the bubble---
    //	bubble_width = 300;
	
	//---contains the messages---
	
     offset = 0;
    self.tableView.allowsSelection = NO;
   // self.editing = TRUE;
    logMod = [[Login alloc] init];
    [self CommentsGetRequest];
    pageIndexView.backgroundColor = [UIColor clearColor];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableFooterView = view;
    [super viewDidLoad];
}

- (void) Get_Comments {
    
    [self LoadingIcon];
    responseData = [[NSMutableData data] retain];
	//Send http request
    
    NSMutableString *urlMore;
    
    if(magCmnt == TRUE) {
        urlMore = [[NSMutableString alloc] initWithString:KMAGCMNTURL];
        [urlMore appendString:articleID];
        [urlMore appendString:@"&"];
        [urlMore appendString:@"mid="];
        [urlMore appendString:magID];
        [urlMore appendString:@"&"];
        [urlMore appendString:@"offset="];[urlMore appendString:[NSString stringWithFormat:@"%d", offset]];
        [urlMore appendString:@"&cmtlimit="];[urlMore appendString:@"10"];
        
    } else {
        urlMore = [[NSMutableString alloc] initWithString:KCMNTURL];
        [urlMore appendString:articleID];
        [urlMore appendString:@"&"];
        [urlMore appendString:@"offset="];[urlMore appendString:[NSString stringWithFormat:@"%d", offset]];
        [urlMore appendString:@"&cmtlimit="];[urlMore appendString:@"10"];
    }
   
   	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[logMod getDeviceDetails:urlMore]]];
    
    NSLog(@"URL MORE:%@",request);
    
    
	[[NSURLConnection alloc] initWithRequest:request delegate:self] ;

    NSLog(urlMore);
}
- (void) Next_Page_Clicked:(id) sender {
    
    for(UIView *subview in [pageIndexView subviews]) 
    {
        if([subview isKindOfClass:[UIButton class]]) 
        {
            UIButton *bt = (UIButton*)subview;
            [bt setBackgroundColor:[UIColor clearColor]];
            [bt setSelected:FALSE];
        }
    }
    
    UIButton *btn = (UIButton*) sender;
    offset =  (btn.tag -1 ) * KCMNTSOFFSET;
    [self Get_Comments];
   // [self BtnUnhighlight];
    [btn setBackgroundColor:[UIColor lightGrayColor]];
    [btn setSelected:TRUE];
    [pageIndexView setBackgroundColor:[UIColor clearColor]];
    [[pageIndexView viewWithTag:buttonTag] setBackgroundColor:[UIColor clearColor]];
   
    buttonTag = btn.tag;
    NSLog(@"Button Tag %d", buttonTag);
    //pageIndexView.contentOffset = CGPointMake(buttonTag, <#CGFloat y#>)
}

- (void) Prev_Page_Clicked:(id) sender {
    
    offset = offset - KCMNTSOFFSET;
    [self Get_Comments];
    
}

- (void) LoadingIcon {
    
    //Initialize activity window for loading process
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//Set position
	[spinner setCenter:CGPointMake(KIPHNWIDTH/2.0, KIPHNHEIGHT/2.0)];
	//spinner.backgroundColor = [UIColor lightGrayColor];
	spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
	//Add the loading spinner to view
	[self.view addSubview:spinner];
	//Start animating
	[spinner startAnimating];
}

- (void ) close_clicked:(id)sender {
    
    [postMsgView removeFromSuperview];
    [postMsgView release];
}
- (void) viewDidAppear:(BOOL)animated {
    
     [self.navigationController.navigationBar addSubview:postView];
       
}
-(void) viewDidDisappear:(BOOL)animated {
    
    [postView removeFromSuperview];

}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}


// -------------------------------------------------------------------------------
//	connection:didFailWithError:error
// -------------------------------------------------------------------------------
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self _showAlert:KINTERNETNOTAVBL];
    [spinner stopAnimating];
    [spinner removeFromSuperview];
}
- (void) _showAlert:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Service Unavailable" message:title delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	if (connection!=nil) { [connection release]; }
   
    NSMutableString *responseString = [[NSMutableString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
	SBJSON *jsonParser = [[SBJSON new] autorelease];
	id response = [jsonParser objectWithString:responseString];
    NSDictionary *newsfeed = (NSDictionary *)response;
   
	// get the array of "stream" from the feed and cast to NSArray
	NSArray *comments = (NSArray *)[newsfeed valueForKey:@"cmtlist"];
    
    cmntCount = [NSString stringWithFormat:@"%@",[newsfeed valueForKey:@"cmtcount"]];
    
     NSLog(@"oout0: %@",cmntCount);
    //paging option
    if([ cmntCount intValue] >KCMNTSOFFSET) {
        for(int i  = 0 ; i<ceil([cmntCount floatValue]/KCMNTSOFFSET) ;i++) {
            UIButton *pageBtn = [UIButton     buttonWithType:UIButtonTypeCustom];
            pageBtn.tag = i + 1;
            pageBtn.frame =  CGRectMake(i * xCoord +2, 1, xCoord - 5, xCoord - 5);
            pageBtn.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:fontsize];
            [pageBtn setBackgroundColor:[UIColor clearColor]];
            
            [pageBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            pageBtn.layer.cornerRadius = 5;
            pageBtn.clipsToBounds = YES;
            [pageBtn setTitle:[NSString stringWithFormat:@"%d", i + 1 ]  forState:UIControlStateNormal];
            [pageBtn addTarget:self action:@selector(Next_Page_Clicked:) forControlEvents:UIControlEventTouchUpInside];
            [pageIndexView addSubview:pageBtn];
        }
        pageIndexView.contentSize = CGSizeMake([cmntCount intValue] * xCoord+50, xCoord-5);
        [self.view addSubview:pakanGal]; 
        [self.view addSubview:pageIndexView]; 
        
    }
     
	//NSDictionary *comment;
	// loop over all the stream objects and print their titles
    listOfMessages = [[NSMutableArray alloc] init];
	//---contains the date for each message---
	dateOfMessages = [[NSMutableArray alloc] init];	
    
    postedBy = [[NSMutableArray alloc] init];
    [listOfMessages addObject:@""];
    [dateOfMessages addObject:@""];
    [postedBy addObject:@""];
   // NSString *trimmedString ;
	for (int ndx = 0; ndx < comments.count ; ndx++) {
        NSDictionary *comment = (NSDictionary *)[comments objectAtIndex:ndx];
		
        @try {
             
            [listOfMessages addObject:urlStringDecode([comment valueForKey:@"cmt"]) ];
            [dateOfMessages addObject:[comment valueForKey:@"cmttime"]];
            [postedBy addObject:[comment valueForKey:@"uname"]];
        }
        @catch (NSException *exception) {
            NSLog(@"Error");
        }
        @finally {
           // NSLog(@"Final");
        }
    }// NSLog(@"oout");

    [listOfMessages addObject:@""];
    [dateOfMessages addObject:@""];NSLog(@"oout1");
    [postedBy addObject:@""];

    [self.tableView reloadData];NSLog(@"oout3");
    [spinner stopAnimating];
    [spinner removeFromSuperview];
}

-(void) CommentsGetRequest {
    [self Get_Comments];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listOfMessages count];
}

//---calculate the height for the message---
-(CGFloat) labelHeight:(NSMutableString *) text {
    CGSize maximumLabelSize;
     if (!resStatus) {
        maximumLabelSize = CGSizeMake(600,9999);
     } else {
	 maximumLabelSize = CGSizeMake(280,9999);
	 
     }
    CGSize expectedLabelSize;
  
    //text = @"ஆட்டோ கட்டணமும்";
    @try { //if([text length] < 1000) 
        NSLog(text);
         expectedLabelSize = [text sizeWithFont:[UIFont systemFontOfSize: labelsize] 
                                    constrainedToSize:maximumLabelSize 
                                        lineBreakMode:UILineBreakModeWordWrap ];
    }
    @catch (NSException *exception) {
        return 200;
    }
    @finally {
        
    }
    
	return expectedLabelSize.height;
}

//---returns the height for the table view row---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {  
  
	int labelHeight = [self labelHeight:[listOfMessages objectAtIndex:indexPath.row]];
    int dateHeight =  [self labelHeight:[dateOfMessages objectAtIndex:indexPath.row]];
    /*
	labelHeight -= bubbleFragment_height;
	if (labelHeight<0) labelHeight = 0;
    
	return (bubble_y + bubbleFragment_height * 2 + labelHeight) + 5;
     */
     if (!resStatus) {
         if(indexPath.row == 0 )
             labelHeight = labelHeight + 32;
             return labelHeight + dateHeight + 10;
     } else {
         if(indexPath.row == 0 )
             labelHeight = labelHeight + 3;
         return labelHeight + dateHeight + 35;
     }
}  

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    xWidth = self.view.bounds.size.width - 10;
    
    static NSString *CellIdentifier = @"Cell";
    
	UILabel* dateLabel = nil;
    UILabel* messageLabel = nil;
	UILabel* postedByLbl = nil;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        
            //---date---
		dateLabel = [[[UILabel alloc] init] autorelease];
        dateLabel.tag = DATELABEL_TAG;
		[cell.contentView addSubview: dateLabel];
        
        postedByLbl = [[[UILabel alloc] init] autorelease];
        postedByLbl.tag = POSTEDBY_TAG;
        [cell.contentView addSubview: postedByLbl];
        
        messageLabel = [[[UILabel alloc] init] autorelease];
        messageLabel.tag = MESSAGELABEL_TAG;		
        [cell.contentView addSubview: messageLabel];
        
       	} else {		
		//---reuse the old views---		
        dateLabel = (UILabel*)[cell.contentView viewWithTag: DATELABEL_TAG];
        messageLabel = (UILabel*)[cell.contentView viewWithTag: MESSAGELABEL_TAG];
        postedByLbl = (UILabel*)[cell.contentView viewWithTag: POSTEDBY_TAG];
                
	}
    
    UIImage *bgImage = [UIImage imageNamed:@"ipad_comment_box_large.png"];
	UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.bounds.size.width, cell.bounds.size.height)];
	bgView.image = bgImage;
	int labelHeight = [self labelHeight:[listOfMessages objectAtIndex:indexPath.row]];
	dateLabel.text = [dateOfMessages objectAtIndex:indexPath.row];;
	//---you can customize the look and feel for the date for each message here---
	dateLabel.frame = CGRectMake(bubble_x + 10, 5.0, 200, 15.0);
    dateLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14]; 
	//dateLabel.font = [UIFont boldSystemFontOfSize: kAccountDetailFontSize];
	dateLabel.textAlignment = UITextAlignmentLeft;
	dateLabel.textColor = UIColorFromRGB(0x3399FF);
	dateLabel.backgroundColor = [UIColor clearColor];
    // 
	messageLabel.frame = CGRectMake(bubble_x + 10, bubble_y + 5, xWidth,labelHeight );
	messageLabel.font = [UIFont systemFontOfSize:labelsize];		
    messageLabel.textAlignment = UITextAlignmentLeft;
    messageLabel.textColor = [UIColor blackColor];
	messageLabel.numberOfLines = 0; //---display multiple lines---
	messageLabel.backgroundColor = [UIColor clearColor];
	messageLabel.lineBreakMode = UILineBreakModeWordWrap;		
	messageLabel.text = [listOfMessages objectAtIndex:indexPath.row];	
    messageLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin;;
    [messageLabel setAutoresizesSubviews:YES];
    [messageLabel setAutoresizingMask:
     UIViewAutoresizingFlexibleWidth];

	//---you can customize the look and feel for the date for each message here---
	postedByLbl.frame = CGRectMake(bubble_x + xWidth- 200, 5.0, 200, 15.0) ;
    postedByLbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14]; 
	//postedByLbl.font = [UIFont boldSystemFontOfSize: kAccountDetailFontSize];
	postedByLbl.textAlignment = UITextAlignmentRight;
	postedByLbl.textColor = UIColorFromRGB(0x3399FF);//UIColorFromRGB(0x3399FF);
	postedByLbl.backgroundColor = [UIColor clearColor];
     postedByLbl.text =  [postedBy objectAtIndex:indexPath.row];
    NSLog(@"cell");
    return cell;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return PSIsIpad() ? YES : toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
}
/*
- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;	
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	NSLog(@"heelo");
}
*/
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    logMod = nil;
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[listOfMessages release];
	[dateOfMessages release];
    [pageIndexView release];
    [pakanGal release], pakanGal = nil;
    [super dealloc];
}

@end
