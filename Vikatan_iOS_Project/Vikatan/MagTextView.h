//
//  MagTextView.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 25/09/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface MagTextView : UIViewController {
    
    NSString *articleURLPath;
    
    UIWebView *textHTML;
    
    NSUInteger textFontSize;
}
@property (nonatomic, retain) NSString *articleURLPath;

@end
