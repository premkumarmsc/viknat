//
//  Information_iPad.m
//  Vikatan
//
//  Created by Saravanan Nagarajan on 20/07/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Information_iPad.h"
#import "Login.h"
#import "Constant.h"
#import "LoginView.h"

@implementation Information_iPad
@synthesize popoverController;


- (id)initWithStyle:(UITableViewStyle)style
{
    style = UITableViewStyleGrouped;
    if (self = [super initWithStyle:style]) {
    }
    
    return self;
}

- (void)dealloc
{
    [popoverController release], popoverController = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(callBack) name:@"inforeferesh" object: nil];
    
    likeApp = FALSE;
    
    [super viewDidLoad];
    //[self.tableView setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
}

- (void)viewDidUnload
{
    popoverController = nil;
    if (listOfItems !=NULL) {
        
        listOfItems = nil;
    }
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{

    NSLog(@"view did appear after social network");

    [super viewDidAppear:animated];
    [self.view setBackgroundColor:[UIColor blackColor]];    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
   	UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
	UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:
                                    CGRectMake(0, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-20, 5, 100, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"விகடன்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];

    self.navigationItem.titleView = titleView;    

    Login *loginMod = [[Login alloc]init];
    listOfItems = [[NSMutableArray alloc] init];

    NSArray *informationArr = [NSArray arrayWithObjects:@"FAQ", @"Terms & Conditions", @"Contact Us",@"Developed by: Fublish Platform, www.mobileveda.com",@"Version: V1.4" ,@"Restore My Purchases", nil];
    NSDictionary *informationArrDict = [NSDictionary dictionaryWithObject:informationArr forKey:@"Information"];
    
    NSArray *urlInfo = [NSArray arrayWithObjects:KVIKTFAQIPAD, KVIKTERMSCONDTIONIPAD,KVIKTCONTACTUSIPAD,@"http://www.fublish.com/",@"V1.4",@"restore" ,nil];
    
    NSDictionary *urlInfoDict = [NSDictionary dictionaryWithObject:urlInfo forKey:@"URLINFO"];
    
    NSArray *loginArr = [NSArray arrayWithObjects:@"User ID: ",@"", @"User Name",
                         @"Logout", nil];
    NSDictionary *loginArrDict = [NSDictionary dictionaryWithObject:loginArr forKey:@"Login"];
    
    NSArray *socialNetworkingArray = [NSArray arrayWithObjects:@"Facebook ", nil];
    NSDictionary *socialNetworkingArrayDict = [NSDictionary dictionaryWithObject:socialNetworkingArray forKey:@"SocialNetwork"];

    [listOfItems addObject:loginArrDict];
    [listOfItems addObject:@""];
    [listOfItems addObject:informationArrDict];
    [listOfItems addObject:urlInfoDict];
    
    informationArr=nil, [informationArr release];
    informationArrDict=nil, [informationArrDict release];
    urlInfo=nil, [urlInfo release];
    urlInfoDict=nil, [urlInfoDict release];
    loginArr=nil, [loginArr release];
    loginArrDict=nil, [loginArrDict release];
    socialNetworkingArray=nil, [socialNetworkingArray release];
    socialNetworkingArrayDict=nil, [socialNetworkingArrayDict release];
    
    //Set the title
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    self.tableView.separatorColor = [UIColor whiteColor];
    [self.tableView reloadData];

    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(closePops) name:@"infopopviewclose" object: nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount;
    NSDictionary *dictionary = [listOfItems objectAtIndex:section];
    NSArray *array;
    switch (section) {
        case 0:
            rowCount = 2;
            
            break;
        case 1:
            rowCount = 1;
            break;
        case 2:
            
            array = [dictionary objectForKey:@"Information"];
            rowCount =  [array count];
            break;
        default:
            break;
    }
    return rowCount;
}

//---returns the height for the table view row---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(indexPath.row == 4 )
    
     return 50;

    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   // if (cell == nil) {
        //cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
  //  }
    // Set up the cell...
    //First get the dictionary object
    NSDictionary *dictionary = [listOfItems objectAtIndex:indexPath.section];
    NSArray *array;
    
    //User Details
    Login *loginMod = [[Login alloc]init];
    NSString *userName;
    
    NSInteger section = [indexPath section];
    switch (section) {
        case 2:
            
            array = [dictionary objectForKey:@"Information"];
            NSString *cellValue = [array objectAtIndex:indexPath.row];
            cell.textLabel.text = cellValue;
            cell.backgroundColor = [UIColor lightGrayColor];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            break;
        case 0:
        {
            dictionary = [listOfItems objectAtIndex:indexPath.section ];
            if(loginMod.isUserDetailsExists)
            {
                userName = @"Log out";
            } else {
                
                userName = @"Login";
            }
            array = [dictionary objectForKey:@"Login"];
            if(indexPath.row == 0)
            {
                NSString *cellValue = [array objectAtIndex:indexPath.row];
                
                if(loginMod.emailID == NULL)
                    loginMod.emailID = @"";
                
                cell.textLabel.text= [NSString stringWithFormat:@"%@%@", cellValue,loginMod.emailID];
                
                UIButton *lgn = [UIButton buttonWithType:UIButtonTypeCustom];
                lgn.frame = CGRectMake(cell.frame.origin.x + 600, cell.textLabel.frame.origin.y + 10, 110, 30);
                [lgn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal]; 
                [lgn setTitle:userName forState:UIControlStateNormal];
                [lgn addTarget:self action:@selector(show_login_clicked:) forControlEvents:UIControlEventTouchUpInside];
                [lgn.layer setMasksToBounds:YES];
                [lgn.layer setCornerRadius:5.0f];
                [lgn.layer setBorderWidth:1.0f];

                [cell addSubview:lgn];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
               
            } else {
                array = [dictionary objectForKey:@"Login"];
                cellValue = [array objectAtIndex:indexPath.row +1];
                if(loginMod.username == NULL)
                    loginMod.username = @"";
                cell.textLabel.text = [NSString stringWithFormat:@"%@%@%@", cellValue,@": ",loginMod.username]; ;
                cell.backgroundColor = [UIColor lightGrayColor];
            }
            cell.backgroundColor = [UIColor lightGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            
            break;

        case 1:
        {           
            if(indexPath.row == 0) {
                
                UIButton *connectFBBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
                connectFBBtn1.frame = CGRectMake(70, 12, 181, 27);
                [connectFBBtn1 setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
                [connectFBBtn1 setTitle:@"Feedback" forState:UIControlStateNormal];
                [connectFBBtn1 addTarget:self action:@selector(openFeedbackView:) forControlEvents:UIControlEventTouchUpInside];
                [connectFBBtn1.layer setMasksToBounds:YES];
                [connectFBBtn1.layer setCornerRadius:5.0f];
                [connectFBBtn1.layer setBorderWidth:1.0f];
                
                [cell addSubview:connectFBBtn1];
    
                UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                likeBtn.frame = CGRectMake(290, 12, 181, 27);
                [likeBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
                [likeBtn setTitle:@"Rate Vikatan App" forState:UIControlStateNormal];
                [likeBtn addTarget:self action:@selector(likeAppButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [likeBtn.layer setMasksToBounds:YES];
                [likeBtn.layer setCornerRadius:5.0f];
                [likeBtn.layer setBorderWidth:1.0f];

                [cell addSubview:likeBtn];

                UIButton *tellFriendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                tellFriendBtn.frame = CGRectMake(510, 12, 181, 27);
                [tellFriendBtn setBackgroundImage:[UIImage imageNamed:@"header-blue-plain.jpg"] forState:UIControlStateNormal];
                [tellFriendBtn setTitle:@"Refer A friend" forState:UIControlStateNormal];
                [tellFriendBtn addTarget:self action:@selector(tellFriendButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [tellFriendBtn.layer setMasksToBounds:YES];
                [tellFriendBtn.layer setCornerRadius:5.0f];
                [tellFriendBtn.layer setBorderWidth:1.0f];
                
                [cell addSubview:tellFriendBtn];

                connectFBBtn1 = nil, likeBtn=nil, tellFriendBtn=nil;
                cell.backgroundColor = [UIColor lightGrayColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
            break;
            
        default:
            break;
    }
    [loginMod release], loginMod = nil;
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return @"User Details";
            break;
        case 1:
            return @"Feedback";
            break;
        case 2:
            return  @"Info";
            break;
        default:
            break;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.section == 2) {

        NSDictionary *dictionary = [listOfItems objectAtIndex:indexPath.section + 1];
        NSArray *array = [dictionary objectForKey:@"URLINFO"];
        NSString *selectedURL = [array objectAtIndex:indexPath.row];
        if([selectedURL isEqualToString:@"restore"])
        {
            //logic for restore
            restore = TRUE;
            UIAlertView *alert  = [[UIAlertView alloc]
                                   initWithTitle: @"Vikatan"
                                   message: KRESTOREMSG
                                   delegate: self
                                   cancelButtonTitle:@"Cancel"
                                   otherButtonTitles:@"OK",nil];
            [alert show];
            [alert release];  
        } else {
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:selectedURL]];
        }
    }
}

- (void) deletAllIssues {
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *newDir;
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, 
                                                   NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    newDir = [docsDir stringByAppendingPathComponent:KMAGROOTPATH];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:newDir])
    {
       // NSLog(@"Inside the loop");
        if ([filemgr removeItemAtURL:[NSURL fileURLWithPath:newDir ] error:NULL] == NO)
        {
           // NSLog(@"Failed to create directoy");
            // Failed to create directory
        } else {
            //[[PSPDFCache sharedPSPDFCache] clearCache];
            docsDir = [dirPaths objectAtIndex:0];
            newDir = [docsDir stringByAppendingPathComponent:KMAGPREVROOTPATH];
            [filemgr removeItemAtURL:[NSURL fileURLWithPath:newDir ] error:NULL];
            
           // NSLog(@"Sucess");
        }
    }
    [filemgr release];
}
- (void) show_login_clicked:(id)sender {
   // NSLog(@"Inside");
    
    Login *loginMod = [[Login alloc]init];
    
    if (![loginMod isUserDetailsExists]) {
        loginMod.fromInfo = TRUE;
        
        LoginView *ct = [[LoginView alloc] init];ct.fromInfo = TRUE;
        [ct.view setBackgroundColor:[UIColor clearColor]];
        
        UIPopoverController *popoverController1 = [[UIPopoverController alloc] initWithContentViewController:ct] ;

        CGRect popoverRect = [self.view convertRect:[sender frame]  fromView:[sender superview]];
        
        //CGRect popoverRect = CGRectMake(0,0, 10, 10);
        popoverRect.size.width = MIN(popoverRect.size.width, 100);
        CGSize size = CGSizeMake(658, 450); // size of view in popover 768, 450
        popoverController1.popoverContentSize = size;
        [popoverController1 
         presentPopoverFromRect:popoverRect 
         inView:self.view 
         permittedArrowDirections:UIPopoverArrowDirectionUp
         animated:YES];
        self.popoverController = popoverController1;

    } else {
        restore = FALSE;
        UIAlertView *alert  = [[UIAlertView alloc]
                               initWithTitle: @"Logout"
                               message: @"Do you want to logout?"
                               delegate: self
                               cancelButtonTitle:@"Cancel"
                               otherButtonTitles:@"OK",nil];
        [alert show];
        [alert release];

    }
     
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    Login *loginMod = [[Login alloc]init];

    if(likeApp) {
        likeApp = FALSE;
        if (buttonIndex == 1) {
                     
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/in/app/vikatan/id452698974?mt=8"]];

        }
    } else {
        if (buttonIndex != 0)
        {
            if(restore) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"purcrestoreipad" object: nil];
            } else {
                [loginMod logout];
                [self.tableView reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"bookreferesh" object: nil];
            }
        }
    }
}

-(IBAction)openFeedbackView:(id)sender {

    FeedBackForm *FeedBackFormViewController;

    FeedBackFormViewController = [[FeedBackForm alloc] initWithNibName:@"FeedBackForm" bundle:nil];
    
    FeedBackFormViewController.isPresentedFromPopOver = TRUE;
    
    [FeedBackFormViewController.view setBackgroundColor:[UIColor clearColor]];
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:FeedBackFormViewController] ;
    
    CGRect popoverRect = [self.view convertRect:[sender frame] fromView:[sender superview]];

    popoverRect.size.width = MIN(popoverRect.size.width, 100);
    CGSize size = CGSizeMake(768, 600); // size of view in popover

    popoverController.popoverContentSize = size;
    [popoverController
     presentPopoverFromRect:popoverRect
     inView:self.view
     permittedArrowDirections:UIPopoverArrowDirectionAny
     animated:YES];
}

-(IBAction)likeAppButtonPressed:(id)sender {
    UIAlertView *buttonAlert = [[UIAlertView alloc] initWithTitle:@"Help Spread the Word" message:@"If you like this app, please rate it in the App Store. Thanks!"                                                          delegate:self cancelButtonTitle:@"Maybe Later" otherButtonTitles:@"Rate It Now", nil];
    [buttonAlert show];
    [buttonAlert release];
    
    likeApp = TRUE;
}

-(IBAction)tellFriendButtonPressed:(id)sender {

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Like This App? Refer A Friend!" delegate:self cancelButtonTitle:@"Maybe Later" destructiveButtonTitle:nil otherButtonTitles:@"Share on Facebook", @"Post on Twitter", @"Send Email", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];

}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {

    NSURL *url = [NSURL URLWithString:APP_ITUNES_URL];
    SHKItem *item = [SHKItem URL:url title:APP_NAME_DESC
                     contentType:(SHKURLContentTypeUndefined)];
    [SHK setRootViewController:self];

    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        if (buttonIndex == 0) { //[actionSheet destructiveButtonIndex]
            // Add Facebook Connect code here.
            [SHKFacebook shareItem:item];
        }
        if (buttonIndex == 1) {
            // Add Twitter code here.
            [SHKTwitter shareItem:item];
        }
        if (buttonIndex == 2) {
            // Add Email code here.

            [SHKMail shareItem:item];
        }
    }
    
    url=nil, item=nil;
}

- (void) callBack {
    [self.tableView reloadData];
}
-(void)closeClicked:(id)sender
{
    [loginView removeFromSuperview];
}
- (void) closePops {
    NSLog(@"closing ino sir");
    [self.popoverController dismissPopoverAnimated:FALSE];
}

// MV modifications started

-(BOOL)shouldAutoRotate
{
    
    return YES;
    
}

-(NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskAll;

}

// MV modifications completed


@end
