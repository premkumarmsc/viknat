

#import <UIKit/UIKit.h>
#import "AQGridViewCell.h"
#import "LibCustomButton.h"
#import "AppConstant.h"


@interface LibBookCell : AQGridViewCell <UINavigationControllerDelegate>
{
    
   UIButton * button;
   UIViewController *parent;
}


@property (nonatomic, retain) LibCustomButton * libcustombutton;

@property (nonatomic, assign) UIViewController *parent;

@end
