//
//  Library_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 15/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Library_iPhone.h"
#import "MagazineLib.h"
#import "LibraryWrapperButton.h"
#import "ImageDemoFilledCell.h"
#define THUMB_SIZE 100,144


enum
{
    ImageDemoCellTypePlain,
    ImageDemoCellTypeFill,
    ImageDemoCellTypeOffset
};

@implementation Library_iPhone

#define SAMPLE_DOCUMENT @"magazine.pdf"

#define DEMO_VIEW_CONTROLLER_PUSH FALSE
@synthesize headerImageView,
mainView, catPlacHolder,
magShelfRowview,editBtn;
@synthesize gridView=_gridView;
@synthesize tmpIssueFldPath;
@synthesize thumbnailSize = thumbnailSize_;
@synthesize thumbnailMargin = thumbnailMargin_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    // if([self.view.subviews containsObject:mainView])
    [mainView release];
    //  if([self.view.subviews containsObject:catPlacHolder])
    [catPlacHolder release];
    [ magShelfRowview release], magShelfRowview = nil;
    gridView.delegate = nil;
    [_gridView release];
    [_imageNames release];
    Issueid = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)applyShinyBackgroundWithColor:(UIColor *)color {
    
    // create a CAGradientLayer to draw the gradient on
    CAGradientLayer *layer = [CAGradientLayer layer];
    
    // get the RGB components of the color
    const CGFloat *cs = CGColorGetComponents(color.CGColor);
    
    // create the colors for our gradient based on the color passed in
    layer.colors = [NSArray arrayWithObjects:
                    (id)[color CGColor],
                    (id)[[UIColor colorWithRed:0.98f*cs[0] 
                                         green:0.98f*cs[1] 
                                          blue:0.98f*cs[2] 
                                         alpha:1] CGColor],
                    (id)[[UIColor colorWithRed:0.95f*cs[0] 
                                         green:0.95f*cs[1] 
                                          blue:0.95f*cs[2] 
                                         alpha:1] CGColor],
                    (id)[[UIColor colorWithRed:0.93f*cs[0] 
                                         green:0.93f*cs[1] 
                                          blue:0.93f*cs[2] 
                                         alpha:1] CGColor],
                    nil];
    
    // create the color stops for our gradient
    layer.locations = [NSArray arrayWithObjects:
                       [NSNumber numberWithFloat:0.0f],
                       [NSNumber numberWithFloat:0.49f],
                       [NSNumber numberWithFloat:0.51f],
                       [NSNumber numberWithFloat:1.0f],
                       nil];
    
    layer.frame = self.view.bounds;
    [self.view.layer insertSublayer:layer atIndex:0];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeGridView];
    int toolBarHght = self.navigationController.navigationBar.frame.size.height;
    if(!PSIsIpad())
        toolBarHght = 44.01f;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
    titleView.autoresizingMask = UIViewAutoresizingNone;
   	UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
	headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, header.size.width, header.size.height)];
	headerImageView.image = header;
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-19, 5, 100, 20)]; 
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"நூலகம்";
    lblTitle.textAlignment= UITextAlignmentCenter;
    [titleView addSubview:headerImageView];
    [titleView addSubview:lblTitle];
    self.navigationItem.titleView = titleView;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50.0)];
    editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
    editBtn.frame = CGRectMake(5, 10, 26, 26);
    [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(edit_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [leftView addSubview:editBtn];
    [self.navigationController.navigationBar addSubview:leftView];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(215, 0, 100, 50.0)];
   [self.navigationController.navigationBar addSubview:rightView];
    
    
    UIButton *magBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[magBtn setBackgroundImage:[UIImage imageNamed:@"magazines_norm.png"] forState:UIControlStateNormal];
    magBtn.frame = CGRectMake(0, 5, 30, 30);
    [rightView addSubview:magBtn];
    [magBtn setEnabled:FALSE];
    
    UIButton *bookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[bookBtn setBackgroundImage:[UIImage imageNamed:@"books_norm.png"] forState:UIControlStateNormal];
    bookBtn.frame = CGRectMake(35, 5, 30, 30);
    [bookBtn addTarget:self action:@selector(booklib_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:bookBtn];bookBtn.enabled = TRUE;
    
    UIButton *refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
	refreshImageView.frame =  CGRectMake(70, 5, 32, 32);
	[refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:refreshImageView];
    
    
    xClosePos = 140;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        thumbnailSize_   = CGSizeMake(162.f, 224.f);
        thumbnailMargin_ = CGSizeMake(10.f, 40.f);
    } else {
        thumbnailSize_   = CGSizeMake(150/1.2,  168/*275 - 65*/);
        thumbnailMargin_ = CGSizeMake(10.f/1.75, 40.f/1.75);
        xClosePos        = self.thumbnailSize.width - 15;
    }
    [self loadMagCateg];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reloadLibView) name:@"Libraryreferesh" object: nil];
    
}

- (void) reloadLibView {
    
    //if(self.gridView)
    [self viewWillDisappear:NO];
    [self viewWillAppear:NO];
    [self.gridView beginUpdates];
    [self.gridView setHidden:TRUE];
    [self.gridView endUpdates];
}
- (void) loadMagCateg
{
    int noIssXPos = 5;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) noIssXPos = 160;
    
    [noIssues removeFromSuperview];
    //Load magazine name 
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    NSArray *fields     = [[NSArray alloc] initWithObjects:@"lib_magazine_id",@"lib_magazine_name", @"magazine_id", @"issue_id", nil];
    NSMutableArray *mutableArr = [magazineLibObj selectMagazineField:fields when:@"" order:@" group by magazine_id order by magazine_id,download_date" recordsCount:KLIBMAXMAGCOUNT startWith:0];
    
    int magSelect = [magazineLibObj getLatestMag];
    
    NSLog(@"Selected magazine %d",magSelect);
    
    [catPlacHolder removeFromSuperview],[catPlacHolder release], catPlacHolder = nil;
    catPlacHolder = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                  48)];
    catPlacHolder.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    catPlacHolder.backgroundColor = [UIColor lightGrayColor];
    catPlacHolder.opaque = 0.9;
	catPlacHolder.alpha = 0.9;
    
    LibraryWrapperButton *magBtn;
    NSString *magName;
    int xPos = 0;
    //check for non - empty 
    if([mutableArr count] != 0) 
    {
        editBtn.hidden = FALSE;
        for(NSDictionary *dicObj in mutableArr ) 
       //for(int i=1;i<10;i++ ) 
        {
            magName = [dicObj objectForKey:@"lib_magazine_name"] ;
            magBtn  = [[[LibraryWrapperButton alloc] initWithFrame:CGRectMake(xPos, 9, 120, 32)] autorelease];
            [magBtn setBackgroundColor:[UIColor clearColor]];
            [magBtn setBackgroundImage:[UIImage imageNamed:@"button_bg.png"] forState:UIControlStateSelected];
            [magBtn addTarget:self action:@selector(mag_catg_clicked:) forControlEvents:UIControlEventTouchUpInside];
            [magBtn setMagID:[[dicObj objectForKey:@"magazine_id"] intValue]];
            magBtn.tag = [[dicObj objectForKey:@"magazine_id"] intValue];
            magBtn.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
            magBtn.titleLabel.textAlignment = UITextAlignmentCenter;
            magBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
            magBtn.titleLabel.textColor = UIColorFromRGB(0x3399FF);
            [magBtn setTitle:magName forState:UIControlStateNormal];
            [catPlacHolder addSubview:magBtn];
            xPos += 120;
            [magBtn setBackgroundColor:[UIColor clearColor]];
            if(magBtn.MagID == magSelect)
                magBtn.selected = TRUE;
        }
        [self.view addSubview:catPlacHolder];
        [catPlacHolder setContentSize:CGSizeMake(155 * [mutableArr count], 48)];
        [self LoadMagazines:magSelect];
        [magazineLibObj release];
    } else {
        noIssues = [[UIView alloc] initWithFrame:CGRectMake(noIssXPos, 130, self.view.frame.size.width, 100)]; 
       // noIssues.center = self.view.center;
        UILabel *theText = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, self.view.frame.size.width, 100)];
        //theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
       // theText.text = @"No Issues available, please download from இதழ்கள்";
        //theText.center = self.view.center;
        theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
        theText.text = KINOISSUES;
        
        theText.backgroundColor = [UIColor clearColor];
        theText.textColor = [UIColor whiteColor];//UIColorFromRGB(0x3399FF);
        theText.lineBreakMode = UILineBreakModeWordWrap;
        theText.numberOfLines = 3;
        theText.textAlignment = UITextAlignmentCenter;
        [noIssues addSubview:theText];
        [self.view addSubview:noIssues];
        editBtn.hidden = TRUE;
    }
}
- (void) booklib_clicked:(id) sender {
    
    Library_Book_iPAD *libView = [[Library_Book_iPAD alloc]init];
    
    /*if(IsIpad()) {
        libView = [[Library_Book_iPAD alloc] initWithNibName:@"Library_Book_iPAD" bundle:nil];
    } else {
        libView = [[Library_Book_iPAD alloc] initWithNibName:@"Library_Book_iPhone" bundle:nil];
    }*/
    
    UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:libView] autorelease];
    pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    pdfNavController.navigationBar.barStyle = UIBarStyleBlack;
    pdfNavController.navigationBar.tintColor = [UIColor blackColor];
   // [self hidesBottomBarWhenPushed:NO];
    pdfNavController.hidesBottomBarWhenPushed = NO;
    [self presentModalViewController:pdfNavController animated:YES];
   
    
    /*
    Library_Book_iPAD *libView = [[Library_Book_iPAD alloc]init];
    [self.navigationController pushViewController:libView animated:TRUE];
    */
}
- (void) LoadMagazines:(int)magazineID
{
    [self.gridView setHidden:FALSE];    
    //Reset edit button
    [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
    editMode = FALSE;
    _imageNames = [[NSMutableArray alloc] init];
    Issueid      = [[NSMutableArray alloc] init];
    issueDate    = [[NSMutableArray alloc] init];
    NSMutableArray *mutableArr;
    NSArray *fields;
    int yPos = 5, xPos = 10 ;
    NSString *wrapperImageName;
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    
    [magShelfRowview removeFromSuperview];
    magShelfRowview = [[UIScrollView alloc]initWithFrame:CGRectMake(40, 75, self.view.bounds.size.width, 
                                                                    self.view.bounds.size.height)];
    fields     = [[NSArray alloc] initWithObjects:@"lib_magazine_id", @"magazine_id", @"issue_id",@"issue_date", nil];
    NSString *condition = [NSString stringWithFormat:@"%@%d", @"magazine_id=",magazineID];
    mutableArr = [magazineLibObj selectMagazineField:fields when:condition order:@" order by  issue_id desc" recordsCount:KLIBMAXMAGCOUNT startWith:0];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    
    for(NSDictionary *dicObj in mutableArr ) 
    {
        int curMagID = [ [dicObj objectForKey:@"magazine_id"] intValue];
        magID = curMagID;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSString* magPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",[paths objectAtIndex:0],@"/",KMAGROOTPATH,@"/",curMagID,@"_",
                             [[dicObj objectForKey:@"issue_id"] intValue],
                             @"/magazine_",curMagID,@"_",[[dicObj objectForKey:@"issue_id"] intValue],@".pdf"];
        
       // NSLog(@"Image path is found to be: %@", magPath);
        if([fileManager fileExistsAtPath:magPath]) 
        {
            wrapperImageName = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",[paths objectAtIndex:0],@"/",KMAGROOTPATH,@"/",curMagID,@"_",[[dicObj objectForKey:@"issue_id"] intValue],@"/wrapper.jpg"];
            [_imageNames addObject:wrapperImageName];
            [Issueid addObject:[dicObj objectForKey:@"issue_id"]];
            [issueDate addObject:[dicObj objectForKey:@"issue_date"]];
            
        } else  {
            
            MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
            [magazineLibObj deleteMagazineWhen:[NSString stringWithFormat:@" issue_id=%d", [dicObj objectForKey:@"issue_id"]]];
            [magazineLibObj release];magazineLibObj = nil;
        }
    }
    [magazineLibObj closeDatabase];
    [magazineLibObj release];magazineLibObj = nil;
    [self.gridView reloadData];
    //[self.gridView reloadInputViews];
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController 
                    animated:(BOOL)animated {
    /*
     if ([viewController isKindOfClass:[PSPDFViewController class]])
     {
     [self viewDidAppear:FALSE];
     }
     */
}
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    if(pdfMode) {
        pdfMode = FALSE;
        return;
    }
    [self loadMagCateg];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}
-(void) hideTabBarView {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 1064, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 1064)];
        }
    }
    [UIView commitAnimations];
}
- (void) showTabBar {
	
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, screenHeight-48, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, screenHeight-48)];
        }
    }
    [UIView commitAnimations]; 
}
-(void) refresh_clicked:(id)sender {
   // NSLog(@"Referesh");
    [self loadMagCateg];
}
- (void) mag_catg_clicked:(id) sender {
    
    LibraryWrapperButton *btn = (LibraryWrapperButton*)sender;
    for(UIView *subview in [catPlacHolder subviews]) 
    {
        if([subview isKindOfClass:[LibraryWrapperButton class]]) 
        {
            
            LibraryWrapperButton *btn1 = (LibraryWrapperButton*)subview;
            if(btn.tag == subview.tag)
                btn.selected = TRUE;
            //[subview setBackgroundColor:[UIColor lightGrayColor]];
            else
                btn1.selected = FALSE;
        }
        //[subview setBackgroundColor:[UIColor clearColor]];
    }
    [self LoadMagazines:[btn MagID]];
}

- (void)book_clicked:(id)sender {
    
    [self LoadMagazines:TRUE];
}

- (void)magazine_clicked:(id)sender {
    [self LoadMagazines:FALSE];
}

- (void)close_clicked:(id)sender {
    
    LibraryCloseButton *closeBtn = (LibraryCloseButton*)sender;
    
    tmpIssueFldPath = closeBtn.issueFolderPath;
    tmpIssueID      = closeBtn.IssueID;
    itemIndex = closeBtn.grdInx;
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Remove Magazine"
                          message: @"Do you want to remove the magazine..?"
                          delegate: self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"OK",nil];
    [alert show];
    //[alert release];
    
    
    
    
}
- (void)edit_clicked:(id)sender 
{
    //UIButton *editBtn = (UIButton*)sender;
    for( int i = 0; i<[self.gridView numberOfItems];i++)
    {
        AQGridViewCell *cell = [self.gridView cellForItemAtIndex:i];
        for(UIView *subview1 in [cell subviews]) 
        { 
            if([subview1 isKindOfClass:[UIButton class]]) 
            {
                UIButton *btn = (UIButton*)subview1;
                btn.hidden = editMode;
            }
            
        }
    }
    
    [self editModeDone];
    
}
- (void) editModeDone {
    
    if(!editMode){
        [editBtn setImage:[UIImage imageNamed: @"done.png"] forState:UIControlStateNormal];
        editMode = TRUE;
    }
    else {
        [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
        editMode = FALSE;
    }
}

-(BOOL) deletIssue{
    
   // NSLog(@"issue folder path is %@", tmpIssueFldPath);
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                            documentsDirectory,@"/",
                            KMAGROOTPATH,@"/",magID,@"_",tmpIssueID,
                            @"/magazine_",magID,@"_",tmpIssueID,@".pdf"];
    
    
    if([[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:tmpIssueFldPath ] error:NULL])
    {
       // NSLog(@"issue folder path is %@", tmpIssueFldPath);
        [magazineLibObj deleteMagazineWhen:[NSString stringWithFormat:@" issue_id=%d", tmpIssueID]];
        return  TRUE;
    }
    else
    {
        return FALSE;
    }
    return FALSE;
    
}
- (void)viewDidUnload
{
    self.gridView = nil;
    Issueid = nil;
    issueDate = nil;
    //[editBtn removeFromSuperview], [editBtn release];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return (interfaceOrientation == UIInterfaceOrientationPortrait);
     
	return YES;
}
- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self dismissModalViewControllerAnimated:TRUE];
	
}

- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aGridView {
    /*CGFloat thumbnailSizeReductionFactor = 1.f;
    CGSize thumbGridSize =  CGSizeMake(roundf(self.thumbnailSize.width + self.thumbnailMargin.width), roundf(self.thumbnailSize.height*thumbnailSizeReductionFactor + self.thumbnailMargin.height));
    return thumbGridSize;*/
    
    CGFloat thumbnailSizeReductionFactor = 1.f;
    CGSize thumbGridSize;
    if (IsIpad()) {
        thumbGridSize =  CGSizeMake(roundf(self.thumbnailSize.width + self.thumbnailMargin.width), roundf(self.thumbnailSize.height*thumbnailSizeReductionFactor + self.thumbnailMargin.height));
    } else {
        thumbGridSize =  CGSizeMake(roundf(self.thumbnailSize.width + self.thumbnailMargin.width)-15, roundf(self.thumbnailSize.height*thumbnailSizeReductionFactor + self.thumbnailMargin.height)+30);
        
    }
    return thumbGridSize;

}
#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    // return 5;
    return [_imageNames count] ;
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    
    static NSString *ThumbCellIdentifier = @"PSPDFThumbnailGridViewCell";
    ImageDemoGridViewCell * cell = (ImageDemoGridViewCell *)[self.gridView dequeueReusableCellWithIdentifier:ThumbCellIdentifier];
    cell = [[[ImageDemoGridViewCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, roundf(self.thumbnailSize.width*1.f), roundf(self.thumbnailSize.height*1.f + 40)) reuseIdentifier:ThumbCellIdentifier] autorelease];
       
    [cell addSubview:[[UIImageView alloc] initWithImage:[[UIImage alloc] initWithContentsOfFile:[_imageNames objectAtIndex: index]]]];
    UILabel *siteLabel_ = [[UILabel alloc] initWithFrame:CGRectZero];         
    siteLabel_.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.7f];
    siteLabel_.textColor = [UIColor colorWithWhite:0.8f alpha:1.f];
    siteLabel_.shadowColor = [UIColor blackColor];
    siteLabel_.shadowOffset = CGSizeMake(1.f, 1.f);
    siteLabel_.textAlignment = UITextAlignmentCenter;
    siteLabel_.font = [UIFont boldSystemFontOfSize:14];
    siteLabel_.text = [NSString stringWithFormat:@"%@",[issueDate objectAtIndex:index]];
    [cell addSubview:siteLabel_];
    siteLabel_.frame = CGRectMake(0, self.thumbnailSize.height+20, self.thumbnailSize.width+25, ksiteLabelHeight);
    //cell.imageView.image = [[UIImage alloc] initWithContentsOfFile:[_imageNames objectAtIndex: index]];
   // cell.siteLabel.text = [NSString stringWithFormat:@"%@",[issueDate objectAtIndex:index]];
    LibraryCloseButton *closeBtn = [[LibraryCloseButton alloc] initWithFrame:CGRectMake(xClosePos, -10 , 32, 32)];
    int issueId = [[Issueid objectAtIndex:index] intValue];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    if(!editMode) {
        closeBtn.hidden = TRUE;
    } else {
        closeBtn.hidden = FALSE;
    
    }
    [closeBtn addTarget:self action:@selector(close_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [closeBtn setIssueID:issueId];
    [closeBtn setGrdInx:index];
    [closeBtn setIssueFolderPath:[NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],@"/",KMAGROOTPATH,@"/",
                                  magID,@"_",issueId]];
    [cell addSubview:closeBtn];
    
    //cell.selectionStyle = AQGridViewCellSeparatorStyleNone;
    return ( cell );
}

- (void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)gridIndex {
    
    int issueId = [[Issueid objectAtIndex:gridIndex] intValue];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    /*NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@%d%@%d%@",
                            documentsDirectory,@"/",
                            KMAGROOTPATH,@"/",magID,@"_",issueId,
                            @"/magazine_",magID,@"_",issueId,@".pdf"];*/
    
    VikatanAppDelegate_iPad *appDelegate  = [[UIApplication sharedApplication] delegate];
    appDelegate.globalCount = [NSNumber numberWithInt:0];
    
    [appDelegate setPdfDocPath:[NSString stringWithFormat:@"%d%@%d%@",magID,@"_",issueId,@"/menu.json"]];
    
    
     NSString* pdfDocPath = [NSString stringWithFormat:@"%@%@%@%@%d%@%d%@",
     documentsDirectory,
     @"/",KMAGROOTPATH,@"/",magID,@"_",issueId,
     @"/wrapper.jpg"];

    
    if(!editMode) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:pdfDocPath]) 
        {
            
            TestPDFReader * prevMag = [[TestPDFReader alloc] init];
            [prevMag setWrapperUrl:pdfDocPath];
            [self.navigationController pushViewController:prevMag animated:TRUE];
            
        }
    }
    else {
        [self setTmpIssueFldPath:[NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],@"/",
                                  KMAGROOTPATH,@"/", magID,@"_",issueId]];
        tmpIssueID          =  issueId; 
        UIAlertView *alert  = [[UIAlertView alloc]
                               initWithTitle: @"Remove Magazine"
                               message: @"Do you want to remove the magazine..?"
                               delegate: self
                               cancelButtonTitle:@"Cancel"
                               otherButtonTitles:@"OK",nil];
        itemIndex = gridIndex;
        [alert show];
        [alert release];
    }
    [self.gridView deselectItemAtIndex:gridIndex animated:NO];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex != 0)
    {
        if([self deletIssue])
        {
            NSIndexSet* set = [NSIndexSet indexSetWithIndex:itemIndex]; 
            // if([_imageNames count]>0 )
            [_imageNames removeObjectAtIndex:itemIndex];
            [Issueid removeObjectAtIndex:itemIndex];
            [self.gridView beginUpdates];    
            [self.gridView deleteItemsAtIndices:set  withAnimation:AQGridViewItemAnimationFade];
            [self.gridView endUpdates];
            
            //[self.gridView reloadData];
            //[self.gridView reloadInputViews];
        } else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Failed" message:KMAGDELETEFAILED delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        }
    }
    
}

- (void) cellChooser: (ImageDemoCellChooser *) chooser selectedItemAtIndex: (NSUInteger) index
{
    self.gridView.separatorStyle = AQGridViewCellSeparatorStyleEmptySpace;
    self.gridView.resizesCellWidthToFit = NO;
    self.gridView.separatorColor = nil;
    [self.gridView reloadData];
}
//Customize Grid View
- (void)initializeGridView {
    if (!self.gridView) {
        // init thumbs view
        self.gridView = [[[AQGridView alloc] initWithFrame:CGRectZero] autorelease];
        self.gridView.backgroundColor = [UIColor clearColor];
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;  
        self.gridView.resizesCellWidthToFit = NO;
        self.gridView.layoutDirection = AQGridViewLayoutDirectionVertical;
        
        // if we have a translucent bar, make some room
        if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent) {
            self.gridView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
        }else {
            self.gridView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        self.gridView.hidden = NO;
        self.gridView.scrollsToTop = YES;
        [self.view addSubview:self.gridView];
        CGRect frame = CGRectMake(0, 65, self.view.bounds.size.width, self.view.bounds.size.height - 65 );   
        self.gridView.frame = frame;
        
        // add a footer view for tabbar
        if (!PSIsIpad()) {
            self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 50)] autorelease];
        }
        
    }
    
}
-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return YES;
}
#pragma mark -
#pragma mark Grid View Delegate

@end
