//
//  Library_iPad.m
//  Vikatan
//
//  Created by MobileVeda on 15/05/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import "Library_Book_iPAD.h"
#import "MagazineLib.h"
#import "LibraryWrapperButton.h"
#import "ImageDemoFilledCell.h"
#define THUMB_SIZE 100,144


enum
{
    ImageDemoCellTypePlain,
    ImageDemoCellTypeFill,
    ImageDemoCellTypeOffset
};

@implementation Library_Book_iPAD

#define SAMPLE_DOCUMENT @"magazine.pdf"

#define DEMO_VIEW_CONTROLLER_PUSH FALSE
@synthesize headerImageView,
mainView, catPlacHolder,
magShelfRowview,editBtn;
@synthesize gridView=_gridView;
@synthesize tmpIssueFldPath;
@synthesize thumbnailSize = thumbnailSize_;
@synthesize thumbnailMargin = thumbnailMargin_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    // if([self.view.subviews containsObject:mainView])
    [mainView release];
    //  if([self.view.subviews containsObject:catPlacHolder])
    [catPlacHolder release];
    [ magShelfRowview release], magShelfRowview = nil;
    gridView.delegate = nil;
    [_gridView release];
    [_imageNames release];
    Issueid = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)applyShinyBackgroundWithColor:(UIColor *)color {
    
    // create a CAGradientLayer to draw the gradient on
    CAGradientLayer *layer = [CAGradientLayer layer];
    
    // get the RGB components of the color
    const CGFloat *cs = CGColorGetComponents(color.CGColor);
    
    // create the colors for our gradient based on the color passed in
    layer.colors = [NSArray arrayWithObjects:
                    (id)[color CGColor],
                    (id)[[UIColor colorWithRed:0.98f*cs[0] 
                                         green:0.98f*cs[1] 
                                          blue:0.98f*cs[2] 
                                         alpha:1] CGColor],
                    (id)[[UIColor colorWithRed:0.95f*cs[0] 
                                         green:0.95f*cs[1] 
                                          blue:0.95f*cs[2] 
                                         alpha:1] CGColor],
                    (id)[[UIColor colorWithRed:0.93f*cs[0] 
                                         green:0.93f*cs[1] 
                                          blue:0.93f*cs[2] 
                                         alpha:1] CGColor],
                    nil];
    
    // create the color stops for our gradient
    layer.locations = [NSArray arrayWithObjects:
                       [NSNumber numberWithFloat:0.0f],
                       [NSNumber numberWithFloat:0.49f],
                       [NSNumber numberWithFloat:0.51f],
                       [NSNumber numberWithFloat:1.0f],
                       nil];
    
    layer.frame = self.view.bounds;
    [self.view.layer insertSublayer:layer atIndex:0];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self showTabBar];
    
    [self initializeGridView];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    int toolBarHght = self.navigationController.navigationBar.frame.size.height;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"ipad_row_bg.png"]]];
    
    if(!PSIsIpad()) {
        toolBarHght = 44.01f;
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
        titleView.autoresizingMask = UIViewAutoresizingNone;
        UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
        headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, header.size.width, header.size.height)];
        headerImageView.image = header;
       
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-19, 5, 100, 20)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.text = @"நூலகம்";
        lblTitle.textAlignment= UITextAlignmentCenter;
        [titleView addSubview:headerImageView];
        [titleView addSubview:lblTitle];
        self.navigationItem.titleView = titleView;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50.0)];
        editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
        editBtn.frame = CGRectMake(5, 10, 26, 26);
        [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
        [editBtn addTarget:self action:@selector(edit_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [leftView addSubview:editBtn];
        [self.navigationController.navigationBar addSubview:leftView];
        
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(215, 0, 100, 50.0)];
        [self.navigationController.navigationBar addSubview:rightView];
        
        UIButton *magBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [magBtn setBackgroundImage:[UIImage imageNamed:@"magazines_norm.png"] forState:UIControlStateNormal];
        magBtn.frame = CGRectMake(0, 5, 30, 30);
        [rightView addSubview:magBtn];
        [magBtn addTarget:self action:@selector(maglib_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [magBtn setEnabled:TRUE];
        
        UIButton *bookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [bookBtn setBackgroundImage:[UIImage imageNamed:@"books_norm.png"] forState:UIControlStateNormal];
        bookBtn.frame = CGRectMake(35, 5, 30, 30);
        [bookBtn addTarget:self action:@selector(booklib_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [rightView addSubview:bookBtn];bookBtn.enabled = FALSE;
        
        UIButton *refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
        refreshImageView.frame =  CGRectMake(70, 5, 32, 32);
        [refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [rightView addSubview:refreshImageView];
    } else {
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 118, 36.0)];
        titleView.autoresizingMask = UIViewAutoresizingNone;
        //Add Header Image Control
        UIImage *header = [UIImage imageNamed:@"vikatan_logo_top.png"];
        headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, header.size.width, header.size.height)];
        headerImageView.image = header;
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(header.size.width-20, 5, 200, 20)];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:18];
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.text = @"நூலகம் - புத்தகங்கள்";
        lblTitle.textAlignment= UITextAlignmentCenter;
        [titleView addSubview:headerImageView];
        [titleView addSubview:lblTitle];
        
        self.navigationItem.titleView = titleView;
        
        editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
        editBtn.frame = CGRectMake(-10, 5, 26, 26);
        [editBtn setTitle:@"Edit" forState:UIControlStateNormal];
        [editBtn addTarget:self action:@selector(edit_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIToolbar *leftToolBar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 90.f,toolBarHght)] autorelease];        
        leftToolBar.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        // leftToolBar.barStyle = !self.isDarkHUD ? UIBarStyleDefault : UIBarStyleBlack;
        leftToolBar.barStyle = UIBarStyleBlackOpaque;
        leftToolBar.tintColor = [UIColor blackColor];
        
        editBtn.frame = CGRectMake(0.0, 0.0, 48, 48);
        NSMutableArray *leftItems = [NSMutableArray array];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:editBtn];
        // [buttonImage release];[aButton release];
        [leftItems addObjectsFromArray:[NSArray arrayWithObjects:backButton, nil]];
        leftToolBar.items = leftItems;
        
        self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:leftToolBar] autorelease];
        
        UIToolbar *compoundView = [[[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, 140.f , toolBarHght)] autorelease];
        compoundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        compoundView.barStyle = UIBarStyleBlackOpaque;
        compoundView.tintColor = [UIColor blackColor];
        
        UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil] autorelease];
        space.width = 3.f;
        
        NSMutableArray *compountItems = [NSMutableArray array];
        UIButton *magBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [magBtn setBackgroundImage:[UIImage imageNamed:@"magazines_norm.png"] forState:UIControlStateNormal];
        magBtn.frame = CGRectMake(0, 5, 30, 30);
        [magBtn addTarget:self action:@selector(maglib_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *magButton = [[UIBarButtonItem alloc] initWithCustomView:magBtn];
        [compountItems addObjectsFromArray:[NSArray arrayWithObjects:magButton, space, nil]];
        
        UIButton *bookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [bookBtn setBackgroundImage:[UIImage imageNamed:@"books_norm.png"] forState:UIControlStateNormal];
        bookBtn.frame = CGRectMake(10, 5, 30, 30);
        bookBtn.enabled = TRUE;
        UIBarButtonItem *bkButton = [[UIBarButtonItem alloc] initWithCustomView:bookBtn];
        [compountItems addObjectsFromArray:[NSArray arrayWithObjects:bkButton, space, nil]];
        [bookBtn addTarget:self action:@selector(booklib_clicked:) forControlEvents:UIControlEventTouchUpInside];
        [bkButton setEnabled:FALSE];
        
        UIButton *refreshImageView = [UIButton buttonWithType:UIButtonTypeCustom];
        [refreshImageView setImage:[UIImage imageNamed:@"reload_norm_32.png"] forState:UIControlStateNormal];
        refreshImageView.frame =  CGRectMake(0, 5, 32, 32);
        [refreshImageView addTarget:self action:@selector(refresh_clicked:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *refButton = [[UIBarButtonItem alloc] initWithCustomView:refreshImageView];
        [compountItems addObjectsFromArray:[NSArray arrayWithObjects:refButton, space, nil]];
        compoundView.items = compountItems;
        
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:compoundView] autorelease];
        
    }
    
    xClosePos = 140;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        thumbnailSize_   = CGSizeMake(162.f, 224.f);
        thumbnailMargin_ = CGSizeMake(10.f, 40.f);
    } else {
        thumbnailSize_   = CGSizeMake(150/1.2,  168/*275 - 65*/);
        thumbnailMargin_ = CGSizeMake(10.f/1.75, 40.f/1.75);
        xClosePos        = self.thumbnailSize.width - 15;
    }
    [self loadMagCateg];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reloadLibView) name:@"Libraryreferesh" object: nil];
    
}

- (void) maglib_clicked:(id) sender {
    // [self initializeGridView];
    if (IsIpad()) {
        Library_iPad *libView = [[Library_iPad alloc]init];
        [self.navigationController pushViewController:libView animated:TRUE];
    }else {
        
        [self.navigationController dismissModalViewControllerAnimated:TRUE];
    }
}
- (void) booklib_clicked:(id) sender {
    [self loadMagCateg];
}

- (void) cover_clicked:(id) sender {
    
    NSString* DocPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"MahaPeriyavar_V3.epub"] ;
    
    EpubViewControlleriPAD* epubView = [[EpubViewControlleriPAD alloc] init];
    
    [epubView loadEpub:[NSURL fileURLWithPath:DocPath]];
    epubView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:epubView animated:YES];
    [epubView release];
    
}

- (void) reloadLibView {
    
    //if(self.gridView)
    [self viewWillDisappear:NO];
    [self viewWillAppear:NO];
    [self.gridView beginUpdates];
    [self.gridView setHidden:TRUE];
    [self.gridView endUpdates];
}
- (void) loadMagCateg
{
    //Temp
    [coverButton removeFromSuperview];
    int noIssXPos = 0;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) noIssXPos = 160;
    [noIssues removeFromSuperview];
    //Load magazine name 
    DBClass *bookLibObj = [[DBClass alloc] init];
    NSArray *fields     = [[NSArray alloc] initWithObjects:@"cat_id",@"cat_name", nil];
    NSMutableArray *mutableArr = [bookLibObj selectMagazineField:fields tblname:@"lib_book" when:@"" order:@" group by cat_id " recordsCount:KLIBMAXMAGCOUNT startWith:0];
    int magSelect ;
    [catPlacHolder removeFromSuperview],[catPlacHolder release], catPlacHolder = nil;
    catPlacHolder = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,
                                                                  48)];
    LibraryWrapperButton *magBtn;
    NSString *magName;
    int xPos = 0;
    //check for non - empty 
    if([mutableArr count] != 0) 
    {
        editBtn.hidden = FALSE;
        for(NSDictionary *dicObj in mutableArr ) 
        {
            magName = [dicObj objectForKey:@"cat_name"] ;
            if(![magName isEqualToString:@""]) {
                if([magName isEqualToString:@"(null)"] )
                    magName = @"General";
                magBtn  = [[[LibraryWrapperButton alloc] initWithFrame:
                            CGRectMake(xPos, 9, 120, 32)] autorelease];
                [magBtn setBackgroundColor:[UIColor clearColor]];
                [magBtn setBackgroundImage:[UIImage imageNamed:@"button_bg.png"] 
                                  forState:UIControlStateSelected];
                [magBtn addTarget:self action:@selector(mag_catg_clicked:) forControlEvents:UIControlEventTouchUpInside];
                [magBtn setMagID:[[dicObj objectForKey:@"cat_id"] intValue]];
                magBtn.tag = [[dicObj objectForKey:@"cat_id"] intValue];
                magBtn.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
                magBtn.titleLabel.textAlignment = UITextAlignmentCenter;
                magBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
                magBtn.titleLabel.textColor = UIColorFromRGB(0x3399FF);
                [magBtn setTitle:magName forState:UIControlStateNormal];
                [catPlacHolder addSubview:magBtn];
                xPos += 120;
                [magBtn setBackgroundColor:[UIColor clearColor]];
                if(xPos == 120){
                    magSelect = magBtn.MagID;
                    magBtn.selected = TRUE;
                    [magBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
                }
            }
        }
        catPlacHolder.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        catPlacHolder.backgroundColor = [UIColor lightGrayColor];
        catPlacHolder.opaque = 0.9;
        catPlacHolder.alpha = 0.9;
        [self.view addSubview:catPlacHolder];
        [catPlacHolder setContentSize:CGSizeMake(155 * [mutableArr count], 46)];
        [self LoadMagazines:magSelect];
        [bookLibObj release];
    } else {
        noIssues = [[UIView alloc] initWithFrame:
                    CGRectMake(noIssXPos, self.view.frame.size.height/2,
                               self.view.frame.size.width, 100)]; 
        UILabel *theText = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 8, self.view.frame.size.width, 100)];
        noIssues.center = self.view.center;
        theText.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
        theText.text = KINOBOOKS;
        theText.backgroundColor = [UIColor clearColor];
        theText.textColor = [UIColor whiteColor];
        theText.lineBreakMode = UILineBreakModeWordWrap;
        theText.numberOfLines = 3;
        theText.textAlignment = UITextAlignmentCenter;
        [noIssues addSubview:theText];
        [self.view addSubview:noIssues];
        
        editBtn.hidden = TRUE;
    }
}
- (void) LoadMagazines:(int)magazineID
{
    
    //Reset edit button
    [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
    editMode = FALSE;
    _imageNames = [[NSMutableArray alloc] init];
    Issueid      = [[NSMutableArray alloc] init];
    issueDate    = [[NSMutableArray alloc] init];
    NSMutableArray *mutableArr;
    NSArray *fields;
    int yPos = 5, xPos = 10 ;
    NSString *wrapperImageName;
    DBClass *bookLibObj = [[DBClass alloc] init];
    [magShelfRowview removeFromSuperview];
    magShelfRowview = [[UIScrollView alloc]initWithFrame:CGRectMake(40, 75, self.view.bounds.size.width, 
                                                                    self.view.bounds.size.height)];
    fields     = [[NSArray alloc] initWithObjects:@"lib_book_id", @"cat_id", @"book_id",@"book_name",AUTHOR_NAME, nil];
    NSString *condition = [NSString stringWithFormat:@"%@%d", @"cat_id=",magazineID];
    mutableArr = [bookLibObj selectMagazineField:fields tblname:@"lib_book" when:condition order:@" order by  book_id desc" recordsCount:KLIBMAXMAGCOUNT startWith:0];
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES); 
    
    for(NSDictionary *dicObj in mutableArr ) 
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString * checkfilepath = [cachePath stringByAppendingPathComponent:[dicObj objectForKey:CONTENT_ID]];
        NSString *contentID = [dicObj objectForKey:CONTENT_ID];
        if ([fileManager fileExistsAtPath:checkfilepath]) {
            NSString * fullwrapper_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"wrapper.jpg"]];
            
            if ([fileManager fileExistsAtPath:fullwrapper_path]) 
            {
                NSString * epub_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentID, @".epub"]];
                
                 NSString * pdf_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentID, @".pdf"]];
                
                if ([fileManager fileExistsAtPath:epub_path]) {
                    [_imageNames addObject:fullwrapper_path];
                    [Issueid addObject:contentID];
                    [issueDate addObject:[dicObj objectForKey:AUTHOR_NAME]];
                    
                } else if ([fileManager fileExistsAtPath:pdf_path]) {
                    [_imageNames addObject:fullwrapper_path];
                    [Issueid addObject:contentID];
                    [issueDate addObject:[dicObj objectForKey:AUTHOR_NAME]];
                }
            }
        }
    }
    [bookLibObj release];bookLibObj = nil;
   
    [self.gridView reloadData];
    
}
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController 
                    animated:(BOOL)animated {
    /*
     if ([viewController isKindOfClass:[PSPDFViewController class]])
     {
     [self viewDidAppear:FALSE];
     }
     */
}
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    if(pdfMode) {
        pdfMode = FALSE;
        return;
    }
    [self loadMagCateg];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void) hideTabBarView {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 1064, view.frame.size.width, view.frame.size.height)];
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 1064)];
        }
    }
    [UIView commitAnimations];
}

- (void) showTabBar {

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, screenHeight-48, view.frame.size.width, view.frame.size.height)];//48
        } 
        else 
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, screenHeight-48)];
        }
    }
    [UIView commitAnimations]; 
}
-(void) refresh_clicked:(id)sender {
    
    [self loadMagCateg];
}
- (void) mag_catg_clicked:(id) sender {
    NSLog(@"First Time connecting in");
    LibraryWrapperButton *btn = (LibraryWrapperButton*)sender;
    for(UIView *subview in [catPlacHolder subviews]) 
    {
        if([subview isKindOfClass:[LibraryWrapperButton class]]) 
        {
            
            LibraryWrapperButton *btn1 = (LibraryWrapperButton*)subview;
            if(btn.tag == subview.tag)
                btn.selected = TRUE;
            //[subview setBackgroundColor:[UIColor lightGrayColor]];
            else
                btn1.selected = FALSE;
        }
        //[subview setBackgroundColor:[UIColor clearColor]];
    }
    [self LoadMagazines:[btn MagID]];
}

- (void)book_clicked:(id)sender {
    
    [self LoadMagazines:TRUE];
}

- (void)magazine_clicked:(id)sender {
    [self LoadMagazines:FALSE];
}

- (void)close_clicked:(id)sender {
    
    LibraryCloseButton *closeBtn1 = (LibraryCloseButton*)sender;
    
    tmpIssueFldPath = closeBtn1.issueFolderPath;
    tmpIssueID      = closeBtn1.IssueID;
    itemIndex = closeBtn1.grdInx;
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Remove Magazine"
                          message: @"Do you want to remove the magazine..?"
                          delegate: self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"OK",nil];
    [alert show];
    //[alert release];
    
    
    
    
}
- (void)edit_clicked:(id)sender 
{
    //UIButton *editBtn = (UIButton*)sender;
    for( int i = 0; i<[self.gridView numberOfItems];i++)
    {
        AQGridViewCell *cell = [self.gridView cellForItemAtIndex:i];
        for(UIView *subview1 in [cell subviews]) 
        { 
            if([subview1 isKindOfClass:[UIButton class]]) 
            {
                UIButton *btn = (UIButton*)subview1;
                btn.hidden = editMode;
            }
            
        }
    }
    
    [self editModeDone];
    
}
- (void) editModeDone {
    
    if(!editMode){
        //closeBtn.hidden=FALSE;
        [editBtn setImage:[UIImage imageNamed: @"done.png"] forState:UIControlStateNormal];
        editMode = TRUE;
        
    }
    else {
        //closeBtn.hidden=TRUE;
        [editBtn setImage:[UIImage imageNamed: @"edit.png"] forState:UIControlStateNormal];
        editMode = FALSE;
    }
}

-(BOOL) deletIssue{
    
   // NSLog(@"issue folder path is %@", tmpIssueFldPath);
    MagazineLib *magazineLibObj = [[MagazineLib alloc] init];
    if([[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:tmpIssueFldPath ] error:NULL])
    {
       // NSLog(@"issue folder path is %@", tmpIssueFldPath);
        //[magazineLibObj deleteMagazineWhen:[NSString stringWithFormat:@" issue_id=%d", tmpIssueID]];
        [magazineLibObj deleteBookWhen:[NSString stringWithFormat:@" book_id=%d", tmpIssueID]];
        return  TRUE;
    }
    else
    {
        return FALSE;
    }
    return FALSE;
    
}
- (void)viewDidUnload
{
    self.gridView = nil;
    Issueid = nil;
    issueDate = nil;
    //[editBtn removeFromSuperview], [editBtn release];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
		return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
	return YES;
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self dismissModalViewControllerAnimated:TRUE];
	
}

- (BOOL)isLandscape {
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

- (CGSize)portraitGridCellSizeForGridView:(AQGridView *)aGridView {
    
    CGFloat thumbnailSizeReductionFactor = 1.f;
    CGSize thumbGridSize;
    if (IsIpad()) {
        thumbGridSize =  CGSizeMake(roundf(self.thumbnailSize.width + self.thumbnailMargin.width), roundf(self.thumbnailSize.height*thumbnailSizeReductionFactor + self.thumbnailMargin.height));
    } else {
        thumbGridSize =  CGSizeMake(roundf(self.thumbnailSize.width + self.thumbnailMargin.width)-15, roundf(self.thumbnailSize.height*thumbnailSizeReductionFactor + self.thumbnailMargin.height)+30);
        
    }
    return thumbGridSize;
    
}
#pragma mark -
#pragma mark Grid View Data Source

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    // return 5;
    return [_imageNames count] ;
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    NSString *ThumbCellIdentifier = [Issueid objectAtIndex:index];
    AQGridViewCell * cell = (AQGridViewCell *)[self.gridView dequeueReusableCellWithIdentifier:ThumbCellIdentifier];
    LibraryCloseButton *closeBtn = [[LibraryCloseButton alloc] init];

    if (IsIpad()) {
        cell = [[[AQGridViewCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, roundf(self.thumbnailSize.width*1.0f), roundf(self.thumbnailSize.height*1.0f)) reuseIdentifier:ThumbCellIdentifier] autorelease];
        closeBtn.frame = CGRectMake(xClosePos-12, 0 , 32, 32);
    } else {
          cell = [[[AQGridViewCell alloc] initWithFrame:CGRectMake(0.0f, 0.0f, roundf(self.thumbnailSize.width*1.5f), roundf(self.thumbnailSize.height*1.1f)) reuseIdentifier:ThumbCellIdentifier] autorelease];
          closeBtn.frame = CGRectMake(xClosePos+15, 0 , 32, 32);
    }

    UIImageView* wrapView = [[UIImageView alloc] initWithImage:[[UIImage alloc] initWithContentsOfFile:[_imageNames objectAtIndex: index]]];
    [cell addSubview:wrapView];
    
    int issueId = [[Issueid objectAtIndex:index] intValue];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    if(!editMode) {
        closeBtn.hidden = TRUE;
    } else {
        closeBtn.hidden = FALSE;
    }

    [closeBtn addTarget:self action:@selector(close_clicked:) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [closeBtn setIssueID:issueId];
    [closeBtn setGrdInx:index];
    [closeBtn setIssueFolderPath:[NSString stringWithFormat:@"%@%@%@%@%d%@%d",[paths objectAtIndex:0],@"/",KMAGROOTPATH,@"/",
                                  magID,@"_",issueId]];
    [cell addSubview:closeBtn];
    
    //cell.selectionStyle = AQGridViewCellSeparatorStyleNone;
    return ( cell );
}

- (void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)gridIndex {
    
    NSString *contentID = [Issueid objectAtIndex:gridIndex];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * checkfilepath = [cachePath stringByAppendingPathComponent:contentID];
    if(!editMode)
    {
        if ([fileManager fileExistsAtPath:checkfilepath]) {
            NSString * fullwrapper_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"content_", contentID, @".jpg"]];

            NSString * epub_path = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentID, @".epub"]];

            NSString * pdfDocPath = [checkfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"book_", contentID, @".pdf"]];

            if ([fileManager fileExistsAtPath:epub_path]) {
               
                [self openbook:epub_path contentid:contentID];
            } else if ([fileManager fileExistsAtPath:pdfDocPath]) {
                //epubDocPath = pdfDocPath;

                /* commented for disabling pdf reader
                 
                MagazineLib *magLib = [[MagazineLib alloc] init];
                PSPDFDocument *document = [PSPDFDocument PDFDocumentWithUrl:[NSURL fileURLWithPath:pdfDocPath]];
                document.searchEnabled = FALSE;
                document.annotationsEnabled = FALSE;
                document.outlineEnabled = FALSE;
                document.twoStepRenderingEnabled = FALSE;
                document.aspectRatioEqual = TRUE;
                document.title = @"Vikatan";

                MVPDFViewController *pdfController = [[MVPDFViewController alloc] initWithDocument:document] ;
                pdfController.preview = FALSE;
                pdfController.custTitle = @"Book";
                pdfController.navigationItem.title = @"";

                if(PSIsIpad())
                    pdfController.custTitle = @"Back";
                else
                    pdfController.custTitle = @"<";

                UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:pdfController] autorelease];
                pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                [self presentModalViewController:pdfNavController animated:NO];
                [pdfController release];
                [magLib release];*/

               // NSLog(@"PDF file exists in cache directory");
            }
        }
    }
    else 
    {
        [self setTmpIssueFldPath:checkfilepath];
        tmpIssueID          =  [contentID intValue]; 
        UIAlertView *alert  = [[UIAlertView alloc]
                               initWithTitle: @"Remove Book"
                               message: @"Do you want to remove the book..?"
                               delegate: self
                               cancelButtonTitle:@"Cancel"
                               otherButtonTitles:@"OK",nil];
        itemIndex = gridIndex;
        [alert show];
        [alert release];
        
    }
    [self.gridView deselectItemAtIndex:gridIndex animated:NO];}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex != 0)
    {
        if([self deletIssue])
        {
            NSIndexSet* set = [NSIndexSet indexSetWithIndex:itemIndex]; 
            [_imageNames removeObjectAtIndex:itemIndex];
            [Issueid removeObjectAtIndex:itemIndex];
            [self.gridView beginUpdates];    
            [self.gridView deleteItemsAtIndices:set  withAnimation:AQGridViewItemAnimationFade];
            [self.gridView endUpdates];

        } else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Failed" message:KMAGDELETEFAILED delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        }
    }
    
}
-(void) openbook:(NSString *) epubDocPathval contentid:(NSString *) content_id
{
    
    /*EpubViewControlleriPAD *epubView = [[EpubViewControlleriPAD alloc] init];
    [epubView loadEpub:[NSURL fileURLWithPath:epubDocPathval] contentid:content_id];
    
    epubView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController *pdfNavController = [[[UINavigationController alloc] initWithRootViewController:epubView] autorelease];
    pdfNavController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    pdfNavController.navigationController.toolbar.hidden=FALSE;
    [self presentModalViewController:pdfNavController animated:YES];*/
    
    EpubViewControlleriPAD *epubView = [[EpubViewControlleriPAD alloc] init]; //EpubViewController
    [epubView loadEpub:[NSURL fileURLWithPath:epubDocPathval] contentid:content_id type:@"main"];
    
   // [epubView loadEpub:[NSURL fileURLWithPath:epubDocPathval] contentid:content_id];
    epubView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:epubView animated:YES];
    [self dismissModalViewControllerAnimated:NO];

}
- (void) cellChooser: (ImageDemoCellChooser *) chooser selectedItemAtIndex: (NSUInteger) index
{
    self.gridView.separatorStyle = AQGridViewCellSeparatorStyleEmptySpace;
    self.gridView.resizesCellWidthToFit = NO;
    self.gridView.separatorColor = nil;
    [self.gridView reloadData];
}
//Customize Grid View
- (void)initializeGridView {
    if (!self.gridView) {
        // init thumbs view
        self.gridView = [[[AQGridView alloc] initWithFrame:CGRectZero] autorelease];
        self.gridView.backgroundColor = [UIColor clearColor];
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;  
        self.gridView.resizesCellWidthToFit = NO;
        self.gridView.layoutDirection = AQGridViewLayoutDirectionVertical;

        // if we have a translucent bar, make some room
        if (self.navigationController.navigationBar.barStyle == UIBarStyleBlackTranslucent) {
            self.gridView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
        }else {
            self.gridView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        self.gridView.hidden = NO;
        self.gridView.scrollsToTop = YES;
        [self.view addSubview:self.gridView];
        CGRect frame = CGRectMake(0, 65, self.view.bounds.size.width, self.view.bounds.size.height - 65 );   
        self.gridView.frame = frame;
        
        // add a footer view for tabbar
        if (!PSIsIpad()) {
            self.gridView.gridFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 50)] autorelease];
        }
    }
}

#pragma mark -
#pragma mark Grid View Delegate

@end
