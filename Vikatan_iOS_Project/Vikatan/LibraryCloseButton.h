//
//  LibraryWrapperButton.h
//  Vikatan
//
//  Created by Saravanan Nagarajan on 19/08/11.
//  Copyright 2011 MobileVeda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LibraryCloseButton : UIButton  {
    
    NSInteger IssueID, MagID,grdInx;
    NSString *issueDate, *issueFolderPath;
    
}

@property (nonatomic, retain) NSString *issueDate,*issueFolderPath;


@property (nonatomic) NSInteger IssueID, MagID,grdInx;

@end
