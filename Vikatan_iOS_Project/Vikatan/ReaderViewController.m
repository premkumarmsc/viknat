//
//	ReaderViewController.m
//	Reader v2.0.0
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011 Julius Oklamcak. All rights reserved.
//
//	This work is being made available under a Creative Commons Attribution license:
//		«http://creativecommons.org/licenses/by/3.0/»
//	You are free to use this work and any derivatives of this work in personal and/or
//	commercial products and projects as long as the above copyright is maintained and
//	the original author is attributed.
//

#import "ReaderConstants.h"
#import "ReaderViewController.h"
#import "ReaderScrollView.h"
#import "SHK.h"

@implementation ReaderViewController

#pragma mark Constants

#define PAGING_VIEWS 3

#define TOOLBAR_HEIGHT 44.0f

#define PAGING_AREA_WIDTH 48.0f

#define HEADER_HEIGHT 45

#define DEFAULT_ROW_HEIGHT 100

#pragma mark Properties

@synthesize delegate;

@synthesize plays=plays_, sectionInfoArray=sectionInfoArray_, quoteCell=newsCell_ , openSectionIndex=openSectionIndex_;

@synthesize magazineName;

#pragma mark Support methods

- (void)updateScrollViewContentSize
{
	NSInteger count = [document.pageCount integerValue];
    
	if (count > PAGING_VIEWS) count = PAGING_VIEWS; // Limit
    
	CGFloat contentHeight = theScrollView.bounds.size.height;
    
	CGFloat contentWidth = (theScrollView.bounds.size.width * count);
    
	theScrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
}

- (void)showDocumentPage:(NSInteger)page
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    NSLog(@"second issue");

    
    [ self hidePOPS];
    
    articleTitle = [articleDIC objectForKey:[NSString stringWithFormat:@"%d",page]];
    
    artWebURL = [artRefDIC objectForKey:[NSString stringWithFormat:@"%d",page]]; //[arrArticleWebURl objectAtIndex:page];
    
    articleID = [magDIC objectForKey:[NSString stringWithFormat:@"%d",page]];
    
    magID     = document.magID;
    //Disable TOC and Text View 
    
    NSMutableArray *toolbarItems = [NSMutableArray new];
    
    toolbarItems = [[NSMutableArray arrayWithArray:mainToolbar.items] retain];
    
    for (NSUInteger i = 0; i < [toolbarItems count]; i++) {
        
         
        UIBarButtonItem *barButtonItem = [toolbarItems objectAtIndex:i];
        
        UIButton *btn = (UIButton*) barButtonItem.customView;
        
        switch(btn.tag)
        {
            case 1: // general, network-related error
            {
                if(magDIC!=nil )
                {
                    //NSString *val = [artWebURL objectForKey:[NSString stringWithFormat:@"%d",page]];
                    
                    if (artWebURL == nil || artWebURL == @""|| artWebURL == NULL) 
                    {
                        barButtonItem.enabled = FALSE;
                        [barButtonItem setStyle: UIBarButtonItemStylePlain];
                        [btn setImage:[UIImage imageNamed:@"none.png"] forState:UIControlStateNormal];
                        
                    } 
                    else
                    {
                        barButtonItem.enabled = TRUE;
                        [btn setImage:[UIImage imageNamed:@"socialnetwork_32.png"] forState:UIControlStateNormal];
                    }
                }

            }
                break;
            case 2:{
               
                if(magDIC!=nil )
                {
                    if (articleID == nil || articleID == @""|| articleID == NULL) 
                    {
                        barButtonItem.enabled = FALSE;
                        [barButtonItem setStyle: UIBarButtonItemStylePlain];
                        [btn setImage:[UIImage imageNamed:@"none.png"] forState:UIControlStateNormal];
                        
                    } 
                    else
                    {
                        barButtonItem.enabled = TRUE;
                        [btn setImage:[UIImage imageNamed:@"iphone_comment_box_ver_1.png"] forState:UIControlStateNormal];
                    } 
                     
                }
                
            }
                break;
                
            case 3:{
                
                if(magDIC!=nil )
                {
                    if (articleID == nil || articleID == @""|| articleID == NULL) 
                    {
                        barButtonItem.enabled = FALSE;
                        [barButtonItem setStyle: UIBarButtonItemStylePlain];
                        [btn setImage:[UIImage imageNamed:@"none.png"] forState:UIControlStateNormal];
                        
                    } 
                    else
                    {
                        barButtonItem.enabled = TRUE;
                        [btn setImage:[UIImage imageNamed:@"comments_post_32.png"] forState:UIControlStateNormal];
                    } 
                    
                }
            }
                break;
            case 4:{
                
            }
                break;
            case 5:{
                if(magDIC!=nil )
                {
                    NSLog(@"Coming brother for you web is URL %@",articleTitle);
                    
                    NSString *val = [magDIC objectForKey:[NSString stringWithFormat:@"%d",page]];NSLog(@"Value printed buddy: %d",[document.pageNumber intValue]);
                    
                    if (val == nil || val == @""|| val == NULL) 
                    {
                        barButtonItem.enabled = FALSE;
                        [barButtonItem setStyle: UIBarButtonItemStylePlain];
                        
                        [btn setImage:[UIImage imageNamed:@"none.png"] forState:UIControlStateNormal];
                        
                    } else
                    {
                        barButtonItem.enabled = TRUE;
                        [btn setImage:[UIImage imageNamed:@"text_view_32.png"] forState:UIControlStateNormal];
                    }
                }

            }
                break;
            default:
            {
                
            }
                break;
        }

    }
    
       
	if (page != currentPage) // Only if different
	{
		NSInteger minValue; NSInteger maxValue;
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1;
        
		if (maxPage <= PAGING_VIEWS)
		{
			minValue = minPage;
			maxValue = maxPage;
		}
		else // Handle more pages
		{
			minValue = (page - 1);
			maxValue = (page + 1);
            
			if (minValue < minPage)
            {minValue++; maxValue++;}
			else
				if (maxValue > maxPage)
                {minValue--; maxValue--;}
		}
        // NSLog(@"looping brothere");
        CGRect viewRect = CGRectZero; viewRect.size = CGSizeMake(theScrollView.bounds.size.width, theScrollView.bounds.size.height); //theScrollView.bounds.size;
        
		NSMutableDictionary *unusedViews = [[contentViews mutableCopy] autorelease];
        
		for (NSInteger number = minValue; number <= maxValue; number++)
		{
           
			NSNumber *key = [NSNumber numberWithInteger:number]; // # key
            
			ReaderContentView *contentView = [contentViews objectForKey:key];
            //ReaderContentPage *contentView = [contentViews objectForKey:key];
            
			if (contentView == nil) // Create brand new content view
			{
				NSURL *fileURL = document.fileURL; NSString *phrase = document.password; // Document properties
                
				contentView = [[ReaderContentView alloc] initWithFrame:viewRect fileURL:fileURL page:number password:phrase];
                                
                //contentView = [[ReaderContentPage alloc] initWithURL:fileURL page:page password:phrase];
                //contentView.frame = CGRectMake(0,0,768,1024);// <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
                
				[theScrollView addSubview:contentView]; [contentViews setObject:contentView forKey:key];
                
				contentView.delegate = self; 
                [contentView release];
                
                NSLog(@"If Loop brother");

			}
			else // Reposition the existing content view
			{
                NSLog(@"Change the position of the view in the main");
				contentView.frame = viewRect; [unusedViews removeObjectForKey:key];
			}
            viewRect.origin.x += viewRect.size.width;
		}
        
		[unusedViews enumerateKeysAndObjectsUsingBlock: // Remove unused views
         ^(id key, id object, BOOL *stop)
         {
             [contentViews removeObjectForKey:key];
             
             ReaderContentView *contentView = object;
             //ReaderContentPage *contentView = object;
             
             [contentView removeFromSuperview];
         }
         ];
       
        int y = 1;
        if(isLandscape == TRUE)y=3;
		CGFloat viewWidthX1 = viewRect.size.width / y;
        
		CGFloat viewWidthX2 = (viewWidthX1 * 2.0f);
        
		CGPoint contentOffset = CGPointZero;
        
		if (maxPage >= PAGING_VIEWS)
		{
			if (page == maxPage)
				contentOffset.x = viewWidthX2;
			else
				if (page != minPage)
					contentOffset.x = viewWidthX1;
		}
		else
			if (page == (PAGING_VIEWS - 1))
				contentOffset.x = viewWidthX1;
        
		if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
		{
          //  if(isLandscape == TRUE)
         //       contentOffset.x = contentOffset.x + 50;
            
            
			theScrollView.contentOffset = contentOffset  ; // Update content offset
		}
        
		if ([document.pageNumber integerValue] != page) // Only if different
		{
			document.pageNumber = [NSNumber numberWithInteger:page]; // Update it
		}
        
		[mainPagebar updatePageNumberDisplay]; // Update display
        
		currentPage = page; // Track current page number
	}
    
}

- (void)showDocument:(id)object
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[self updateScrollViewContentSize]; // Set content size
    
	[self showDocumentPage:[document.pageNumber integerValue]]; // Show
    
	document.lastOpen = [NSDate date]; // Update last opened date
    
	isVisible = YES; // iOS present modal WTF bodge
    
    [mainPagebar updateHighLightView:[document.pageNumber integerValue]];
    
    //Build Dictionary using JSON
   // [self readMagazinIndex];

}

#pragma mark UIViewController methods

- (id)initWithReaderDocument:(ReaderDocument *)object
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	assert(object != nil); // Ensure that the ReaderDocument object is not nil
    
	if ((self = [super initWithNibName:nil bundle:nil])) // Designated initializer
	{
		NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
		[notificationCenter addObserver:self selector:@selector(saveReaderDocument:) name:UIApplicationWillTerminateNotification object:nil];
        
		[notificationCenter addObserver:self selector:@selector(saveReaderDocument:) name:UIApplicationWillResignActiveNotification object:nil];
        
		document = [object retain]; // Retain the supplied ReaderDocument object for our use
	}
    
	return self;
}

/*
 - (void)loadView
 {
 #ifdef DEBUGX
 NSLog(@"%s", __FUNCTION__);
 #endif
 
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 }
 */

- (void)viewDidLoad
{
#ifdef DEBUGX
	NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.view.bounds));
#endif
    
	[super viewDidLoad];
    
	assert(delegate != nil); assert(document != nil);
    
    textFontSize = 120;
    
	//self.view.backgroundColor = [UIColor clearColor]; // Transparent
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage: [UIImage imageNamed: @"Magazine_Wrap_iPad.png"]]];
    
	CGRect viewRect = self.view.bounds; // View controller's view bounds
    
	theScrollView = [[ReaderScrollView alloc] initWithFrame:viewRect]; // All
    
	theScrollView.scrollsToTop = NO;
	theScrollView.pagingEnabled = YES;
	theScrollView.delaysContentTouches = NO;
	theScrollView.showsVerticalScrollIndicator = NO;
	theScrollView.showsHorizontalScrollIndicator = NO;
	theScrollView.contentMode = UIViewContentModeRedraw;
	theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	theScrollView.backgroundColor = [UIColor clearColor];
	theScrollView.userInteractionEnabled = YES;
	theScrollView.autoresizesSubviews = NO;
	theScrollView.delegate = self;
    
	[self.view addSubview:theScrollView];
    
	CGRect toolbarRect = viewRect;
	toolbarRect.size.height = TOOLBAR_HEIGHT;
    
	mainToolbar = [[ReaderMainToolbar alloc] initWithFrame:toolbarRect]; // At top
    
	mainToolbar.delegate = self;
    
    [self readMagazinIndex];
    
	//NSString *toolbarTitle = (self.title == nil) ? [document.fileName stringByDeletingPathExtension] : self.title;
    
    NSString *toolbarTitle = (self.title == nil) ? magazineName : self.title;
    
	[mainToolbar setToolbarTitle:toolbarTitle];
    
	[self.view addSubview:mainToolbar];
    
	/*CGRect pagebarRect = viewRect;
	pagebarRect.size.height = TOOLBAR_HEIGHT;
	pagebarRect.origin.y = (viewRect.size.height - TOOLBAR_HEIGHT);*/
    
    CGRect pagebarRect = viewRect;
    pagebarRect.size.height = TOOLBAR_HEIGHT + 155;
    pagebarRect.origin.y = 825;
    
    
	mainPagebar = [[ReaderMainPagebar alloc] initWithFrame:pagebarRect]; // At bottom
    
	mainPagebar.delegate = self;
    
    
    
	[mainPagebar setReaderDocument:document];
    
	[self.view addSubview:mainPagebar];
    
	UITapGestureRecognizer *singleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
	singleTapOne.numberOfTouchesRequired = 1; singleTapOne.numberOfTapsRequired = 1; singleTapOne.delegate = self;
    
	UITapGestureRecognizer *doubleTapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	doubleTapOne.numberOfTouchesRequired = 1; doubleTapOne.numberOfTapsRequired = 2; doubleTapOne.delegate = self;
    
	UITapGestureRecognizer *doubleTapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
	doubleTapTwo.numberOfTouchesRequired = 2; doubleTapTwo.numberOfTapsRequired = 2; doubleTapTwo.delegate = self;
    
	[singleTapOne requireGestureRecognizerToFail:doubleTapOne]; // Single tap requires double tap to fail
    
	[self.view addGestureRecognizer:singleTapOne]; [singleTapOne release];
	[self.view addGestureRecognizer:doubleTapOne]; [doubleTapOne release];
	[self.view addGestureRecognizer:doubleTapTwo]; [doubleTapTwo release];
    
	contentViews = [NSMutableDictionary new];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
#ifdef DEBUGX
	NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.view.bounds));
#endif
    
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
#ifdef DEBUGX
	NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.view.bounds));
#endif
    
	[super viewDidAppear:animated];
    
	if (CGSizeEqualToSize(theScrollView.contentSize, CGSizeZero)) // First time
	{
		[self performSelector:@selector(showDocument:) withObject:nil afterDelay:0.0];
	}
    
#if (READER_DISABLE_IDLE == TRUE) // Option
    
	[UIApplication sharedApplication].idleTimerDisabled = YES;
    
#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewWillDisappear:(BOOL)animated
{
#ifdef DEBUGX
	NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.view.bounds));
#endif
    
	[super viewWillDisappear:animated];
    
#if (READER_DISABLE_IDLE == TRUE) // Option
    
	[UIApplication sharedApplication].idleTimerDisabled = NO;
    
#endif // end of READER_DISABLE_IDLE Option
}

- (void)viewDidDisappear:(BOOL)animated
{
#ifdef DEBUGX
	NSLog(@"%s %@", __FUNCTION__, NSStringFromCGRect(self.view.bounds));
#endif
    
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	//[mainToolbar release], mainToolbar = nil; [mainPagebar release], mainPagebar = nil;
    
    if([self.view.subviews containsObject:theScrollView])
        [theScrollView release], theScrollView = nil;
    if([self.view.subviews containsObject:contentViews])
        [contentViews release], contentViews = nil;
    
	[lastHideTime release], lastHideTime = nil; currentPage = 0;
    
    if (arrArticles != NULL) 
        [arrArticles release];arrArticles = nil;
    if (arrArticlePage != NULL) 
        [arrArticlePage release];arrArticlePage = nil;
    if (arrArticleWebURl != NULL) 
        [arrArticleWebURl release];arrArticleWebURl = nil;
    if (arrCategory != NULL)
        [arrCategory release];arrCategory = nil;
    
    //[arrCategoryID release];
    if (magDIC != NULL)
        [magDIC release];magDIC = nil;
    if (articleDIC != NULL)
        [articleDIC release];articleDIC = nil;
    if (artRefDIC != NULL)
         [artRefDIC release];artRefDIC = nil;
    
    if([self.view.subviews containsObject:textHTML])
        [textHTML release];textHTML = nil;

    /*
    if([self.view.subviews containsObject:table])
        [table removeFromSuperview];
     */
    
	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
	NSLog(@"%s (%d)", __FUNCTION__, interfaceOrientation);
#endif
    
	return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), toInterfaceOrientation);
#endif
    
	if (isVisible == NO) return; // iOS present modal WTF bodge
    
	if (printInteraction != nil) [printInteraction dismissAnimated:NO];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), interfaceOrientation);
#endif
    
    
	if (isVisible == NO) return; // iOS present modal WTF bodge
    
	
	//[self updateScrollViewContentSize]; // Update the content size
    

	
    NSMutableIndexSet *pageSet = [[NSMutableIndexSet new] autorelease];
    
	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
     ^(id key, id object, BOOL *stop)
     {
         ReaderContentView *contentView = object;
         
         [pageSet addIndex:contentView.tag];
     }
     ];
    
	__block CGRect viewRect = CGRectZero; viewRect.size = CGSizeMake(1024, 768);//theScrollView.bounds.size;
    
	__block CGPoint contentOffset = CGPointZero; NSInteger page = [document.pageNumber integerValue];
    
	[pageSet enumerateIndexesUsingBlock: // Enumerate page number set
     ^(NSUInteger number, BOOL *stop)
     {
         NSNumber *key = [NSNumber numberWithInteger:number]; // # key
         
         ReaderContentView *contentView = [contentViews objectForKey:key];
         
         contentView.frame = viewRect; if (page == number) contentOffset = viewRect.origin;
         
         //viewRect.size.width = 1024/2;
         viewRect.origin.x += viewRect.size.width ; // Next view frame position
                  
         
         
     }
     ];
    
	if (CGPointEqualToPoint(theScrollView.contentOffset, contentOffset) == false)
	{
        contentOffset.x = contentOffset.x;
		theScrollView.contentOffset = contentOffset; // Update content offset
	}
       
     
     /*
    if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        theScrollView.contentSize = CGSizeMake(1024*3, 768);
        theScrollView.frame = CGRectMake(0,0,1024*3,768);
        theScrollView.backgroundColor = [UIColor redColor];
        //theScrollView.zoomScale = theScrollView.zoomScale * 0.5;
        for (UIView *subview in [theScrollView subviews]) 
            
        {
            int width = 0;
            if([subview isKindOfClass:[ReaderContentView class]])
            {
                for (UIView *subview1 in [subview subviews]) 
                    
                {
                    if([subview1 isKindOfClass:[ReaderScrollView class]])
                    {
                        subview1.frame = CGRectMake(width,-100,660,870);
                        NSLog(@"many %f", subview.frame.origin.x);
                        width =  width + 5;
                    }
                }
                
            }
            
            
        }
    }
    
    */
   
    
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
#ifdef DEBUGX
	NSLog(@"%s %@ (%d to %d)", __FUNCTION__, NSStringFromCGRect(self.view.bounds), fromInterfaceOrientation, self.interfaceOrientation);
#endif
    
	//if (isVisible == NO) return; // iOS present modal WTF bodge
    
	//if (fromInterfaceOrientation == self.interfaceOrientation) return;
}

- (void)didReceiveMemoryWarning
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[super didReceiveMemoryWarning];
}

- (void)dealloc
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
	[notificationCenter removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    
	[notificationCenter removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    /*
    if([self.view.subviews containsObject:mainToolbar])
	  [mainToolbar release], mainToolbar = nil;
    if([self.view.subviews containsObject:mainPagebar])
        [mainPagebar release], mainPagebar = nil;    
    */
    
	if([self.view.subviews containsObject:theScrollView])
        [theScrollView release], theScrollView = nil;
    if([self.view.subviews containsObject:contentViews])
        [contentViews release], contentViews = nil;
    
	[lastHideTime release], lastHideTime = nil; currentPage = 0;
    
    if (arrArticles != NULL) 
        [arrArticles release];arrArticles = nil;
    if (arrArticlePage != NULL) 
        [arrArticlePage release];arrArticlePage = nil;
    if (arrArticleWebURl != NULL) 
        [arrArticleWebURl release];arrArticleWebURl = nil;
    if (arrCategory != NULL)
        [arrCategory release];arrCategory = nil;
    
    //[arrCategoryID release];
    if (magDIC != NULL)
        [magDIC release];magDIC = nil;
    if (articleDIC != NULL)
        [articleDIC release];articleDIC = nil;
    if (artRefDIC != NULL)
        [artRefDIC release];artRefDIC = nil;
    
    if([self.view.subviews containsObject:textHTML])
        [textHTML release];textHTML = nil;
     
    /*
    if ([self.view.subviews containsObject:table]) {
         [table removeFromSuperview];
    }
     */
    
	[super dealloc];
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    if ([scrollView isMemberOfClass:[UITableView class]])
        return;
    
    NSLog(@"Scrollign inside table as well");
    
	__block NSInteger page = 0;
    
	CGFloat contentOffsetX = scrollView.contentOffset.x;NSLog(@"Content offset width %f",contentOffsetX);
    if(isLandscape == TRUE)
        contentOffsetX = contentOffsetX/2;
	[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
     ^(id key, id object, BOOL *stop)
     {
         ReaderContentView *contentView = object;
         NSLog(@"view offset width %f",contentView.frame.origin.x);

         
         if (contentView.frame.origin.x == contentOffsetX)
         {
             page = contentView.tag; *stop = YES;
         }
     }
     ];
    
	if (page != 0)  { NSLog(@"First Phase Version method");[self showDocumentPage:page]; 
        
        if(isLandscape == TRUE) [self showDocumentPage:page+1];[mainPagebar  updateHighLightView:page];} // Show page
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[self showDocumentPage:theScrollView.tag]; // Show page
    
    [mainPagebar  updateHighLightView:theScrollView.tag];
    
	theScrollView.tag = 0; // Clear page number tag
    
    NSLog(@"Second method");
}

- (void)scrollViewTouchesBegan:(UIScrollView *)scrollView touches:(NSSet *)touches
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	if ((mainToolbar.hidden == NO) || (mainPagebar.hidden == NO))
	{
		if (touches.count == 1) // Single touches only
		{
			UITouch *touch = [touches anyObject]; // Touch info
            
			CGPoint point = [touch locationInView:self.view]; // Location
            
			CGRect areaRect = CGRectInset(self.view.bounds, PAGING_AREA_WIDTH, TOOLBAR_HEIGHT);
            
			if (CGRectContainsPoint(areaRect, point) == false) return;
		}
        
		[mainToolbar hideToolbar]; [mainPagebar hidePagebar]; // Hide
        
		[lastHideTime release]; lastHideTime = [NSDate new];
	}
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldReceiveTouch:(UITouch *)touch
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	if ([touch.view isKindOfClass:[UIScrollView class]]) return YES;
    
	return NO;
}

#pragma mark UIGestureRecognizer action methods

- (void)decrementPageNumber
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	if (theScrollView.tag == 0)
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum
        
		if ((maxPage > minPage) && (page != minPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;
            
			contentOffset.x -= theScrollView.bounds.size.width; // -= 1
            
			[theScrollView setContentOffset:contentOffset animated:YES];
            
			theScrollView.tag = (page - 1); // Decrement page number
		}
	}
}

- (void)incrementPageNumber
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	if (theScrollView.tag == 0)
	{
		NSInteger page = [document.pageNumber integerValue];
		NSInteger maxPage = [document.pageCount integerValue];
		NSInteger minPage = 1; // Minimum
        
		if ((maxPage > minPage) && (page != maxPage))
		{
			CGPoint contentOffset = theScrollView.contentOffset;
            
			contentOffset.x += theScrollView.bounds.size.width; // += 1
            
			[theScrollView setContentOffset:contentOffset animated:YES];
            
			theScrollView.tag = (page + 1); // Increment page number
		}
	}
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
     
	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds
        
		CGPoint point = [recognizer locationInView:recognizer.view];
        
		CGRect areaRect = CGRectInset(viewRect, PAGING_AREA_WIDTH, 0.0f);
        
		if (CGRectContainsPoint(areaRect, point)) // Tap is inside this area
		{
			if ([lastHideTime timeIntervalSinceNow] < -0.8) // Delay since hide
			{
				if ((mainToolbar.hidden == YES) || (mainPagebar.hidden == YES))
				{
					[mainToolbar showToolbar]; [mainPagebar showPagebar]; // Show
				}
			}
            
			return;
		}
        
		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = PAGING_AREA_WIDTH;
		nextPageRect.origin.x = (viewRect.size.width - PAGING_AREA_WIDTH);
        
		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}
        
		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = PAGING_AREA_WIDTH;
        
		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)recognizer
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
    
	if (recognizer.state == UIGestureRecognizerStateRecognized)
	{
		CGRect viewRect = recognizer.view.bounds; // View bounds
        
		CGPoint point = [recognizer locationInView:recognizer.view]; // Location
        
		CGRect zoomArea = CGRectInset(viewRect, PAGING_AREA_WIDTH, TOOLBAR_HEIGHT);
        
		if (CGRectContainsPoint(zoomArea, point)) // Double tap is in the zoom area
		{
			__block ReaderContentView *targetView = nil; /* Target view of zoo*/ __block ReaderContentView *targetView1 = nil; // Target view of zoom
            
			NSInteger page = [document.pageNumber integerValue]; // Zoom current page
            
			[contentViews enumerateKeysAndObjectsUsingBlock: // Enumerate content views
             ^(id key, id object, BOOL *stop)
             {
                 ReaderContentView *contentView = object;
                 
                 if (contentView.tag == page) // Found it
                 {
                     targetView = contentView;// *stop = YES;
                 }
                 
               //  if(contentView.tag == page + 1 ) {
                 //    targetView1 = contentView; *stop = YES; }
             }
             ];
            
			switch (recognizer.numberOfTouchesRequired) // Touches count
			{
				case 1: // One finger double tap: zoom ++
				{
					[targetView zoomIncrement]; break;//[targetView1 zoomIncrement];
                    
				}
                    
				case 2: // Two finger double tap: zoom --
				{
					[targetView zoomDecrement]; break;
				}
			}
            
			return;
		}
        
		CGRect nextPageRect = viewRect;
		nextPageRect.size.width = PAGING_AREA_WIDTH;
		nextPageRect.origin.x = (viewRect.size.width - PAGING_AREA_WIDTH);
        
		if (CGRectContainsPoint(nextPageRect, point)) // page++ area
		{
			[self incrementPageNumber]; return;
		}
        
		CGRect prevPageRect = viewRect;
		prevPageRect.size.width = PAGING_AREA_WIDTH;
        
		if (CGRectContainsPoint(prevPageRect, point)) // page-- area
		{
			[self decrementPageNumber]; return;
		}
	}
}

#pragma mark ReaderMainToolbarDelegate methods

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar doneButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[document saveReaderDocument]; // Save any ReaderDocument object changes
    if([self.view.subviews containsObject:table])
        [table removeFromSuperview];// [table release];table = nil;
	[delegate dismissReaderViewController:self]; // Dismiss view controller
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar shareButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
    
    NSMutableString *webMainURL = [[NSMutableString alloc] initWithString:KIPADBIMGPATH];
    [webMainURL appendString:artWebURL];
    SHKItem *item = [SHKItem URL:[ NSURL URLWithString:webMainURL]  title:articleTitle];
    [item setShareType:SHKShareTypeURL];
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
	//[actionSheet showFromToolbar:self.navigationController.toolbar];
    
    [[SHK currentHelper] setRootViewController:self];
    [actionSheet showInView:self.view];
   
    
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar viewCmntButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
    
    if(![self.view.subviews containsObject:commentsListView])
    {
        commentsListView = [[UIView alloc] initWithFrame:CGRectMake(34,100,700,700)];
        //commentsListView.alpha = 0.4;
        [[commentsListView layer] setBorderColor:[[UIColor blackColor] CGColor]];
        [[commentsListView layer] setBorderWidth:5];
        //[[commentsListView layer] setCornerRadius:15];
        [commentsListView setClipsToBounds: YES];
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,50)];
        //[cmntHeader setBackgroundColor:[UIColor darkGrayColor]];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[cmntHeader layer] setBorderWidth:5];
        //[[cmntHeader layer] setCornerRadius:15];
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        [commentTitle setTitle:@"கருத்துகள்" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
        commentTitle.frame =  CGRectMake(290, 10, 100, 25);
        commentTitle.enabled = FALSE;
        [cmntHeader addSubview:commentTitle];
        [commentsListView addSubview:cmntHeader];
        [commentsListView addSubview:cmntHeader];
        
        
        CommentView_iPad *viewController = [[CommentView_iPad alloc] init] ;
        // NSLog([NSString stringWithFormat:@"%d", cmnt.tag]);
        viewController.articleID = articleID;
        viewController.cmntCount = @"20";
        viewController.magID     = magID;
        viewController.magCmnt   = TRUE;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
        UIView   *cmntBox = [[UIView alloc] initWithFrame:CGRectMake(0,45,700,600)];
        [cmntBox addSubview:viewController.view];
        
        [commentsListView addSubview:cmntBox];
        [self.view addSubview:commentsListView];
        
    }

}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar postCmntButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
    
    if(![self.view.subviews containsObject:postMsgView])
    {
        
        postMsgView = [[UITextView alloc] initWithFrame:CGRectMake	(34, 100,700, 700)];
        [postMsgView setBackgroundColor:[UIColor blackColor]];
        //[postMsgView setAlpha:0.4];
        // For the border and rounded corners
        [[postMsgView layer] setBorderColor:[[UIColor blackColor] CGColor]];
        [[postMsgView layer] setBorderWidth:5];
        //[[commentsListView layer] setCornerRadius:15];
        [postMsgView setClipsToBounds: YES];    //[[postMsgView layer] setCornerRadius:15];
        [postMsgView setClipsToBounds: YES];
        [self.view addSubview:postMsgView];
        
        
        UIView   *cmntHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,50)];
        [cmntHeader setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]]; 
        [[cmntHeader layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [[cmntHeader layer] setBorderWidth:5];
        [cmntHeader setClipsToBounds: YES];   
        UIButton   *commentTitle = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [commentTitle setTitle:@"உங்கள் கருத்து" forState:UIControlStateNormal];
        //[commentTitle setImage:[UIImage imageNamed:@"ipad_comments_title_white.png"] forState:UIControlStateNormal];
         commentTitle.frame =  CGRectMake(200, 10, 300, 25);
        [cmntHeader addSubview:commentTitle];
        [postMsgView addSubview:cmntHeader];
        
        UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, 700,665)] autorelease];
        postView.backgroundColor = [UIColor blackColor];
        postView.userInteractionEnabled = TRUE;
        UIWebView *postWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,700, 665)] ;
        postWebView.scalesPageToFit = NO;
        postWebView.delegate = self;
        
        [postMsgView  addSubview:postView];
        [postView addSubview:postWebView];
        
        NSMutableString *postUrl = [NSMutableString stringWithString:KMAGIPADPOSTCMNTSURL];//[urlMore appendString:categoryID];
        [postUrl appendString:articleID];
        
        NSMutableString *uriString = [[NSMutableString alloc] initWithString:postUrl];
        NSURL* url = [[NSURL alloc] initWithString:uriString];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
        
        [postWebView loadRequest:urlRequest];
        [postWebView setNeedsLayout];
        
    }
    
}
- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar tocButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
    [self hidePOPS];
    
    if(![self.view.subviews containsObject:table])
    {
        
        [self buildTOCPage]; 
    }
    /*
     else {
        
        if(table.frame.size.width == 0)
        {
            table.frame = CGRectMake(400, 55, 300, 625);
        }
        else
        {
            table.frame = CGRectMake(0, 0, 0, 0);
            NSLog(@"Hiding the table %@");
        }
    }
     */

}
- (IBAction)changeTextFontSize:(id)sender
{
    UIButton *btn = (UIButton*)sender; 
    btn.enabled = FALSE;
    switch ([sender tag]) {
        case 1: // A-
            textFontSize = (textFontSize > 80) ? textFontSize -20 : textFontSize;
            break;
        case 2: // A+
            textFontSize = (textFontSize < 260) ? textFontSize + 20 : textFontSize;
            //textFontSize = textFontSize + 5;
            break;
    }
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'", 
                          textFontSize];
    [textHTML stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    btn.enabled = TRUE;
}

- (void)tappedInToolbar:(ReaderMainToolbar *)toolbar textButton:(UIBarButtonItem *)button
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
#if (READER_TXTVIEW_ENABLED == TRUE) // Option
    
  //  UIButton *txtButton = (UIButton *)sender;
    
    [self hidePOPS];
    
    if(magDIC!=nil )
    {
        NSString *val = [magDIC objectForKey:[NSString stringWithFormat:@"%d",[document.pageNumber intValue]]];NSLog(@"Value printed buddy: %d",[document.pageNumber intValue]);
      
        if (val!= nil) 
        {
            if(![self.view.subviews containsObject:_textView])
            {
                
           // [mainPagebar setHidden:TRUE];
            _textView = [[UIView alloc] initWithFrame:CGRectMake(34, 100,700, 700)];
            [_textView setBackgroundColor:[UIColor clearColor]];
            [[_textView layer] setBorderColor:[[UIColor blackColor] CGColor]];
            [[_textView layer] setBorderWidth:5];
            [_textView setClipsToBounds: YES];    //[[postMsgView layer] setCornerRadius:15];
            [_textView setClipsToBounds: YES];
                
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,700,50)];
            [headerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header-blue-plain.jpg"]]];     
            [_textView addSubview:headerView];   
                
            UIButton   *textTitle = [UIButton buttonWithType:UIButtonTypeCustom];
            [textTitle setTitle:@"எழுத்து வடிவம்" forState:UIControlStateNormal];
            textTitle.frame =  CGRectMake(200, 10, 300, 25);
            textTitle.enabled = FALSE;
            [headerView addSubview:textTitle];
                
            UIView *fontView = [[UIView alloc] initWithFrame:CGRectMake(520,0,100,48)];
                
            UIButton *increase = [UIButton buttonWithType:UIButtonTypeCustom];
                
            [increase setImage:[UIImage imageNamed:@"A+_white_32.png"] forState:UIControlStateNormal];
                
            increase.frame =  CGRectMake(0, 0, 48, 48);
                
            increase.tag = 2;
                
            [increase addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
                
            [fontView addSubview:increase];
                
            UIButton *decrease = [UIButton buttonWithType:UIButtonTypeCustom];
                
            [decrease setImage:[UIImage imageNamed:@"A-_white_32.png"] forState:UIControlStateNormal];
                
            decrease.frame =  CGRectMake(50, 0, 48, 48);
                
            decrease.tag = 1;
                
            [decrease addTarget:self action:@selector(changeTextFontSize:) forControlEvents:UIControlEventTouchUpInside];
                
            [fontView addSubview:decrease];
            
            [headerView addSubview:fontView];
                
            UIScrollView *postView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, 700,665)] autorelease];
            postView.backgroundColor = [UIColor blackColor];
            postView.userInteractionEnabled = TRUE;    
            textHTML = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,700, 665)] ;
            textHTML.scalesPageToFit = NO;
                
            [postView addSubview:textHTML];
            [_textView addSubview:postView];
            [self.view addSubview:_textView];
            NSString *urlFilePath = [self returnDocumentDirectory:document.folderName];
            urlFilePath = [NSString stringWithFormat:@"%@%@%@%@",urlFilePath,@"/",val,@".html"];
                
            [textHTML loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:urlFilePath]]];
                
            }
            
        }
    } 	
    
#endif // end of Text View Option
    
}

#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
#ifdef DEBUG
	if ((result == MFMailComposeResultFailed) && (error != NULL)) NSLog(@"%@", error);
#endif
    
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark ReaderMainPagebarDelegate methods

- (void)pagebar:(ReaderMainPagebar *)pagebar gotoPage:(NSInteger)page
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[self showDocumentPage:page]; // Show page
}

#pragma mark Notification methods

- (void)saveReaderDocument:(NSNotification *)notification
{
#ifdef DEBUGX
	NSLog(@"%s", __FUNCTION__);
#endif
    
	[document saveReaderDocument]; // Save any ReaderDocument object changes
}

-(NSString *)readFile:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager=[NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:fileName])
    {
        NSLog(@"File path is to be decided %@", fileName);

        NSError *error= NULL;
        id resultData=[NSString stringWithContentsOfFile:appFile encoding:NSUTF8StringEncoding error:&error];
        if (error == NULL)
        {
            return resultData;
        }
    }
}

-(NSString *) returnDocumentDirectory:(NSString *)subDir {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:subDir];
    
    return appFile;
    
}
//Read Magazine TOC Content

-(void) readMagazinIndex {
    
    self.sectionInfoArray = nil;
    
    magDIC = [[NSMutableDictionary alloc] init] ;
    
    articleDIC = [[NSMutableDictionary alloc] init] ;
    
    artRefDIC = [[NSMutableDictionary alloc] init] ;
    
    arrCategory =  [[NSMutableArray alloc] init];
    
   // arrCategoryID = [[NSMutableArray alloc] init];
    
    arrArticles =  [[NSMutableArray alloc] init];
    
    arrArticleWebURl = [[NSMutableArray alloc] init];
    
    arrArticlePage = [[NSMutableArray alloc] init];
    
    NSString* tocFilePath =  [document.folderName stringByAppendingPathComponent:@"menu.json"];
    
     //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"menu" ofType:@"json"]; 
    
    if (tocFilePath) {  
        
        NSString *myText = [self readFile:tocFilePath];
        
        NSLog(@"File path is to be decided %@", myText);
        
        if (myText) {  
            
            
            SBJSON *jsonParser = [[SBJSON new] autorelease];
            
            id response = [jsonParser objectWithString:myText];
            
            NSDictionary *newsfeed = (NSDictionary *)response;
            
            magazineName = [newsfeed valueForKey:@"mag_name"];
            
            NSArray *categories = (NSArray *)[newsfeed valueForKey:@"categories"];
            
            
            
            NSDictionary *category;
            
            
            playsArray = [NSMutableArray arrayWithCapacity:categories.count ];
            
            for (int ndx = 0; ndx < categories.count ; ndx++) {
                
                category = (NSDictionary *)[categories objectAtIndex:ndx];
                
                [arrCategory addObject:[category valueForKey:@"cat_name"]];
                
               
                
                Play *play = [[Play alloc] init];
                play.name = [category valueForKey:@"cat_name"];
                 
                NSArray *articles = (NSArray *)[category valueForKey:@"articles"];
                
                NSMutableArray *quotations = [NSMutableArray arrayWithCapacity:[articles count]];
                //Loop through individual articles in each category
                
                for (int y = 0 ; y < articles.count; y++){
                                        
                    NSDictionary *pdfPage = (NSDictionary *)[articles objectAtIndex:y];
                    
                    NSString *key = [pdfPage valueForKey:@"p_no"];
                    
                    NSString *val = [pdfPage valueForKey:@"aid"];
                    
                    [arrArticles addObject:[pdfPage valueForKey:@"a_title"]];
                    [arrArticleWebURl addObject:[pdfPage valueForKey:@"weburl"]];
                    
                    [magDIC setValue:val forKey:key];
                    
                    [artRefDIC setValue:[pdfPage valueForKey:@"weburl"] forKey:key];
                  
                    NSLog(@"Article's Webpagec page url %@",[pdfPage valueForKey:@"weburl"]);
                    
                    [arrArticlePage addObject:[pdfPage valueForKey:@"p_no"]];
                    //[articleDIC setValue:[pdfPage valueForKey:@"a_title"] forKey:play.name];
                    
                     [articleDIC setValue:[pdfPage valueForKey:@"a_title"] forKey:key];
                    
                    
                    //Custom table view binding values
                    Quotation *quotation = [[Quotation alloc] init];
                    [quotation setCharacter:[pdfPage valueForKey:@"a_title"]];
                    //[quotation setQuotation:[pdfPage valueForKey:@"a_title"]];
                    [quotation setAct:[[pdfPage valueForKey:@"p_no"] integerValue]];
                    [quotations addObject:quotation];
                    [quotation release];

                }
                
                play.quotations = quotations;
                
                
                [playsArray addObject:play];
                [play release];
            }
            self.plays = playsArray;
        }  
    }  
    
    //[self buildTOCPage];
}

- (void) buildTOCPage {
    
    NSLog(@"Coming in");
   
    //rowHeight = DEFAULT_ROW_HEIGHT;
    
    openSectionIndex_ = NSNotFound;
    
    if ((self.sectionInfoArray == nil) || ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:[playsArray count]])) {
		
        NSLog(@"Coming in: 1");

        // For each play, set up a corresponding SectionInfo object to contain the default height for each row.
		NSMutableArray *infoArray = [[NSMutableArray alloc] init];
		
		for (Play *play in self.plays) {
			
            NSLog(@"Coming in: 2");
            
			SectionInfo *sectionInfo = [[SectionInfo alloc] init];			
			sectionInfo.play = play;
			sectionInfo.open = NO;
			
            NSNumber *defaultRowHeight = [NSNumber numberWithInteger:DEFAULT_ROW_HEIGHT];
			NSInteger countOfQuotations = [[sectionInfo.play quotations] count];
			for (NSInteger i = 0; i < countOfQuotations; i++) {
				[sectionInfo insertObject:defaultRowHeight inRowHeightsAtIndex:i]; NSLog(@"Coming in: 3");
			}
			
			[infoArray addObject:sectionInfo];
			[sectionInfo release];
		}
		
		self.sectionInfoArray = infoArray;
		[infoArray release];
	}
     NSLog(@"Coming in: 4");
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(400, 55, 300, 625)  style:UITableViewStylePlain];
    table.dataSource = self;
    table.delegate = self;
    table.sectionHeaderHeight = HEADER_HEIGHT;
    [[table layer] setBorderWidth:3];
    [[table layer] setCornerRadius:8];
    [[table layer] setBorderColor:[UIColor blackColor].CGColor];
    //[[table layer] setBorderColor:UIColorFromRGB(0x3399FF).CGColor] ;
    [self.view addSubview:table];
    
    
    
}
#pragma mark Table view data source and delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    
    return [self.plays count];
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
	NSInteger numStoriesInSection = [[sectionInfo.play quotations] count];
	
    return sectionInfo.open ? numStoriesInSection : 0;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    
    static NSString *QuoteCellIdentifier = @"QuoteCellIdentifier";
    
      QuoteCell *cell = (QuoteCell*)[tableView dequeueReusableCellWithIdentifier:QuoteCellIdentifier];
    
    //Alternative for testing
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (!cell) {
        //Alternative for testing
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        
        
         UINib *quoteCellNib = [UINib nibWithNibName:@"QuoteCell" bundle:nil];
         [quoteCellNib instantiateWithOwner:self options:nil];
         cell = self.quoteCell;
         self.quoteCell = nil;
         /*
         if ([MFMailComposeViewController canSendMail]) {
         UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
         [cell addGestureRecognizer:longPressRecognizer];        
         [longPressRecognizer release];
         }
         else {
         NSLog(@"Mail not available");
         }
         */
    }
    
    Play *play = (Play *)[[self.sectionInfoArray objectAtIndex:indexPath.section] play];
    
    /*
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10,0 ,table.bounds.size.width,50)];//<#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
    Quotation *qt = [play.quotations objectAtIndex:indexPath.row];
    [lbl setText:qt.character];
    [cell addSubview:lbl];
     */
    
    cell.quotation = [play.quotations objectAtIndex:indexPath.row];
    // [cell setText:@"Mananiyaa"];
    return cell;
}


-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    
    /*
     Create the section header views lazily.
     */
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    if (!sectionInfo.headerView) {
		NSString *playName = sectionInfo.play.name; NSLog(@"Play Name is: %@",playName);
        sectionInfo.headerView = [[[SectionHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, table.bounds.size.width,HEADER_HEIGHT) title:playName section:section delegate:self] autorelease];
    }
    
    return sectionInfo.headerView;
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return  [[sectionInfo objectInRowHeightsAtIndex:indexPath.row] floatValue ] / 2.0;
    // Alternatively, return rowHeight.
}


-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    
  //  [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Play *play = (Play *)[[self.sectionInfoArray objectAtIndex:indexPath.section] play];
    
    
    
    Quotation *qt = [play.quotations objectAtIndex:indexPath.row];
    [self showDocumentPage:qt.act];
    
    //table.frame = CGRectZero;
    //[table removeFromSuperview];
   // [table release];
    [mainPagebar updateHighLightView:qt.act];
}

- (void) hidePOPS
{
    
    if([self.view.subviews containsObject:table]) {
        //table.dataSource = nil;
        [table removeFromSuperview];
        
    }
     
    if([self.view.subviews containsObject:commentsListView]) {
       
        [commentsListView removeFromSuperview];
        [commentsListView release];
    }
    if([self.view.subviews containsObject:postMsgView]) {
        [postMsgView removeFromSuperview];
        [postMsgView release];
    }
    
    if([self.view.subviews containsObject:_textView]) {
        [_textView removeFromSuperview];
        [_textView release];
    }
    
    //[table release];
}
#pragma mark Section header delegate

-(void)sectionHeaderView:(SectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
	
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:sectionOpened];
	
	sectionInfo.open = YES;
    
    /*
     Create an array containing the index paths of the rows to insert: These correspond to the rows for each quotation in the current section.
     */
    NSInteger countOfRowsToInsert = [sectionInfo.play.quotations count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) { NSLog(@"Logging...");
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
		
		SectionInfo *previousOpenSection = [self.sectionInfoArray objectAtIndex:previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        NSInteger countOfRowsToDelete = [previousOpenSection.play.quotations count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    // Style the animation so that there's a smooth flow in either direction.
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // Apply the updates.
    [table beginUpdates];
    [table insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [table deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [table endUpdates];
    self.openSectionIndex = sectionOpened;
    
    [indexPathsToInsert release];
    [indexPathsToDelete release];
}


-(void)sectionHeaderView:(SectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
	SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:sectionClosed];
	
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [table numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [table deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
        [indexPathsToDelete release];
    }
    self.openSectionIndex = NSNotFound;
}




@end
