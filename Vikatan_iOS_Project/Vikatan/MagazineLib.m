//
//  MagazineLib.m
//  SQL
//
//  Created by svp on 8/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MagazineLib.h"

static sqlite3 *database = nil;
static sqlite3_stmt *deleteStmt = nil;
static sqlite3_stmt *addStmt = nil;

@implementation MagazineLib

@synthesize magazinePrimaryID;
@synthesize magazineID;
@synthesize issueID;
@synthesize issueDate;
@synthesize supplementary;

-(id) init {
    self = [super init];
    [self openDatabase];
    return self;
}

-(NSString *) magazineFieldNameOfLIBID {
    return FN_LIBMAGAZINEID;
}

-(NSString *) magazineFieldNameOfMagazineID {
    return FN_MAGAZINEID;
}

-(NSString *) magazineFieldNameOfIssueID {
    return FN_ISSUEID;
}

-(NSString *) magazineFieldNameOfIssueDate {
    return FN_ISSUEDATE;
}

-(NSString *) magazineFieldNameOfSupplementary {
    return FN_SUPPLEMENTARY;
}

/*
- (BOOL) copyDatabaseIfNeeded {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath]; 
	
	if(!success) {
		
		NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
		success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
		
		if (!success) 
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        else 
            return TRUE;
	}	
    return FALSE;
}*/

- (BOOL) copyDatabaseIfNeeded {
    
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath = [self getDBPath];
	BOOL success = [fileManager fileExistsAtPath:dbPath]; 
	
    NSLog(@"Database path is to be : %@",dbPath);
    
	if(!success) { 
        
        NSFileManager *fmngr = [[NSFileManager alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"vikatan.sqlite" ofType:nil];
        NSError *error;
        if(![fmngr copyItemAtPath:filePath toPath:[NSString stringWithFormat:@"%@/Library/Caches/vikatan.sqlite", NSHomeDirectory()] error:&error]) {
            // handle the error
            NSLog(@"Error creating the database: %@", [error description]);
            
        }
        [fmngr release];
        
    }	
    return FALSE;
}


- (NSString *) getDBPath {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory , NSUserDomainMask, YES);
	NSString *documentsDir = [paths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:DATABASE_NAME];
}

-(BOOL) openDatabase {
    
    if (sqlite3_open([ [self getDBPath] UTF8String], &database) == SQLITE_OK) {
        NSLog(@"Opened Succesfuly Chnaged to check whether coming here not ");
        return TRUE;
    } else if ([self copyDatabaseIfNeeded]) {  NSLog(@"Error Opening");
        if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK) {
            return TRUE;
        }
    }
    return FALSE;
    
}

-(BOOL) closeDatabase {
    if (sqlite3_close(database) == SQLITE_OK)
        return TRUE;
    return FALSE;
}

-(BOOL) insertMagazine {
    BOOL flag = FALSE;

        NSString *queryStr = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@ ) values (?, ?, ?, ?)", TABLE_NAME, FN_MAGAZINEID, FN_ISSUEID, FN_ISSUEDATE, FN_SUPPLEMENTARY];
      //  NSLog(@"Prepare Query %@", queryStr);
        
        const char *query = [queryStr UTF8String];
        
        if(sqlite3_prepare_v2(database, query, -1, &addStmt, NULL) == SQLITE_OK) {
            sqlite3_bind_int(addStmt, 1, [self magazineID]);
            sqlite3_bind_int(addStmt, 2, [self issueID]);
            sqlite3_bind_text(addStmt, 3, [[self issueDate] UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(addStmt, 4, [self supplementary]);
            
            if (sqlite3_step(addStmt) == SQLITE_DONE) {
                [self setMagazinePrimaryID:sqlite3_last_insert_rowid(database)];
                flag = TRUE;
            } else {
               // NSLog(@"DB ERR :: Cant insert data");
            }
        } else {
            NSLog(@"DB ERR :: Cant create prepare statement");
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }

	sqlite3_reset(addStmt);

    return flag;
}

-(BOOL) insertMagazineFields:(NSString *)fields values:(NSString *)values {
    BOOL flag = FALSE;
    
    NSString *queryStr = [NSString stringWithFormat:@"insert into %@ (%@) values (%@)", TABLE_NAME, fields, values];
   // NSLog(@"Prepare Query %@", queryStr);
    
    const char *query = [queryStr UTF8String];
    
    if(sqlite3_prepare_v2(database, query, -1, &addStmt, NULL) == SQLITE_OK) {
        if (sqlite3_step(addStmt) == SQLITE_DONE) {
            [self setMagazinePrimaryID:sqlite3_last_insert_rowid(database)];
            flag = TRUE;
        } else {
            NSLog(@"DB ERR :: Cant insert data");
        }
    } else {
        NSLog(@"DB ERR :: Cant create prepare statement");
        NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    }
    
	sqlite3_reset(addStmt);
    
    return flag;    
}

-(BOOL) updateMagazineWithFieldAndValues:(NSString *)fieldAndValues when:(NSString *)condition {
    BOOL flag = FALSE;
    
    NSString *queryStr = [NSString stringWithFormat:@"update %@ set %@", TABLE_NAME, fieldAndValues ];
    
    if (![condition isEqualToString:@""] && ![condition isEqual:nil]) {
        queryStr = [queryStr stringByAppendingFormat:@" where %@", condition];
        
       // NSLog(@"Prepare Query %@", queryStr);
        
        const char *query = [queryStr UTF8String];
        
        if(sqlite3_prepare_v2(database, query, -1, &addStmt, NULL) == SQLITE_OK) {
            
            if (sqlite3_step(addStmt) == SQLITE_DONE) {
                flag = TRUE;
            } else {
                NSLog(@"DB ERR :: Cant update data");
            }
        } else {
            NSLog(@"DB ERR :: Cant create prepare statement");
            NSLog(@"Error while creating add statement. '%s'", sqlite3_errmsg(database));
        }
        
        sqlite3_reset(addStmt);
    }

    return FALSE;
}

-(BOOL) deleteMagazineWhen:(NSString *)condition {
    BOOL flag = FALSE;
    

        if (![condition isEqualToString:@""]) {
            NSString *queryStr = [NSString stringWithFormat:@"delete from %@ where %@", TABLE_NAME, condition];
            
           // NSLog(@"Delete Statement %@",queryStr);
            
            const char *sql = [queryStr UTF8String];
            if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK) {
                if (sqlite3_step(deleteStmt) == SQLITE_DONE) {
                    flag = TRUE;
                } else {
                    NSLog(@"DB ERR :: Cant delete. '%s'", sqlite3_errmsg(database));
                }
            } else {
                NSLog(@"DB ERR :: cant create delete statement. '%s'", sqlite3_errmsg(database));
            }
        }

		
	sqlite3_reset(deleteStmt);

    return flag;
}

-(BOOL) deleteAllMagazines{
    BOOL flag = FALSE;

        NSString *queryStr = [NSString stringWithFormat:@"delete from %@", TABLE_NAME];
        
       // NSLog(@"Delete Statement %@",queryStr);
        
        const char *sql = [queryStr UTF8String];
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(deleteStmt) == SQLITE_DONE) {
                flag = TRUE;
            } else {
                NSLog(@"DB ERR :: Cant delete. '%s'", sqlite3_errmsg(database));
            }
        } else {
            NSLog(@"DB ERR :: cant create delete statement. '%s'", sqlite3_errmsg(database));
        }
    
	sqlite3_reset(deleteStmt);
    
    return flag;
}

-(BOOL) deleteBookWhen:(NSString *)condition {
    BOOL flag = FALSE;
    
    
    if (![condition isEqualToString:@""]) {
        NSString *queryStr = [NSString stringWithFormat:@"delete from %@ where %@", BOOK_LIB_TABLE, condition];
        
       // NSLog(@"Delete Statement %@",queryStr);
        
        const char *sql = [queryStr UTF8String];
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(deleteStmt) == SQLITE_DONE) {
                flag = TRUE;
            } else {
                NSLog(@"DB ERR :: Cant delete. '%s'", sqlite3_errmsg(database));
            }
        } else {
            NSLog(@"DB ERR :: cant create delete statement. '%s'", sqlite3_errmsg(database));
        }
    }
    
    
	sqlite3_reset(deleteStmt);
    
    return flag;
}

-(NSMutableArray *) selectMagazineField:(NSArray *)fields when:(NSString *)condition {

    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    NSString *commaSepFields = @"";
    
    BOOL isFirstTime = TRUE;
    for (NSString *field in fields) {
       // NSLog(@"%@", field);
        if (isFirstTime) {
            commaSepFields = [commaSepFields stringByAppendingFormat:@" %@", field];
            isFirstTime = FALSE;
        } else {
            commaSepFields = [commaSepFields stringByAppendingFormat:@", %@", field];
        }
    }
    
    NSLog(@"Select Fields : %@", commaSepFields);
    
    if ([condition isEqualToString:@""]) {
        query = [NSString stringWithFormat:@"select %@ from %@", commaSepFields, TABLE_NAME];
    } else {
        query = [NSString stringWithFormat:@"select %@ from %@ where %@", commaSepFields, TABLE_NAME, condition];        
    }
    
   // NSLog(@"Select Query : %@ ", query);
    const char *sql = [query UTF8String];
    NSDictionary *selectDict;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            
            NSMutableArray *innerArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < [fields count]; i++) {
                NSString *fieldName = [NSString stringWithCString:sqlite3_column_name(selectstmt, i) encoding:NSUTF8StringEncoding];
               // NSLog(@"Field Name :: %@", fieldName);
                
                if ([fieldName isEqualToString:FN_LIBMAGAZINEID] || [fieldName isEqualToString:FN_MAGAZINEID] || [fieldName isEqualToString:FN_ISSUEID] || [fieldName isEqualToString:FN_SUPPLEMENTARY]) {
                    [innerArray addObject:[NSString stringWithFormat:@"%d", sqlite3_column_int(selectstmt, i)]];
                }
                if ([fieldName isEqualToString:FN_ISSUEDATE]) {
                    [innerArray addObject:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectstmt, i)]];  
                }
            }
            
           // NSLog(@"Inner Array Length : %d", [innerArray count]);
            
            selectDict = [[NSDictionary alloc] initWithObjects:innerArray forKeys:fields];
            [innerArray release];
            
            [resultArray addObject:selectDict];
            
        }        
    } else {
        NSLog(@"%@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }

    
	sqlite3_reset(selectstmt);

    return resultArray;
}

-(NSMutableArray *) selectMagazineField:(NSArray *)fields when:(NSString *)condition order:(NSString *)order recordsCount:(NSInteger)limit startWith:(NSInteger)offset {

    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    NSString *commaSepFields = @"";
    
    BOOL isFirstTime = TRUE;
    for (NSString *field in fields) {
      //  NSLog(@"%@", field);
        if (isFirstTime) {
            commaSepFields = [commaSepFields stringByAppendingFormat:@" %@", field];
            isFirstTime = FALSE;
        } else {
            commaSepFields = [commaSepFields stringByAppendingFormat:@", %@", field];
        }
    }
    
    NSLog(@"Select Fields : %@", commaSepFields);
    
    if ([condition isEqualToString:@""]) {
        query = [NSString stringWithFormat:@"select %@ from %@", commaSepFields, TABLE_NAME];
    } else {
        query = [NSString stringWithFormat:@"select %@ from %@ where %@", commaSepFields, TABLE_NAME, condition];        
    }
    
    if (![order isEqualToString:@""] || ![order isEqual:nil]) {
        query = [query stringByAppendingString:order];
    }
    
    if (limit) {
        query = [query stringByAppendingFormat:@" limit %d", limit];
        if (offset) {
            query = [query stringByAppendingFormat:@" offset %d", offset];
        }
    }
        
    
   // NSLog(@"Select Query : %@ ", query);
    const char *sql = [query UTF8String];
    NSDictionary *selectDict;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            
            NSMutableArray *innerArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < [fields count]; i++) {
                NSString *fieldName = [NSString stringWithCString:sqlite3_column_name(selectstmt, i) encoding:NSUTF8StringEncoding];
               // NSString *fieldNameVal = [NSString stringWithFormat:@"Name : %@", fieldName];
                
                
                if ([fieldName isEqualToString:FN_LIBMAGAZINEID] || [fieldName isEqualToString:FN_MAGAZINEID] || [fieldName isEqualToString:FN_ISSUEID] || [fieldName isEqualToString:FN_SUPPLEMENTARY] ) {
                    [innerArray addObject:[NSString stringWithFormat:@"%d", sqlite3_column_int(selectstmt, i)]];
                  //  fieldNameVal = [fieldNameVal stringByAppendingFormat:@" Value : %d", sqlite3_column_int(selectstmt, i)];
                }
                if ([fieldName isEqualToString:FN_ISSUEDATE] || [fieldName isEqualToString:FN_MAGAZINENAME]) {
                    [innerArray addObject:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectstmt, i)]];  
                    //fieldNameVal = [fieldNameVal stringByAppendingFormat:@" Value : %@", sqlite3_column_text(selectstmt, i)];                    
                }
                
               // NSLog(@"%@", fieldNameVal);
              //  [fieldNameVal retain];
            }
            
            NSLog(@"Inner Array Length : %d", [innerArray count]);
            
            selectDict = [[NSDictionary alloc] initWithObjects:innerArray forKeys:fields];
            [innerArray release];
            
            [resultArray addObject:selectDict];
            
        }        
    } else {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }


sqlite3_reset(selectstmt);

    return resultArray;
}

-(NSMutableArray *) selectValues:(NSArray *)fields when:(NSString *)condition tableName:(NSString *)table {
    
    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    NSString *commaSepFields = @"";
    
    BOOL isFirstTime = TRUE;
    for (NSString *field in fields) {
        NSLog(@"%@", field);
        if (isFirstTime) {
            commaSepFields = [commaSepFields stringByAppendingFormat:@" %@", field];
            isFirstTime = FALSE;
        } else {
            commaSepFields = [commaSepFields stringByAppendingFormat:@", %@", field];
        }
    }
    
    NSLog(@"Select Fields : %@", commaSepFields);
    
    if ([condition isEqualToString:@""]) {
        query = [NSString stringWithFormat:@"select %@ from %@", commaSepFields, table];
    } else {
        query = [NSString stringWithFormat:@"select %@ from %@ where %@", commaSepFields, table, condition];        
    }
    
   // NSLog(@"Select Query : %@ ", query);
    const char *sql = [query UTF8String];
    NSDictionary *selectDict;
    
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            
            NSMutableArray *innerArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < [fields count]; i++) {
                NSString *fieldName = [NSString stringWithCString:sqlite3_column_name(selectstmt, i) encoding:NSUTF8StringEncoding];
               // NSLog(@"Field Name :: %@", fieldName);
                /*
                 if ([fieldName isEqualToString:FN_LIBMAGAZINEID] || [fieldName isEqualToString:FN_MAGAZINEID] || [fieldName isEqualToString:FN_ISSUEID] || [fieldName isEqualToString:FN_SUPPLEMENTARY]) {
                 [innerArray addObject:[NSString stringWithFormat:@"%d", sqlite3_column_int(selectstmt, i)]];
                 }
                 if ([fieldName isEqualToString:FN_ISSUEDATE]) {
                 [innerArray addObject:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectstmt, i)]];  
                 }
                 */
                if(sqlite3_column_text(selectstmt, i) !=NULL){
                    [innerArray addObject:[NSString stringWithUTF8String:(char *) sqlite3_column_text(selectstmt, i)]];}else {
                        [innerArray addObject:@""];
                    }
            }
            
           // NSLog(@"Inner Array Length : %d", [innerArray count]);
            
            selectDict = [[NSDictionary alloc] initWithObjects:innerArray forKeys:fields];
            
            [innerArray release];
            
            [resultArray addObject:selectDict];
            
        }        
    } else {
        NSLog(@"%@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }
    
    
	sqlite3_reset(selectstmt);
    
    return resultArray;
}
-(NSInteger) countMagazinesWhen:(NSString*) condition {
    NSInteger magCount = 0;
    
    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
       
    if ([condition isEqualToString:@""] || [condition isEqual:nil]) {
        query = [NSString stringWithFormat:@"select count(%@)from %@", FN_LIBMAGAZINEID, TABLE_NAME];
    } else {
        query = [NSString stringWithFormat:@"select count(%@) from %@ where %@", FN_LIBMAGAZINEID, TABLE_NAME, condition];        
    }
    
      
   // NSLog(@"Select Query : %@ ", query);
    const char *sql = [query UTF8String];
       
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        if(sqlite3_step(selectstmt) == SQLITE_ROW) {	
           magCount = sqlite3_column_int(selectstmt, 0);
        } else {
             NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        }
    
    } else {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }
    
    
    sqlite3_reset(selectstmt);
    
    return magCount;
}

-(NSInteger) getLatestMag {
    NSInteger magCount = 0;
    
    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    
    query = [NSString stringWithFormat:@"select magazine_id from %@ order by download_date desc ", TABLE_NAME];        
    const char *sql = [query UTF8String];
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        if(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            magCount = sqlite3_column_int(selectstmt, 0);
        } else {
            NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        }
        
    } else {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }
    
    
    sqlite3_reset(selectstmt);
    
    return magCount;
}
-(NSInteger) getLatestBook {
    NSInteger magCount = 0;
    
    sqlite3_stmt *selectstmt;    
    
    NSString *query = @"";    
    
    query = [NSString stringWithFormat:@"select book_id from %@ order by download_date desc ", BOOK_LIB_TABLE];        
    const char *sql = [query UTF8String];
    if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK) {
        if(sqlite3_step(selectstmt) == SQLITE_ROW) {	
            magCount = sqlite3_column_int(selectstmt, 0);
        } else {
            NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
        }
        
    } else {
        NSLog(@"SQL ERR : %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(database)]);
    }
    
    
    sqlite3_reset(selectstmt);
    
    return magCount;
}

-(NSString*) generate_device_access:(NSString *)title contentid:(NSString *)contid magazineid:(NSString *)magid{
    //String[] array = MagIssueDate.split("-");
   // NSLog(@"content title  is found to be:%@",title);
    NSArray *listItems = [title componentsSeparatedByString:@"-"];
    NSString *password = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",@"vik0",
                          [listItems objectAtIndex:2],contid,@"9",[listItems objectAtIndex:1],
                          magid,[listItems objectAtIndex:0],@"mv"];
   // NSLog(@"Password is found to be:%@",password);
    return password;
    // return "vik0"+array[2]+content.issueID+"9"+array[1]+content.magID+array[0]+"mv";
}

-(void) deleteEpubUnzippath:(NSString*)deleteFolder {
    
    
    
    NSLog(@"fileExistsAtPath:coming inside  deleteEpubUnzippath");
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
  //  NSString* cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    
    NSString * unzippedEpubPath = deleteFolder;///[cachePath stringByAppendingPathComponent:@"UnzippedEpub"];
    
    
    
   // NSLog(@"unzippedEpubPath is %@", unzippedEpubPath);
    
    
    
    // check if Delete unzippedEpubPath dir exists
    
    
    
    if ([fileManager fileExistsAtPath:unzippedEpubPath]) {
        
        
        
       // NSLog(@"fileExistsAtPath:unzippedEpubPath %@", unzippedEpubPath);
        
        
        
        // get all files in this directory
        
        NSArray *fileList = [fileManager contentsOfDirectoryAtPath: unzippedEpubPath error: nil];
        
        
        
        // remove
        
        for(NSInteger i = 0; i < [fileList count]; ++i)
            
        {
            
            NSString *fp =  [fileList objectAtIndex: i];
            
            NSString *remPath = [unzippedEpubPath stringByAppendingPathComponent: fp];
            
            [fileManager removeItemAtPath: remPath error: nil];
            
        }
        
        [fileManager removeItemAtPath: unzippedEpubPath error: nil];
        
    }
    
}
@end
